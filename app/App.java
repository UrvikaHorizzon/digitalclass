package com.horizzon.vendorcustshop;

import android.app.Application;
import android.os.StrictMode;

public class App extends Application {



    @Override public void onCreate() {
        super.onCreate();
        TypefaceUtil.setDefaultFont(this, "DEFAULT", "fonts/proximanovaregular.ttf");
        TypefaceUtil.setDefaultFont(this, "MONOSPACE", "fonts/proximanovaregular.ttf");
        TypefaceUtil.setDefaultFont(this, "SANS_SERIF", "fonts/proximanovaregular.ttf");
        TypefaceUtil.setDefaultFont(this, "SERIF", "fonts/proximanovaregular.ttf");
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
// OneSignal Initialization

    }

    // This fires when a notification is opened by tapping on it or one is received while the app is running.
}