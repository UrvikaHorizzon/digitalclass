package com.horizzon.vcec;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.horizzon.vcec.util.MyAppPrefsManager;

public class addvirtiseActivity extends Activity {

    MyAppPrefsManager myAppPrefsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_addvirtise);

        myAppPrefsManager = new MyAppPrefsManager(this);


        findViewById(R.id.skip).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(addvirtiseActivity.this, HomeActivity.class);
                startActivity(i);
                // close this activity
                finish();
            }
        });
    }

    @Override
    public void onBackPressed() {

    }
}
