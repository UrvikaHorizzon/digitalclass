package com.horizzon.vcec;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.horizzon.vcec.adapter.CustomVListadapter;
import com.horizzon.vcec.model.CountVideo;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.model.ResultDetails;
import com.horizzon.vcec.model.VideoData;
import com.horizzon.vcec.model.VideoDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.Database;
import com.horizzon.vcec.util.FullScreenMediaController;
import com.horizzon.vcec.util.MyAppPrefsManager;
import com.horizzon.vcec.util.OnVideoClickListner;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.horizzon.vcec.util.MyAppPrefsManager.PREF_NAME;
import static com.horizzon.vcec.util.MyAppPrefsManager.VIDEO_COUNT;

public class VideoListActivity extends AppCompatActivity implements OnVideoClickListner, View.OnClickListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private VideoView videoView;
    private MediaController mediaController;
    public static String uriPth, videoName;
    RecyclerView rv_videoes;
    ProgressBar progressBar;
    TextView tv_vname;
    public static List<VideoDetails> movieVList=new ArrayList<>();
    public static String course_id, v_type, course_name;
    public List<ResultDetails> resultDetailsList;
    private CustomVListadapter mAdapter;
    RelativeLayout rv_video;
    public String view_video, customerID;
    int youtubez_flg = 0;
    Menu menu;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    public static final String API_KEY = "AIzaSyDapMnujfMpvv-PNwGKPblZcHLeyW7HSMw";
    Database database;
    //https://www.youtube.com/watch?v=<VIDEO_ID>
    public static String VIDEO_ID = "-m3V8w_7vhk";
    YouTubePlayer youTubePlayer1;
    FragmentManager fm;
    String tag, pos;
    private int total_count = 0;
    private int count = 0;
    private RelativeLayout mYoutubeLayout;
    MyAppPrefsManager manager;
    private int cachedHeight;
    // YouTube player view
    private int youtubez_flg1 = 0;
    YouTubePlayerFragment playerFragment;
    private FrameLayout f_layout;
    private TextView mPlayTimeTextView;

    private Handler mHandler = null;
    private SeekBar mSeekBar;
    private View mPlayButtonLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_video_list);

        videoView = findViewById(R.id.video);
        videoView.setSecure(true);
        progressBar = findViewById(R.id.progrss);
        rv_video = findViewById(R.id.rv_video);
        tv_vname = findViewById(R.id.tv_vname);
        rv_videoes = findViewById(R.id.rv_videoes);

        f_layout = findViewById(R.id.content);
        mYoutubeLayout = findViewById(R.id.youtube_layout);
        //Add play button to explicitly play video in YouTubePlayerView
        findViewById(R.id.play_video).setOnClickListener(this);
        findViewById(R.id.pause_video).setOnClickListener(this);
        database = new Database(this);
        mPlayTimeTextView = (TextView) findViewById(R.id.play_time);
        mSeekBar = (SeekBar) findViewById(R.id.video_seekbar);
        mSeekBar.setOnSeekBarChangeListener(mVideoSeekBarChangeListener);
        mHandler = new Handler();
        mPlayButtonLayout = findViewById(R.id.video_control);


        getSupportActionBar().setTitle("Videos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        manager = new MyAppPrefsManager(this);
        customerID = getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");




//        youTubeView = (YouTubePlayerSupportFragment) getSupportFragmentManager().findFragmentById(R.id.youtube_fragment);

        fm = getFragmentManager();
        tag = YouTubePlayerFragment.class.getSimpleName();
        playerFragment = (YouTubePlayerFragment) fm.findFragmentByTag(tag);
        if (playerFragment == null) {
            FragmentTransaction ft = fm.beginTransaction();
            playerFragment = YouTubePlayerFragment.newInstance();
            ft.replace(R.id.content, playerFragment, tag);
            ft.commit();
        }


        if (getIntent().getStringExtra("fullScreenInd") != null) {
            String fullScreen = getIntent().getStringExtra("fullScreenInd");
            if ("y".equals(fullScreen)) {
                this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_FULL_SENSOR);
                getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                        WindowManager.LayoutParams.FLAG_FULLSCREEN);
                getSupportActionBar().hide();
                videoView.setLayoutParams(new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT));
                rv_video.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT));
                tv_vname.setVisibility(View.GONE);
                rv_videoes.setVisibility(View.GONE);
            } else {
                tv_vname.setVisibility(View.VISIBLE);
                rv_videoes.setVisibility(View.VISIBLE);
            }
        }

        if (getIntent().getStringExtra("url") != null) {
            v_type = getIntent().getStringExtra("url");
            videoName = getIntent().getStringExtra("video_name");
            course_name = getIntent().getStringExtra("course_name");
            course_id = getIntent().getStringExtra("course_id");
//            view_video = getIntent().getStringExtra("view_video");
            movieVList = (List<VideoDetails>) getIntent().getSerializableExtra("video");
            resultDetailsList = (List<ResultDetails>) getIntent().getSerializableExtra("result");
            getSupportActionBar().setTitle(course_name);


            for (int i=0;i<resultDetailsList.size();i++){
                Log.e("videoid",resultDetailsList.get(i).getVideo_id());
            }

          if (movieVList.size()>0){
              for (int i = 0; i < movieVList.size(); i++) {
                  for (int j = 0; j < resultDetailsList.size(); j++) {
                      if (movieVList.get(i).getId().equals(resultDetailsList.get(j).getVideo_id())) {


                          movieVList.get(i).setResultDetails(resultDetailsList.get(j));
                      }
                  }
              }
          }
            total_count = database.getToatalCount(course_id);
            count = database.getCount(course_id);

            if (count > 0) {
                view_video = count + "" + "/" + total_count;
            }
            countVideo(videoName);
//            Toast.makeText(this, "" + String.valueOf(total_count), Toast.LENGTH_SHORT).show();
        }
        tv_vname.setText(videoName);
        LinearLayoutManager layoutManager = new LinearLayoutManager(VideoListActivity.this, LinearLayoutManager.VERTICAL, false);
        rv_videoes.setLayoutManager(layoutManager);
        mAdapter = new CustomVListadapter(VideoListActivity.this, movieVList, this, resultDetailsList);
        rv_videoes.setAdapter(mAdapter);


        for (int i = 0; i < movieVList.size(); i++) {
            if (movieVList.get(i).getName().equals(videoName)) {
                pos = String.valueOf(i);
                onVideoClick(Integer.parseInt(pos), v_type);
            }
        }
        if (getIntent().getStringExtra("flg") != null) {
            getVideoes(course_id);


        }

        checkOrientation();
    }

    private void checkOrientation() {

        int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200.0f, getResources().getDisplayMetrics());
        ActionBar actionBar = getSupportActionBar();
        ViewGroup.LayoutParams params = mYoutubeLayout.getLayoutParams();
        if (getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_PORTRAIT) {
            if (!actionBar.isShowing())
                actionBar.show();

            params.width = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = height;
            mYoutubeLayout.requestLayout();
            rv_videoes.setVisibility(View.VISIBLE);
//            displayCurrentTime();
        } else {
            if(actionBar.isShowing())
                actionBar.hide();
            params.width  = ViewGroup.LayoutParams.MATCH_PARENT;
            params.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mYoutubeLayout.requestLayout();
            rv_videoes.setVisibility(View.GONE);
//            displayCurrentTime();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        getSharedPreferences(PREF_NAME, MODE_PRIVATE).registerOnSharedPreferenceChangeListener(this);
    }

    private void getVideoes(String id) {
        Call<VideoData> call = APIClient.getInstance()
                .getVideoList(id, customerID);

        call.enqueue(new Callback<VideoData>() {
            @Override
            public void onResponse(Call<VideoData> call, Response<VideoData> response) {
                if (response.isSuccessful()) {
                    // Get the User Details from Response
                    movieVList = new ArrayList<>();
                    movieVList = response.body().getMsg();

                    if (movieVList.size() > 0) {
                        String course_name = response.body().getCourse_name();
                        String view_video = response.body().getView_video();
                        getSupportActionBar().setTitle(course_name);
                        menu.findItem(R.id.nav_video).setTitle(view_video);
                        List<ResultDetails> resultDetails = new ArrayList<>();
                        resultDetails = response.body().getResult();
                        for (int i = 0; i < movieVList.size(); i++) {
                            for (int j = 0; j < resultDetails.size(); j++) {
                                if (movieVList.get(i).getId().equals(resultDetails.get(j).getVideo_id())) {
                                    movieVList.get(i).setResultDetails(resultDetails.get(j));
                                }
                            }
                        }

                        mAdapter = new CustomVListadapter(VideoListActivity.this, movieVList, VideoListActivity.this, resultDetails);
                        rv_videoes.setAdapter(mAdapter);

                    }


                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<VideoData> call, Throwable t) {
            }
        });
    }

    private void playVideo(String uriPth) {
        Uri videoUri = Uri.parse(uriPth);

        videoView.setVideoURI(videoUri);

        mediaController = new FullScreenMediaController(this);
        mediaController.setAnchorView(videoView);
        progressBar.setVisibility(View.VISIBLE);

        videoView.setMediaController(mediaController);
        videoView.start();

        videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onVideoClick(int pos, String type) {
        VideoDetails videoDetails = movieVList.get(pos);
        tv_vname.setText(videoDetails.getName());
        mSeekBar.setProgress(0);
        if (type.equalsIgnoreCase("file")) {
            try {
                if (youTubePlayer1 != null) {
                    youTubePlayer1.pause();
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    Fragment fragment = new Fragment();
                    ft.replace(R.id.content, fragment);
                    ft.commit();
                }
                youtubez_flg = 0;
                rv_video.setVisibility(View.VISIBLE);
//            youTubeView.setVisibility(View.GONE);
                f_layout.setVisibility(View.GONE);
                mYoutubeLayout.setVisibility(View.GONE);
                mPlayButtonLayout.setVisibility(View.GONE);

                uriPth = "https://digitalclassworld.com/storage/app/" + videoDetails.getFile();
                playVideo(uriPth);
                viewVideo(videoDetails.getCourse_id(), videoDetails.getId());
            } catch (Exception e) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                Fragment fragment = new Fragment();
                ft.replace(R.id.content, fragment);
                ft.commit();
                youtubez_flg = 0;
                rv_video.setVisibility(View.VISIBLE);
//            youTubeView.setVisibility(View.GONE);
                f_layout.setVisibility(View.GONE);
                mYoutubeLayout.setVisibility(View.GONE);
                mPlayButtonLayout.setVisibility(View.GONE);

                uriPth = "https://digitalclassworld.com/storage/app/" + videoDetails.getFile();
                playVideo(uriPth);
                viewVideo(videoDetails.getCourse_id(), videoDetails.getId());

            }

        } else {
            rv_video.setVisibility(View.GONE);
            f_layout.setVisibility(View.VISIBLE);
            mYoutubeLayout.setVisibility(View.VISIBLE);
            youtubez_flg = 1;
            videoView.stopPlayback();
            String[] paths = videoDetails.getLink().split("/");
//            paths[3] = paths[3].replace("?showinfo=0","");
            VIDEO_ID = paths[3].trim();
            fm = getFragmentManager();
            tag = YouTubePlayerFragment.class.getSimpleName();
            FragmentTransaction ft = fm.beginTransaction();
            playerFragment = YouTubePlayerFragment.newInstance();
            ft.replace(R.id.content, playerFragment, tag);
            ft.commit();
            mSeekBar.setProgress(0);

            playerFragment.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                    youTubePlayer1 = youTubePlayer;
                    displayCurrentTime();
                    youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
                    youTubePlayer.loadVideo(VIDEO_ID);
                    mPlayButtonLayout.setVisibility(View.VISIBLE);
                    mSeekBar.setProgress(0);
                    // Add listeners to YouTubePlayer instance
                    youTubePlayer.setPlayerStateChangeListener(mPlayerStateChangeListener);
                    youTubePlayer.setPlaybackEventListener(mPlaybackEventListener);
                }

                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                    Toast.makeText(VideoListActivity.this, "Error while initializing YouTubePlayer.", Toast.LENGTH_SHORT).show();
                }
            });
            viewVideo(videoDetails.getCourse_id(), videoDetails.getId());

            String name = videoDetails.getName();
            countVideo(name);

        }
    }

    private void viewVideo(String course_id, String id) {
        Call<ResponseModel> call = APIClient.getInstance()
                .sendViewVideo
                        (
                                customerID,
                                id,
                                course_id
                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {


                if (response.isSuccessful()) {
                    invalidateOptionsMenu();
                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        finish();

        int count = getSupportFragmentManager().getBackStackEntryCount();

        if (count == 0) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("result","activity");
            setResult(Activity.RESULT_OK,returnIntent);
            finish();
            super.onBackPressed();
            //additional code
        } else {
            getSupportFragmentManager().popBackStack();
        }


    }

    YouTubePlayer.PlaybackEventListener mPlaybackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onBuffering(boolean arg0) {
        }

        @Override
        public void onPaused() {
            mHandler.removeCallbacks(runnable);
        }

        @Override
        public void onPlaying() {
            displayCurrentTime();
        }

        @Override
        public void onSeekTo(int arg0) {
            mHandler.postDelayed(runnable, 100);
        }

        @Override
        public void onStopped() {
            mHandler.removeCallbacks(runnable);
        }
    };

    YouTubePlayer.PlayerStateChangeListener mPlayerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onAdStarted() {
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }

        @Override
        public void onLoaded(String arg0) {
        }

        @Override
        public void onLoading() {
        }

        @Override
        public void onVideoEnded() {
        }

        @Override
        public void onVideoStarted() {
            mSeekBar.setProgress(0);
            displayCurrentTime();
        }
    };

    SeekBar.OnSeekBarChangeListener mVideoSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            long lengthPlayed = (youTubePlayer1.getDurationMillis() * progress) / 100;
            youTubePlayer1.seekToMillis((int) lengthPlayed);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play_video:
                if (null != youTubePlayer1 && !youTubePlayer1.isPlaying())
                    youTubePlayer1.play();
                break;
            case R.id.pause_video:
                if (null != youTubePlayer1 && youTubePlayer1.isPlaying())
                    youTubePlayer1.pause();
                break;
        }
    }

    private void displayCurrentTime() {
        if (null == youTubePlayer1) return;

        if(youTubePlayer1!= null){
            String formattedTime = formatTime(youTubePlayer1.getDurationMillis() - youTubePlayer1.getCurrentTimeMillis());
            mPlayTimeTextView.setText(formattedTime);
        }

    }

    @Override
    public void onDestroy() {
        if (youTubePlayer1 != null) {
            youTubePlayer1.release();
        }
        super.onDestroy();
    }

    private String formatTime(int millis) {
        int seconds = millis / 1000;
        int minutes = seconds / 60;
        int hours = minutes / 60;

        return (hours == 0 ? "--:" : hours + ":") + String.format("%02d:%02d", minutes % 60, seconds % 60);
    }


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            mHandler.postDelayed(this, 100);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_video, menu);
        this.menu = menu;
        menu.findItem(R.id.nav_video).setTitle(view_video);
        return true;
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
//            getYouTubePlayerProvider().initialize(Config.DEVELOPER_KEY, this);
        }
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals(manager)) {
            int count = sharedPreferences.getInt(VIDEO_COUNT, 0);
            view_video = count + " " + "/" + total_count;
        }

    }

    public void countVideo(String name) {
        boolean success = !database.check_Video(name);
        if (success) {
            if (count < total_count) {
                count++;

                manager.setCount(count,total_count);
                CountVideo video = new CountVideo(Integer.parseInt(course_id), count, name);
                database.store_video_count(video);
                view_video = count + " " + "/" + total_count;

            }
        }
    }


    private void switchTitleBar(boolean show) {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            if (show) {
                supportActionBar.show();
            } else {
                supportActionBar.hide();
            }
        }
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {

     if (v_type.equals("file")){

     }else {

         int          height     = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200.0f,getResources().getDisplayMetrics());
         ActionBar    actionBar  = getSupportActionBar();
         ViewGroup.LayoutParams params     = mYoutubeLayout.getLayoutParams();

         if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
         {
             if(actionBar.isShowing())
                 actionBar.hide();
             params.width  = ViewGroup.LayoutParams.MATCH_PARENT;
             params.height = ViewGroup.LayoutParams.MATCH_PARENT;
             mYoutubeLayout.requestLayout();
             rv_videoes.setVisibility(View.GONE);
             displayCurrentTime();
         }
         else if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
         {
             if(!actionBar.isShowing())
                 actionBar.show();

             params.width  = ViewGroup.LayoutParams.MATCH_PARENT;
             params.height = height;
             mYoutubeLayout.requestLayout();
             rv_videoes.setVisibility(View.VISIBLE);
             displayCurrentTime();
         }
     }

        super.onConfigurationChanged(newConfig);
    }


}
