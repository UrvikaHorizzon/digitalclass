package com.horizzon.vcec.util;

public interface IOnBackPressed {
    boolean onBackPressed();
}
