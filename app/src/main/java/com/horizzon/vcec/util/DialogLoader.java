package com.horizzon.vcec.util;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import androidx.appcompat.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;


import com.horizzon.vcec.R;


/**
 * DialogLoader will be used to show and hide Dialog with ProgressBar
 **/

public class DialogLoader {

    private Context context;
    private AlertDialog alertDialog;
    private AlertDialog.Builder dialog;
    private LayoutInflater layoutInflater;


    public DialogLoader(Context context) {
        this.context = context;
        layoutInflater = LayoutInflater.from(context);

        initDialog();
    }

    
    private void initDialog() {
        dialog = new AlertDialog.Builder(context);
        View dialogView = layoutInflater.inflate(R.layout.layout_progress_dialog, null);
        dialog.setView(dialogView);
        dialog.setCancelable(false);

        ImageView dialog_progressBar = (ImageView) dialogView.findViewById(R.id.dialog_progressBar);
        Animation aniRotate = AnimationUtils.loadAnimation(context,R.anim.rotate_img);
        dialog_progressBar.startAnimation(aniRotate);
        alertDialog = dialog.create();
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }


    public void showProgressDialog() {
        alertDialog.show();
    }

    
    public void hideProgressDialog() {
        alertDialog.dismiss();
    }

    
}

