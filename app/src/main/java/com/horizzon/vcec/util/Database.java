package com.horizzon.vcec.util;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;

import com.horizzon.vcec.model.CountVideo;

public class Database extends SQLiteOpenHelper {

    public static int DATABASE_VERSION = 1;
    public static String DATABASE_NAME = "Digital Class";
    public static String view_VIDEO = "video";
    public static String ID = "id";
    public static String COURSE_ID = "course_id";
    public static String COUNT = "count";
    public static String TOTAL_COUNT = "total_count";
    public static final String TABLE_MAIN = "main_table";
    public static final String TABLE_VIDEO = "video_table";
    Context context;

    private static Database dbHelper;

    public Database(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        String CREATE_MAIN_TABLE = "CREATE TABLE " + TABLE_MAIN + "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COURSE_ID + " INTEGER,"
                + TOTAL_COUNT + " INTEGER"
                + ")";


        String CREATE_VIDEO_TABLE = "CREATE TABLE " + TABLE_VIDEO + "("
                + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COURSE_ID + " INTEGER," + view_VIDEO + " TEXT," + COUNT + " INTEGER"
                + ")";

        db.execSQL(CREATE_MAIN_TABLE);
        db.execSQL(CREATE_VIDEO_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_MAIN);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_VIDEO);
        onCreate(db);
    }


    public long store_course_id(CountVideo video) {
        String selectQuery = "SELECT *  FROM "
                + TABLE_MAIN;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(COURSE_ID,video.getCourse_id());
        values.put(TOTAL_COUNT,video.getTotal_count());

        // insert row
        long scroll_id = db.insert(TABLE_MAIN, null, values);
        // close db connection
        db.close();
        // return newly inserted row id
        return scroll_id;
    }

    public boolean check_Course_id(String view) {
        // array of columns to fetch
        String[] columns = {
                COURSE_ID
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = COURSE_ID + " = ?";
        // selection argument
        String[] selectionArgs = {view};

        // query user table with condition
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
         */
        Cursor cursor = db.query(TABLE_MAIN, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null);                      //The sort order
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;
    }



    public boolean check_Video(String view) {
        // array of columns to fetch
        String[] columns = {
                view_VIDEO
        };
        SQLiteDatabase db = this.getReadableDatabase();
        // selection criteria
        String selection = view_VIDEO + " = ?";
        // selection argument
        String[] selectionArgs = {view};

        // query user table with condition
        /**
         * Here query function is used to fetch records from user table this function works like we use sql query.
         * SQL query equivalent to this query function is
         * SELECT user_id FROM user WHERE user_email = 'jack@androidtutorialshub.com';
         */
        Cursor cursor = db.query(TABLE_VIDEO, //Table to query
                columns,                    //columns to return
                selection,                  //columns for the WHERE clause
                selectionArgs,              //The values for the WHERE clause
                null,                       //group the rows
                null,                      //filter by row groups
                null);                      //The sort order
        int cursorCount = cursor.getCount();
        cursor.close();
        db.close();
        if (cursorCount > 0) {
            return true;
        }
        return false;
    }


    public int getToatalCount(String id) {

        int uid=0;
        SQLiteDatabase db = this.getReadableDatabase();

        //String selectQuery = "SELECT * FROM " + TABLE_USER + " WHERE " + KEY_EMAIL + " =?" + emailId;
        String selectQuery = "SELECT * FROM " + TABLE_MAIN + " WHERE " + COURSE_ID + " =?";

        Cursor c = db.rawQuery(selectQuery, new String[]{id});
        if (c != null) {
            c.moveToFirst();
            uid=c.getInt(c.getColumnIndex(TOTAL_COUNT));

            c.close();
        }

        return uid;
    }

    public int getCount(String id) {
        int uid = 0;
        SQLiteDatabase db = this.getReadableDatabase();
        //String selectQuery = "SELECT * FROM " + TABLE_USER + " WHERE " + KEY_EMAIL + " =?" + emailId;
        String selectQuery = "SELECT * FROM " + TABLE_VIDEO + " WHERE " + COURSE_ID + " =?";
        Cursor c = db.rawQuery(selectQuery, new String[]{id});
        if (c != null) {
            c.moveToFirst();
            if (c.moveToFirst()) {
                do {
                    uid=c.getInt(c.getColumnIndex(COUNT));

                } while (c.moveToNext());


                c.close();
            }

        }
        return uid;
    }


    public long store_video_count(CountVideo video) {
        String selectQuery = "SELECT *  FROM "
                + TABLE_VIDEO;
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(COURSE_ID,video.getCourse_id());
        values.put(COUNT,video.getCount());
        values.put(view_VIDEO,video.getView_video());

        // insert row
        long scroll_id = db.insert(TABLE_VIDEO, null, values);
        // close db connection
        db.close();
        // return newly inserted row id
        return scroll_id;
    }



}
