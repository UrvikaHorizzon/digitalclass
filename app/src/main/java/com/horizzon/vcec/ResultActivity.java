package com.horizzon.vcec;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.network.APIClient;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ResultActivity extends AppCompatActivity {
    String video_id, customerID;
    TextView tv, tv2, tv3, tv_result;
    Button btnRestart;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle("Final Result");

        video_id = getIntent().getStringExtra("video_id");
        customerID = getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");

        tv = (TextView)findViewById(R.id.tvres);
        tv2 = (TextView)findViewById(R.id.tvres2);
        tv3 = (TextView)findViewById(R.id.tvres3);
        tv_result = (TextView)findViewById(R.id.tv_result);
        btnRestart = (Button) findViewById(R.id.btnRestart);

        StringBuffer sb = new StringBuffer();
        sb.append("Correct answers: " + ExamActivity.correct + "");
        StringBuffer sb2 = new StringBuffer();
        sb2.append("Wrong Answers: " + ExamActivity.wrong + "");
        StringBuffer sb3 = new StringBuffer();
        sb3.append("Final Score: " + ExamActivity.correct + "");
        tv.setText(sb);
        tv2.setText(sb2);
        tv3.setText(sb3);
        getCategories(ExamActivity.correct, ExamActivity.wrong);

        double percentage = (ExamActivity.correct*100)/(ExamActivity.correct+ExamActivity.wrong);
        if(percentage<35){
            btnRestart.setVisibility(View.VISIBLE);
            tv_result.setText("Oops!! You have failed this exam..Better Luck next time!!");
        } else {
            btnRestart.setVisibility(View.GONE);
            tv_result.setText("Congratulations !! You have passed this exam.!!");
        }


        ExamActivity.correct=0;
        ExamActivity.wrong=0;

        btnRestart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(getApplicationContext(),ExamActivity.class);
                in.putExtra("video_id", video_id);
                startActivity(in);
                finish();
            }
        });
    }

    private void getCategories(int correct, int wrong) {
        Call<ResponseModel> call = APIClient.getInstance()
                .sendResult
                        (
                                customerID,
                                video_id,
                                String.valueOf(correct),
                                String.valueOf(wrong)
                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {


                if (response.isSuccessful()) {

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent in = new Intent(ResultActivity.this, VideoListActivity.class);
        in.putExtra("flg", "yes");
        startActivity(in);
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
