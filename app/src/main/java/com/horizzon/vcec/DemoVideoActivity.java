package com.horizzon.vcec;

import android.content.pm.ActivityInfo;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.content.res.Configuration;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.horizzon.vcec.adapter.CustomPagerAdapter;
import com.horizzon.vcec.model.DemoVideoModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DemoVideoActivity extends AppCompatActivity {

    private String course_id;
    private DialogLoader dialogLoader;
    ViewPager viewPager;
    CustomPagerAdapter customPagerAdapter;
    ArrayList<DemoVideoModel>arrayList;
    DemoVideoModel model;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demo_video);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        getSupportActionBar().setTitle("Demo Videos");
        dialogLoader = new DialogLoader(DemoVideoActivity.this);

        getSupportActionBar().hide();


        course_id = getIntent().getStringExtra("course_id");
        Log.e("demo_id",course_id);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                customPagerAdapter.stop();
                customPagerAdapter.start(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        getDemos(course_id);

    }

    private void getDemos(String course_id) {
        Toast.makeText(this, ""+course_id, Toast.LENGTH_SHORT).show();
        Call<DemoVideoModel> call = APIClient.getInstance()
                .getDemos(course_id);

        call.enqueue(new Callback<DemoVideoModel>() {
            @Override
            public void onResponse(Call<DemoVideoModel> call, Response<DemoVideoModel> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        Toast.makeText(getApplicationContext(), "Swipe Left/Right to play next video!!", Toast.LENGTH_LONG).show();
                        customPagerAdapter = new CustomPagerAdapter(DemoVideoActivity.this, response.body().getData());

                        String responsebody[]=response.body().getData();

                        arrayList=new ArrayList<>();
                        for (int i=0;i<responsebody.length;i++){
                             model=new DemoVideoModel();
                            model.setUrl(responsebody[i]);
                        }
                        arrayList.add(model);
                        Log.e("array_size",String.valueOf(arrayList.size()));


                        viewPager.setAdapter(customPagerAdapter);
                        // Get the User Details from Response
//                        movieList = response.body().getMsg();
//                        if(movieList.size() > 0) {
//                            tv_nofound.setVisibility(View.GONE);
//                            ll_exam.setVisibility(View.VISIBLE);
//                            tv.setText(movieList.get(flag).getQuestion());
//                            rb1.setText(movieList.get(flag).getOption_a());
//                            rb2.setText(movieList.get(flag).getOption_b());
//                            rb3.setText(movieList.get(flag).getOption_c());
//                            rb4.setText(movieList.get(flag).getOption_d());
//                        } else {
//                            tv_nofound.setVisibility(View.VISIBLE);
//                            ll_exam.setVisibility(View.GONE);
//                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "No demo videos found!!", Toast.LENGTH_LONG).show();
                        finish();
                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    Toast.makeText(getApplicationContext(), "No demo videos found!!", Toast.LENGTH_LONG).show();
                    finish();
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<DemoVideoModel> call, Throwable t) {
//                Toast.makeText(getActivity(), "No data Found!!", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }


}
