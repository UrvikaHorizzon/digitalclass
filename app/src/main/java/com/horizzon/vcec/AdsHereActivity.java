package com.horizzon.vcec;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class AdsHereActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ads_here);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }

    @Override
    public boolean onSupportNavigateUp() {

        super.onBackPressed();
        return true;

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

    }
}