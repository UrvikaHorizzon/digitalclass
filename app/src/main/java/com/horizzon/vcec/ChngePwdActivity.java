package com.horizzon.vcec;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.ValidateInputs;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChngePwdActivity extends AppCompatActivity {
    ImageView iv_back;
    Button btn_login;
    DialogLoader dialogLoader;
    EditText et_mobile;
    ShowHidePasswordEditText et_pwd, et_pwd1;
    String mobile_no, otp;
    SharedPreferences sharedPreferences;
    String type="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chnge_pwd);

        getSupportActionBar().hide();
        sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);

        type = getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userType", "");
        iv_back = findViewById(R.id.iv_back);
        btn_login = findViewById(R.id.btn_login);
        et_mobile = findViewById(R.id.et_mobile);
        et_pwd = findViewById(R.id.et_pwd);
        et_pwd1 = findViewById(R.id.et_pwd1);
        dialogLoader = new DialogLoader(this);

        mobile_no = getIntent().getStringExtra("mobile_no");
        otp = getIntent().getStringExtra("otp");

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isValidData = validateInfoForm();
                if (isValidData) {
                    registerData();
                }

            }
        });
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChngePwdActivity.this, ForgetPwdActivity.class);
                startActivity(intent);
                finish();
            }
        });
    }

    private boolean validateInfoForm() {
        if (!ValidateInputs.isValidNumber(et_mobile.getText().toString().trim())) {
            et_mobile.setError("Please Enter Valid OTP");
            return false;
        } else if (!ValidateInputs.isValidPassword(et_pwd.getText().toString().trim())) {
            et_pwd.setError("Please Enter Valid Password");
            return false;
        }  else if (!ValidateInputs.isValidPassword(et_pwd1.getText().toString().trim())) {
            et_pwd1.setError("Please Enter Valid Password");
            return false;
        } else {
            return true;
        }
    }

    private void registerData() {
        Call<ResponseModel> call = null;
        dialogLoader.showProgressDialog();
        String password=et_pwd.getText().toString();
        if(type.equals("s")) {
             call = APIClient.getInstance()
                    .chngePwdUser
                            (
                                    mobile_no,
                                    otp,
                                    et_mobile.getText().toString().trim(),
                                    password,
                                    password
                            );

        }  if(type.equals("t")) {
            call = APIClient.getInstance()
                    .tutor_chngePwdUser
                            (
                                    mobile_no,
                                    otp,
                                    et_mobile.getText().toString().trim(),
                                    password,
                                    password
                            );

        }

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        Toast.makeText(getApplicationContext(), "Password Changed Successfully!!", Toast.LENGTH_LONG).show();
                        Intent in = new Intent(ChngePwdActivity.this,MainActivity.class);
                        startActivity(in);
                        finish();
                    }
                    else if (response.body().getSuccess().equalsIgnoreCase("0"))
                    {
                        // Get the Error Message from Response
                        Toast.makeText(getApplicationContext(), response.body().getMsg(), Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!",Toast  .LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
               // Toast.makeText(getApplicationContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
