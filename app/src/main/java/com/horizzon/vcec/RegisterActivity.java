package com.horizzon.vcec;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.provider.Settings;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.telephony.TelephonyManager;

import com.google.firebase.iid.FirebaseInstanceId;
import com.horizzon.vcec.model.checkmobile;
import com.horizzon.vcec.util.MyAppPrefsManager;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.model.CityData;
import com.horizzon.vcec.model.CityDetails;
import com.horizzon.vcec.model.CountryData;
import com.horizzon.vcec.model.CountryDetails;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.model.StateData;
import com.horizzon.vcec.model.StateDetails;
import com.horizzon.vcec.model.UserData;
import com.horizzon.vcec.model.UserDetails;
import com.horizzon.vcec.model.UserModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.ValidateInputs;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegisterActivity extends AppCompatActivity implements SharedPreferences.OnSharedPreferenceChangeListener {
    EditText et_name, et_email, et_mobile, et_address, et_bdate, et_photo, et_referal;
    ShowHidePasswordEditText et_pwd, et_cpwd;
    TextView tv_signup, tv_pwd,tv_term;
    CheckBox cb_terms;
    AutoCompleteTextView et_city, et_state, et_country;
    Button btn_signup;
    ImageView iv_back, iv_photo;
    DialogLoader dialogLoader;
    RadioGroup rg_gender;
    RadioButton radioButton;
    String imei_number, otp, city_id = "244", state_id = "3954", country_id = "111", photo = "";
    Calendar myCalendar = Calendar.getInstance();
    private Dialog dialog;
    private EditText et_otp;
    File file;
    List<CountryDetails> countrydetails;
    List<StateDetails> countrydetails1;
    List<CityDetails> countrydetails2;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    public static final String COUNTRY_ID = "cid";
    public static final String STATE_ID = "sid";
    public static final String CITY_ID = "cid";
    private WebView webView;
    private ImageView btClose;
    checkmobile mobile;
    ProgressBar progress_phone, progress_email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        getSupportActionBar().hide();

//        FirebaseApp.initializeApp(this);
//        FirebaseMessaging.getInstance().subscribeToTopic("global");

        //checking the permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(RegisterActivity.this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }
        sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);
        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId", FirebaseInstanceId.getInstance().getToken());


        imei_number = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
        Log.e("FCMID", "Firebase reg id: " + regId + "\n" + imei_number);
       // Toast.makeText(RegisterActivity.this, ""+regId, Toast.LENGTH_SHORT).show();


        et_address = findViewById(R.id.et_address);
        et_name = findViewById(R.id.et_name);
        et_email = findViewById(R.id.et_email);
        et_mobile = findViewById(R.id.et_mobile);
        et_city = findViewById(R.id.et_city);
        et_state = findViewById(R.id.et_state);
        et_bdate = findViewById(R.id.et_bdate);
        et_country = findViewById(R.id.et_country);
        et_pwd = findViewById(R.id.et_pwd);
        et_cpwd = findViewById(R.id.et_cpwd);
        et_photo = findViewById(R.id.et_photo);
        tv_signup = findViewById(R.id.tv_signup);
        tv_pwd = findViewById(R.id.tv_pwd);
        btn_signup = findViewById(R.id.btn_signup);
        iv_back = findViewById(R.id.iv_back);
        iv_photo = findViewById(R.id.iv_photo);
        rg_gender = findViewById(R.id.rg_gender);
        cb_terms = findViewById(R.id.cb_terms);
        et_referal = findViewById(R.id.et_referal);
        tv_term=findViewById(R.id.text_term);


        dialogLoader = new DialogLoader(this);
        progress_phone = findViewById(R.id.pb_phone);
        progress_email = findViewById(R.id.pb_email);
        countrydetails = new ArrayList<>();
        countrydetails1 = new ArrayList<>();
        countrydetails2 = new ArrayList<>();

        et_country.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i = 0; i < countrydetails.size(); i++) {
                    if (countrydetails.get(i).getCountry_name().equals(selection)) {
                        country_id = countrydetails.get(i).getId();
                        setCountry(country_id);

                        return;
                    }
                }
            }
        });

        et_state.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i = 0; i < countrydetails1.size(); i++) {
                    if (countrydetails1.get(i).getState_name().equals(selection)) {
                        state_id = countrydetails1.get(i).getId();
                      //  Toast.makeText(RegisterActivity.this, "", Toast.LENGTH_SHORT).show();
                        setState(state_id);
                        Log.e("state", state_id);
                        return;
                    }
                }
            }
        });

        et_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i = 0; i < countrydetails2.size(); i++) {
                    if (countrydetails2.get(i).getCity_name().equals(selection)) {
                        city_id = countrydetails2.get(i).getId();
                        setCity(city_id);
                        Log.e("city", city_id);
                        return;
                    }
                }
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            imei_number = Settings.Secure.getString(
                    RegisterActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        }else {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1111);
            } else {
                //TODO
                TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                if (null != tm) {
                    imei_number = tm.getDeviceId();
                }
                if (null == imei_number || 0 == imei_number.length()) {
                    imei_number = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                }
//        imei_number = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                Log.d("IMEI", imei_number);
            }
        }
        getCountries();

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        et_bdate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(RegisterActivity.this, date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        et_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);
            }
        });

        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, SelectRoleActivity.class);
                startActivity(intent);
                finish();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                finish();
            }
        });

        tv_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, ForgetPwdActivity.class);
                startActivity(intent);
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = rg_gender.getCheckedRadioButtonId();
                if (selectedId == -1) {
                    Toast.makeText(getApplicationContext(), "Please select Gender", Toast.LENGTH_LONG).show();
                } else {
                    SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                    String regId = pref.getString("regId", FirebaseInstanceId.getInstance().getToken());

                    Log.e("FCMID", "Firebase reg id: " + regId + "\n" + imei_number);
                //    Toast.makeText(RegisterActivity.this, ""+regId, Toast.LENGTH_SHORT).show();

                    // find the radiobutton by returned id
                    radioButton = (RadioButton) findViewById(selectedId);
                    boolean isValidData = validateInfoForm();

                    if (isValidData) {
                        if (cb_terms.isChecked()) {
                            registerData(regId);

                        } else {
                            Toast.makeText(getApplicationContext(), "Please accept Terms & Conditions first!!", Toast.LENGTH_LONG).show();
                        }
                    }
                }
            }
        });


//        cb_terms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                                                @Override
//                                                public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
//                                                    if (isChecked){
//
//                                                    }
//                                                }
//                                            }
//        );


        tv_term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webDialog();
            }
        });

        et_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                String enteredValue = cs.toString();
                String type = "phone";
                if (enteredValue.length() < 10) {
                    et_mobile.setError("Please Enter Valid Mobile number");
                    et_mobile.requestFocus();
                } else {
                    if (!ValidateInputs.isValidPhoneNo(enteredValue)) {
                        et_mobile.setError("Please Enter Valid Mobile number");
                        et_mobile.requestFocus();
                    } else
                        checkmobilenumber(enteredValue, type, new ApiCallback() {
                            @Override
                            public void onResponse(boolean success) {

                                if (success == true) {
                                    et_mobile.setError("this number already used");
                                    et_mobile.requestFocus();
                                    et_mobile.setNextFocusDownId(R.id.et_mobile);
                                    et_email.setEnabled(false);
                                    et_email.setClickable(false);
                                    et_address.setClickable(false);
                                    et_address.setEnabled(false);
                                    et_country.setClickable(false);
                                    et_country.setEnabled(false);
                                    et_city.setEnabled(false);
                                    et_city.setClickable(false);
                                    et_state.setClickable(false);
                                    et_state.setEnabled(false);
                                    et_bdate.setEnabled(false);
                                    et_bdate.setClickable(false);
                                    et_pwd.setClickable(false);
                                    et_cpwd.setClickable(false);
                                    et_pwd.setEnabled(false);
                                    et_cpwd.setEnabled(false);
                                    et_name.setClickable(false);
                                    et_name.setEnabled(false);
                                    et_referal.setEnabled(false);
                                    et_referal.setClickable(false);
                                    btn_signup.setClickable(false);
                                }

                                if (success == false) {
                                    et_email.setEnabled(true);
                                    et_email.setClickable(true);
                                    et_address.setClickable(true);
                                    et_address.setEnabled(true);
                                    et_country.setClickable(true);
                                    et_country.setEnabled(true);
                                    et_city.setEnabled(true);
                                    et_city.setClickable(true);
                                    et_state.setClickable(true);
                                    et_state.setEnabled(true);
                                    et_bdate.setEnabled(true);
                                    et_bdate.setClickable(true);
                                    et_pwd.setClickable(true);
                                    et_cpwd.setClickable(true);
                                    et_pwd.setEnabled(true);
                                    et_cpwd.setEnabled(true);
                                    et_name.setClickable(true);
                                    et_name.setEnabled(true);
                                    et_referal.setEnabled(true);
                                    et_referal.setClickable(true);
                                    btn_signup.setClickable(true);
                                }

                            }
                        });
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });


        et_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                String enteredValue = cs.toString();
                String type = "email";
                if (enteredValue.length() > 11) {
                    if (!ValidateInputs.isValidEmail(et_email.getText().toString().trim())) {
                        et_email.setError("Please Enter Valid Email");
                        et_email.requestFocus();
                    } else
                        checkmobilenumber(enteredValue, type, new ApiCallback() {
                            @Override
                            public void onResponse(boolean success) {
                                if (success == true) {
                                    et_email.setError("this email id already used");
                                    et_email.requestFocus();
                                    et_email.setNextFocusDownId(R.id.et_email);
                                    et_mobile.setEnabled(false);
                                    et_mobile.setClickable(false);
                                    et_address.setClickable(false);
                                    et_address.setEnabled(false);
                                    et_country.setClickable(false);
                                    et_country.setEnabled(false);
                                    et_city.setEnabled(false);
                                    et_city.setClickable(false);
                                    et_state.setClickable(false);
                                    et_state.setEnabled(false);
                                    et_bdate.setEnabled(false);
                                    et_bdate.setClickable(false);
                                    et_pwd.setClickable(false);
                                    et_cpwd.setClickable(false);
                                    et_pwd.setEnabled(false);
                                    et_cpwd.setEnabled(false);
                                    et_name.setClickable(false);
                                    et_name.setEnabled(false);
                                    et_referal.setEnabled(false);
                                    et_referal.setClickable(false);
                                    btn_signup.setClickable(false);
                                }


                                if (success == false) {
                                    et_mobile.setEnabled(true);
                                    et_mobile.setClickable(true);
                                    et_address.setClickable(true);
                                    et_address.setEnabled(true);
                                    et_country.setClickable(true);
                                    et_country.setEnabled(true);
                                    et_city.setEnabled(true);
                                    et_city.setClickable(true);
                                    et_state.setClickable(true);
                                    et_state.setEnabled(true);
                                    et_bdate.setEnabled(true);
                                    et_bdate.setClickable(true);
                                    et_pwd.setClickable(true);
                                    et_cpwd.setClickable(true);
                                    et_pwd.setEnabled(true);
                                    et_cpwd.setEnabled(true);
                                    et_name.setClickable(true);
                                    et_name.setEnabled(true);
                                    et_referal.setEnabled(true);
                                    et_referal.setClickable(true);
                                    btn_signup.setClickable(true);
                                }

                            }
                        });

                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });


    }

    @Override
    protected void onResume() {
        super.onResume();
        sharedPreferences.registerOnSharedPreferenceChangeListener(this);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            //the image URI
            Uri selectedImage = data.getData();
            photo = "selected";

            String pth = getPathFromUri(RegisterActivity.this, selectedImage);
            Bitmap bitmap = BitmapFactory.decodeFile(pth);
            iv_photo.setVisibility(View.VISIBLE);
            iv_photo.setImageBitmap(bitmap);
            et_photo.setText("Profile photo selected");

            file = new File(getPathFromUri(RegisterActivity.this, selectedImage));

            //calling the upload file method after choosing the file
//            uploadFile(selectedImage, "My Image");
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(RegisterActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            case 1111:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                    TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                    if (null != tm) {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        imei_number = tm.getDeviceId();
                    }
                    if (null == imei_number || 0 == imei_number.length()) {
                        imei_number = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                    }
//        imei_number = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                    Log.d("IMEI", imei_number);
                }
                break;

            default:
                break;

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String getPathFromUri(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private String getRealPathFromURI(Uri contentUri) {
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = getContentResolver().query(contentUri, proj, null, null, null);
            assert cursor != null;
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    private void registerData(String regId) {


//        country_id = getCountryId();
//        state_id = getStateId();
//        city_id = getCityId();

        dialogLoader.showProgressDialog();


        Log.e("state", state_id);
        Log.e("country", country_id);
        Log.e("city", city_id);

//        RequestBody mFile = RequestBody.create(MediaType.parse("image/*"), file);
//        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("photo", file.getName(), mFile);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), et_name.getText().toString().trim());
        RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), et_mobile.getText().toString().trim());
        RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), radioButton.getText().toString().trim());
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), et_email.getText().toString().trim());
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), et_address.getText().toString().trim());
        RequestBody bdate = RequestBody.create(MediaType.parse("text/plain"), et_bdate.getText().toString().trim());
        RequestBody pwd = RequestBody.create(MediaType.parse("text/plain"), et_pwd.getText().toString().trim());
        RequestBody cpwd = RequestBody.create(MediaType.parse("text/plain"), et_cpwd.getText().toString().trim());
        RequestBody fcmid = RequestBody.create(MediaType.parse("text/plain"), regId);
        RequestBody imeino = RequestBody.create(MediaType.parse("text/plain"), imei_number);
        RequestBody refer_code = RequestBody.create(MediaType.parse("text/plain"), et_referal.getText().toString().trim());
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), city_id);
        RequestBody state = RequestBody.create(MediaType.parse("text/plain"), state_id);
        RequestBody country = RequestBody.create(MediaType.parse("text/plain"), country_id);

        Call<UserData> call = APIClient.getInstance()
                .registerData
                        (
                                name, mobile, gender, email, address, city, state, country,
                                bdate, pwd, cpwd, fcmid, imeino, refer_code
                        );

        call.enqueue(new Callback<UserData>() {
            @Override
            public void onResponse(Call<UserData> call, Response<UserData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        UserModel userDatas = response.body().getData();
                        final UserDetails userDetails = userDatas.getInfo();
                        otp = userDetails.getOtp();
                        Log.d("OTP", otp);
                        dialog = new Dialog(RegisterActivity.this);
                        // Include dialog.xml file
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setContentView(R.layout.dlog_verify_otp_layout);
                        dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
                        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                        dialog.setCanceledOnTouchOutside(false);
                        dialog.setCancelable(false);

                        et_otp = dialog.findViewById(R.id.et_otp);
                        Button btn_login = dialog.findViewById(R.id.btn_login);
                        btn_login.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (et_otp.getText().toString().trim().isEmpty()) {
                                    Toast.makeText(getApplicationContext(), "Please enter OTP!!", Toast.LENGTH_LONG).show();
                                    et_otp.setError("Please enter OTP!!");
                                } else {
                                    verifyOTP(userDetails);
                                }
                            }
                        });
                        dialog.show();

                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        // Get the Error Message from Response
                        Toast.makeText(getApplicationContext(), "User already registered or Password does not match!!", Toast.LENGTH_LONG).show();
                    }
                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<UserData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void verifyOTP(final UserDetails userDetails) {
        dialogLoader.showProgressDialog();

        Call<ResponseModel> call = APIClient.getInstance()
                .otpVerify
                        (
                                userDetails.getName(),
                                userDetails.getMobile(),
                                userDetails.getGender(),
                                userDetails.getEmail(),
                                userDetails.getAddress(),
                                userDetails.getCity(),
                                userDetails.getState(),
                                userDetails.getCountry(),
                                userDetails.getBirthdate(),
                                userDetails.getPassword(),
                                userDetails.getPassword(),
                                userDetails.getOtp(),
                                et_otp.getText().toString().trim(),
                                userDetails.getFcm_id(),
                                userDetails.getImai_no(),
                                userDetails.getReferal()
                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                Log.e("response", response.message());

                if (response.isSuccessful()) {
                    if (response.body().getData().equalsIgnoreCase("Resgister successfully")) {
                        // Get the User Details from Response


                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        // Get the Error Message from Response
                        Toast.makeText(getApplicationContext(), "Your Verification Code does not match!!", Toast.LENGTH_LONG).show();
                    } else {
//                        Toast.makeText(getApplicationContext(), "Register succefully", Toast.LENGTH_SHORT).show();
//                        Intent intent=new Intent(RegisterActivity.this,HomeActivity.class);
//                        startActivity(intent);


                        dialog.dismiss();
                        editor = sharedPreferences.edit();
                        editor.putString("userID", response.body().getUserId().getId());
                        editor.putString("userName", userDetails.getName());
                        editor.putString("userMail", userDetails.getEmail());
                        try {
                            editor.putString("userMobile", userDetails.getMobile());
                        } catch (Exception e) {
                            editor.putString("userMobile", userDetails.getMobile_no());
                        }

                        editor.putString("userType", "s");
                        editor.putBoolean("isLogged_in", true);
                        editor.apply();

                        // Set UserLoggedIn in MyAppPrefsManager
                        MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(RegisterActivity.this);
                        myAppPrefsManager.setUserLoggedIn(true);


                        // Set isLogged_in of ConstantValues
                        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
                        editor.apply();

                        Toast.makeText(getApplicationContext(), "registration successfully!", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(RegisterActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getApplicationContext(), "Your Verification Code does not match!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getApplicationContext(), "Your Verification Code does not match!!", Toast.LENGTH_LONG).show();

            }
        });


    }

    private boolean validateInfoForm() {


        if (!ValidateInputs.isValidName(et_name.getText().toString().trim())) {
            et_name.setError("Please Enter Valid Name");
            return false;
        } else if (!ValidateInputs.isValidEmail(et_email.getText().toString().trim())) {
            et_email.setError("Please Enter Valid Email");
            return false;
        } else if (!ValidateInputs.isValidPhoneNo(et_mobile.getText().toString().trim())) {
            et_mobile.setError("Please Enter Valid Mobile number");
            return false;
        } else if (!ValidateInputs.isValidPassword(et_pwd.getText().toString().trim())) {
            et_pwd.setError("Please Enter Valid Password");
            return false;
        } else if (!ValidateInputs.isValidPassword(et_cpwd.getText().toString().trim())) {
            et_cpwd.setError("Please Enter Valid Password");
            return false;
        } else if (et_bdate.getText().toString().trim().isEmpty()) {
            et_bdate.setError("Please Enter Birthdate");
            return false;
        } else if (et_address.getText().toString().trim().isEmpty()) {
            et_address.setError("Please Enter address");
            return false;
        } else if (et_city.getText().toString().trim().isEmpty()) {
            et_city.setError("Please Enter City");
            return false;
        } else if (et_state.getText().toString().trim().isEmpty()) {
            et_state.setError("Please Enter State");
            return false;
        } else if (et_country.getText().toString().trim().isEmpty()) {
            et_country.setError("Please Enter Country");
            return false;
        } else {
            return true;
        }
    }

    private void updateLabel() {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        et_bdate.setText(sdf.format(myCalendar.getTime()));
    }

    private void getCountries() {
        Call<CountryData> call = APIClient.getInstance()
                .getCountryList();

        call.enqueue(new Callback<CountryData>() {
            @Override
            public void onResponse(Call<CountryData> call, Response<CountryData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        countrydetails = response.body().getData();
                        String[] mStringArray = new String[countrydetails.size()];
                        for (int i = 0; i < countrydetails.size(); i++) {
                            mStringArray[i] = countrydetails.get(i).getCountry_name();
                        }
                        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.dropdown, mStringArray);
                        et_country.setThreshold(1);//will start working from first character
                        et_country.setAdapter(countryAdapter);//setting the adapter data into the AutoCompleteTextView
                        getStates();

                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CountryData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getStates() {
        Call<StateData> call = APIClient.getInstance()
                .getStateList();

        call.enqueue(new Callback<StateData>() {
            @Override
            public void onResponse(Call<StateData> call, Response<StateData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        countrydetails1 = response.body().getData();
                        String[] mStringArray = new String[countrydetails1.size()];
                        for (int i = 0; i < countrydetails1.size(); i++) {
                            mStringArray[i] = countrydetails1.get(i).getState_name();
                        }
                        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.dropdown, mStringArray);
                        et_state.setThreshold(1);//will start working from first character
                        et_state.setAdapter(stateAdapter);//setting the adapter data into the AutoCompleteTextView
                        getCities();

                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<StateData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getCities() {
        Call<CityData> call = APIClient.getInstance()
                .getCityList();

        call.enqueue(new Callback<CityData>() {
            @Override
            public void onResponse(Call<CityData> call, Response<CityData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        countrydetails2 = response.body().getData();
                        String[] mStringArray = new String[countrydetails2.size()];
                        for (int i = 0; i < countrydetails2.size(); i++) {
                            mStringArray[i] = countrydetails2.get(i).getCity_name();
                        }
                        ArrayAdapter<String> cityyAdapter = new ArrayAdapter<String>(RegisterActivity.this, R.layout.dropdown, mStringArray);
                        et_city.setThreshold(1);//will start working from first character
                        et_city.setAdapter(cityyAdapter);//setting the adapter data into the AutoCompleteTextView
                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CityData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        if (key.equals("check")) {
//            boolean value=sharedPreferences.getBoolean("check",false);
//            checkmobile=value;
        }

//        if (key.equals(prefsManager.STATE_ID)){
//            state_id=sharedPreferences.getString(key,"");
//        }
//
//        if (key.equals(prefsManager.CITY_ID)){
//            city_id=sharedPreferences.getString(key,"");
//        }
    }


    public void setCountry(String id) {
        SharedPreferences.Editor prefsEditor;
        prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(COUNTRY_ID, id);
        prefsEditor.apply();
        prefsEditor.commit();
    }


    public void setCity(String cid) {
        SharedPreferences.Editor prefsEditor;
        prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(CITY_ID, cid);
        prefsEditor.apply();
        prefsEditor.commit();
    }


    public void setState(String sid) {
        SharedPreferences.Editor prefsEditor;
        prefsEditor = sharedPreferences.edit();
        prefsEditor.putString(STATE_ID, sid);
        prefsEditor.apply();
        prefsEditor.commit();
    }


    public String getCountryId() {
        return sharedPreferences.getString(COUNTRY_ID, "");
    }

    public String getStateId() {
        return sharedPreferences.getString(STATE_ID, "");
    }

    public String getCityId() {
        return sharedPreferences.getString(CITY_ID, "");
    }


    private void webDialog() {
//        LayoutInflater inflater = getLayoutInflater()
        LayoutInflater inflater = (LayoutInflater) RegisterActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View alertLayout = inflater.inflate(R.layout.dialog_term_condition_tutor, null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        webView = (WebView) alertLayout.findViewById(R.id.text);
        btClose = alertLayout.findViewById(R.id.close_btn);
        alert.setView(alertLayout);
        AlertDialog alertDialog = alert.create();
        webView.loadUrl("file:///android_asset/student_policy.htm");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }
        });
        //Define what should happen when the close button is pressed.
        btClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {//Dismiss the dialog
                alertDialog.dismiss();
            }
        });
        alertDialog.show();


    }

    public interface ApiCallback {
        void onResponse(boolean success);
    }


    private void checkmobilenumber(String number, String type, ApiCallback callback) {

        Call<ResponseModel> call = null;

        if (type.equals("phone")) {
            progress_phone.setVisibility(View.VISIBLE);
            call = APIClient.getInstance().check_mobile_number(number);
        }


        if (type.equals("email")) {
            progress_email.setVisibility(View.VISIBLE);
            call = APIClient.getInstance().check_email(number);
        }

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                progress_phone.setVisibility(View.GONE);
                progress_email.setVisibility(View.GONE);

                if (response.body().getSuccess().equalsIgnoreCase("1")) {
                    callback.onResponse(response.body().getSuccess() != null);
                } else {
                    callback.onResponse(false);
                }

            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                progress_phone.setVisibility(View.GONE);
                progress_email.setVisibility(View.GONE);
                call.cancel();
                callback.onResponse(false);

            }
        });
    }


}
