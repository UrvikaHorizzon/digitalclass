package com.horizzon.vcec;

import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.ValidateInputs;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgetPwdActivity extends AppCompatActivity {
    ImageView iv_back;
    EditText et_mobile;
    Button btn_login;
    DialogLoader dialogLoader;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_pwd);

        iv_back = findViewById(R.id.iv_back);
        et_mobile = findViewById(R.id.et_mobile);
        btn_login = findViewById(R.id.btn_login);
        dialogLoader = new DialogLoader(this);
        getSupportActionBar().hide();

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isValidData = validateInfoForm();
                if (isValidData) {
                    registerData();
                }
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    private boolean validateInfoForm() {
        if (!ValidateInputs.isValidPhoneNo(et_mobile.getText().toString().trim())) {
            et_mobile.setError("Please Enter Valid Mobile number");
            return false;
        } else {
            return true;
        }
    }

    private void registerData() {
        dialogLoader.showProgressDialog();

        Call<ResponseModel> call = APIClient.getInstance()
                .forgotUser
                        (
                                et_mobile.getText().toString().trim()
                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {Toast.makeText(getApplicationContext(), "Pls Verify Your Mobile Number", Toast.LENGTH_LONG).show();
                        // Get the User Details from Response

                        String otp = response.body().getOtp();
                        Log.d("OTP", otp);
                        Intent in = new Intent(ForgetPwdActivity.this, ChngePwdActivity.class);
                        in.putExtra("mobile_no", et_mobile.getText().toString().trim());
                        in.putExtra("otp", otp);
                        startActivity(in);

                    }
                    else if (response.body().getSuccess().equalsIgnoreCase("0"))
                    {
                        // Get the Error Message from Response
                        Toast.makeText(getApplicationContext(),"number not valid , enter correct number please", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!",Toast  .LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
}

    }