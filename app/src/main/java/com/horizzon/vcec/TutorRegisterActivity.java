package com.horizzon.vcec;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessaging;
import com.horizzon.vcec.model.CategoryData;
import com.horizzon.vcec.model.CategoryModel;
import com.horizzon.vcec.model.CityData;
import com.horizzon.vcec.model.CityDetails;
import com.horizzon.vcec.model.CountryData;
import com.horizzon.vcec.model.CountryDetails;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.model.StateData;
import com.horizzon.vcec.model.StateDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.AppEnvironment;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.MyAppPrefsManager;
import com.horizzon.vcec.util.ValidateInputs;
import com.payumoney.core.PayUmoneyConfig;
import com.payumoney.core.PayUmoneyConstants;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
import com.payumoney.sdkui.ui.utils.ResultModel;
import com.razorpay.Checkout;
import com.razorpay.PaymentResultListener;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TutorRegisterActivity extends AppCompatActivity  implements PaymentResultListener {
    EditText et_cname, et_pname, et_email, et_mobile, et_addr_institute, et_addr_owner, et_refcode,
            et_fmobile, et_pwd, et_cpwd, et_website, et_offc_area, et_system_no, et_admin_offc,
            et_faculty_cabin, et_center, et_bname, et_accnt_name, et_accnt_no, et_ifsc, et_pan_no,
            et_lab, et_air_conditions;
    TextView tv_signup, tv_pwd,tv_term;
    AutoCompleteTextView et_city, et_state, et_country, et_franchise_type, et_firm_type, et_package;
    Button btn_signup;
    ImageView iv_back;
    DialogLoader dialogLoader;
    String imei_number, city_id="244", state_id="3954", country_id="111";
    List<CategoryModel> franchisedetails;
    List<CategoryModel> packagedetails;
    List<CountryDetails> countrydetails;
    List<StateDetails> countrydetails1;
    List<CityDetails> countrydetails2;
    private String franchise_id, package_id;
    private String txnid, customerID="";
    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;
    private boolean isDisableExitConfirmation = false;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    String price;
    ImageView showhidepass,showconfmpas;
    boolean show=true,show1=true;
    CheckBox cb_terms;
    private Dialog webViewDialog;
    //a WebView object to display a web page
    private WebView webView;

    //The button to launch the WebView dialog
    private Button btLaunchWVD;
    //The button that closes the dialog
    private ImageView btClose;
    ProgressBar progress_phone, progress_email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutor_register);

        getSupportActionBar().hide();

        FirebaseApp.initializeApp(this);
        FirebaseMessaging.getInstance().subscribeToTopic("global");
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);
        sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);
        webViewDialog=new Dialog(this);
        et_cname = findViewById(R.id.et_cname);
        et_addr_owner = findViewById(R.id.et_addr_owner);
        et_addr_institute = findViewById(R.id.et_addr_institute);
        et_accnt_name = findViewById(R.id.et_accnt_name);
        et_accnt_no = findViewById(R.id.et_accnt_no);
        et_admin_offc = findViewById(R.id.et_admin_offc);
        et_bname = findViewById(R.id.et_bname);
        et_center = findViewById(R.id.et_center);
        et_faculty_cabin = findViewById(R.id.et_faculty_cabin);
        et_firm_type = findViewById(R.id.et_firm_type);
        et_fmobile = findViewById(R.id.et_fmobile);
        et_franchise_type = findViewById(R.id.et_franchise_type);
        et_ifsc = findViewById(R.id.et_ifsc);
        et_offc_area = findViewById(R.id.et_offc_area);
        et_air_conditions = findViewById(R.id.et_air_conditions);
        et_lab = findViewById(R.id.et_lab);
        et_package = findViewById(R.id.et_package);
        et_pan_no = findViewById(R.id.et_pan_no);
        et_pname = findViewById(R.id.et_pname);
        et_refcode = findViewById(R.id.et_refcode);
        et_system_no = findViewById(R.id.et_system_no);
        et_website = findViewById(R.id.et_website);
        et_email = findViewById(R.id.et_email);
        et_mobile = findViewById(R.id.et_mobile);
        et_city = findViewById(R.id.et_city);
        et_state = findViewById(R.id.et_state);
        et_country = findViewById(R.id.et_country);
        et_pwd = findViewById(R.id.et_pwd);
        et_cpwd = findViewById(R.id.et_cpwd);
        tv_signup = findViewById(R.id.tv_signup);
        tv_pwd = findViewById(R.id.tv_pwd);
        btn_signup = findViewById(R.id.btn_signup);
        iv_back = findViewById(R.id.iv_back);
        cb_terms = findViewById(R.id.cb_terms);
        dialogLoader = new DialogLoader(this);
        showhidepass=findViewById(R.id.imgshowhide);
        showconfmpas=findViewById(R.id.imgshowhide1);
        tv_term=findViewById(R.id.text_term);
        progress_phone = findViewById(R.id.pb_phone);
        progress_email = findViewById(R.id.pb_email);
        Checkout.preload(getApplicationContext());

        countrydetails = new ArrayList<>();
        countrydetails1 = new ArrayList<>();
        countrydetails2 = new ArrayList<>();
        franchisedetails = new ArrayList<>();
        packagedetails = new ArrayList<>();
        final String[] mStringArray = new String[]{"Pvt. Ltd", "LTD", "LLP", "Proprietorship", "Partnership", "Government recognized", "Other"};
        ArrayAdapter<String> countryAdapter1 = new ArrayAdapter<String>(this, R.layout.dropdown, mStringArray);
        et_firm_type.setThreshold(0);//will start working from first character
        et_firm_type.setAdapter(countryAdapter1);


        SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
        String regId = pref.getString("regId",FirebaseInstanceId.getInstance().getToken() );

//        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
//            @Override
//            public void onComplete(@NonNull Task<InstanceIdResult> task) {
//
//                if (task.isSuccessful()){
//                    String token=task.getResult().getToken();
//                    Toast.makeText(TutorRegisterActivity.this, "token is"+token, Toast.LENGTH_SHORT).show();
//                }
//            }
//        });


        et_package.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_package.showDropDown();
                et_package.requestFocus();
                return false;
            }
        });

        et_franchise_type.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_franchise_type.showDropDown();
                et_franchise_type.requestFocus();
                return false;
            }
        });

        et_firm_type.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_firm_type.showDropDown();
                et_firm_type.requestFocus();
                return false;
            }
        });

        et_franchise_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<franchisedetails.size();i++) {
                    if(franchisedetails.get(i).getName().equals(selection)) {
                        franchise_id = franchisedetails.get(i).getId();
                        return;
                    }
                }
            }
        });

        et_package.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<packagedetails.size();i++) {
                    if(packagedetails.get(i).getName().equals(selection)) {
                        package_id = packagedetails.get(i).getId();
                        return;
                    }
                }
            }
        });

        et_country.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<countrydetails.size();i++) {
                    if(countrydetails.get(i).getCountry_name().equals(selection)) {
                        country_id = countrydetails.get(i).getId();
                        return;
                    }
                }
            }
        });

        et_state.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<countrydetails1.size();i++) {
                    if(countrydetails1.get(i).getState_name().equals(selection)) {
                        state_id = countrydetails1.get(i).getId();
                        return;
                    }
                }
            }
        });

        et_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<countrydetails2.size();i++) {
                    if(countrydetails2.get(i).getCity_name().equals(selection)) {
                        city_id = countrydetails2.get(i).getId();
                        return;
                    }
                }
            }
        });

//        cb_terms.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                                               @Override
//                                               public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
//                                                   if (isChecked){
//                                                       webDialog();
//                                                   }
//                                               }
//                                           }
//        );

        tv_term.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                webDialog();
            }
        });

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            imei_number = Settings.Secure.getString(
                    TutorRegisterActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        }else {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

            if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1111);
            } else {
                //TODO
                TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                if (null != tm) {
                    imei_number = tm.getDeviceId();
                }
                if (null == imei_number || 0 == imei_number.length()) {
                    imei_number = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                }
//        imei_number = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                Log.d("IMEI", imei_number);
            }
        }
        getFranchies();

        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TutorRegisterActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TutorRegisterActivity.this, SelectRoleActivity.class);
                startActivity(intent);
                finish();
            }
        });

        tv_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(TutorRegisterActivity.this, ForgetPwdActivity.class);
                startActivity(intent);
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences pref = getApplicationContext().getSharedPreferences(Config.SHARED_PREF, 0);
                String regId = pref.getString("regId",FirebaseInstanceId.getInstance().getToken() );
                Log.e("FCMID", "Firebase reg id: " + regId + "\n" + imei_number);

                // find the radiobutton by returned id
                boolean isValidData = validateInfoForm();
                if (isValidData) {
                    if (cb_terms.isChecked()) {
                        String web_url=et_website.getText().toString().trim();
                        if (web_url.length()>0){
                            registerData(regId,web_url);
                        }
                        registerData(regId,"");
                    } else {
                        Toast.makeText(getApplicationContext(), "Please accept Terms & Conditions first!!", Toast.LENGTH_LONG).show();
                    }
                }

            }
        });


        showhidepass.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(show){
                     showhidepass.setBackgroundResource(0);
                     showhidepass.setBackgroundResource(R.drawable.hide);
                    et_pwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    et_pwd.setSelection(et_pwd.getText().length());

                    show=false;
                }else{
                     showhidepass.setBackgroundResource(0);
                     showhidepass.setBackgroundResource(R.drawable.show);
                    //etpass1.setInputType(InputType.TYPE_TEXT);
                    et_pwd.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    et_pwd.setSelection(et_pwd.getText().length());
                    show=true;
                }
            }
        });

        showconfmpas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if(show1){
                    showconfmpas.setBackgroundResource(0);
                    showconfmpas.setBackgroundResource(R.drawable.hide);
                    et_cpwd.setInputType(InputType.TYPE_TEXT_VARIATION_VISIBLE_PASSWORD);
                    et_cpwd.setSelection( et_cpwd.getText().length());

                    show1=false;
                }else{
                   showconfmpas.setBackgroundResource(0);
                   showconfmpas.setBackgroundResource(R.drawable.show);
                    //etpass1.setInputType(InputType.TYPE_TEXT);
                    et_cpwd.setInputType(InputType.TYPE_CLASS_TEXT |
                            InputType.TYPE_TEXT_VARIATION_PASSWORD);
                    et_cpwd.setSelection( et_cpwd.getText().length());
                    show1=true;
                }
            }
        });

        et_mobile.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                String enteredValue = cs.toString();
                String type = "phone";
                if (enteredValue.length() < 10) {
                    et_mobile.setError("Please Enter Valid Mobile number");
                    et_mobile.requestFocus();
                } else {
                    if (!ValidateInputs.isValidPhoneNo(enteredValue)) {
                        et_mobile.setError("Please Enter Valid Mobile number");
                        et_mobile.requestFocus();
                    } else
                        checkmobilenumber(enteredValue, type, new RegisterActivity.ApiCallback() {
                            @Override
                            public void onResponse(boolean success) {

                                if (success == true) {
                                    et_mobile.setError("This number already used");
                                    et_mobile.requestFocus();
                                    et_mobile.setNextFocusDownId(R.id.et_mobile);

                                    et_email.setEnabled(false);
                                    et_country.setEnabled(false);
                                    et_city.setEnabled(false);
                                    et_state.setEnabled(false);
                                    et_pwd.setEnabled(false);
                                    et_cpwd.setEnabled(false);
                                    btn_signup.setClickable(false);
                                    et_cname.setEnabled(false);
                                    et_addr_owner.setEnabled(false);
                                    et_accnt_name.setEnabled(false);
                                    et_accnt_no.setEnabled(false);
                                    et_admin_offc.setEnabled(false);
                                    et_bname.setEnabled(false);
                                    et_center.setEnabled(false);
                                    et_faculty_cabin.setEnabled(false);
                                    et_pan_no.setEnabled(false);
                                    et_pname.setEnabled(false);
                                    et_refcode.setEnabled(false);
                                    et_website.setEnabled(false);
                                    et_pwd.setEnabled(false);
                                    et_cpwd.setEnabled(false);
                                    et_package.setEnabled(false);
                                    et_franchise_type.setEnabled(false);
                                    et_firm_type.setEnabled(false);
                                    et_addr_institute.setEnabled(false);
                                    et_ifsc.setEnabled(false);
                                    cb_terms.setEnabled(false);
                                }

                                if (success == false) {
                                    et_email.setEnabled(true);
                                    et_country.setEnabled(true);
                                    et_city.setEnabled(true);
                                    et_state.setEnabled(true);
                                    et_pwd.setEnabled(true);
                                    et_cpwd.setEnabled(true);
                                    btn_signup.setClickable(true);
                                    et_addr_owner.setEnabled(true);
                                    et_accnt_name.setEnabled(true);
                                    et_accnt_no.setEnabled(true);
                                    et_admin_offc.setEnabled(true);
                                    et_bname.setEnabled(true);
                                    et_center.setEnabled(true);
                                    et_faculty_cabin.setEnabled(true);
                                    et_pan_no.setEnabled(true);
                                    et_pname.setEnabled(true);
                                    et_refcode.setEnabled(true);
                                    et_website.setEnabled(true);
                                    et_pwd.setEnabled(true);
                                    et_cpwd.setEnabled(true);
                                    et_package.setEnabled(true);
                                    et_franchise_type.setEnabled(true);
                                    et_firm_type.setEnabled(true);
                                    et_addr_institute.setEnabled(true);
                                    et_ifsc.setEnabled(true);
                                    cb_terms.setEnabled(true);
                                }

                            }
                        });
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });


        et_email.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                String enteredValue = cs.toString();
                String type = "email";
                if (enteredValue.length() > 11) {
                    if (!ValidateInputs.isValidEmail(et_email.getText().toString().trim())) {
                        et_email.setError("Please Enter Valid Email");
                        et_email.requestFocus();
                    } else
                        checkmobilenumber(enteredValue, type, new RegisterActivity.ApiCallback() {
                            @Override
                            public void onResponse(boolean success) {
                                if (success == true) {
                                    et_email.setError("this email id already used");
                                    et_email.requestFocus();
                                    et_email.setNextFocusDownId(R.id.et_email);
                                    et_mobile.setEnabled(false);
                                    et_mobile.setClickable(false);
                                    et_country.setClickable(false);
                                    et_country.setEnabled(false);
                                    et_city.setEnabled(false);
                                    et_city.setClickable(false);
                                    et_state.setClickable(false);
                                    et_state.setEnabled(false);
                                    et_pwd.setClickable(false);
                                    et_cpwd.setClickable(false);
                                    et_pwd.setEnabled(false);
                                    et_cpwd.setEnabled(false);
                                    btn_signup.setClickable(false);
                                    et_cname.setEnabled(false);
                                    et_addr_owner.setEnabled(false);
                                    et_accnt_name.setEnabled(false);
                                            et_accnt_no.setEnabled(false);
                                    et_admin_offc.setEnabled(false);
                                            et_bname.setEnabled(false);
                                    et_center.setEnabled(false);
                                    et_faculty_cabin.setEnabled(false);
                                    et_pan_no.setEnabled(false);
                                    et_pname.setEnabled(false);
                                    et_refcode.setEnabled(false);
                                    et_website.setEnabled(false);
                                    et_pwd.setEnabled(false);
                                    et_cpwd.setEnabled(false);
                                    et_package.setEnabled(false);
                                    et_franchise_type.setEnabled(false);
                                    et_firm_type.setEnabled(false);
                                    et_addr_institute.setEnabled(false);
                                    et_ifsc.setEnabled(false);
                                    cb_terms.setEnabled(false);
                                }


                                if (success == false) {
                                    et_mobile.setEnabled(true);
                                    et_mobile.setClickable(true);
                                    et_country.setClickable(true);
                                    et_country.setEnabled(true);
                                    et_city.setEnabled(true);
                                    et_city.setClickable(true);
                                    et_state.setClickable(true);
                                    et_state.setEnabled(true);
                                    et_pwd.setClickable(true);
                                    et_cpwd.setClickable(true);
                                    et_pwd.setEnabled(true);
                                    et_cpwd.setEnabled(true);
                                    btn_signup.setClickable(true);
                                    et_addr_owner.setEnabled(true);
                                    et_accnt_name.setEnabled(true);
                                    et_accnt_no.setEnabled(true);
                                    et_admin_offc.setEnabled(true);
                                    et_bname.setEnabled(true);
                                    et_center.setEnabled(true);
                                    et_faculty_cabin.setEnabled(true);
                                    et_pan_no.setEnabled(true);
                                    et_pname.setEnabled(true);
                                    et_refcode.setEnabled(true);
                                    et_website.setEnabled(true);
                                    et_pwd.setEnabled(true);
                                    et_cpwd.setEnabled(true);
                                    et_package.setEnabled(true);
                                    et_franchise_type.setEnabled(true);
                                    et_firm_type.setEnabled(true);
                                    et_addr_institute.setEnabled(true);
                                    et_ifsc.setEnabled(true);
                                    cb_terms.setEnabled(true);
                                }

                            }
                        });

                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            }

            @Override
            public void afterTextChanged(Editable s) {

            }

        });

    }

    private void registerData(String regId, String web_url) {


        dialogLoader.showProgressDialog();

        Call<ResponseModel> call = APIClient.getInstance()
                .registerCenter
                        (
                                franchise_id,
                                et_offc_area.getText().toString().trim(),
                                et_firm_type.getText().toString().trim(),
                                 "ABCD1234@123",
                                et_cname.getText().toString().trim(),
                                "yes",
                                et_pname.getText().toString().trim(),
                                 "4",et_addr_institute.getText().toString().trim(),
                                "jaipur",
                                "jaipur",
                               "4",
                                "djfjf",
                                et_mobile.getText().toString().trim(),
                                "9079004421",
                                et_email.getText().toString().trim(),
                                web_url,
                                country_id,
                                state_id,
                                city_id,
                                et_pwd.getText().toString().trim(),
                                et_cpwd.getText().toString().trim(),
                                package_id,
                                et_bname.getText().toString().trim(),
                                et_accnt_name.getText().toString().trim(),
                                et_accnt_no.getText().toString().trim(),
                                et_ifsc.getText().toString().trim(),
                                et_pan_no.getText().toString().trim(),
                                regId,
                                imei_number

                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        customerID = response.body().getId();

                        editor = sharedPreferences.edit();
                        editor.putString("userType","t");
                        editor.putString("userID", customerID);
                        editor.putString("userName",et_cname.getText().toString().trim());
                        editor.putBoolean("isLogged_in", true);
                        editor.apply();

                        // Set UserLoggedIn in MyAppPrefsManager
                        MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(TutorRegisterActivity.this);
                        myAppPrefsManager.setUserLoggedIn(false);
                        // Set isLogged_in of ConstantValues
                        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
                        Toast.makeText(getApplicationContext(),"Register Successfully", Toast.LENGTH_LONG).show();
                        Intent intent = new Intent(TutorRegisterActivity.this,MainActivity.class);
                        startActivity(intent);
                        finish();
//                        String[] splited = response.body().getData().split("\\s+");
//                        price = splited[2];
//                        customerID = response.body().getId();
//                        startPayment(price);
//                        launchPayUMoneyFlow(price);
                    }
                    else if (response.body().getSuccess().equalsIgnoreCase("0"))
                    {
                        // Get the Error Message from Response
                        Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later",Toast  .LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("ResultData","Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later", Toast.LENGTH_LONG).show();

            }
        });


    }

    private void webDialog() {
//        LayoutInflater inflater = getLayoutInflater()
        LayoutInflater inflater = (LayoutInflater)TutorRegisterActivity.this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View alertLayout = inflater.inflate(R.layout.dialog_term_condition_tutor,null);
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        webView = (WebView) alertLayout.findViewById(R.id.text);
        btClose = alertLayout.findViewById(R.id.close_btn);


        alert.setView(alertLayout);
        AlertDialog alertDialog = alert.create();

        webView.loadUrl("file:///android_asset/tutor_policy.htm");
        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);

                return true;
            }
        });
        //Define what should happen when the close button is pressed.
        btClose.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Dismiss the dialog
                alertDialog.dismiss();

            }
        });


//        alert.setTitle(Html.fromHtml("<font color='#000000'>Terms & Conditions</font>"));
//        alert.setNegativeButton("Close", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//            }
//        });

//        WebView wv = new WebView(this);
//        wv.loadUrl("file:///android_asset/terms_condition.html");




        alertDialog.show();


    }

    public void startPayment(String price) {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        String phone = et_mobile.getText().toString().trim();
        String productName = et_center.getText().toString().trim();
        String firstName = et_cname.getText().toString().trim();
        String email = et_email.getText().toString().trim();
        price += "00";
//        final Activity activity = getApplicationContext();

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Digital Class");
            options.put("description", productName);
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://digitalclassworld.com//storage/app/courses/Logo.png");
            options.put("currency", "INR");
            options.put("amount", price);

            JSONObject preFill = new JSONObject();
            preFill.put("email", email);
            preFill.put("contact", phone);

            options.put("prefill", preFill);

            co.open(TutorRegisterActivity.this, options);
        } catch (Exception e) {
            Toast.makeText(getApplicationContext(), "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }

    private void launchPayUMoneyFlow(String price) {

        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();

        //Use this to set your custom text on result screen button

        //Use this to set your custom title for the activity
        payUmoneyConfig.setPayUmoneyActivityTitle("PayUMoney Digital Class");

        payUmoneyConfig.disableExitConfirmation(isDisableExitConfirmation);

        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();

        double amount = 0;
        try {
            amount = Double.parseDouble(price);

        } catch (Exception e) {
            e.printStackTrace();
        }
        txnid = "TXNID"+System.currentTimeMillis() + "";
        //String txnId = "TXNID720431525261327973";
        String phone = et_mobile.getText().toString().trim();
        String productName = "Center Registration";
        String firstName = et_cname.getText().toString().trim();
        String email = et_email.getText().toString().trim();
        String udf1 = "";
        String udf2 = "";
        String udf3 = "";
        String udf4 = "";
        String udf5 = "";
        String udf6 = "";
        String udf7 = "";
        String udf8 = "";
        String udf9 = "";
        String udf10 = "";

        AppEnvironment appEnvironment = ((App) getApplication()).getAppEnvironment();
        builder.setAmount(String.valueOf(amount))
                .setTxnId(txnid)
                .setPhone(phone)
                .setProductName(productName)
                .setFirstName(firstName)
                .setEmail(email)
                .setsUrl(appEnvironment.surl())
                .setfUrl(appEnvironment.furl())
                .setUdf1(udf1)
                .setUdf2(udf2)
                .setUdf3(udf3)
                .setUdf4(udf4)
                .setUdf5(udf5)
                .setUdf6(udf6)
                .setUdf7(udf7)
                .setUdf8(udf8)
                .setUdf9(udf9)
                .setUdf10(udf10)
                .setIsDebug(appEnvironment.debug())
                .setKey(appEnvironment.merchant_Key())
                .setMerchantId(appEnvironment.merchant_ID());

        try {
            mPaymentParams = builder.build();

            /*
             * Hash should always be generated from your server side.
             * */
            //    generateHashFromServer(mPaymentParams);

            /*            *//**
             * Do not use below code when going live
             * Below code is provided to generate hash from sdk.
             * It is recommended to generate hash from server side only.
             * */
            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);


            PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams,TutorRegisterActivity.this, R.style.AppTheme_default, true);


        } catch (Exception e) {
            // some exception occurred
           // Toast.makeText(TutorRegisterActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {

        StringBuilder stringBuilder = new StringBuilder();
        HashMap<String, String> params = paymentParam.getParams();
        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");

        AppEnvironment appEnvironment = ((App) getApplication() ).getAppEnvironment();
        stringBuilder.append(appEnvironment.salt());

        String hash = hashCal(stringBuilder.toString());
        paymentParam.setMerchantHash(hash);

        return paymentParam;
    }

    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // Result Code is -1 send from Payumoney activity
        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data !=
                null) {
            TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
                    .INTENT_EXTRA_TRANSACTION_RESPONSE);

            ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);

            // Check which object is non-null
            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
                if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                    //Success Transaction
                    String payuResponse = transactionResponse.getPayuResponse();
                    if(payuResponse.contains("success")) {
                        Toast.makeText(getApplicationContext(), "Payment Succesfully!!", Toast.LENGTH_SHORT).show();
                        editor = sharedPreferences.edit();
                        editor.putString("userType","t");
                        editor.putBoolean("isLogged_in", true);
                        editor.apply();

                        // Set UserLoggedIn in MyAppPrefsManager
                        MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(TutorRegisterActivity.this);
                        myAppPrefsManager.setUserLoggedIn(true);

                        // Set isLogged_in of ConstantValues
                        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
                        Intent intent = new Intent(TutorRegisterActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
//                        buyNow(customerID);
                    } else {
                        Toast.makeText(getApplicationContext(), "Payment Failed", Toast.LENGTH_LONG).show();
                    }
//                    buyNow(customerID);
                } else {
                    //Failure Transaction
                    Toast.makeText(getApplicationContext(), "Payment Failed", Toast.LENGTH_LONG).show();

                }

            } else if (resultModel != null && resultModel.getError() != null) {
                Log.d("PUMONEY", "Error response : " + resultModel.getError().getTransactionResponse());
            } else {
                Log.d("PUMONEY", "Both objects are null!");
            }
        }
    }

    private void getFranchies() {
        Call<CategoryData> call = APIClient.getInstance()
                .getBranchList();

        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        franchisedetails = response.body().getData();
                        String[] mStringArray = new String[franchisedetails.size()];
                        for (int i=0;i<franchisedetails.size();i++) {
                            mStringArray[i] = franchisedetails.get(i).getName();
                        }
                        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(TutorRegisterActivity.this, R.layout.dropdown, mStringArray);
                        et_franchise_type.setThreshold(0);//will start working from first character
                        et_franchise_type.setAdapter(countryAdapter);//setting the adapter data into the AutoCompleteTextView
                        getPackageList();

                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getPackageList() {
        Call<CategoryData> call = APIClient.getInstance()
                .getPackageList();

        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        packagedetails = response.body().getData();
                        String[] mStringArray = new String[packagedetails.size()];
                        for (int i=0;i<packagedetails.size();i++) {
                            mStringArray[i] = packagedetails.get(i).getName();
                        }
                        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(TutorRegisterActivity.this, R.layout.dropdown, mStringArray);
                        et_package.setThreshold(0);//will start working from first character
                        et_package.setAdapter(countryAdapter);//setting the adapter data into the AutoCompleteTextView
                        getCountries();

                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean validateInfoForm() {
        if (et_franchise_type.getText().toString().trim().isEmpty()) {
            et_franchise_type.setError("Please select franchise type");
            return false;
        } else if (et_firm_type.getText().toString().trim().isEmpty()) {
            et_firm_type.setError("Please select firm type");
            return false;
        } else if (!ValidateInputs.isValidName(et_cname.getText().toString().trim())) {
            et_cname.setError("Please Enter Center Name");
            return false;
        } else if (!ValidateInputs.isValidName(et_pname.getText().toString().trim())) {
            et_pname.setError("Please Enter Name of Proprietor");
            return false;
//        } else if (!ValidateInputs.isValidPhoneNo(et_fmobile.getText().toString().trim())) {
//            et_fmobile.setError("Please Enter Valid Mobile Number");
//            return false;
//        } else if (et_website.getText().toString().trim().isEmpty()) {
//            et_website.setError("Please Enter Valid Website URL");
//            return false;
        } else if (!ValidateInputs.isValidEmail(et_email.getText().toString().trim())) {
            et_email.setError("Please Enter Valid Email");
            return false;
        } else if (!ValidateInputs.isValidPhoneNo(et_mobile.getText().toString().trim())) {
            et_mobile.setError("Please Enter Valid Mobile number");
            return false;
        } else if (!ValidateInputs.isValidPassword(et_pwd.getText().toString().trim())) {
            et_pwd.setError("Please Enter Valid Password");
            return false;
        }  else if (!ValidateInputs.isValidPassword(et_cpwd.getText().toString().trim())) {
            et_cpwd.setError("Please Enter Valid Password");
            return false;
//        } else if(et_addr_owner.getText().toString().trim().isEmpty()) {
//            et_addr_owner.setError("Please Enter Owner address");
//            return false;
        } else if(et_addr_institute.getText().toString().trim().isEmpty()) {
            et_addr_institute.setError("Please Enter address");
            return false;
        } else if(et_city.getText().toString().trim().isEmpty()) {
            et_city.setError("Please Enter City");
            return false;
        } else if(et_state.getText().toString().trim().isEmpty()) {
            et_state.setError("Please Enter State");
            return false;
        } else if(et_country.getText().toString().trim().isEmpty()) {
            et_country.setError("Please Enter Country");
            return false;
//        } else if(et_package.getText().toString().trim().isEmpty()) {
//            et_package.setError("Please select package");
//            return false;
//        } else if(et_offc_area.getText().toString().trim().isEmpty()) {
//            et_offc_area.setError("Please Enter Office Carpet area");
//            return false;
//        } else if(et_system_no.getText().toString().trim().isEmpty()) {
//            et_system_no.setError("Please Enter System No.");
//            return false;
//        } else if(et_admin_offc.getText().toString().trim().isEmpty()) {
//            et_admin_offc.setError("Please enter admin office");
//            return false;
//        } else if(et_faculty_cabin.getText().toString().trim().isEmpty()) {
//            et_faculty_cabin.setError("Please enter faculty cabin");
//            return false;
//        }else if(et_air_conditions.getText().toString().trim().isEmpty()) {
//            et_air_conditions.setError("Please enter no. of air conditions");
//            return false;
//        } else if(et_lab.getText().toString().trim().isEmpty()) {
//            et_lab.setError("Please enter no. of lab");
//            return false;
//        } else if(et_center.getText().toString().trim().isEmpty()) {
//            et_center.setError("Please Enter Center other details");
//            return false;
        } /*else if(et_bname.getText().toString().trim().isEmpty()) {
            et_bname.setError("Please Enter Bank Name");
            return false;
        } else if(et_accnt_name.getText().toString().trim().isEmpty()) {
            et_accnt_name.setError("Please enter account holder name");
            return false;
        } else if(et_accnt_no.getText().toString().trim().isEmpty()) {
            et_accnt_no.setError("Please enter account no.");
            return false;
        } else if(et_ifsc.getText().toString().trim().isEmpty()) {
            et_ifsc.setError("Please Enter IFSC Code");
            return false;
        } else if(et_pan_no.getText().toString().trim().isEmpty()) {
            et_pan_no.setError("Please Enter Pan Card No.");
            return false;
        }*/ else {
            return true;
        }
    }


    private void getCountries() {
        Call<CountryData> call = APIClient.getInstance()
                .getCountryList();

        call.enqueue(new Callback<CountryData>() {
            @Override
            public void onResponse(Call<CountryData> call, Response<CountryData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        countrydetails = response.body().getData();
                        String[] mStringArray = new String[countrydetails.size()];
                        for (int i=0;i<countrydetails.size();i++) {
                            mStringArray[i] = countrydetails.get(i).getCountry_name();
                        }
                        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(TutorRegisterActivity.this, R.layout.dropdown, mStringArray);
                        et_country.setThreshold(1);//will start working from first character
                        et_country.setAdapter(countryAdapter);//setting the adapter data into the AutoCompleteTextView
                        getStates();

                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CountryData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getStates() {
        Call<StateData> call = APIClient.getInstance()
                .getStateList();

        call.enqueue(new Callback<StateData>() {
            @Override
            public void onResponse(Call<StateData> call, Response<StateData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        countrydetails1 = response.body().getData();
                        String[] mStringArray = new String[countrydetails1.size()];
                        for (int i=0;i<countrydetails1.size();i++) {
                            mStringArray[i] = countrydetails1.get(i).getState_name();
                        }
                        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(TutorRegisterActivity.this, R.layout.dropdown, mStringArray);
                        et_state.setThreshold(1);//will start working from first character
                        et_state.setAdapter(stateAdapter);//setting the adapter data into the AutoCompleteTextView
                        getCities();

                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<StateData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getCities() {
        Call<CityData> call = APIClient.getInstance()
                .getCityList();

        call.enqueue(new Callback<CityData>() {
            @Override
            public void onResponse(Call<CityData> call, Response<CityData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        countrydetails2 = response.body().getData();
                        String[] mStringArray = new String[countrydetails2.size()];
                        for (int i=0;i<countrydetails2.size();i++) {
                            mStringArray[i] = countrydetails2.get(i).getCity_name();
                        }
                        ArrayAdapter<String> cityyAdapter = new ArrayAdapter<String>(TutorRegisterActivity.this, R.layout.dropdown, mStringArray);
                        et_city.setThreshold(1);//will start working from first character
                        et_city.setAdapter(cityyAdapter);//setting the adapter data into the AutoCompleteTextView
                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CityData> call, Throwable t) {
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1111:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                    TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                    if (null != tm) {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        imei_number = tm.getDeviceId();
                    }
                    if (null == imei_number || 0 == imei_number.length()) {
                        imei_number = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                    }
//        imei_number = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                    Log.d("IMEI", imei_number);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onPaymentSuccess(String s) {
        try {
            txnid = s;
            buyNow(customerID);
            Log.d("RESPONSE_PY", s);
            Toast.makeText(getApplicationContext(), "Payment Succesfully!!", Toast.LENGTH_SHORT).show();

        } catch (Exception e) {
            Log.e("PAYMENT", "Exception in onPaymentSuccess", e);
        }
    }

    private void buyNow(final String customerID) {
        dialogLoader.showProgressDialog();
        Call<ResponseModel> call = APIClient.getInstance()
                .buyCentreNow
                        (
                                customerID,
                                price,
                                txnid
                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getData().equalsIgnoreCase("Payment successfully!"))
                    {
                        // Get the User Details from Response
                        editor = sharedPreferences.edit();
                        editor.putString("userType","t");
                        editor.putString("userID", customerID);
                        editor.putString("userName",et_cname.getText().toString().trim());
                        editor.putBoolean("isLogged_in", true);
                        editor.apply();

                        // Set UserLoggedIn in MyAppPrefsManager
                        MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(TutorRegisterActivity.this);
                        myAppPrefsManager.setUserLoggedIn(true);

                        // Set isLogged_in of ConstantValues
                        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
                        Intent intent = new Intent(TutorRegisterActivity.this, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }
                    else if (response.body().getSuccess().equalsIgnoreCase("0"))
                    {
                        // Get the Error Message from Response
                        Toast.makeText(getApplicationContext(), response.body().getData(), Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!",Toast  .LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();

            }
        });
    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(getApplicationContext(), "Payment failed", Toast.LENGTH_LONG).show();
    }


    private void checkmobilenumber(String number, String type, RegisterActivity.ApiCallback callback) {

        Call<ResponseModel> call = null;

        if (type.equals("phone")) {
            progress_phone.setVisibility(View.VISIBLE);
            call = APIClient.getInstance().check_mobile_number_tutor(number);
        }


        if (type.equals("email")) {
            progress_email.setVisibility(View.VISIBLE);
            call = APIClient.getInstance().check_email_tutor(number);
        }

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                progress_phone.setVisibility(View.GONE);
                progress_email.setVisibility(View.GONE);

                if (response.body().getSuccess().equalsIgnoreCase("1")) {
                    callback.onResponse(response.body().getSuccess() != null);
                } else {
                    callback.onResponse(false);
                }

            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                progress_phone.setVisibility(View.GONE);
                progress_email.setVisibility(View.GONE);
                call.cancel();
                callback.onResponse(false);

            }
        });
    }


}
