package com.horizzon.vcec.adapter;

import android.content.Context;
import android.content.Intent;

import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.horizzon.vcec.ExamActivity;
import com.horizzon.vcec.FreQueActivity;
import com.horizzon.vcec.util.OnVideoClickListner;
import com.horizzon.vcec.R;
import com.horizzon.vcec.model.ResultDetails;
import com.horizzon.vcec.model.VideoDetails;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomVListadapter extends RecyclerView.Adapter<CustomVListadapter.MyViewHolder> {
    private Context context;
    private List<VideoDetails> movieList;
    OnVideoClickListner onVideoClickListner;
    List<ResultDetails> resultDetailsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_nme;
        ImageView iv_video;
        Button  btn_faq;
        TextView btn_exam;
        public MyViewHolder(View view) {
            super(view);
            iv_video = view.findViewById(R.id.video);
            tv_nme = view.findViewById(R.id.tv_name);
            btn_exam = view.findViewById(R.id.btn_exam);
           // btn_faq = view.findViewById(R.id.btn_faq);

        }
    }


    public CustomVListadapter(Context context, List<VideoDetails> movieList, OnVideoClickListner onVideoClickListner, List<ResultDetails> resultDetailsList) {
        this.context = context;
        this.movieList = movieList;
        this.onVideoClickListner = onVideoClickListner;
        this.resultDetailsList = resultDetailsList;
    }

    @Override
    public CustomVListadapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_vlist_layout, parent, false);
        return new CustomVListadapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CustomVListadapter.MyViewHolder holder, final int position) {
        final VideoDetails movie = movieList.get(position);
        holder.tv_nme.setText(movie.getName());
        Picasso.get().load("https://digitalclassworld.com/storage/app/"+movie.getImage()).placeholder(R.drawable.icon).into(holder.iv_video);
        holder.iv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(movie.getType().equals("file")) {
//                    onVideoClickListner.onVideoClick(position);
//                } else {
//                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(movie.getLink())));
//                }
                onVideoClickListner.onVideoClick(position, movie.getType());


            }
        });

        if(movie.getResultDetails()!=null) {
            ResultDetails resultDetails = movie.getResultDetails();
            if (resultDetails.getStatus().equals("Fail")) {
                holder.btn_exam.setEnabled(true);
            } else {
                holder.btn_exam.setEnabled(false);
                double correct = Double.parseDouble(resultDetails.getCorrect_ans());
                double total = Double.parseDouble(resultDetails.getCorrect_ans()) + Double.parseDouble(resultDetails.getWrong_ans());
                double percentage = (correct*100)/total;
                holder.btn_exam.setText("Pass ("+String.format("%.2f", percentage)+" % result)");
            }
        }

        holder.btn_exam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, ExamActivity.class);
                intent.putExtra("video_id", movie.getId());
                context.startActivity(intent);
            }
        });

        /*holder.btn_faq.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =new Intent(context, FreQueActivity.class);
                intent.putExtra("video_id", movie.getId());
                context.startActivity(intent);
            }
        });*/

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }
}
