package com.horizzon.vcec.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horizzon.vcec.R;
import com.horizzon.vcec.model.StudentDetails;

import java.util.List;

public class OrderDetailadapter extends RecyclerView.Adapter<OrderDetailadapter.MyViewHolder> {
    private Context context;
    private List<StudentDetails> movieList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_serial, tv_pinno, tv_userid, tv_unme;


        public MyViewHolder(View view) {
            super(view);
            tv_serial = view.findViewById(R.id.tv_serial);
            tv_pinno = view.findViewById(R.id.tv_pinno);
            tv_userid = view.findViewById(R.id.tv_userid);
            tv_unme = view.findViewById(R.id.tv_unme);
        }
    }


    public OrderDetailadapter(Context context, List<StudentDetails> movieList) {
        this.context = context;
        this.movieList = movieList;
    }

    @Override
    public OrderDetailadapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_order_layout, parent, false);

        return new OrderDetailadapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(OrderDetailadapter.MyViewHolder holder, final int position) {
        final StudentDetails movie = movieList.get(position);
        holder.tv_serial.setText(movie.getSrno());
        holder.tv_unme.setText(movie.getCreated_date());
        holder.tv_pinno.setText(movie.getTrazection_id());
        holder.tv_userid.setText("\u20B9 "+movie.getPrise());

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }
}

