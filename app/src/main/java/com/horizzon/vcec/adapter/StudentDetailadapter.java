package com.horizzon.vcec.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.R;
import com.horizzon.vcec.model.StudentDetails;

import java.util.List;

public class StudentDetailadapter extends RecyclerView.Adapter<StudentDetailadapter.MyViewHolder> {
    private Context context;
    private List<StudentDetails> movieList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_serial, tv_pinno, tv_userid;


        public MyViewHolder(View view) {
            super(view);
            tv_serial = view.findViewById(R.id.tv_serial);
            tv_pinno = view.findViewById(R.id.tv_pinno);
            tv_userid = view.findViewById(R.id.tv_userid);
        }
    }


    public StudentDetailadapter(Context context, List<StudentDetails> movieList) {
        this.context = context;
        this.movieList = movieList;
    }

    @Override
    public StudentDetailadapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_student_layout, parent, false);

        return new StudentDetailadapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(StudentDetailadapter.MyViewHolder holder, final int position) {
        final StudentDetails movie = movieList.get(position);
        holder.tv_serial.setText(movie.getSrno());
        holder.tv_userid.setText(movie.getCreated_date());

//        String date=movie.getCreated_date();
//        Toast.makeText(context, "date is"+date, Toast.LENGTH_SHORT).show();
        holder.tv_pinno.setText(movie.getName());

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }
}

