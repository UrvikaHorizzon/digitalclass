package com.horizzon.vcec.adapter;
import android.content.Context;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Parcelable;
import androidx.viewpager.widget.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.VideoView;

import com.horizzon.vcec.R;

public class CustomPagerAdapter extends PagerAdapter {

    private String[] imageModelArrayList;
    private LayoutInflater inflater;
    private Context context;
    VideoView videoView;
    ProgressBar progressBar;


    public CustomPagerAdapter(Context context, String[] imageModelArrayList) {
        this.context = context;
        this.imageModelArrayList = imageModelArrayList;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return imageModelArrayList.length;
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.pager_layout, view, false);

        assert imageLayout != null;
        videoView = imageLayout.findViewById(R.id.video);
        progressBar = imageLayout.findViewById(R.id.progrss);


//        imageView.setImageResource(imageModelArrayList.get(position).getImage_drawable());

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

    public void start(int position) {
        if(videoView != null) {
            String uriPth = "https://digitalclassworld.com/storage/app/"+imageModelArrayList[position];
            Uri videoUri = Uri.parse(uriPth);

            videoView.setVideoURI(videoUri);

            MediaController mediaController = new MediaController(context);
            mediaController.setAnchorView(videoView);
//            progressBar.setVisibility(View.VISIBLE);

            videoView.setMediaController(mediaController);

            videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
                @Override
                public void onPrepared(MediaPlayer mp) {
                    videoView.start();
//                    if (mp.isPlaying()){
//                        progressBar.setVisibility(View.GONE);
//                    }
                }
            });

        }
    }

    public void stop() {
        if(videoView != null) {
            videoView.stopPlayback();
        }
    }
}

