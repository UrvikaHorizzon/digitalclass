package com.horizzon.vcec.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.horizzon.vcec.R;
import com.horizzon.vcec.YouTubeVideoActivity;
import com.horizzon.vcec.model.DemoDetails;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

public class DemoListAdapter extends RecyclerView.Adapter<DemoListAdapter.MyViewHolder> {


    private Context context;
    private List<DemoDetails> demoList;
    private String course_id;

    public DemoListAdapter(FragmentActivity activity, ArrayList<DemoDetails> arrayList, String id) {
        this.context = activity;
        this.demoList = arrayList;
        this.course_id = id;
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_demo_videos, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final DemoDetails movie = demoList.get(position);

        Picasso.get().load("https://digitalclassworld.com/storage/app/" + movie.getVideo_image()).placeholder(R.drawable.icon).into(holder.iv_video);
        holder.iv_progrss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(movie.getType().equals("file")) {

                Intent intent = new Intent(context, YouTubeVideoActivity.class);
                intent.putExtra("url", movie.getVideotype());
                intent.putExtra("video_id", movie.getId());
                intent.putExtra("course_id", course_id);
                intent.putExtra("video", (Serializable) demoList);
                intent.putExtra("videolink", movie.getVideoLink());
                intent.putExtra("position", position);

                ((Activity) context).startActivityForResult(intent, 1);
//
//                String result = imgpath.substring(imgpath.lastIndexOf("/") + 1);
//                System.out.println("Image name " + result);


//                } else {
//                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(movie.getLink())));
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return demoList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_nme;
        ImageView iv_video, iv_progrss;
        Button btn_exam;

        public MyViewHolder(@NonNull View view) {
            super(view);

            iv_video = view.findViewById(R.id.video);
            iv_progrss = view.findViewById(R.id.progrss);
//            tv_nme = view.findViewById(R.id.tv_name);
//            btn_exam = view.findViewById(R.id.btn_exam);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
