package com.horizzon.vcec.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.vcec.R;
import com.horizzon.vcec.YouTubeVideoActivity;
import com.horizzon.vcec.model.DemoDetails;
import com.horizzon.vcec.model.ResultDetails;
import com.horizzon.vcec.model.VideoDetails;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CustomDemoAdapter extends RecyclerView.Adapter<CustomDemoAdapter.MyViewHolder> {

    private Context context;
    private List<DemoDetails> movieList;
    public String course_name, view_video, tuts;
    List<ResultDetails> resultDetails;

    public CustomDemoAdapter(FragmentActivity activity, List<DemoDetails> movieList, String course_name, String view_video, List<ResultDetails> resultDetails, String yes) {
        this.context = activity;
        this.movieList = movieList;
        this.course_name = course_name;
        this.view_video = view_video;
        this.resultDetails = resultDetails;
        this.tuts=yes;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_demo_videos,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final DemoDetails movie = movieList.get(position);
//        holder.tv_nme.setText(movie.g);
        if(tuts.equals("no")){
            holder.iv_progrss.setVisibility(View.GONE);
          //  holder.btn_exam.setVisibility(View.VISIBLE);
        } else {
            holder.iv_progrss.setVisibility(View.VISIBLE);
           // holder.btn_exam.setVisibility(View.GONE);

        }
//        holder.course_by.setText(course_name);
        Picasso.get().load("https://digitalclassworld.com/storage/app/"+movie.getImage()).placeholder(R.drawable.icon).into(holder.iv_video);
        holder.iv_progrss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(movie.getType().equals("file")) {final String uriPath = "https://digitalclassworld.com/storage/app/" + movie.getFile();

                Intent intent = new Intent(context, YouTubeVideoActivity.class);
                intent.putExtra("url",movie.getVideotype());
                intent.putExtra("course_id", movie.getCourseId());
                intent.putExtra("video", (Serializable)movieList);
                intent.putExtra("videolink",movie.getVideoLink());
                intent.putExtra("video_id",movie.getId());
                intent.putExtra("course_name", course_name);
                intent.putExtra("position",1);
                intent.putExtra("view_video", view_video);
                intent.putExtra("result", (Serializable) resultDetails);
                context.startActivity(intent);


//
//                String result = imgpath.substring(imgpath.lastIndexOf("/") + 1);
//                System.out.println("Image name " + result);


//                } else {
//                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(movie.getLink())));
//                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_nme,course_by;
        ImageView iv_video, iv_progrss;
       // ImageView btn_exam;

        public MyViewHolder(@NonNull View view) {
            super(view);

            iv_video = view.findViewById(R.id.video);
            iv_progrss = view.findViewById(R.id.progrss);
            tv_nme = view.findViewById(R.id.tv_name);
        //    btn_exam = view.findViewById(R.id.btn_exam);
//            course_by = view.findViewById(R.id.course_by);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }
}
