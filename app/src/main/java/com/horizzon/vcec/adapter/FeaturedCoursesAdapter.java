package com.horizzon.vcec.adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.fragments.CouceDetailFragment;
import com.horizzon.vcec.model.CourceDetails;
import com.squareup.picasso.Picasso;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

public class FeaturedCoursesAdapter extends RecyclerView.Adapter<FeaturedCoursesAdapter.MyViewHolder> {
    private Activity context;
    private List<CourceDetails> movieList;
    private String cotype;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public RatingBar ratingBar;
        public TextView tv_cource_title, tv_cource_price, tv_cource_price1, tv_cource_by;
        ImageView iv_cat;
        LinearLayout ll_cources;

        public MyViewHolder(View view) {
            super(view);
            tv_cource_title = view.findViewById(R.id.tv_cource_title);
            tv_cource_price = view.findViewById(R.id.tv_price);
            tv_cource_price1 = view.findViewById(R.id.tv_price1);
            tv_cource_by = view.findViewById(R.id.tv_cource_by);
            iv_cat = view.findViewById(R.id.iv_cource);
            ll_cources = view.findViewById(R.id.ll_cources);


        }
    }


    public FeaturedCoursesAdapter(Activity context, List<CourceDetails> movieList, String courselist) {
        this.context = context;
        this.movieList = movieList;
        this.cotype = courselist;
    }

    @Override
    public FeaturedCoursesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_new_courses, parent, false);

        return new FeaturedCoursesAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final CourceDetails movie = movieList.get(position);
        holder.tv_cource_title.setText(movie.getMain_category_id());
        holder.tv_cource_by.setText("Course by : " + movie.getName());
        holder.tv_cource_price.setText("\u20B9 " + movie.getPrice());
        holder.tv_cource_price1.setText("\u20B9 " + movie.getActual_price());


        holder.tv_cource_price1.setPaintFlags(holder.tv_cource_price1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        Picasso.get().load("https://digitalclassworld.com/storage/app/" + movie.getImage()).into(holder.iv_cat);

        holder.ll_cources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new CouceDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("CourseDetail", movie);
                bundle.putString("type", cotype);
                fragment.setArguments(bundle);
                FragmentTransaction transaction = ((HomeActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment, "cource_detail");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

    }


    @Override
    public int getItemCount() {
        return movieList.size();
    }
}
