package com.horizzon.vcec.adapter;

import android.content.Context;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horizzon.vcec.R;
import com.horizzon.vcec.model.FaQDetails;

import java.util.List;

public class FaqListadapter extends RecyclerView.Adapter<FaqListadapter.MyViewHolder> {
    private Context context;
    private List<FaQDetails> movieList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cource_title, tv_cource_no;


        public MyViewHolder(View view) {
            super(view);
            tv_cource_title = view.findViewById(R.id.tv_que);
            tv_cource_no = view.findViewById(R.id.tv_ans);

        }
    }


    public FaqListadapter(Context context, List<FaQDetails> movieList) {
        this.context = context;
        this.movieList = movieList;
    }

    @Override
    public FaqListadapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_fq_layout, parent, false);

        return new FaqListadapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(FaqListadapter.MyViewHolder holder, final int position) {
        final FaQDetails movie = movieList.get(position);
        holder.tv_cource_title.setText(movie.getQuery());
        if(movie.getAnswer() !=null) {

            holder.tv_cource_no.setVisibility(View.GONE);

        } else {

            holder.tv_cource_no.setVisibility(View.VISIBLE);
            holder.tv_cource_no.setText(movie.getAnswer());

        }

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }
}
