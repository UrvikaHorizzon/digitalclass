package com.horizzon.vcec.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.google.android.material.snackbar.Snackbar;
import com.horizzon.vcec.BecomeTutor;
import com.horizzon.vcec.R;

import java.util.ArrayList;

import androidx.viewpager.widget.PagerAdapter;

public class CustomViewPagerDealAdapter extends PagerAdapter {

    private ArrayList<Integer> IMAGES;
    private LayoutInflater inflater;
    private Context context;


    public CustomViewPagerDealAdapter(Context context, ArrayList<Integer> IMAGES) {
        this.context = context;
        this.IMAGES=IMAGES;
        inflater = LayoutInflater.from(context);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((View) object);
    }

    @Override
    public int getCount() {
        return IMAGES.size();
    }

    @Override
    public Object instantiateItem(ViewGroup view, int position) {
        View imageLayout = inflater.inflate(R.layout.viewpager_deal, view, false);

        assert imageLayout != null;

        final ImageView imageView = (ImageView) imageLayout
                .findViewById(R.id.image);

       final FrameLayout framelayout = (FrameLayout) imageLayout.findViewById(R.id.framelayout);


        imageView.setImageResource(IMAGES.get(position));
        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Snackbar snackbar = Snackbar
                        .make(framelayout, "We are currently working on features this please visit after some times", Snackbar.LENGTH_LONG);

                snackbar.show();
            }
        });

        view.addView(imageLayout, 0);

        return imageLayout;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public void restoreState(Parcelable state, ClassLoader loader) {
    }

    @Override
    public Parcelable saveState() {
        return null;
    }

}