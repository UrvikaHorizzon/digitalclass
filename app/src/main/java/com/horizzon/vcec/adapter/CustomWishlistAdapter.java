package com.horizzon.vcec.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.Config;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.SelectRoleActivity;
import com.horizzon.vcec.fragments.CouceDetailFragment;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.MyAppPrefsManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class CustomWishlistAdapter extends RecyclerView.Adapter<CustomWishlistAdapter.MyViewHolder> {
    private MyAppPrefsManager myAppPrefsManager;
    private Activity context;
    private List<CourceDetails> movieList;
    String img_flg;
    DialogLoader dialogLoader;
    private String customerID;
    private String cotype;
    List<CourceDetails> storelist = new ArrayList<CourceDetails>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cource_title, tv_cource_price, tv_cource_price1, tv_cource_by;
        ImageView iv_cat, iv_wishlist;
        LinearLayout ll_cources;
        RatingBar ratingBar;


        public MyViewHolder(View view) {
            super(view);
            tv_cource_title = view.findViewById(R.id.tv_cource_title);
            ll_cources = view.findViewById(R.id.ll_cources);
            tv_cource_price = view.findViewById(R.id.tv_price);
            tv_cource_price1 = view.findViewById(R.id.tv_price1);
            tv_cource_by = view.findViewById(R.id.tv_cource_by);
            iv_cat = view.findViewById(R.id.iv_cource);
            iv_wishlist = view.findViewById(R.id.iv_wishlist);
            ratingBar = view.findViewById(R.id.ratingBar);

        }
    }

    public void findItem(List<CourceDetails> productlist) {
        this.storelist.addAll(productlist);
    }


    public CustomWishlistAdapter(Activity cxt, List<CourceDetails> movieList, String img_flg, String type) {
        this.context = cxt;
        this.movieList = movieList;
        this.img_flg = img_flg;
        dialogLoader = new DialogLoader(cxt);
        customerID = context.getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");
        myAppPrefsManager = new MyAppPrefsManager(context);
        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
        this.cotype = type;
    }

    @Override
    public CustomWishlistAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_wishlist_layout, parent, false);

        return new CustomWishlistAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final CourceDetails movie = movieList.get(position);
        holder.tv_cource_by.setText("Course by : " + movie.getName());
        holder.tv_cource_title.setText(movie.getCourse_name());
        holder.tv_cource_price.setText("\u20B9 " + movie.getPrice());
        holder.tv_cource_price1.setText("\u20B9 " + movie.getActual_price());

        try {
            holder.ratingBar.setRating(Float.parseFloat(movie.getRating()));
        } catch (Exception e) {

        }
        holder.tv_cource_price1.setPaintFlags(holder.tv_cource_price1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        Picasso.get().load("https://digitalclassworld.com/storage/app/" + movie.getImage()).into(holder.iv_cat);
        if (img_flg.equals("no")) {
            holder.iv_wishlist.setVisibility(View.VISIBLE);
        } else {
            holder.iv_wishlist.setVisibility(View.GONE);
        }

        holder.iv_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Config.IS_USER_LOGGED_IN) {
                    dialogLoader.showProgressDialog();

                    Call<ResponseModel> call = APIClient.getInstance()
                            .addToWishList
                                    (customerID,
                                            movie.getId()
                                    );

                    call.enqueue(new Callback<ResponseModel>() {
                        @Override
                        public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                            dialogLoader.hideProgressDialog();

                            if (response.isSuccessful()) {
                                if (response.body().getData().equalsIgnoreCase("Whislist add successfully")) {
                                    // Get the User Details from Response
                                    Toast.makeText(context, "Added to Wishlist!", Toast.LENGTH_LONG).show();
                                    holder.iv_wishlist.setImageResource(R.drawable.ic_turned_in_black_24dp);
                                    holder.iv_wishlist.setEnabled(false);
                                    notifyDataSetChanged();
                                } else if (response.body().getSuccess().equalsIgnoreCase("1")) {
                                    // Get the Error Message from Response
                                    Toast.makeText(context, response.body().getData(), Toast.LENGTH_LONG).show();
                                } else {
                                    Toast.makeText(context, "Something went wrong..Please try again later!!", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                Log.d("RES", "Not SuccessFull Error Msg For Logcat");
                                // Show the Error Message
                                Toast.makeText(context, "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<ResponseModel> call, Throwable t) {
                            dialogLoader.hideProgressDialog();
                            Toast.makeText(context, "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();

                        }
                    });
                } else {
                    Intent in = new Intent(context, SelectRoleActivity.class);
                    context.startActivity(in);
                }
            }
        });

        holder.ll_cources.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment fragment = new CouceDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("CourseDetail", movie);
                bundle.putString("type", cotype);
                fragment.setArguments(bundle);
                FragmentTransaction transaction = ((HomeActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment, "cource_detail");
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public void filter(String filterText) {
        filterText = filterText.toLowerCase(Locale.getDefault());
        movieList.clear();
        if (filterText.length() == 0) {
            movieList.addAll(storelist);
        } else {
            for (int i = 0; i < storelist.size(); i++) {
                CourceDetails ud = storelist.get(i);
                try {
                    String name = ud.getName().toLowerCase(Locale.getDefault());
                    String desc = ud.getDescription().toLowerCase(Locale.getDefault());
                    String cnme = ud.getMain_category_id().toLowerCase(Locale.getDefault());

                    if (name.contains(filterText) || desc.contains(filterText) || cnme.contains(filterText)) {
                        movieList.add(ud);
                    }
                } catch (Exception e) {

                }

            }
        }
        notifyDataSetChanged();
    }
}
