package com.horizzon.vcec.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.horizzon.vcec.R;
import com.horizzon.vcec.YouTubeVideoActivity;
import com.horizzon.vcec.model.DemoDetails;
import com.horizzon.vcec.model.VideoDetails;
import com.horizzon.vcec.util.OnVideoClickListner;
import com.squareup.picasso.Picasso;

import java.util.List;

public class DemoVideoAdapter extends RecyclerView.Adapter<DemoVideoAdapter.MyVideoHolder> {

    private Context context;
    private List<DemoDetails> movieList;
    OnVideoClickListner onVideoClickListner;

    public DemoVideoAdapter(YouTubeVideoActivity activity, List<DemoDetails> movieVList,OnVideoClickListner onVideoClickListner) {
        this.context=activity;
        this.movieList=movieVList;
        this.onVideoClickListner=onVideoClickListner;
    }

    @NonNull
    @Override
    public MyVideoHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.demo_adapter_layout, parent, false);
        return new MyVideoHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyVideoHolder holder, int position) {

        final DemoDetails movie = movieList.get(position);
//        holder.tv.setText(movie.getName());
        Picasso.get().load("https://digitalclassworld.com/storage/app/"+movie.getVideo_image()).placeholder(R.drawable.icon).into(holder.iv_video);
        holder.iv_video.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onVideoClickListner.onVideoClick(position, movie.getVideotype());


            }
        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class MyVideoHolder extends RecyclerView.ViewHolder {
        ImageView iv_video, iv_progrss;

        public MyVideoHolder(@NonNull View view) {
            super(view);
            iv_video = view.findViewById(R.id.video);
            iv_progrss = view.findViewById(R.id.progrss);

        }
    }
}
