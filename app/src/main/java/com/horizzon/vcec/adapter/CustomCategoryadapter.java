package com.horizzon.vcec.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.fragments.CourceListFragment;
import com.horizzon.vcec.fragments.SubCategoryFragment;
import com.horizzon.vcec.model.CategoryModel;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class CustomCategoryadapter extends RecyclerView.Adapter<CustomCategoryadapter.MyViewHolder> {
    private Context context;
    private List<CategoryModel> movieList;
    List<CategoryModel> storelist = new ArrayList<CategoryModel>();

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cource_title, tv_cource_no;
        LinearLayout ll_categories;


        public MyViewHolder(View view) {
            super(view);
            tv_cource_title = view.findViewById(R.id.tv_cource_title);
            ll_categories = view.findViewById(R.id.ll_categories);

        }
    }

    public void findItem(List<CategoryModel> productlist){
        this.storelist.addAll(productlist);
    }



    public CustomCategoryadapter(Context context, List<CategoryModel> movieList) {
        this.context = context;
        this.movieList = movieList;
    }

    @Override
    public CustomCategoryadapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_category_layout, parent, false);

        return new CustomCategoryadapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(CustomCategoryadapter.MyViewHolder holder, final int position) {
        final CategoryModel movie = movieList.get(position);

        if (movie.getStatus().equalsIgnoreCase("1")){
            holder.tv_cource_title.setText(movie.getName());
            holder.ll_categories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (!movie.getParent_id().equals("")) {
                            Fragment fragment = new CourceListFragment();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("cat_id", movie);
                            fragment.setArguments(bundle);
                            FragmentTransaction transaction = ((HomeActivity) context).getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.frame_container, fragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        } else {
                            Fragment fragment = new SubCategoryFragment();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("cat_id", movie);
                            fragment.setArguments(bundle);
                            FragmentTransaction transaction = ((HomeActivity) context).getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.frame_container, fragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        }
                    } catch (Exception e) {
                        Fragment fragment = new SubCategoryFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("cat_id", movie);
                        fragment.setArguments(bundle);
                        FragmentTransaction transaction = ((HomeActivity) context).getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_container, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public void filter(String filterText)
    {
        filterText = filterText.toLowerCase(Locale.getDefault());
        movieList.clear();
        if(filterText.length()==0)
        {
            movieList.addAll(storelist);
        }
        else
        {
            for(int i=0; i<storelist.size(); i++)
            {
                CategoryModel ud = storelist.get(i);
                String name = ud.getName().toLowerCase(Locale.getDefault());
                String desc = ud.getDescription().toLowerCase(Locale.getDefault());

                if(name.contains(filterText) || desc.contains(filterText))
                {
                    movieList.add(ud);
                }
            }
        }
        notifyDataSetChanged();
    }
}
