package com.horizzon.vcec.adapter;

import android.app.Activity;
import android.content.DialogInterface;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.fragments.AddCourceFragment;
import com.horizzon.vcec.fragments.DialogFragment;
import com.horizzon.vcec.fragments.TutorVideoFragment;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomCCourseadapter extends RecyclerView.Adapter<CustomCCourseadapter.MyViewHolder> {
    private Activity context;
    private List<CourceDetails> movieList;
    DialogLoader dialogLoader;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cource_title, tv_cource_price, tv_cource_price1;
        ImageView iv_cat;
        LinearLayout ll_cources;
        Button btn_edit, btn_lession;
        RatingBar ratingBar;


        public MyViewHolder(View view) {
            super(view);
            tv_cource_title = view.findViewById(R.id.tv_cource_title);
            ll_cources = view.findViewById(R.id.ll_cources);
            tv_cource_price = view.findViewById(R.id.tv_price);
            tv_cource_price1 = view.findViewById(R.id.tv_price1);
            iv_cat = view.findViewById(R.id.iv_cource);
            btn_edit = view.findViewById(R.id.btn_edit);
            btn_lession = view.findViewById(R.id.btn_lession);
            ratingBar = view.findViewById(R.id.ratingBar);
        }
    }



    public CustomCCourseadapter(Activity context, List<CourceDetails> movieList) {
        this.context = context;
        this.movieList = movieList;
        dialogLoader = new DialogLoader(context);

    }

    @Override
    public CustomCCourseadapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_ccourse_layout, parent, false);

        return new CustomCCourseadapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomCCourseadapter.MyViewHolder holder, final int position) {
        final CourceDetails movie = movieList.get(position);
        holder.tv_cource_title.setText(movie.getCourse_name());
        holder.tv_cource_price.setText("\u20B9 "+movie.getPrice());
        holder.tv_cource_price1.setText("\u20B9 "+movie.getActual_price());

        holder.ratingBar.setRating(Float.parseFloat(movie.getRating()));
        holder.tv_cource_price1.setPaintFlags(holder.tv_cource_price1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        Picasso.get().load("https://digitalclassworld.com/storage/app/"+movie.getImage()).into(holder.iv_cat);

        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AddCourceFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("CourseDetail", movie);
                fragment.setArguments(bundle);
                FragmentTransaction transaction = ((HomeActivity)context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        holder.btn_lession.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Fragment fragment = new TutorVideoFragment();
//                Bundle bundle = new Bundle();
//                bundle.putString("CourseId", movie.getId());
//                fragment.setArguments(bundle);
//                FragmentTransaction transaction = ((HomeActivity)context).getSupportFragmentManager().beginTransaction();
//                transaction.replace(R.id.frame_container, fragment);
//                transaction.addToBackStack(null);
//                transaction.commit();

                DialogFragment blankFragment = new DialogFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("CourseDetail", movie);
                bundle.putString("key",movie.getId());
                blankFragment.setArguments(bundle);
                blankFragment.show(((HomeActivity)context).getSupportFragmentManager(),"Dialog");
            }
        });



        /*holder.btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(context);
                builder.setMessage("Are you sure you want to exit?")
                        .setCancelable(false)
                        .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialogLoader.showProgressDialog();
                                Call<ResponseModel> call = APIClient.getInstance()
                                        .courseDelete(movie.getId());

                                call.enqueue(new Callback<ResponseModel>() {
                                    @Override
                                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                                        dialogLoader.hideProgressDialog();
                                        if (response.isSuccessful()) {
                                            if(response.body().getSuccess().equals("1")) {

                                                Toast.makeText(context, response.body().getData(), Toast.LENGTH_LONG).show();
                                                movieList.remove(holder.getAdapterPosition());
                                                notifyItemRemoved(holder.getAdapterPosition());
                                                notifyItemRangeChanged(holder.getAdapterPosition(), movieList.size());
                                            } else {
                                                Toast.makeText(context, response.body().getData(), Toast.LENGTH_LONG).show();
                                            }


                                        } else {
                                            Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                                            // Show the Error Message
                                        }
                                    }

                                    @Override
                                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                                        dialogLoader.hideProgressDialog();
                                        Toast.makeText(context,t.getMessage(), Toast.LENGTH_LONG).show();

                                    }
                                });
                            }
                        })
                        .setNegativeButton("No", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();

            }
        });*/
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

   
}
