package com.horizzon.vcec.adapter;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.horizzon.vcec.R;
import com.horizzon.vcec.model.videolist;
import com.squareup.picasso.Picasso;

import androidx.recyclerview.widget.RecyclerView;
import fm.jiecao.jcvideoplayer_lib.JCVideoPlayerStandard;

public class VideoViewAdapter extends RecyclerView.Adapter<VideoViewAdapter.ViewHolder> {
    private videolist[] videolist;
    Context context;

    // RecyclerView recyclerView;
    public VideoViewAdapter(Context context, videolist[] videolists) {
        this.context = context;
        this.videolist = videolists;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View listItem = layoutInflater.inflate(R.layout.layout_video_view, parent, false);
        ViewHolder viewHolder = new ViewHolder(listItem);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final videolist myListData = videolist[position];

        holder.tv_view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Snackbar snackbar = Snackbar
                        .make(holder.relativeLayout, "We are working on it", Snackbar.LENGTH_LONG);

                snackbar.show();

            }
        });

        holder.tv_name.setText(videolist[position].getName());
        holder.tv_cource_by.setText("Course By " + videolist[position].getCourse_by());
        holder.tv_price.setText(videolist[position].getPrice());
        holder.tv_discount_price.setText("\u20B9" + videolist[position].getDiscount_price());
        holder.tv_discount_price.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        holder.tv_discount.setText(videolist[position].getDiscount());

        Picasso.get()
                .load(videolist[position].getThumbnail_url())
                .into(holder.video_view.thumbImageView);

        holder.video_view.setUp(videolist[position].getVideo_url(), JCVideoPlayerStandard.SCREEN_WINDOW_FULLSCREEN, videolist[position].getName());

        holder.relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  Toast.makeText(view.getContext(),"click on item: "+myListData.getDescription(),Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public int getItemCount() {
        return videolist.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public JCVideoPlayerStandard video_view;
        public TextView tv_name, tv_cource_by, tv_price, tv_discount_price, tv_discount, tv_view;
        public RelativeLayout relativeLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            this.video_view = (JCVideoPlayerStandard) itemView.findViewById(R.id.video_view);
            this.tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            this.tv_cource_by = (TextView) itemView.findViewById(R.id.tv_cource_by);
            this.tv_view = (TextView) itemView.findViewById(R.id.tv_view);
            this.tv_price = (TextView) itemView.findViewById(R.id.tv_price);
            this.tv_discount_price = (TextView) itemView.findViewById(R.id.tv_discount_price);
            this.tv_discount = (TextView) itemView.findViewById(R.id.tv_discount);
            relativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativeLayout);
        }
    }
}