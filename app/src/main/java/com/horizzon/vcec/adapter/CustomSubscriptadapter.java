package com.horizzon.vcec.adapter;

import android.app.Activity;
import android.graphics.Paint;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.fragments.OrderDetailFragment;
import com.horizzon.vcec.fragments.StudentDetailFragment;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.util.DialogLoader;
import com.squareup.picasso.Picasso;

import java.util.List;

public class CustomSubscriptadapter  extends RecyclerView.Adapter<CustomSubscriptadapter.MyViewHolder> {
    private Activity context;
    private List<CourceDetails> movieList;
    DialogLoader dialogLoader;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cource_title, tv_cource_price, tv_cource_price1;
        ImageView iv_cat;
        LinearLayout ll_cources;
        Button btn_s_detail, btn_o_detail;
        RatingBar ratingBar;


        public MyViewHolder(View view) {
            super(view);
            tv_cource_title = view.findViewById(R.id.tv_cource_title);
            ll_cources = view.findViewById(R.id.ll_cources);
            tv_cource_price = view.findViewById(R.id.tv_price);
            tv_cource_price1 = view.findViewById(R.id.tv_price1);
            iv_cat = view.findViewById(R.id.iv_cource);
            btn_s_detail = view.findViewById(R.id.btn_s_detail);
            btn_o_detail = view.findViewById(R.id.btn_o_detail);
            ratingBar = view.findViewById(R.id.ratingBar);
        }
    }


    public CustomSubscriptadapter(Activity context, List<CourceDetails> movieList) {
        this.context = context;
        this.movieList = movieList;
    }

    @Override
    public CustomSubscriptadapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_subscription_layout, parent, false);

        return new CustomSubscriptadapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomSubscriptadapter.MyViewHolder holder, final int position) {
        final CourceDetails movie = movieList.get(position);
        holder.tv_cource_title.setText(movie.getCourse_name());
        holder.tv_cource_price.setText("\u20B9 " + movie.getActual_price());
        holder.tv_cource_price1.setText("\u20B9 " + movie.getPrice());
        holder.ratingBar.setRating(Float.parseFloat(movie.getRating()));
        holder.tv_cource_price1.setPaintFlags(holder.tv_cource_price1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        Picasso.get().load("https://digitalclassworld.com/storage/app/" + movie.getImage()).into(holder.iv_cat);

        holder.btn_s_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new StudentDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("CourseId", movie.getId());
                bundle.putString("type", "student");
                fragment.setArguments(bundle);
                FragmentTransaction transaction = ((HomeActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        holder.btn_o_detail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new OrderDetailFragment();
                Bundle bundle = new Bundle();
                bundle.putString("CourseId", movie.getId());
                bundle.putString("type", "order");
                fragment.setArguments(bundle);
                FragmentTransaction transaction = ((HomeActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

}
