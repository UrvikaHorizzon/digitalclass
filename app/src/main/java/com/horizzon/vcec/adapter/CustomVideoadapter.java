package com.horizzon.vcec.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.gson.Gson;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.VideoListActivity;
import com.horizzon.vcec.fragments.TutorExamFragment;
import com.horizzon.vcec.model.ResultDetails;
import com.horizzon.vcec.model.VideoDetails;
import com.squareup.picasso.Picasso;

import java.io.Serializable;
import java.util.List;

public class CustomVideoadapter extends RecyclerView.Adapter<CustomVideoadapter.MyViewHolder> {
    private Context context;
    private List<VideoDetails> movieList;
    public String course_name, view_video, tuts;
    List<ResultDetails> resultDetails;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_nme;
        ImageView iv_video, iv_progrss;
        Button btn_exam;

        public MyViewHolder(View view) {
            super(view);
            iv_video = view.findViewById(R.id.video);
            iv_progrss = view.findViewById(R.id.progrss);
            tv_nme = view.findViewById(R.id.tv_nme);
            btn_exam = view.findViewById(R.id.btn_exam);

            view.setOnClickListener(this);

        }


        @Override
        public void onClick(View v) {

        }
    }


    public CustomVideoadapter(Context context, List<VideoDetails> movieList, String course_name, String view_video, List<ResultDetails> resultDetails, String no) {
        this.context = context;
        this.movieList = movieList;
        this.course_name = course_name;
        this.view_video = view_video;
        this.resultDetails = resultDetails;
        this.tuts=no;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_video_layout, parent, false);

        return new MyViewHolder(itemView);


    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final VideoDetails movie = movieList.get(position);
//        holder.tv_nme.setText(movie.getName());
        if(tuts.equals("no")){
            holder.iv_progrss.setVisibility(View.GONE);
            holder.btn_exam.setVisibility(View.VISIBLE);
        } else {
            holder.iv_progrss.setVisibility(View.VISIBLE);
            holder.tv_nme.setText(movie.getName());
            holder.btn_exam.setVisibility(View.GONE);

        }
        Picasso.get().load("https://digitalclassworld.com/storage/app/"+movie.getImage()).placeholder(R.drawable.icon).into(holder.iv_video);
        holder.iv_progrss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(movie.getType().equals("file")) {
                    final String uriPath = "https://digitalclassworld.com/storage/app/" + movie.getFile();

                SharedPreferences preferences=context.getSharedPreferences("exam",Context.MODE_PRIVATE);
                Gson gson = new Gson();
                String json = gson.toJson(movieList);
                String json1 = gson.toJson(view_video);
                String json2 = gson.toJson(resultDetails);
                SharedPreferences.Editor e=preferences.edit();
                e.putString("url",movie.getType());
                e.putString("course_id",movie.getCourse_id());
                e.putString("video_name",movie.getName());
                e.putString("videolink",movie.getLink());
                e.putString("video_id",movie.getId());
                e.putString("course_name",course_name);
                e.putInt("position",0);
                e.putString("movie",json);
                e.putString("view_video",json1);
                e.putString("result",json2);
                e.apply();
                e.commit();

                    Intent intent = new Intent(context, VideoListActivity.class);
                    intent.putExtra("url",movie.getType());
                    intent.putExtra("course_id", movie.getCourse_id());
                    intent.putExtra("video_name", movie.getName());
                    intent.putExtra("video", (Serializable) movieList);
                    intent.putExtra("videolink",movie.getLink());
                    intent.putExtra("video_id",movie.getId());
                    intent.putExtra("course_name", course_name);
                    intent.putExtra("position",0);
                    intent.putExtra("view_video", view_video);
                    intent.putExtra("result", (Serializable) resultDetails);
                    context.startActivity(intent);

//
//                String result = imgpath.substring(imgpath.lastIndexOf("/") + 1);
//                System.out.println("Image name " + result);


//                } else {
//                    context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(movie.getLink())));
//                }
            }
        });

        holder.btn_exam.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new TutorExamFragment();
                Bundle bundle = new Bundle();
                bundle.putString("video_id", movie.getId());
                fragment.setArguments(bundle);
                FragmentTransaction transaction = ((HomeActivity)context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }



    public interface clickListner{
        public void onClick(View view, int position, ResultDetails details);
    }


}
