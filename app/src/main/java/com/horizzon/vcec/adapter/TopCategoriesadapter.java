package com.horizzon.vcec.adapter;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.fragments.CourceListFragment;
import com.horizzon.vcec.fragments.SubCategoryFragment;
import com.horizzon.vcec.model.CategoryModel;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TopCategoriesadapter extends RecyclerView.Adapter<TopCategoriesadapter.MyViewHolder> {
    private Context context;
    private List<CategoryModel> movieList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_cource_title, tv_cource_no;
        ImageView iv_cat;
        LinearLayout ll_categories;


        public MyViewHolder(View view) {
            super(view);
            tv_cource_title = view.findViewById(R.id.tv_cource_title);
            tv_cource_no = view.findViewById(R.id.tv_cource_no);
            iv_cat = view.findViewById(R.id.iv_cat);
            ll_categories = view.findViewById(R.id.ll_categories);

        }
    }


    public TopCategoriesadapter(Context context, List<CategoryModel> movieList) {
        this.context = context;
        this.movieList = movieList;
    }

    @Override
    public TopCategoriesadapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_cat_layout, parent, false);

        return new TopCategoriesadapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(TopCategoriesadapter.MyViewHolder holder, final int position) {
        final CategoryModel movie = movieList.get(position);

        if (movie.getStatus().equalsIgnoreCase("1")) {
            holder.tv_cource_title.setText(movie.getName());
            holder.tv_cource_no.setText("\u20B9 " + movie.getPrice());
            try {
                holder.iv_cat.setImageResource(movie.getImg());
            } catch (Exception e) {
                Picasso.get().load("https://digitalclassworld.com/storage/app/" + movie.getImage()).placeholder(R.drawable.icon).into(holder.iv_cat);

            }
            holder.tv_cource_no.setVisibility(View.GONE);

            holder.ll_categories.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (!movie.getParent_id().equals("")) {
                            Fragment fragment = new CourceListFragment();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("cat_id", movie);
                            fragment.setArguments(bundle);
                            FragmentTransaction transaction = ((HomeActivity) context).getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.frame_container, fragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        } else {
                            Fragment fragment = new SubCategoryFragment();
                            Bundle bundle = new Bundle();
                            bundle.putSerializable("cat_id", movie);
                            fragment.setArguments(bundle);
                            FragmentTransaction transaction = ((HomeActivity) context).getSupportFragmentManager().beginTransaction();
                            transaction.replace(R.id.frame_container, fragment);
                            transaction.addToBackStack(null);
                            transaction.commit();
                        }
                    } catch (Exception e) {
                        Fragment fragment = new SubCategoryFragment();
                        Bundle bundle = new Bundle();
                        bundle.putSerializable("cat_id", movie);
                        fragment.setArguments(bundle);
                        FragmentTransaction transaction = ((HomeActivity) context).getSupportFragmentManager().beginTransaction();
                        transaction.replace(R.id.frame_container, fragment);
                        transaction.addToBackStack(null);
                        transaction.commit();
                    }
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }
}