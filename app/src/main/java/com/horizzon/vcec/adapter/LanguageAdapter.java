package com.horizzon.vcec.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.vcec.R;
import com.horizzon.vcec.model.CategoryModel;

import java.util.ArrayList;
import java.util.List;

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.MyViewHolder> {

    private List<CategoryModel> checklist;
    Context context;
    public int count=0;


    public LanguageAdapter(FragmentActivity testActivity, List<CategoryModel> title_list) {
        this.checklist=title_list;
        this.context=testActivity;
        setHasStableIds(true);
        count=0;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view= LayoutInflater.from(parent.getContext()).inflate(R.layout.tille_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final int pos = position;
        holder.tvName.setText(checklist.get(position).getLanguageName());
        holder.chkSelected.setChecked(checklist.get(position).isSelected());
        holder.chkSelected.setTag(checklist.get(position));

        holder.chkSelected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                CheckBox cb = (CheckBox) v;
                CategoryModel contact = (CategoryModel) cb.getTag();

                contact.setSelected(cb.isChecked());
                checklist.get(pos).setSelected(cb.isChecked());
//                Toast.makeText(
//                        v.getContext(),
//                        "Clicked on Checkbox: " + cb.getText() + " is "
//                                + cb.isChecked(), Toast.LENGTH_LONG).show();
            }
        });


        holder.chkSelected.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    count++;
                }else {
                    count--;
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return checklist.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tvName;
        public CheckBox chkSelected;
        public CategoryModel model;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);

            this.setIsRecyclable(false);
            tvName = (TextView) itemView.findViewById(R.id.tvName);
            chkSelected = (CheckBox) itemView
                    .findViewById(R.id.chkSelected);
        }
    }

    public int totalCount(){
        return count;
    }
    public void setTotalCount(int value){
        count=value;
    }
    public List<CategoryModel> getStudentist() {
        return checklist;
    }
    public void clear() {
        int size = checklist.size();
        checklist.clear();
        notifyItemRangeRemoved(0, size);
    }

}
