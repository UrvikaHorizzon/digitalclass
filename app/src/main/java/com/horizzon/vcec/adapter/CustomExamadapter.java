package com.horizzon.vcec.adapter;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.fragments.AddExamFragment;
import com.horizzon.vcec.model.ExamDetails;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CustomExamadapter extends RecyclerView.Adapter<CustomExamadapter.MyViewHolder> {
    private Activity context;
    private List<ExamDetails> movieList;
    DialogLoader dialogLoader;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView tv_que, tv_opta, tv_optb, tv_optc, tv_optd, tv_desc;
        Button btn_edit, btn_del;


        public MyViewHolder(View view) {
            super(view);
            tv_que = view.findViewById(R.id.tv_que);
            tv_opta = view.findViewById(R.id.tv_opta);
            tv_optb = view.findViewById(R.id.tv_optb);
            tv_optc = view.findViewById(R.id.tv_optc);
            tv_optd = view.findViewById(R.id.tv_optd);
            tv_desc = view.findViewById(R.id.tv_desc);
            btn_edit = view.findViewById(R.id.btn_edit);
            btn_del = view.findViewById(R.id.btn_del);
        }
    }


    public CustomExamadapter(Activity context, List<ExamDetails> movieList) {
        this.context = context;
        this.movieList = movieList;
        dialogLoader = new DialogLoader(context);
    }

    @Override
    public CustomExamadapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.custom_exam_layout, parent, false);

        return new CustomExamadapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomExamadapter.MyViewHolder holder, final int position) {
        final ExamDetails movie = movieList.get(position);
        holder.tv_que.setText(movie.getQuestion());
        holder.tv_opta.setText(movie.getOption_a());
        holder.tv_optb.setText(movie.getOption_b());
        holder.tv_optc.setText(movie.getOption_c());
        holder.tv_optd.setText(movie.getOption_d());

        if(movie.getAnswer().equals("a")) {
            holder.tv_desc.setText("Option A");
        }else if(movie.getAnswer().equals("b")) {
            holder.tv_desc.setText("Option B");
        }else if(movie.getAnswer().equals("c")) {
            holder.tv_desc.setText("Option C");
        }else if(movie.getAnswer().equals("d")) {
            holder.tv_desc.setText("Option D");
        }

        holder.btn_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AddExamFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("QueId", movie);
                fragment.setArguments(bundle);
                FragmentTransaction transaction = ((HomeActivity) context).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        holder.btn_del.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogLoader.showProgressDialog();
                Call<ResponseModel> call = APIClient.getInstance()
                        .courseQueDelete(movie.getId());

                call.enqueue(new Callback<ResponseModel>() {
                    @Override
                    public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                        dialogLoader.hideProgressDialog();
                        if (response.isSuccessful()) {
                            if(response.body().getSuccess().equals("0") && response.body().getData().equals("questions Deleted successfully!") ) {

                                Toast.makeText(context, response.body().getData(), Toast.LENGTH_LONG).show();
                                movieList.remove(holder.getAdapterPosition());
                                notifyItemRemoved(holder.getAdapterPosition());
                                notifyItemRangeChanged(holder.getAdapterPosition(), movieList.size());
                            } else {
                                Toast.makeText(context, response.body().getData(), Toast.LENGTH_LONG).show();
                            }


                        } else {
                            Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                            // Show the Error Message
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseModel> call, Throwable t) {
                        dialogLoader.hideProgressDialog();
                        Toast.makeText(context,t.getMessage(), Toast.LENGTH_LONG).show();

                    }
                });
            }
        });
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

}
