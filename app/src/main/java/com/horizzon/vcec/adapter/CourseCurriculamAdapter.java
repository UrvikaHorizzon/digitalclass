package com.horizzon.vcec.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horizzon.vcec.R;
import com.horizzon.vcec.model.CourseCurriculamDetails;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class CourseCurriculamAdapter extends RecyclerView.Adapter<CourseCurriculamAdapter.MyViewHolder> {


    private ArrayList<CourseCurriculamDetails> courseCurriculamDetails;
    private LayoutInflater inflater;
    private Context context;


    public CourseCurriculamAdapter(Context context, ArrayList<CourseCurriculamDetails> courseCurriculamDetailsArrayList) {
        this.context = context;
        this.courseCurriculamDetails = courseCurriculamDetailsArrayList;
        inflater = LayoutInflater.from(context);
    }


    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_course_curriculam, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        final CourseCurriculamDetails movie = courseCurriculamDetails.get(position);
        holder.tv_title.setText(movie.getTitle());
        holder.tv_desc.setText(movie.getDescription());

    }

    @Override
    public int getItemCount() {
        return courseCurriculamDetails.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView tv_title, tv_desc;


        public MyViewHolder(@NonNull View view) {
            super(view);

            tv_title = view.findViewById(R.id.tv_title);
            tv_desc = view.findViewById(R.id.tv_desc);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {

        }
    }


}
