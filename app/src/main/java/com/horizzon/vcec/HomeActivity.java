package com.horizzon.vcec;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.horizzon.vcec.fragments.CategoryFragment;
import com.horizzon.vcec.fragments.FeatureFragment;
import com.horizzon.vcec.fragments.MyCourcesFragment;
import com.horizzon.vcec.fragments.MyProfileFragment;
import com.horizzon.vcec.fragments.ReferFragment;
import com.horizzon.vcec.fragments.TutoeSubscriptFragment;
import com.horizzon.vcec.fragments.TutorCourseFragment;
import com.horizzon.vcec.fragments.TutorDadhboardFragment;
import com.horizzon.vcec.fragments.WishlistFragment;
import com.horizzon.vcec.model.ProfileData;
import com.horizzon.vcec.model.ProfileDetails;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.AppUpdateDialog;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.ForceUpdateChecker;
import com.horizzon.vcec.util.MyAppPrefsManager;
import com.horizzon.vcec.util.VersionChecker;
import com.razorpay.PaymentResultListener;

import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutionException;

import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, PaymentResultListener, ForceUpdateChecker.OnUpdateNeededListener {


    NavigationView navigationView;
    private Boolean exit = false;
    Toolbar toolbar;
    Context context;
    View headerView;
    ImageView img_cart;
    TextView username;
    String str_usernme;

    ActionBarDrawerToggle toggle;
    DrawerLayout drawer;

    private MyAppPrefsManager myAppPrefsManager;
    String user_type = "s";
    private String customerID, txnid;
    private FirebaseAnalytics mFirebaseAnalytics;


    private SharedPreferences sharedpref;
    private FirebaseRemoteConfig mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
    private HashMap<String, Object> firebaseDefaultMap;
    public static final String VERSION_CODE_KEY = "latest_app_version";
    private static final String TAG = "HomeActivity";
    private String latestVersion;
    private String version;
    boolean isCancelable = true;
    private DialogLoader dialogLoader;
    /*private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            Fragment fragment;
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    if (!user_type.equals("s")) {
                        fragment = new TutorDadhboardFragment();
                        loadFragment(fragment);
                    } else {
                        fragment = new FeatureFragment();
                        loadFragment(fragment);
                    }
                    return true;
                case R.id.navigation_dashboard:
                    if (!user_type.equals("s")) {
                        fragment = new TutoeSubscriptFragment();
                        loadFragment(fragment);
                    } else {
                        fragment = new CategoryFragment();
                        loadFragment(fragment);
                    }
                    return true;
                case R.id.navigation_notifications:
                    if (Config.IS_USER_LOGGED_IN) {
                        if (!user_type.equals("s")) {
                            fragment = new TutorCourseFragment();
                            loadFragment(fragment);
                        } else {
                            fragment = new MyCourcesFragment();
                            loadFragment(fragment);
                        }
                    } else {
                        Intent intent = new Intent(HomeActivity.this, SelectRoleActivity.class);
                        startActivity(intent);
                    }
                    return true;
                case R.id.navigation_notifications1:
                    if (Config.IS_USER_LOGGED_IN) {
                        fragment = new WishlistFragment();
                        loadFragment(fragment);
                    } else {
                        Intent intent = new Intent(HomeActivity.this, SelectRoleActivity.class);
                        startActivity(intent);
                    }

                    return true;
                case R.id.navigation_notifications2:
                    fragment = new MyProfileFragment();
                    loadFragment(fragment);
                    return true;
            }
            return false;
        }
    };*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (!isConnected(HomeActivity.this)) buildDialog(HomeActivity.this).show();
        versionChecker();
        setContentView(R.layout.activity_home);
        context = HomeActivity.this;
        customerID = HomeActivity.this.getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        //    BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        //  navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
//        navigation.setSelectedItemId(0);
        dialogLoader = new DialogLoader(HomeActivity.this);
        sharedpref = getSharedPreferences("wallet", MODE_PRIVATE);
        myAppPrefsManager = new MyAppPrefsManager(this);
        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
        customerID = getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");
        if (Config.IS_USER_LOGGED_IN) {
            user_type = getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userType", "");
            if (!user_type.equals("s")) {
                //  Menu item = navigation.getMenu();
//                item.findItem(R.id.navigation_notifications1).setVisible(false);
                //               item.findItem(R.id.navigation_dashboard).setTitle("Subscriptions");
                //               item.findItem(R.id.navigation_notifications).setTitle("Cources");
                loadFragment(new TutorDadhboardFragment());
            } else {
                loadFragment(new FeatureFragment());
            }
        } else {
            loadFragment(new FeatureFragment());
        }

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.app_name);
        setSupportActionBar(toolbar);
        getSupportActionBar().setIcon(R.drawable.app_toolbar_icon);


        img_cart = (ImageView) findViewById(R.id.img_cart);
        img_cart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Config.IS_USER_LOGGED_IN) {
                    Fragment fragment = new WishlistFragment();
                    loadFragment(fragment);
                } else {
                    Intent intent = new Intent(HomeActivity.this, SelectRoleActivity.class);
                    startActivity(intent);
                }

            }
        });


        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);


        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int hour = cal.get(Calendar.HOUR_OF_DAY);

        headerView = navigationView.getHeaderView(0);
        username = headerView.findViewById(R.id.tv_username);
        String greeting = null;

        if (hour >= 12 && hour < 17) {
            greeting = "Good Afternoon";
        } else if (hour >= 17 && hour < 21) {
            greeting = "Good Evening";
        } else if (hour >= 21 && hour < 24) {
            greeting = "Good Night";
        } else {
            greeting = "Good Morning";
        }


        if (Config.IS_USER_LOGGED_IN) {
            getProfile(customerID);
            // username.setText(str_usernme);

        } else {
            username.setText("Hi There " + greeting);

        }

    }


    @Override
    public void onBackPressed() {
        Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.frame_container);
        if (fragment instanceof FeatureFragment) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SharedPreferences.Editor editor = sharedpref.edit();
                            editor.clear();
                            editor.commit();
                            finishAffinity();
                            System.exit(0);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        } else if (fragment instanceof TutorDadhboardFragment) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Are you sure you want to exit?")
                    .setCancelable(false)
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            SharedPreferences.Editor editor = sharedpref.edit();
                            editor.clear();
                            editor.commit();
                            finishAffinity();
                            System.exit(0);
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();

                        }
                    });
            AlertDialog alert = builder.create();
            alert.show();
        } else {
            super.onBackPressed();
        }
    }


    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Fragment fragment;

        if (id == R.id.nav_home) {


            if (!user_type.equals("s")) {
                fragment = new TutorDadhboardFragment();
                loadFragment(fragment);
            } else {
                fragment = new FeatureFragment();
                loadFragment(fragment);
            }


        } else if (id == R.id.nav_categories) {

            if (!user_type.equals("s")) {
                fragment = new TutoeSubscriptFragment();
                loadFragment(fragment);
            } else {
                fragment = new CategoryFragment();
                loadFragment(fragment);
            }


        } else if (id == R.id.nav_course) {

            if (Config.IS_USER_LOGGED_IN) {
                if (!user_type.equals("s")) {
                    fragment = new TutorCourseFragment();
                    loadFragment(fragment);
                } else {
                    fragment = new MyCourcesFragment();
                    loadFragment(fragment);
                }
            } else {
                Intent intent = new Intent(HomeActivity.this, SelectRoleActivity.class);
                startActivity(intent);
            }

        } else if (id == R.id.nav_cart) {

            if (Config.IS_USER_LOGGED_IN) {
                fragment = new WishlistFragment();
                loadFragment(fragment);
            } else {
                Intent intent = new Intent(HomeActivity.this, SelectRoleActivity.class);
                startActivity(intent);
            }

        } /*else if (id == R.id.nav_notification) {


        }*/ else if (id == R.id.nav_account) {

            if (Config.IS_USER_LOGGED_IN) {
                fragment = new MyProfileFragment();
                loadFragment(fragment);
            } else {
                Intent intent = new Intent(HomeActivity.this, SelectRoleActivity.class);
                startActivity(intent);
            }


        } else if (id == R.id.nav_earn) {

            if (Config.IS_USER_LOGGED_IN) {
                fragment = new ReferFragment();
                loadFragment(fragment);
            } else {
                Intent intent = new Intent(HomeActivity.this, SelectRoleActivity.class);
                startActivity(intent);
            }


        } else if (id == R.id.nav_become_tutor) {

            Intent tutor = new Intent(HomeActivity.this, BecomeTutor.class);
            startActivity(tutor);

        } else if (id == R.id.nav_help) {

            Intent help = new Intent(HomeActivity.this, HelpActivity.class);
            startActivity(help);

        } else if (id == R.id.nav_about) {

            Intent about = new Intent(HomeActivity.this, AboutActivity.class);
            startActivity(about);


        } else if (id == R.id.nav_rating) {

            try {
                Uri uri = Uri.parse("market://details?id=" + getPackageName() + "");
                Intent goMarket = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(goMarket);
            } catch (ActivityNotFoundException e) {
                Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=" + getPackageName() + "");
                Intent goMarket = new Intent(Intent.ACTION_VIEW, uri);
                startActivity(goMarket);
            }


        } else if (id == R.id.nav_share) {

            try {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Digital Class");
                String shareMessage = "Hey there i just download and join digital class and learning by expert on digital class \ndownload and join today with digital class share this to everyone so others can also take advantage\n\n";
                shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
            } catch (Exception e) {
                //e.toString();
            }

        } else if (id == R.id.nav_registration) {

            Intent about = new Intent(HomeActivity.this, SelectRoleActivity.class);
            startActivity(about);


        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    @Override
    public void onRestart() {
        super.onRestart();

    }

    public boolean isConnected(Context context) {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netinfo = cm.getActiveNetworkInfo();

        if (netinfo != null && netinfo.isConnectedOrConnecting()) {
            android.net.NetworkInfo wifi = cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
            android.net.NetworkInfo mobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

            if ((mobile != null && mobile.isConnectedOrConnecting()) || (wifi != null && wifi.isConnectedOrConnecting()))
                return true;
            else return false;
        } else
            return false;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Fragment fragment = (Fragment) getSupportFragmentManager().findFragmentByTag("cource_detail");
        if (fragment != null) {
            fragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onPaymentSuccess(String s) {
        try {
            Toast.makeText(getApplicationContext(), "Payment Successfully ", Toast.LENGTH_LONG).show();
            txnid = s;
            Log.d("RESPONSE_PY", s);
            buyNow(customerID);
        } catch (Exception e) {
            Log.e("PAYMENT", "Exception in onPaymentSuccess", e);
        }
    }

    @Override
    public void onPaymentError(int i, String s) {
        Toast.makeText(getApplicationContext(), "Payment failed", Toast.LENGTH_LONG).show();
    }

    private void buyNow(String customerID) {
        dialogLoader.showProgressDialog();
        Call<ResponseModel> call = APIClient.getInstance()
                .buyNow
                        (
                                customerID,
                                getSharedPreferences("UserInfo", MODE_PRIVATE).getString("cID", ""),
                                getSharedPreferences("UserInfo", MODE_PRIVATE).getString("cPrise", ""),
                                txnid
                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getData().equalsIgnoreCase("Your Tranzection successfully")) {
                        // Get the User Details from Response
                        Toast.makeText(getApplicationContext(), "Your Transaction successfully!", Toast.LENGTH_LONG).show();
                        finish();
                        startActivity(getIntent());
                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        // Get the Error Message from Response
                        Toast.makeText(getApplicationContext(), response.body().getData(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
//                Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();

            }
        });
    }


    public AlertDialog.Builder buildDialog(Context c) {

        AlertDialog.Builder builder = new AlertDialog.Builder(c);
        builder.setCancelable(false);
        builder.setTitle("No Internet Connection");
        builder.setMessage("You need to have Mobile Data or wifi to access this. Press ok to Exit");

        builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {

                finish();
            }
        });

        return builder;
    }


    @Override
    public void onUpdateNeeded(String updateUrl) {
        isCancelable = false;
        AppUpdateDialog appUpdateDialog = new AppUpdateDialog(HomeActivity.this, "New version available", getString(R.string.app_update_default_description), isCancelable);
        appUpdateDialog.setCancelable(false);
        appUpdateDialog.show();

        Window window = appUpdateDialog.getWindow();
        assert window != null;
        window.setLayout(ConstraintLayout.LayoutParams.MATCH_PARENT, ConstraintLayout.LayoutParams.WRAP_CONTENT);
    }

    private void redirectStore(String updateUrl) {
        final Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(updateUrl));
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private int getCurrentVersionCode() {
        try {
            return getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return -1;
    }

    public void versionChecker() {


        final String appPackageName = getApplicationContext().getPackageName();
        VersionChecker versionChecker = new VersionChecker(appPackageName);
        try {
            latestVersion = versionChecker.execute().get();
            Log.e("version", latestVersion);
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        PackageManager manager = getPackageManager();
        PackageInfo info = null;
        try {
            info = manager.getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        assert info != null;
        version = info.versionName;

        if (version.equals(latestVersion)) {


        } else {
            /*new AlertDialog.Builder(this).setTitle("Please Update the App")
                    .setMessage("A new version of this app is available. Please update it").setPositiveButton(
                    "OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            final String appPackageName = getApplicationContext().getPackageName();
                            try {
                                dialog.cancel();
                                redirectStore("market://details?id=" + appPackageName);
                            } catch (ActivityNotFoundException activityNotFoundException) {
                                dialog.cancel();
                                redirectStore("https://play.google.com/store/apps/details?id=" + appPackageName);
                            }
                        }
                    }).setCancelable(false).show();*/

            ForceUpdateChecker.with(this).onUpdateNeeded(this).check();
        }


    }


    private void getProfile(String customerID) {
        dialogLoader.showProgressDialog();
        Call<ProfileData> call = APIClient.getInstance()
                .getProfile(customerID);

        call.enqueue(new Callback<ProfileData>() {
            @Override
            public void onResponse(Call<ProfileData> call, Response<ProfileData> response) {
                dialogLoader.hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        List<ProfileDetails> profileDetailsList = response.body().getData();
                        Date date = new Date();
                        Calendar cal = Calendar.getInstance();
                        cal.setTime(date);
                        int hour = cal.get(Calendar.HOUR_OF_DAY);

                        String greeting = null;

                        if (hour >= 12 && hour < 17) {
                            greeting = " Good Afternoon";
                        } else if (hour >= 17 && hour < 21) {
                            greeting = " Good Evening";
                        } else if (hour >= 21 && hour < 24) {
                            greeting = " Good Night";
                        } else {
                            greeting = " Good Morning";
                        }

                        username.setText("Hi " + profileDetailsList.get(0).getName() + greeting);


                    } else {
                        Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                        // Show the Error Message
                        //     Toast.makeText(getActivity(), response.message(),Toast.LENGTH_LONG).show();

                    }
                }
            }

            @Override
            public void onFailure(Call<ProfileData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                // Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    public void setDrawerEnabled(boolean enabled) {
        int lockMode = enabled ? DrawerLayout.LOCK_MODE_UNLOCKED :
                DrawerLayout.LOCK_MODE_LOCKED_CLOSED;
        drawer.setDrawerLockMode(lockMode);
        toggle.setDrawerIndicatorEnabled(enabled);
    }

}

