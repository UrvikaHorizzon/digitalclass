package com.horizzon.vcec;

import android.content.Intent;
import android.os.Handler;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.widget.Toast;

import com.horizzon.vcec.fragments.FeatureFragment;
import com.horizzon.vcec.fragments.TutorDadhboardFragment;
import com.horizzon.vcec.util.MyAppPrefsManager;

public class SplashActivity extends AppCompatActivity {
    private static int SPLASH_TIME_OUT = 3000;
    MyAppPrefsManager myAppPrefsManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        getSupportActionBar().hide();
        myAppPrefsManager = new MyAppPrefsManager(this);
        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = new Intent(SplashActivity.this,HomeActivity.class);
                startActivity(i);
                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);


//        if (!myAppPrefsManager.isFirstTime() == true) {
//            new Handler().postDelayed(new Runnable() {
//
//                /*
//                 * Showing splash screen with a timer. This will be useful when you
//                 * want to show case your app logo / company
//                 */
//
//                @Override
//                public void run() {
//                    // This method will be executed once the timer is over
//                    // Start your app main activity
//                    Intent i = new Intent(SplashActivity.this,addvirtiseActivity.class);
//                    startActivity(i);
//                    // close this activity
//                    finish();
//                }
//            }, SPLASH_TIME_OUT);
//        } else {
//            myAppPrefsManager.setFristTime(true);
//            new Handler().postDelayed(new Runnable() {
//
//
//                /*
//                 * Showing splash screen with a timer. This will be useful when you
//                 * want to show case your app logo / company
//                 */
//
//                @Override
//                public void run() {
//                    // This method will be executed once the timer is over
//                    // Start your app main activity
//                    Intent i = new Intent(SplashActivity.this,HomeActivity.class);
//                    startActivity(i);
//                    // close this activity
//                    finish();
//                }
//            }, SPLASH_TIME_OUT);
//
//        }
//
//    }


    }

}
