package com.horizzon.vcec;

import android.content.Context;
import android.content.Intent;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.horizzon.vcec.fragments.CouceDetailFragment;
import com.horizzon.vcec.model.ExamData;
import com.horizzon.vcec.model.ExamDetails;
import com.horizzon.vcec.model.ResultDetails;
import com.horizzon.vcec.model.VideoDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ExamActivity extends AppCompatActivity {
    String video_id;
    List<ExamDetails> movieList;
    DialogLoader dialogLoader;
    TextView tv, tv_nofound;
    Button submitbutton, quitbutton, prevbutton;
    RadioGroup radio_g;
    RadioButton rb1, rb2, rb3, rb4;
    RelativeLayout ll_exam;
    int flag = 0;
    public static int marks = 0, correct = 0, wrong = 0;
    String ans;
    int selectedId;
    int c_flg = 0;
    int w_flg = 0;
    SharedPreferences preferences;
    public List<ResultDetails> resultDetailsList;
    public List<VideoDetails> movieVList;
    public static String course_id, v_type, course_name;
    public static String uriPth, videoName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exam);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        preferences = getSharedPreferences("exam", Context.MODE_PRIVATE);
        getSupportActionBar().setTitle("Exam");

        video_id = getIntent().getStringExtra("video_id");
        final TextView score = (TextView) findViewById(R.id.textView4);
        TextView textView = (TextView) findViewById(R.id.DispName);
        tv_nofound = (TextView) findViewById(R.id.tv_nofound);
        ll_exam = (RelativeLayout) findViewById(R.id.ll_exam);
        textView.setText("Hello " + getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userName", ""));
        submitbutton = (Button) findViewById(R.id.button3);
        prevbutton = (Button) findViewById(R.id.button4);
        quitbutton = (Button) findViewById(R.id.buttonquit);
        tv = (TextView) findViewById(R.id.tvque);

        radio_g = (RadioGroup) findViewById(R.id.answersgrp);
        rb1 = (RadioButton) findViewById(R.id.radioButton);
        rb2 = (RadioButton) findViewById(R.id.radioButton2);
        rb3 = (RadioButton) findViewById(R.id.radioButton3);
        rb4 = (RadioButton) findViewById(R.id.radioButton4);
        movieList = new ArrayList<>();
        dialogLoader = new DialogLoader(ExamActivity.this);

        quitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ResultActivity.class);
                intent.putExtra("video_id", video_id);
                startActivity(intent);
                finish();
            }
        });

        submitbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //int color = mBackgroundColor.getColor();
                //mLayout.setBackgroundColor(color);

                if (radio_g.getCheckedRadioButtonId() == -1) {
                    Toast.makeText(getApplicationContext(), "Please select one choice", Toast.LENGTH_SHORT).show();
                    return;
                }
                selectedId = radio_g.getCheckedRadioButtonId();

                ans = movieList.get(flag).getAnswer();

                if (ans.equals("a") && selectedId == R.id.radioButton) {
                    correct++;
                    c_flg = 1;
                    w_flg = 0;
                } else if (ans.equals("b") && selectedId == R.id.radioButton2) {
                    correct++;
                    c_flg = 1;
                    w_flg = 0;
                } else if (ans.equals("c") && selectedId == R.id.radioButton3) {
                    correct++;
                    c_flg = 1;
                    w_flg = 0;
                } else if (ans.equals("d") && selectedId == R.id.radioButton4) {
                    correct++;
                    c_flg = 1;
                    w_flg = 0;
                } else {
                    wrong++;
                    c_flg = 0;
                    w_flg = 1;
                }

                flag++;

                if (flag > 0) {
                    prevbutton.setVisibility(View.VISIBLE);
                }

                if (score != null)
                    score.setText("" + correct);

                if (flag < movieList.size()) {
                    tv.setText(movieList.get(flag).getQuestion());
                    rb1.setText(movieList.get(flag).getOption_a());
                    rb2.setText(movieList.get(flag).getOption_b());
                    rb3.setText(movieList.get(flag).getOption_c());
                    rb4.setText(movieList.get(flag).getOption_d());
                } else {
                    marks = correct;
                    Intent in = new Intent(getApplicationContext(), ResultActivity.class);
                    in.putExtra("video_id", video_id);
                    startActivity(in);
                    finish();
                }

                if (flag == movieList.size() - 1) {
                    submitbutton.setText("Finish");
                }
                radio_g.clearCheck();

            }
        });

        prevbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                flag--;
                if (flag != movieList.size() - 1) {
                    submitbutton.setText("Next Question");
                }
                tv.setText(movieList.get(flag).getQuestion());
                rb1.setText(movieList.get(flag).getOption_a());
                rb2.setText(movieList.get(flag).getOption_b());
                rb3.setText(movieList.get(flag).getOption_c());
                rb4.setText(movieList.get(flag).getOption_d());

                radio_g.check(selectedId);

                if (c_flg == 1) {
                    correct--;
                }

                if (w_flg == 1) {
                    wrong--;
                }

                if (score != null)
                    score.setText("" + correct);
            }
        });

        getExams(video_id);

        getData();
    }

    private void getData() {

        Gson gson = new Gson();

        v_type = preferences.getString("url", "");
        course_name = preferences.getString("course_name", "");
        uriPth = preferences.getString("videolink", "");
        videoName = preferences.getString("video_name", "");
        course_id = preferences.getString("course_id", "");

        String movie = preferences.getString("movie", null);
        String resut = preferences.getString("result", null);

        Type type = new TypeToken<List<VideoDetails>>() {
        }.getType();

        Type type1 = new TypeToken<List<ResultDetails>>() {
        }.getType();
        movieVList = gson.fromJson(movie, type);
        resultDetailsList = gson.fromJson(resut, type1);


    }

    private void getExams(String video_id) {
        dialogLoader.showProgressDialog();
        Call<ExamData> call = APIClient.getInstance()
                .getQuestionmList(video_id);

        call.enqueue(new Callback<ExamData>() {
            @Override
            public void onResponse(Call<ExamData> call, Response<ExamData> response) {
                dialogLoader.hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        // Get the User Details from Response
                        movieList = response.body().getMsg();
                        if (movieList.size() > 0) {
                            tv_nofound.setVisibility(View.GONE);
                            ll_exam.setVisibility(View.VISIBLE);
                            tv.setText(movieList.get(flag).getQuestion());
                            rb1.setText(movieList.get(flag).getOption_a());
                            rb2.setText(movieList.get(flag).getOption_b());
                            rb3.setText(movieList.get(flag).getOption_c());
                            rb4.setText(movieList.get(flag).getOption_d());
                        } else {
                            tv_nofound.setVisibility(View.VISIBLE);
                            ll_exam.setVisibility(View.GONE);
                        }
                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<ExamData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
//                Toast.makeText(getActivity(), "No data Found!!", Toast.LENGTH_LONG).show();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        startActivity(new Intent(this,HomeActivity.class));

        Intent intent = new Intent(ExamActivity.this, VideoListActivity.class);
        intent.putExtra("url", v_type);
        intent.putExtra("course_id", course_id);
        intent.putExtra("video_id", video_id);
        intent.putExtra("video_name", videoName);
        intent.putExtra("video", (Serializable) movieVList);
        intent.putExtra("videolink", uriPth);
        intent.putExtra("course_name", course_name);
        intent.putExtra("position", 0);
        intent.putExtra("result", (Serializable) resultDetailsList);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

}
