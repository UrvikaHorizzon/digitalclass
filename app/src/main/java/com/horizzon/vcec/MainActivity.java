package com.horizzon.vcec;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;
// import android.support.v4.app.ActivityCompat;
// import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;

import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.model.LoginData;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.MyAppPrefsManager;
import com.horizzon.vcec.util.ValidateInputs;
import com.scottyab.showhidepasswordedittext.ShowHidePasswordEditText;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    TextView tv_signup, tv_pwd;
    Button btn_signin;
    EditText et_email;
    ShowHidePasswordEditText et_pwd;
    DialogLoader dialogLoader;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    String imei_number = "", type = "";
    ImageView iv_close;

    @SuppressLint("MissingPermission")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();
        type = getIntent().getStringExtra("type");

        tv_signup = findViewById(R.id.tv_signup);
        tv_pwd = findViewById(R.id.tv_pwd);
        btn_signin = findViewById(R.id.btn_signin);
        et_pwd = findViewById(R.id.et_pwd);
        et_email = findViewById(R.id.et_email);
        iv_close = findViewById(R.id.iv_close);
        dialogLoader = new DialogLoader(this);

        imei_number = Settings.Secure.getString(
                MainActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.d("IMEI", imei_number);
        sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);

        type = getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userType", "");
//        }
        if (type.equals("s")) {
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                imei_number = Settings.Secure.getString(
                        MainActivity.this.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
                Log.d("IMEI", imei_number);
            }else {
                int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE);

                if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1111);
                } else {
                    //TODO
                    TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                    if (null != tm) {
                        imei_number = tm.getDeviceId();
                    }
                    if (null == imei_number || 0 == imei_number.length()) {
                        imei_number = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                        Log.d("IMEI", imei_number);
                    }
//        imei_number = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                    Log.d("IMEI", imei_number);
                }
            }

        }


        tv_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (type.equals("s")) {
                    Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
                    startActivity(intent);
                    if (sharedPreferences.getString("cFlg", "").equals("w")) {
                        finish();
                    }

                } else if (type.equals("t")) {
                    Intent intent = new Intent(MainActivity.this, TutorRegisterActivity.class);
                    startActivity(intent);
                }
            }
        });


        tv_pwd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ForgetPwdActivity.class);
                startActivity(intent);
            }
        });

        iv_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                finish();
//                Intent intent = new Intent(MainActivity.this, SelectRoleActivity.class);
//                startActivity(intent);

                MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(MainActivity.this);
                myAppPrefsManager.setUserLoggedIn(false);
                Intent i = new Intent(MainActivity.this, HomeActivity.class);
                startActivity(i);
            }
        });

        btn_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isValidData = validateInfoForm();
                if (isValidData) {
                    loginData();
                }

//                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
//                startActivity(intent);
//                finish();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1111:
                if ((grantResults.length > 0) && (grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    //TODO
                    TelephonyManager tm = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
                    if (null != tm) {
                        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return;
                        }
                        imei_number = tm.getDeviceId();
                    }
                    if (null == imei_number || 0 == imei_number.length()) {
                        imei_number = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID);
                    }
//        imei_number = Settings.Secure.getString(getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID);
                    Log.d("IMEI", imei_number);
                }
                break;

            default:
                break;
        }
    }

    private void loginData() {
        dialogLoader.showProgressDialog();

        Call<LoginData> call = APIClient.getInstance().loginUser(et_email.getText().toString().trim(), et_pwd.getText().toString().trim(), imei_number, imei_number, type);

        call.enqueue(new Callback<LoginData>() {
            @Override
            public void onResponse(Call<LoginData> call, Response<LoginData> response) {

                dialogLoader.hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response


                        // Save necessary details in SharedPrefs
                        editor = sharedPreferences.edit();
                        editor.putString("userID", response.body().getData().get(0).getId());
                        editor.putString("userName", response.body().getData().get(0).getName());
                        editor.putString("userMail", response.body().getData().get(0).getEmail());
                        try {
                            editor.putString("userMobile", response.body().getData().get(0).getMobile_no());
                        } catch (Exception e) {
                            editor.putString("userMobile", response.body().getData().get(0).getContact());
                        }
                        editor.putString("userType", type);
                        editor.putBoolean("isLogged_in", true);

                        int status = Integer.parseInt(response.body().getData().get(0).getStatus());

                        String type_is = response.body().getUser_type();
                        // Set UserLoggedIn in MyAppPrefsManager
                        MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(MainActivity.this);


                        // Set isLogged_in of ConstantValues
                        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
                        if (sharedPreferences.getString("cFlg", "").equals("w")) {
                            editor.putString("cFlg", "");
                            editor.apply();
                            finish();
                        } else {
                            if (type.equalsIgnoreCase("s")) {
                                myAppPrefsManager.setUserLoggedIn(true);
                                editor.putString("userID", response.body().getData().get(0).getId());
                                editor.putString("userName", response.body().getData().get(0).getName());
                                editor.putString("userMail", response.body().getData().get(0).getEmail());
                                try {
                                    editor.putString("userMobile", response.body().getData().get(0).getMobile_no());
                                } catch (Exception e) {
                                    editor.putString("userMobile", response.body().getData().get(0).getContact());
                                }
                                editor.putString("userType", type);
                                editor.putBoolean("isLogged_in",true);
                                editor.apply();
                                myAppPrefsManager.setFristTime(true);
                                String message = response.body().getMsg();
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                                Intent intent = new Intent(MainActivity.this, HomeActivity.class);
                                startActivity(intent);
                                finish();
                            }

                            if (type.equalsIgnoreCase("t") && status == 0) {
                                editor.putString("userType", type);
                                editor.putBoolean("isLogged_in", false);
                                editor.apply();
                                myAppPrefsManager.setFristTime(true);
                                myAppPrefsManager.setUserLoggedIn(false);
                                Toast.makeText(MainActivity.this, "Once Admin Approve your Account you will able to upload course from your account. You will notify from SMS.", Toast.LENGTH_LONG).show();
                            } else {

                                editor.putString("userID", response.body().getData().get(0).getId());
                                editor.putString("userName", response.body().getData().get(0).getName());
                                editor.putString("userMail", response.body().getData().get(0).getEmail());
                                try {
                                    editor.putString("userMobile", response.body().getData().get(0).getMobile_no());
                                } catch (Exception e) {
                                    editor.putString("userMobile", response.body().getData().get(0).getContact());
                                }
                                editor.putString("userType", type);
                                editor.putBoolean("isLogged_in", true);
                                editor.apply();
                                myAppPrefsManager.setUserLoggedIn(true);
                                myAppPrefsManager.setFristTime(true);
                                String message = response.body().getMsg();
                                Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                                Intent t = new Intent(MainActivity.this, HomeActivity.class);
                                startActivity(t);
                                finish();
                            }
                        }
                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        // Get the Error Message from Response
                        String message = response.body().getMsg();
                        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<LoginData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean validateInfoForm() {
        if (!ValidateInputs.isValidEmail(et_email.getText().toString().trim())) {
            et_email.setError("Please Enter Valid Email");
            return false;
        } else if (!ValidateInputs.isValidPassword(et_pwd.getText().toString().trim())) {
            et_pwd.setError("Please Enter Valid Password");
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(MainActivity.this);
        myAppPrefsManager.setUserLoggedIn(false);
        Intent i = new Intent(MainActivity.this, SelectRoleActivity.class);
        startActivity(i);
    }

    @Override
    protected void onDestroy() {
        dialogLoader.hideProgressDialog();
        super.onDestroy();
    }

    public static String getDeviceId(Context context) {

        String deviceId;

        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            deviceId = Settings.Secure.getString(
                    context.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        } else {
            final TelephonyManager mTelephony = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
//                return TODO;
            }
            if (mTelephony.getDeviceId() != null) {
                deviceId = mTelephony.getDeviceId();
            } else {
                deviceId = Settings.Secure.getString(
                        context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
            }
        }

        return deviceId;
    }
}
