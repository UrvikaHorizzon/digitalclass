package com.horizzon.vcec;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.horizzon.vcec.R;

public class AboutActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

    }
    @Override
    public boolean onSupportNavigateUp() {

        super.onBackPressed();
        return true;

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

    }
}