package com.horizzon.vcec;

import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;
import android.widget.VideoView;

import com.horizzon.vcec.util.FullScreenMediaController;
import com.horizzon.vcec.util.VideoViewCustom;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;
//import com.universalvideoview.UniversalMediaController;
//import com.universalvideoview.UniversalVideoView;


public class FullVideoActivity extends AppCompatActivity implements UniversalVideoView.VideoViewCallback {

    private static final String TAG = "Full Video Activity";
    private static final String SEEK_POSITION_KEY = "SEEK_POSITION_KEY";
//    private static final String VIDEO_URL = "http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4";

        UniversalVideoView mVideoView;
    UniversalMediaController mMediaController;
    Toolbar toolbar;
    View mVideoLayout;
    Uri videoUri;
    private int mSeekPosition;
    private int cachedHeight;
    private boolean isFullscreen;
    private boolean firsttime = false;
    String uriPath;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_video);
        getSupportActionBar().hide();


        videoUri = Uri.parse("https://digitalclassworld.com/storage/app/course_video/QX73OkdD1w0hRAK8I9QUU01uzhDRd2mg3GHXWFhq.mp4");

        mVideoLayout = findViewById(R.id.video_layout);
        mVideoView = findViewById(R.id.videoView);
        mMediaController = (UniversalMediaController) findViewById(R.id.media_controller);
        mVideoView.setMediaController(mMediaController);
        mVideoView.setVideoURI(videoUri);
        mVideoView.setVideoViewCallback(this);
        setVideoAreaSize();

        if(getIntent().getStringExtra("url")!=null) {
            uriPath = getIntent().getStringExtra("url");

            Log.e("vidoe_url",uriPath);
            Uri videoUri = Uri.parse(uriPath);

            mVideoView.setVideoURI(videoUri);

            mVideoView.start();
            if (mSeekPosition > 0) {
                mVideoView.seekTo(mSeekPosition);
                mVideoView.start();
            }
        }


    }

    private void setVideoAreaSize() {
        mVideoLayout.post(new Runnable() {
            @Override
            public void run() {
                int width = mVideoLayout.getWidth();
                cachedHeight = (int) (width * 405f / 720f);
//                cachedHeight = (int) (width * 3f / 4f);
//                cachedHeight = (int) (width * 9f / 16f);
                ViewGroup.LayoutParams videoLayoutParams = mVideoLayout.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
                mVideoLayout.setLayoutParams(videoLayoutParams);
                mVideoView.setVideoPath(uriPath);
                mVideoView.requestFocus();
            }
        });


    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
//        Log.d(TAG, "onSaveInstanceState Position=" + mVideoView.getCurrentPosition());
//        outState.putInt(SEEK_POSITION_KEY, mSeekPosition);
    }

    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        super.onRestoreInstanceState(outState);
        mSeekPosition = outState.getInt(SEEK_POSITION_KEY);
        Log.d(TAG, "onRestoreInstanceState Position=" + mSeekPosition);
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG, "onPause ");
//        if (mVideoView != null && mVideoView.isPlaying()) {
//            mSeekPosition = mVideoView.getCurrentPosition();
//            Log.d(TAG, "onPause mSeekPosition=" + mSeekPosition);
//            mVideoView.pause();
//        }
    }


    @Override
    public void onScaleChange(boolean isFullscreen) {
        this.isFullscreen = isFullscreen;
        if (isFullscreen) {
            ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoLayout.setLayoutParams(layoutParams);
            switchTitleBar(false);

        } else {
            ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.height = this.cachedHeight;
            mVideoLayout.setLayoutParams(layoutParams);
            switchTitleBar(true);

        }


    }

    private void switchTitleBar(boolean show) {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            if (show) {
                supportActionBar.show();
            } else {
                supportActionBar.hide();
            }
        }

    }



@Override
public void onPause(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onPause UniversalVideoView callback");
        }

@Override
public void onStart(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onStart UniversalVideoView callback");
        }

@Override
public void onBufferingStart(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onBufferingStart UniversalVideoView callback");
        }

@Override
public void onBufferingEnd(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onBufferingEnd UniversalVideoView callback");
        }

@Override
public void onBackPressed() {
        if (this.isFullscreen) {
//            mVideoView.setFullscreen(false);
        } else {
        super.onBackPressed();
        }
        }


@Override
public void onPointerCaptureChanged(boolean hasCapture) {

        }
        }
