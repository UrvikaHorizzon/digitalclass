package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class StudentDetails {
    @SerializedName("trazection_id")
    @Expose
    private String trazection_id;
    @SerializedName("prise")
    @Expose
    private String prise;
    @SerializedName("Created_date")
    @Expose
    private String created_date;
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("srno")
    @Expose
    private String srno;

    public String getSrno() {
        return srno;
    }

    public void setSrno(String srno) {
        this.srno = srno;
    }

    public String getTrazection_id() {
        return trazection_id;
    }

    public void setTrazection_id(String trazection_id) {
        this.trazection_id = trazection_id;
    }

    public String getPrise() {
        return prise;
    }

    public void setPrise(String prise) {
        this.prise = prise;
    }

    public String getCreated_date() {
        return created_date;
    }

    public void setCreated_date(String created_date) {
        this.created_date = created_date;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
