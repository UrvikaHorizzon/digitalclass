package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TutorProfileDetails {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("branch_code")
    @Expose
    private String branch_code;
    @SerializedName("country")
    @Expose
    private String country;
    @SerializedName("state")
    @Expose
    private String state;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("manager_name")
    @Expose
    private String manager_name;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("franchisee_type")
    @Expose
    private String franchisee_type;

    @SerializedName("firm_type")
    @Expose
    private String firm_type;
    @SerializedName("owneraddress")
    @Expose
    private String owneraddress;

    @SerializedName("centerhead")
    @Expose
    private String centerhead;
    @SerializedName("contact")
    @Expose
    private String contact;
    @SerializedName("telephone")
    @Expose
    private String telephone;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("carpet_area")
    @Expose
    private String carpet_area;
    @SerializedName("system_number")
    @Expose
    private String system_number;

    @SerializedName("air_conditions")
    @Expose
    private String air_conditions;
    @SerializedName("admin_office")
    @Expose
    private String admin_office;
    @SerializedName("lab")
    @Expose
    private String lab;
    @SerializedName("faculty_cabin")
    @Expose
    private String faculty_cabin;
 @SerializedName("center_other_details")
    @Expose
    private String center_other_details;
    @SerializedName("bankname")
    @Expose
    private String bankname;
 @SerializedName("acname")
    @Expose
    private String acname;
    @SerializedName("acno")
    @Expose
    private String acno;
 @SerializedName("ifsc")
    @Expose
    private String ifsc;
    @SerializedName("panno")
    @Expose
    private String panno;
    @SerializedName("package")
    @Expose
    private String package1;
    @SerializedName("fcm_id")
    @Expose
    private String fcm_id;
    @SerializedName("referal")
    @Expose
    private String referalcode;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBranch_code() {
        return branch_code;
    }

    public void setBranch_code(String branch_code) {
        this.branch_code = branch_code;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getManager_name() {
        return manager_name;
    }

    public void setManager_name(String manager_name) {
        this.manager_name = manager_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getFranchisee_type() {
        return franchisee_type;
    }

    public void setFranchisee_type(String franchisee_type) {
        this.franchisee_type = franchisee_type;
    }

    public String getFirm_type() {
        return firm_type;
    }

    public void setFirm_type(String firm_type) {
        this.firm_type = firm_type;
    }

    public String getOwneraddress() {
        return owneraddress;
    }

    public void setOwneraddress(String owneraddress) {
        this.owneraddress = owneraddress;
    }

    public String getCenterhead() {
        return centerhead;
    }

    public void setCenterhead(String centerhead) {
        this.centerhead = centerhead;
    }

    public String getContact() {
        return contact;
    }

    public void setContact(String contact) {
        this.contact = contact;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getCarpet_area() {
        return carpet_area;
    }

    public void setCarpet_area(String carpet_area) {
        this.carpet_area = carpet_area;
    }

    public String getSystem_number() {
        return system_number;
    }

    public void setSystem_number(String system_number) {
        this.system_number = system_number;
    }

    public String getAir_conditions() {
        return air_conditions;
    }

    public void setAir_conditions(String air_conditions) {
        this.air_conditions = air_conditions;
    }

    public String getAdmin_office() {
        return admin_office;
    }

    public void setAdmin_office(String admin_office) {
        this.admin_office = admin_office;
    }

    public String getLab() {
        return lab;
    }

    public void setLab(String lab) {
        this.lab = lab;
    }

    public String getFaculty_cabin() {
        return faculty_cabin;
    }

    public void setFaculty_cabin(String faculty_cabin) {
        this.faculty_cabin = faculty_cabin;
    }

    public String getCenter_other_details() {
        return center_other_details;
    }

    public void setCenter_other_details(String center_other_details) {
        this.center_other_details = center_other_details;
    }

    public String getBankname() {
        return bankname;
    }

    public void setBankname(String bankname) {
        this.bankname = bankname;
    }

    public String getAcname() {
        return acname;
    }

    public void setAcname(String acname) {
        this.acname = acname;
    }

    public String getAcno() {
        return acno;
    }

    public void setAcno(String acno) {
        this.acno = acno;
    }

    public String getIfsc() {
        return ifsc;
    }

    public void setIfsc(String ifsc) {
        this.ifsc = ifsc;
    }

    public String getPanno() {
        return panno;
    }

    public void setPanno(String panno) {
        this.panno = panno;
    }

    public String getPackage1() {
        return package1;
    }

    public void setPackage1(String package1) {
        this.package1 = package1;
    }

    public String getFcm_id() {
        return fcm_id;
    }

    public void setFcm_id(String fcm_id) {
        this.fcm_id = fcm_id;
    }

    public String getReferalcode() {
        return referalcode;
    }

    public void setReferalcode(String referalcode) {
        this.referalcode = referalcode;
    }
}
