package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class BannerData {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private List<BannerDetails> data;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<BannerDetails> getData() {
        return data;
    }

    public void setData(List<BannerDetails> data) {
        this.data = data;
    }
}
