package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CourseCurriculam {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private List<CourseCurriculamDetails> data;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<CourseCurriculamDetails> getData() {
        return data;
    }

    public void setData(List<CourseCurriculamDetails> data) {
        this.data = data;
    }
}
