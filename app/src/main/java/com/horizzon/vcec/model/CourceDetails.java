package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class CourceDetails implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("main_category_id")
    @Expose
    private String main_category_id;
    @SerializedName("parent_id")
    @Expose
    private String parent_id;
    @SerializedName("courseid")
    @Expose
    private String course_id;
    @SerializedName("sub_category_id")
    @Expose
    private String sub_category_id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("course_name")
    @Expose
    private String course_name;
    @SerializedName("wish_list")
    @Expose
    private String widh_list="no";
    @SerializedName("buy_now")
    @Expose
    private String buy_now="no";
    @SerializedName("price")
    @Expose
    private String price;
    @SerializedName("actual_price")
    @Expose
    private String actual_price;
    @SerializedName("fee_type")
    @Expose
    private String fee_type;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("pdf_file")
    @Expose
    private String pdf_file;
    @SerializedName("language")
    @Expose
    private String language;
    @SerializedName("hours")
    @Expose
    private String hours;
    @SerializedName("certificated")
    @Expose
    private String certificated;
    @SerializedName("month")
    @Expose
    private String month;
    @SerializedName("video")
    @Expose
    private String video;
    @SerializedName("chapter")
    @Expose
    private String chapter;
    @SerializedName("rating")
    @Expose
    private String rating;
    @SerializedName("rating_value")
    @Expose
    private String rating_value;

    @SerializedName("course_image_content")
    @Expose
    private String content_image;

    @SerializedName("course_id")
    @Expose
    private String corses_id;

    @SerializedName("coursess")
    @Expose
    private List<CourceDetails> coursess = null;



    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMain_category_id() {
        return main_category_id;
    }

    public void setMain_category_id(String main_category_id) {
        this.main_category_id = main_category_id;
    }

    public String getParent_id() {
        return parent_id;
    }

    public void setParent_id(String parent_id) {
        this.parent_id = parent_id;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getSub_category_id() {
        return sub_category_id;
    }

    public void setSub_category_id(String sub_category_id) {
        this.sub_category_id = sub_category_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getWidh_list() {
        return widh_list;
    }

    public void setWidh_list(String widh_list) {
        this.widh_list = widh_list;
    }

    public String getBuy_now() {
        return buy_now;
    }

    public void setBuy_now(String buy_now) {
        this.buy_now = buy_now;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getActual_price() {
        return actual_price;
    }

    public void setActual_price(String actual_price) {
        this.actual_price = actual_price;
    }

    public String getFee_type() {
        return fee_type;
    }

    public void setFee_type(String fee_type) {
        this.fee_type = fee_type;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPdf_file() {
        return pdf_file;
    }

    public void setPdf_file(String pdf_file) {
        this.pdf_file = pdf_file;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getCertificated() {
        return certificated;
    }

    public void setCertificated(String certificated) {
        this.certificated = certificated;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getChapter() {
        return chapter;
    }

    public void setChapter(String chapter) {
        this.chapter = chapter;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRating_value() {
        return rating_value;
    }

    public void setRating_value(String rating_value) {
        this.rating_value = rating_value;
    }

    public String getContent_image() {
        return content_image;
    }

    public void setContent_image(String content_image) {
        this.content_image = content_image;
    }

    public String getCorses_id() {
        return corses_id;
    }

    public void setCorses_id(String corses_id) {
        this.corses_id = corses_id;
    }

    public List<CourceDetails> getCoursess() {
        return coursess;
    }

    public void setCoursess(List<CourceDetails> coursess) {
        this.coursess = coursess;
    }




}
