package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ResponseModel {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private String data;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("msg")
    @Expose
    private String msg;



    @SerializedName("user_id")
    @Expose
    private OtpModel user_id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

 public OtpModel getUserId() {
        return user_id;
    }

    public void setUserId(OtpModel user_id) {
        this.user_id = user_id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
}
