package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DemoModel {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("msg")
    @Expose
    private List<DemoDetails> msg = null;
    @SerializedName("data")
    @Expose
    private List<CommissionModel> data = null;



    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<DemoDetails> getMsg() {
        return msg;
    }

    public void setMsg(List<DemoDetails> msg) {
        this.msg = msg;
    }

    public List<CommissionModel> getData() {
        return data;
    }

    public void setData(List<CommissionModel> data) {
        this.data = data;
    }






}
