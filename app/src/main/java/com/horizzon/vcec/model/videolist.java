package com.horizzon.vcec.model;

public class videolist {

    private String name;
    private String price;
    private String discount_price;
    private String discount;
    private String course_by;
    private String video_url;
    private String thumbnail_url;

    public videolist() {
    }

    public videolist(String name, String price, String discount_price, String discount, String course_by, String video_url, String thumbnail_url) {
        this.name = name;
        this.price = price;
        this.discount_price = discount_price;
        this.discount = discount;
        this.course_by = course_by;
        this.video_url = video_url;
        this.thumbnail_url = thumbnail_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getDiscount_price() {
        return discount_price;
    }

    public void setDiscount_price(String discount_price) {
        this.discount_price = discount_price;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getCourse_by() {
        return course_by;
    }

    public void setCourse_by(String course_by) {
        this.course_by = course_by;
    }

    public String getVideo_url() {
        return video_url;
    }

    public void setVideo_url(String video_url) {
        this.video_url = video_url;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }
}
