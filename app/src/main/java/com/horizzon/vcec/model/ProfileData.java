package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProfileData {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private List<ProfileDetails> data;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<ProfileDetails> getData() {
        return data;
    }

    public void setData(List<ProfileDetails> data) {
        this.data = data;
    }
}
