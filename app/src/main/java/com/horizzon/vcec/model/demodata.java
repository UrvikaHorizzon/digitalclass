package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class demodata {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("course_name")
    @Expose
    private String course_name;
    @SerializedName("view_video")
    @Expose
    private String view_video;
    @SerializedName("msg")
    @Expose
    private List<DemoDetails> msg;
    @SerializedName("result")
    @Expose
    private List<ResultDetails> result;

    public List<ResultDetails> getResult() {
        return result;
    }

    public void setResult(List<ResultDetails> result) {
        this.result = result;
    }

    public String getView_video() {
        return view_video;
    }

    public void setView_video(String view_video) {
        this.view_video = view_video;
    }

    public String getCourse_name() {
        return course_name;
    }

    public void setCourse_name(String course_name) {
        this.course_name = course_name;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<DemoDetails> getMsg() {
        return msg;
    }

    public void setMsg(List<DemoDetails> msg) {
        this.msg = msg;
    }
}
