package com.horizzon.vcec.model;

public class CountVideo {
    int id;
    int course_id;
    int count;
    int total_count;
    String view_video;



    public CountVideo(int course_id, int total_count) {
        this.course_id = course_id;
        this.total_count = total_count;
    }


    public CountVideo(int course_id, int count, String view_video) {
        this.course_id = course_id;
        this.count = count;
        this.view_video = view_video;
    }

    public CountVideo() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCourse_id() {
        return course_id;
    }

    public void setCourse_id(int course_id) {
        this.course_id = course_id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotal_count() {
        return total_count;
    }

    public void setTotal_count(int total_count) {
        this.total_count = total_count;
    }

    public String getView_video() {
        return view_video;
    }

    public void setView_video(String view_video) {
        this.view_video = view_video;
    }


}
