package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class
ResultDetails implements Serializable {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("user_id")
    @Expose
    private String user_id;
    @SerializedName("video_id")
    @Expose
    private String video_id;
    @SerializedName("correct_ans")
    @Expose
    private String correct_ans;
    @SerializedName("wrong_ans")
    @Expose
    private String wrong_ans;
    @SerializedName("Status")
    @Expose
    private String Status;
    @SerializedName("exam_date")
    @Expose
    private String exam_date;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }

    public String getCorrect_ans() {
        return correct_ans;
    }

    public void setCorrect_ans(String correct_ans) {
        this.correct_ans = correct_ans;
    }

    public String getWrong_ans() {
        return wrong_ans;
    }

    public void setWrong_ans(String wrong_ans) {
        this.wrong_ans = wrong_ans;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getExam_date() {
        return exam_date;
    }

    public void setExam_date(String exam_date) {
        this.exam_date = exam_date;
    }
}
