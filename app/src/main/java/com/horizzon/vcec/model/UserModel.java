package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserModel {
    @SerializedName("info")
    @Expose
    private UserDetails info;

    public UserDetails getInfo() {
        return info;
    }

    public void setInfo(UserDetails info) {
        this.info = info;
    }
}
