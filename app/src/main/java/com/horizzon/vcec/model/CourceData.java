package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CourceData {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private List<CourceDetails> data;
    @SerializedName("demovideo")
    @Expose
    private List<Object> demovideo = null;
    @SerializedName("coursess")
    @Expose
    private List<CourceDetails> coursess = null;

    public List<CourceDetails> getCoursess() {
        return coursess;
    }

    public void setCoursess(List<CourceDetails> coursess) {
        this.coursess = coursess;
    }

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<CourceDetails> getData() {
        return data;
    }

    public void setData(List<CourceDetails> data) {
        this.data = data;
    }
}
