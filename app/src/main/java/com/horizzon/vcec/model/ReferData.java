package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReferData {
    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("data")
    @Expose
    private List<ProfileDetails> data;
    @SerializedName("referal_user")
    @Expose
    private List<UserDetails> referal_user;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public List<ProfileDetails> getData() {
        return data;
    }

    public void setData(List<ProfileDetails> data) {
        this.data = data;
    }

    public List<UserDetails> getReferal_user() {
        return referal_user;
    }

    public void setReferal_user(List<UserDetails> referal_user) {
        this.referal_user = referal_user;
    }
}
