package com.horizzon.vcec.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileDetails {
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("roll_number")
    @Expose
    private String roll_number;
    @SerializedName("user_type")
    @Expose
    private String user_type;
    @SerializedName("payment_status")
    @Expose
    private String payment_status;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("refer_persantage")
    @Expose
    private String refer_persantage;
    @SerializedName("promation")
    @Expose
    private String promation;
    @SerializedName("mobile_no")
    @Expose
    private String mobile_no;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("city_id")
    @Expose
    private String city_id;

    @SerializedName("state_id")
    @Expose
    private String state_id;
    @SerializedName("country_id")
    @Expose
    private String country_id;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("password")
    @Expose
    private String password;
    @SerializedName("profile_picture")
    @Expose
    private String profile_picture;
    @SerializedName("isBranchStudent")
    @Expose
    private String isBranchStudent;

    @SerializedName("fcm_id")
    @Expose
    private String fcm_id;
    @SerializedName("imai_no")
    @Expose
    private String imai_no;
    @SerializedName("referal")
    @Expose
    private String referalcode;

    public String getRefer_persantage() {
        return refer_persantage;
    }

    public void setRefer_persantage(String refer_persantage) {
        this.refer_persantage = refer_persantage;
    }

    public String getPromation() {
        return promation;
    }

    public void setPromation(String promation) {
        this.promation = promation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRoll_number() {
        return roll_number;
    }

    public void setRoll_number(String roll_number) {
        this.roll_number = roll_number;
    }

    public String getUser_type() {
        return user_type;
    }

    public void setUser_type(String user_type) {
        this.user_type = user_type;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMobile_no() {
        return mobile_no;
    }

    public void setMobile_no(String mobile_no) {
        this.mobile_no = mobile_no;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }

    public String getState_id() {
        return state_id;
    }

    public void setState_id(String state_id) {
        this.state_id = state_id;
    }

    public String getCountry_id() {
        return country_id;
    }

    public void setCountry_id(String country_id) {
        this.country_id = country_id;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getIsBranchStudent() {
        return isBranchStudent;
    }

    public void setIsBranchStudent(String isBranchStudent) {
        this.isBranchStudent = isBranchStudent;
    }

    public String getFcm_id() {
        return fcm_id;
    }

    public void setFcm_id(String fcm_id) {
        this.fcm_id = fcm_id;
    }

    public String getImai_no() {
        return imai_no;
    }

    public void setImai_no(String imai_no) {
        this.imai_no = imai_no;
    }

    public String getReferalcode() {
        return referalcode;
    }
    public void setReferalcode(String referalcode) {
        this.referalcode = referalcode;
    }
}
