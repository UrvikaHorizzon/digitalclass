package com.horizzon.vcec;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.horizzon.vcec.util.MyAppPrefsManager;

import androidx.appcompat.app.AppCompatActivity;
import de.hdodenhof.circleimageview.CircleImageView;

public class SelectRoleActivity extends AppCompatActivity {
    CircleImageView iv_user, iv_vendor;
    ImageView iv_back;
    private SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    TextView tv_tutor_message, tv_student_message, student;
    String become_tutor;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_role);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {

            become_tutor = extras.getString("become_tutor");

        } else {

            become_tutor = "student";

        }


        getSupportActionBar().hide();

       // iv_back = findViewById(R.id.iv_back);
        iv_user = findViewById(R.id.iv_user);
        iv_vendor = findViewById(R.id.iv_vendor);
        student = findViewById(R.id.student);
        tv_student_message = findViewById(R.id.tv_student_message);
        tv_tutor_message = findViewById(R.id.tv_tutor_message);


        if (become_tutor.equals("tutor")) {

            iv_user.setVisibility(View.GONE);
            tv_student_message.setVisibility(View.GONE);
            student.setVisibility(View.GONE);

        } else {

            iv_user.setVisibility(View.VISIBLE);
            tv_student_message.setVisibility(View.VISIBLE);
            student.setVisibility(View.VISIBLE);
        }


        sharedPreferences = getSharedPreferences("UserInfo", MODE_PRIVATE);

        /*iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });*/

        iv_user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor = sharedPreferences.edit();
                editor.putString("userType", "s");
                editor.apply();
                Intent intent = new Intent(SelectRoleActivity.this, MainActivity.class);
                intent.putExtra("type", "s");
                startActivity(intent);
                if (sharedPreferences.getString("cFlg", "").equals("w")) {
                    finish();
                }

            }
        });

        iv_vendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor = sharedPreferences.edit();
                editor.putString("userType", "t");
                editor.apply();
                Intent intent = new Intent(SelectRoleActivity.this, MainActivity.class);
                intent.putExtra("type", "t");
                startActivity(intent);
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        MyAppPrefsManager myAppPrefsManager = new MyAppPrefsManager(SelectRoleActivity.this);
        myAppPrefsManager.setUserLoggedIn(false);
        Intent i = new Intent(SelectRoleActivity.this, HomeActivity.class);
        startActivity(i);
    }
}
