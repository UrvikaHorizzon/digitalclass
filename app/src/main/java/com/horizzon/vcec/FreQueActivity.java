package com.horizzon.vcec;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.horizzon.vcec.adapter.FaqListadapter;
import com.horizzon.vcec.model.FaQData;
import com.horizzon.vcec.model.FaQDetails;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.model.ResultDetails;
import com.horizzon.vcec.model.VideoDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;

import java.io.Serializable;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FreQueActivity extends AppCompatActivity {
    RecyclerView rv_categories;
    List<FaQDetails> movieList;
    private FaqListadapter mAdapter;
    DialogLoader dialogLoader;
    private TextView tv_nofound;
    private String video_id, customerID;
    EditText et_mobile;
    ImageButton btn_login;
    SharedPreferences preferences;
    public List<ResultDetails> resultDetailsList;
    public List<VideoDetails> movieVList;
    public static String course_id, v_type, course_name;
    public static String uriPth, videoName;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fre_que);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        preferences = getSharedPreferences("exam", Context.MODE_PRIVATE);
        getSupportActionBar().setTitle("FAQs");
        rv_categories = findViewById(R.id.rv_categories);
        tv_nofound = findViewById(R.id.tv_nofound);
        et_mobile = findViewById(R.id.et_mobile);
        btn_login = findViewById(R.id.btn_login);
        movieList = new ArrayList<>();
        dialogLoader = new DialogLoader(FreQueActivity.this);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, true);
        layoutManager1.setReverseLayout(false);
        rv_categories.setLayoutManager(layoutManager1);

//        movieList = (List<CategoryModel>) getArguments().getSerializable("CategoryList");


        video_id = getIntent().getStringExtra("video_id");
        customerID = getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");
        getCategories();
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isValidData = validateInfoForm();
                if (isValidData) {
                    registerData();
                }
            }
        });

        getData();
    }

    private void getCategories() {
        dialogLoader.showProgressDialog();
        Call<FaQData> call = APIClient.getInstance()
                .getFqList(customerID, video_id);

        call.enqueue(new Callback<FaQData>() {
            @Override
            public void onResponse(Call<FaQData> call, Response<FaQData> response) {
                dialogLoader.hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("0"))
                    {
                        // Get the User Details from Response
                        movieList = response.body().getData();

                        if(movieList.size() > 0) {
                            tv_nofound.setVisibility(View.GONE);
                            rv_categories.setVisibility(View.VISIBLE);
                            mAdapter = new FaqListadapter(FreQueActivity.this, movieList);
                            rv_categories.setAdapter(mAdapter);

                        } else {
                            tv_nofound.setVisibility(View.VISIBLE);
                            rv_categories.setVisibility(View.GONE);
                        }
                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                    tv_nofound.setVisibility(View.VISIBLE);
                    rv_categories.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<FaQData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                tv_nofound.setVisibility(View.VISIBLE);
                rv_categories.setVisibility(View.GONE);
            }
        });
    }

    private void getData() {

        Gson gson = new Gson();

        v_type = preferences.getString("url", "");
        course_name = preferences.getString("course_name", "");
        uriPth = preferences.getString("videolink", "");
        videoName = preferences.getString("video_name", "");
        course_id = preferences.getString("course_id", "");

        String movie = preferences.getString("movie", null);
        String resut = preferences.getString("result", null);

        Type type = new TypeToken<List<VideoDetails>>() {
        }.getType();

        Type type1 = new TypeToken<List<ResultDetails>>() {
        }.getType();
        movieVList = gson.fromJson(movie, type);
        resultDetailsList = gson.fromJson(resut, type1);


    }

    private boolean validateInfoForm() {
        if (et_mobile.getText().toString().trim().isEmpty()) {
            et_mobile.setError("Please Enter Your Question");
            return false;
        } else {
            return true;
        }
    }

    private void registerData() {
        dialogLoader.showProgressDialog();

        Call<ResponseModel> call = APIClient.getInstance()
                .sendfQ
                        (
                                customerID,
                                video_id,
                                et_mobile.getText().toString().trim()
                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getMsg().equalsIgnoreCase("FAQ send successfully"))
                    {
                        // Get the User Details from Response
                        Toast.makeText(getApplicationContext(), "FAQ send successfully", Toast.LENGTH_LONG).show();

                        FaQDetails faQDetails = new FaQDetails();
                        faQDetails.setQuery(et_mobile.getText().toString().trim());
                        faQDetails.setAnswer(null);
                        movieList.add(faQDetails);
                        tv_nofound.setVisibility(View.GONE);
                        rv_categories.setVisibility(View.VISIBLE);
                        mAdapter = new FaqListadapter(FreQueActivity.this, movieList);
                        rv_categories.setAdapter(mAdapter);

                        et_mobile.setText("");
                    }
                    else {
                        Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!",Toast  .LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getApplicationContext(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getApplicationContext(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(FreQueActivity.this, VideoListActivity.class);
        intent.putExtra("url", v_type);
        intent.putExtra("course_id", course_id);
        intent.putExtra("video_id", video_id);
        intent.putExtra("video_name", videoName);
        intent.putExtra("video", (Serializable) movieVList);
        intent.putExtra("videolink", uriPth);
        intent.putExtra("course_name", course_name);
        intent.putExtra("position", 0);
        intent.putExtra("result", (Serializable) resultDetailsList);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
