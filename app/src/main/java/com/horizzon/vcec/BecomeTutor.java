package com.horizzon.vcec;

import android.content.Intent;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.horizzon.vcec.R;

import org.w3c.dom.Text;

public class BecomeTutor extends AppCompatActivity {

    TextView tv_how_it_works;
    Button fab_register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_become_tutor);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        fab_register = (Button) findViewById(R.id.fab_register);
        fab_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register = new Intent(BecomeTutor.this,TutorRegisterActivity.class);
                register.putExtra("become_tutor","tutor");
                startActivity(register);
            }
        });

        tv_how_it_works = (TextView) findViewById(R.id.how_it_works);
        tv_how_it_works.setText(Html.fromHtml(getString(R.string.how_it_works)));

    }
    @Override
    public boolean onSupportNavigateUp() {

        super.onBackPressed();
        return true;

    }

    @Override
    public void onBackPressed() {

        super.onBackPressed();

    }
}