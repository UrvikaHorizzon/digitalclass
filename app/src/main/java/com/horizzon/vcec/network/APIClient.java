package com.horizzon.vcec.network;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.horizzon.vcec.Config;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


/**
 * APIClient handles all the Network API Requests using Retrofit Library
 **/

public class APIClient {
    
    
    // Base URL for API Requests
    private static final String BASE_URL = Config.ECOMMERCE_URL;
    private static Retrofit retrofit = null;
    private static APIRequests apiRequests;

//    public static String service_url = "http://statuscool.in/admin_statuscool/api/";
    // Singleton Instance of APIRequests
    public static APIRequests getInstance() {
        if (apiRequests == null) {

            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
                    .connectTimeout(60, TimeUnit.SECONDS)
                    .readTimeout(60, TimeUnit.SECONDS)
                    .writeTimeout(60, TimeUnit.SECONDS)
                    .build();

            Gson gson = new GsonBuilder()
                    .setLenient()
                    .create();
            
            
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .client(okHttpClient)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            apiRequests = retrofit.create(APIRequests.class);
            
            return apiRequests;
        }
        else {
            return apiRequests;
        }
    }


//    public static APIRequests getInstances() {
//        if (apiRequests == null) {
//
//            OkHttpClient okHttpClient = new OkHttpClient().newBuilder()
//                    .connectTimeout(60, TimeUnit.SECONDS)
//                    .readTimeout(60, TimeUnit.SECONDS)
//                    .writeTimeout(60, TimeUnit.SECONDS)
//                    .build();
//
//            Gson gson = new GsonBuilder()
//                    .setLenient()
//                    .create();
//
//
//            Retrofit retrofit = new Retrofit.Builder()
//                    .baseUrl(service_url)
//                    .client(okHttpClient)
//                    .addConverterFactory(GsonConverterFactory.create(gson))
//                    .build();
//
//            apiRequests = retrofit.create(APIRequests.class);
//
//            return apiRequests;
//        }
//        else {
//            return apiRequests;
//        }
//    }
}


