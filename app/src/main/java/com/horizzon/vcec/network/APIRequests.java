package com.horizzon.vcec.network;

import com.google.gson.JsonObject;
import com.horizzon.vcec.model.BannerData;
import com.horizzon.vcec.model.CategoryData;
import com.horizzon.vcec.model.CityData;
import com.horizzon.vcec.model.CountryData;
import com.horizzon.vcec.model.CourceData;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.model.CourseCurriculam;
import com.horizzon.vcec.model.CourseCurriculamDetails;
import com.horizzon.vcec.model.DemoModel;
import com.horizzon.vcec.model.DemoVideoModel;
import com.horizzon.vcec.model.ExamData;
import com.horizzon.vcec.model.FaQData;
import com.horizzon.vcec.model.LoginData;
import com.horizzon.vcec.model.ProfileData;
import com.horizzon.vcec.model.ReferData;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.model.StateData;
import com.horizzon.vcec.model.StudentData;
import com.horizzon.vcec.model.TutorProfileData;
import com.horizzon.vcec.model.UserData;
import com.horizzon.vcec.model.VideoData;
import com.horizzon.vcec.model.demodata;

import java.util.ArrayList;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * APIRequests contains all the Network Request Methods with relevant API Endpoints
 **/

public interface APIRequests {

    String secure_field = "key";
    String secure_value = "DRe34_12fdeSFGDsERGGTq";
    String param_key = "param_key";
    String param_value = "12324560";

    @GET("cities")
    Call<CityData> getCityList();

    @GET("countries")
    Call<CountryData> getCountryList();

    @GET("states")
    Call<StateData> getStateList();

    @GET("HomeSlider")
    Call<BannerData> getBannerList();

    @GET("maincategory")
    Call<CategoryData> getCategoryList();

    @GET("tutorial")
    Call<CategoryData> getTutorialList();

    @GET("languages")
    Call<CategoryData> getLanguageList();

    @GET("branch_type")
    Call<CategoryData> getBranchList();

    @GET("packages")
    Call<CategoryData> getPackageList();


    @FormUrlEncoded
    @POST("course_user")
    Call<StudentData> getStudentList(@Field("course_id") String id);

    @FormUrlEncoded
    @POST("Sub_category")
    Call<CategoryData> getSubCategoryList(@Field("id") String id);

    @FormUrlEncoded
    @POST("questions")
    Call<ExamData> getQuestionmList(@Field("video_id") String id);

    @FormUrlEncoded
    @POST("view_demovideo")
    Call<demodata> getDemoVideoList(@Field("courseid") String id, @Field("subcategory_id") String uesr_id, @Field("videoflag") String flag, @Field("username") String name);


    @FormUrlEncoded
    @POST("video")
    Call<VideoData> getVideoList(@Field("course_id") String id, @Field("user_id") String uesr_id);

    @FormUrlEncoded
    @POST("student_mobile_duplicate")
    Call<ResponseModel> check_mobile_number(@Field("mobile") String mobile_number);

    @FormUrlEncoded
    @POST("student_email_duplicate")
    Call<ResponseModel> check_email(@Field("email") String emailid);

    @FormUrlEncoded
    @POST("tutor_mobile_duplicate")
    Call<ResponseModel> check_mobile_number_tutor(@Field("mobile") String mobile_number);

    @FormUrlEncoded
    @POST("tutor_email_duplicate")
    Call<ResponseModel> check_email_tutor(@Field("email") String emailid);


    @FormUrlEncoded
    @POST("search")
    Call<CourceData> getSerchList(@Field("name") String name);

    @FormUrlEncoded
    @POST("courses_new")
    Call<CourceDetails> getCourceList(@Field("id") String id, @Field("prise") String prise, @Field("fee_type") String fee_type, @Field("language") String language, @Field("certificated") String certificated, @Field("rating") String rating, @Field("tutorial") String tutorial);

    @FormUrlEncoded
    @POST("courses_new")
    Call<CourceData> getCourceLists(@Field("id") String id, @Field("language") String language, @Field("tutorial") String tutorial);


    @POST("courses_new")
    Call<CourceDetails> getList(@Query("language") String language, @Query("tutorial") String tutorial);

    @POST("courses_new")
    Call<JsonObject> getLists(@Query("language") String language, @Query("tutorial") String tutorial);

    @FormUrlEncoded
    @POST("faqs_list")
    Call<FaQData> getFqList(@Field("user_id") String user_id, @Field("video_id") String video_id);

    @FormUrlEncoded
    @POST("rating")
    Call<ResponseModel> getRteList(@Field("user_id") String user_id, @Field("course_id") String course_id, @Field("rating_value") String rating_value);

    @FormUrlEncoded
    @POST("center_course")
    Call<CourceData> getCCourceList(@Field("courses_id") String id);

    @FormUrlEncoded
    @POST("my_course")
    Call<CourceData> getMyCourceList(@Field("user_id") String id);

    @FormUrlEncoded
    @POST("wishlist")
    Call<CourceData> getMyWishList(@Field("user_id") String id);

    @FormUrlEncoded
    @POST("profile")
    Call<ProfileData> getProfile(@Field("user_id") String id);

    @FormUrlEncoded
    @POST("center_view")
    Call<TutorProfileData> getTutorProfile(@Field("id") String id);

    @FormUrlEncoded
    @POST("demo_video")
    Call<DemoVideoModel> getDemos(@Field("id") String id);


    @FormUrlEncoded
    @POST("demo_video")
    Call<DemoModel> getDemoList(@Field("id") String id);

    //jay
    @GET("course_curriculam")
    Call<CourseCurriculam> getcourseCurriculam();

    @FormUrlEncoded
    @POST("referal")
    Call<ReferData> getReferList(@Field("user_id") String id);

    @FormUrlEncoded
    @POST("center_refer")
    Call<ReferData> getReferCList(@Field("id") String id);

    @FormUrlEncoded
    @POST("forgot_password")
    Call<ResponseModel> forgotUser(@Field("mobile_no") String mobile_no);

    @FormUrlEncoded
    @POST("Password_change")
    Call<ResponseModel> chngePwdUser(@Field("mobile_number") String mobilr_no, @Field("system_otp") String send_otp, @Field("user_otp") String user_otp, @Field("password") String password, @Field("Conform_password") String cpassword);

    @FormUrlEncoded
    @POST("Tutor_Password_change")
    Call<ResponseModel> tutor_chngePwdUser(@Field("mobile_number") String mobilr_no, @Field("system_otp") String send_otp, @Field("user_otp") String user_otp, @Field("password") String password, @Field("Conform_password") String cpassword);

    @FormUrlEncoded
    @POST("faqs")
    Call<ResponseModel> sendfQ(@Field("user_id") String user_id, @Field("video_id") String video_id, @Field("message") String message);

    @FormUrlEncoded
    @POST("video_log")
    Call<ResponseModel> sendViewVideo(@Field("user_id") String user_id, @Field("video_id") String video_id, @Field("course_id") String course_id);

    @FormUrlEncoded
    @POST("exams")
    Call<ResponseModel> sendResult(@Field("user_id") String user_id, @Field("video_id") String video_id, @Field("correct_ans") String correct_ans, @Field("wrong_ans") String wrong_ans);

    @Multipart
    @POST("center_video")
    Call<ResponseModel> registerVideo(@Part("id") RequestBody id, @Part("type") RequestBody type, @Part("videoflag") RequestBody flag,
                                      @Part("name") RequestBody name, @Part("description") RequestBody description,
                                      @Part("link") RequestBody link, @Part("franchise_status") RequestBody franchise_status,
                                      @Part MultipartBody.Part image);

    @Multipart
    @POST("add_course")
    Call<ResponseModel> registerCourse(@Part("main_category_id") RequestBody main_category_id, @Part("main_sub_category_id") RequestBody main_sub_category_id,
                                       @Part("sub_category_id") RequestBody sub_category_id, @Part("actual_price") RequestBody actual_price,
                                       @Part("price") RequestBody price, @Part("fee_type") RequestBody fee_type,
                                       @Part("description") RequestBody description, @Part("name") RequestBody name,
                                       @Part("chapter") RequestBody chapter, @Part("hours") RequestBody hours,
                                       @Part("month") RequestBody month, @Part("language") RequestBody language,
                                       @Part("certificated") RequestBody certificated,
                                       @Part("sequence") RequestBody sequence
            , @Part MultipartBody.Part image, @Part MultipartBody.Part course_image, @Part MultipartBody.Part pdf_file);

//    @Multipart
//    @POST("center_course_update")
//    Call<ResponseModel> updateCourse(@Part("center_id") RequestBody center_id,
//                                       @Part("id") RequestBody id, @Part("course_img_old") RequestBody course_img_old,
//                                       @Part("pdf_file_old") RequestBody pdf_file_old, @Part("center_old") RequestBody center_old,
//                                       @Part("old_video") RequestBody old_video,
//            @Part("main_category_id") RequestBody main_category_id, @Part("main_sub_category_id") RequestBody main_sub_category_id,
//                                       @Part("sub_category_id") RequestBody sub_category_id, @Part("actual_price") RequestBody actual_price,
//                                       @Part("price") RequestBody price, @Part("fee_type") RequestBody fee_type,
//                                       @Part("description") RequestBody description, @Part("name") RequestBody name,
//                                       @Part("chapter") RequestBody chapter, @Part("hours") RequestBody hours,
//                                       @Part("month") RequestBody month, @Part("language") RequestBody language,
//                                       @Part("certificated") RequestBody certificated,
//                                       @Part("certificated") RequestBody certificated,
//                                       @Part("sequence") RequestBody sequence, @Part MultipartBody.Part video
//            , @Part MultipartBody.Part image, @Part MultipartBody.Part course_image, @Part MultipartBody.Part pdf_file);

    @Multipart
    @POST("center_course_update")
    Call<ResponseModel> updateCourse(
            @Part("id") RequestBody id,
            @Part("main_category_id") RequestBody main_category_id, @Part("main_sub_category_id") RequestBody main_sub_category_id,
            @Part("actual_price") RequestBody actual_price,
            @Part("price") RequestBody price, @Part("fee_type") RequestBody fee_type,
            @Part("description_sub") RequestBody description, @Part("description_cor") RequestBody description_cor, @Part("name") RequestBody name,
            @Part("chapter") RequestBody chapter, @Part("hours") RequestBody hours,
            @Part("month") RequestBody month, @Part("language") RequestBody language,
            @Part("certificated") RequestBody certificated,
            @Part("sequence") RequestBody sequence, @Part("center_name") RequestBody center_name);

    @Multipart
    @POST("user_register")
    Call<UserData> registerData(@Part("name") RequestBody name, @Part("mobile") RequestBody mobile,
                                @Part("gender") RequestBody gender, @Part("email") RequestBody email,
                                @Part("address") RequestBody address, @Part("city") RequestBody city, @Part("State") RequestBody state,
                                @Part("country") RequestBody country, @Part("Birthdate") RequestBody birthdate, @Part("password") RequestBody password,
                                @Part("confirm_password") RequestBody cpassword,
                                @Part("fcm_id") RequestBody fcm_id, @Part("imai_no") RequestBody imai_no, @Part("referal") RequestBody referal);

    @FormUrlEncoded
    @POST("otp_verfiy")
    Call<ResponseModel> otpVerify(@Field("name") String name, @Field("mobile") String mobile,
                                  @Field("gender") String gender, @Field("email") String email,
                                  @Field("address") String address, @Field("city") String city, @Field("State") String state,
                                  @Field("country") String country, @Field("Birthdate") String birthdate, @Field("password") String password,
                                  @Field("confirm_password") String cpassword,
                                  @Field("system_otp") String system_otp, @Field("user_otp") String user_otp,
                                  @Field("fcm_id") String fcm_id, @Field("imai_no") String imai_no, @Field("referal") String referal);

    @FormUrlEncoded
    @POST("center_register")
    Call<ResponseModel> registerCenter(@Field("franchisee_type") String franchisee_type, @Field("carpet_area") String carpet_area,
                                       @Field("frim_type") String frim_type, @Field("system_number") String system_number,
                                       @Field("name_of_proprietor") String center_name, @Field("air_conditions") String air_conditions, @Field("center_name") String name_of_proprietor,
                                       @Field("lab") String lab, @Field("address_of_institue") String address_of_institue, @Field("admin_office") String admin_office,
                                       @Field("address_of_owner") String address_of_owner, @Field("faculty_cabin") String faculty_cabin,
                                       @Field("center_other_details") String center_other_details, @Field("mobile_number") String mobile_number,
                                       @Field("faculty_number") String faculty_number, @Field("email") String email,
                                       @Field("website_url") String website_url, @Field("country") String country,
                                       @Field("state") String state, @Field("city") String city,
                                       @Field("password") String password, @Field("conform_password") String conform_password,
                                       @Field("package") String package1, @Field("bank_name") String bank_name,
                                       @Field("ac_holder_name") String ac_holder_name, @Field("ac_number") String ac_number,
                                       @Field("ifsc_code") String ifsc_code, @Field("pan_number") String pan_number,
                                       @Field("fcm_id") String fcm_id, @Field("imai_no") String imai_no);

    @FormUrlEncoded
    @POST("center_update")
    Call<ResponseModel> updteCenter(@Field("franchisee_type") String franchisee_type, @Field("carpet_area") String carpet_area,
                                    @Field("frim_type") String frim_type, @Field("system_number") String system_number,
                                    @Field("center_name") String center_name, @Field("air_conditions") String air_conditions, @Field("name_of_proprietor") String name_of_proprietor,
                                    @Field("lab") String lab, @Field("address_of_institue") String address_of_institue, @Field("admin_office") String admin_office,
                                    @Field("address_of_owner") String address_of_owner, @Field("faculty_cabin") String faculty_cabin,
                                    @Field("center_other_details") String center_other_details, @Field("mobile_number") String mobile_number,
                                    @Field("faculty_number") String faculty_number, @Field("email") String email,
                                    @Field("website_url") String website_url, @Field("country") String country,
                                    @Field("state") String state, @Field("city") String city,
                                    @Field("package") String package1, @Field("bank_name") String bank_name,
                                    @Field("ac_holder_name") String ac_holder_name, @Field("ac_number") String ac_number,
                                    @Field("ifsc_code") String ifsc_code, @Field("pan_number") String pan_number,
                                    @Field("fcm_id") String fcm_id, @Field("id") String id);

    @FormUrlEncoded
    @POST("edit_profile")
    Call<ResponseModel> updateProfile(@Field("user_id") String user_id, @Field("name") String name, @Field("mobile") String mobile,
                                      @Field("gender") String gender,
                                      @Field("address") String address, @Field("city") String city, @Field("State") String state,
                                      @Field("country") String country, @Field("Birthdate") String birthdate);

    @FormUrlEncoded
    @POST("center_questions")
    Call<ResponseModel> centerQue(@Field("video_id") String video_id, @Field("question") String question, @Field("option_a") String option_a,
                                  @Field("option_b") String option_b,
                                  @Field("option_c") String option_c, @Field("option_d") String option_d, @Field("answer") String answer);

    @FormUrlEncoded
    @POST("center_questions_edit")
    Call<ResponseModel> centerQueEdit(@Field("question_id") String question_id, @Field("question") String question, @Field("option_a") String option_a,
                                      @Field("option_b") String option_b,
                                      @Field("option_c") String option_c, @Field("option_d") String option_d, @Field("answer") String answer);

    @FormUrlEncoded
    @POST("user_login")
    Call<LoginData> loginUser(@Field("usernmae") String usernmae, @Field("password") String password, @Field("imai_no") String imai_no, @Field("imi_no") String imi_no, @Field("type_user") String type_user);

    @FormUrlEncoded
    @POST("user_subscriptions")
    Call<ResponseModel> buyNow(@Field("user_id") String user_id, @Field("course_id") String course_id, @Field("prise") String prise, @Field("trazection_id") String trazection_id);

    @FormUrlEncoded
    @POST("free_user_subscriptions")
    Call<ResponseModel> free_course_subscriptions(@Field("customer_id") String customer_id, @Field("course_id") String course_id, @Field("course_price") String prise, @Field("fee_type") String fee_type);

    @FormUrlEncoded
    @POST("center_payment")
    Call<ResponseModel> buyCentreNow(@Field("center_id") String center_id, @Field("amount") String amount, @Field("tranzection_id") String trazection_id);

    @FormUrlEncoded
    @POST("add_whishlist")
    Call<ResponseModel> addToWishList(@Field("user_id") String user_id, @Field("course_id") String course_id);

    @FormUrlEncoded
    @POST("center_course_delete")
    Call<ResponseModel> courseDelete(@Field("id") String user_id);

    @FormUrlEncoded
    @POST("center_questions_delete")
    Call<ResponseModel> courseQueDelete(@Field("question_id") String user_id);

    @POST("deenew_api.php?" + secure_field + "=" + secure_value + "&" + param_key + "=" + param_value)
    Call<ResponseBody> getLatestStatus(@Query("req") String req,
                                       @Query("video_type") String video_type,
                                       @Query("page") String page);


    @FormUrlEncoded
    @POST("commission")
    Call<DemoModel> getCommision(@Field("id") String id);


    @Multipart
    @POST("add_demovideo")
    Call<ResponseModel> registerdemofilerservervideo(@Part("course_id") RequestBody id, @Part("video_type") RequestBody type, @Part("videoflag") RequestBody flag,
                                                     @Part MultipartBody.Part video,
                                                     @Part MultipartBody.Part image);

    @Multipart
    @POST("add_demovideo")
    Call<ResponseModel> registerdemoyoutubevideo(@Part("course_id") RequestBody id, @Part("video_type") RequestBody type, @Part("videoflag") RequestBody flag,
                                                 @Part("video_url") RequestBody link,
                                                 @Part MultipartBody.Part image);
}

