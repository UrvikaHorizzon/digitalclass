package com.horizzon.vcec.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horizzon.vcec.Config;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.VideoListActivity;
import com.horizzon.vcec.adapter.CustomExamadapter;
import com.horizzon.vcec.model.ExamData;
import com.horizzon.vcec.model.ExamDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.MyAppPrefsManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TutorExamFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TutorExamFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "video_id";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    RecyclerView rv_videoes;

    // TODO: Rename and change types of parameters
    ImageView iv_back;
    FloatingActionButton btn_add;
    MyAppPrefsManager myAppPrefsManager;
    private String customerID, video_id;
    DialogLoader dialogLoader;
    List<ExamDetails> movieList;
    private CustomExamadapter mAdapter;
    TextView tv_nofound;

    public TutorExamFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TutorExamFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TutorExamFragment newInstance(String param1, String param2) {
        TutorExamFragment fragment = new TutorExamFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            video_id = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).setDrawerEnabled(false);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tutor_video, container, false);
        rv_videoes = view.findViewById(R.id.rv_categories);
        tv_nofound = view.findViewById(R.id.tv_nofound);
        iv_back = view.findViewById(R.id.iv_back);
        btn_add = view.findViewById(R.id.btn_add);
        myAppPrefsManager = new MyAppPrefsManager(getActivity());
        dialogLoader = new DialogLoader(getActivity());
        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
        sharedPreferences = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE);
        customerID = sharedPreferences.getString("userID", "");

        movieList = new ArrayList<>();
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        layoutManager.setReverseLayout(false);
        rv_videoes.setLayoutManager(layoutManager);
        rv_videoes.setNestedScrollingEnabled(false);
        getVideoes(video_id);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AddExamFragment();
                Bundle bundle = new Bundle();
                bundle.putString("video_id", video_id);
                fragment.setArguments(bundle);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity ) getActivity()).onBackPressed();
            }
        });

        return view;
    }


    private void getVideoes(String course_id) {
        dialogLoader.showProgressDialog();
        Call<ExamData> call = APIClient.getInstance()
                .getQuestionmList(course_id);

        call.enqueue(new Callback<ExamData>() {
            @Override
            public void onResponse(Call<ExamData> call, Response<ExamData> response) {
                if (response.isSuccessful()) {
                    dialogLoader.hideProgressDialog();
                    // Get the User Details from Response
                    movieList = response.body().getMsg();
                    if (movieList.size() > 0) {
                        rv_videoes.setVisibility(View.VISIBLE);
                        tv_nofound.setVisibility(View.GONE);

                        mAdapter = new CustomExamadapter(getActivity(), movieList);
                        rv_videoes.setAdapter(mAdapter);
                    } else {
                        tv_nofound.setVisibility(View.VISIBLE);
                        rv_videoes.setVisibility(View.GONE);
                    }


                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    tv_nofound.setVisibility(View.VISIBLE);
                    rv_videoes.setVisibility(View.GONE);
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<ExamData> call, Throwable t) {
                tv_nofound.setVisibility(View.VISIBLE);
                rv_videoes.setVisibility(View.GONE);
            }
        });
    }


}