package com.horizzon.vcec.fragments;


import android.app.Dialog;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.tabs.TabLayout;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.adapter.CustomITCourceadapter;
import com.horizzon.vcec.adapter.CustomViewPagerAdapter;
import com.horizzon.vcec.adapter.CustomViewPagerDealAdapter;
import com.horizzon.vcec.adapter.SlidingImage_Adapter;
import com.horizzon.vcec.adapter.TopCategoriesadapter;
import com.horizzon.vcec.adapter.VideoViewAdapter;
import com.horizzon.vcec.model.BannerData;
import com.horizzon.vcec.model.BannerDetails;
import com.horizzon.vcec.model.CategoryData;
import com.horizzon.vcec.model.CategoryModel;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.model.videolist;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.AutoScrollViewPager;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link FeatureFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class FeatureFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    RecyclerView rv_categories, rv_cources;
    LinearLayout tv_show_cat, tv_show_cource;
    List<CategoryModel> movieList;
    List<CourceDetails> movieList1;
    private TopCategoriesadapter mAdapter;
    private CustomITCourceadapter mAdapter1;
    private static ViewPager mPager;
    AutoScrollViewPager mPager1, mPager2;
    private TabLayout tabLayout;
    private static int currentPage = 0;
    private static int NUM_PAGES = 0;
    EditText et_search;
    private ArrayList<BannerDetails> imageModelArrayList;
    CirclePageIndicator indicator, indicator1, indicator2;
    private ImageView iv_filter;
    TextView tv_tutor;
    private Dialog dialog;
    private RadioGroup rg_filter, rg_lnguge, rg_feetype, rg_cerificte;
    private RadioButton radioButton;
    RatingBar ratingBar;
    String prise = "", language = "", certificated = "", fee_type = "", rating = "", discount = "", tutoril = "''";
    String[] items;
    boolean[] checkedItems;
    ArrayList itemsSelected = new ArrayList();
    private Dialog dialog1;
    SwipeRefreshLayout swifeRefresh;

    LinearLayout ll_deal;
    LinearLayout become_tutor;
    HorizontalScrollView horizontalScrollView;
    private boolean scrollingLeft = false;
    RecyclerView recyclerView;

    CardView cv_card;

    // LinearLayout ll_upcoming, ll_new_course, ll_featured, ll_recommended;

    public FeatureFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment FeatureFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static FeatureFragment newInstance(String param1, String param2) {
        FeatureFragment fragment = new FeatureFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((HomeActivity) getActivity()).setDrawerEnabled(true);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_feature, container, false);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        rv_categories = view.findViewById(R.id.rv_categories);
        rv_cources = view.findViewById(R.id.rv_cources);
        tv_show_cat = view.findViewById(R.id.tv_show_cat);
        tv_show_cource = view.findViewById(R.id.tv_show_cource);
        et_search = view.findViewById(R.id.et_search);
        swifeRefresh = view.findViewById(R.id.swifeRefresh);

        ll_deal = view.findViewById(R.id.ll_deal);
        become_tutor = view.findViewById(R.id.become_tutor);
        recyclerView = (RecyclerView) view.findViewById(R.id.rv_videos);

        /*ll_upcoming = (LinearLayout) view.findViewById(R.id.ll_upcoming);
        ll_new_course = (LinearLayout) view.findViewById(R.id.ll_new_course);
        ll_featured = (LinearLayout) view.findViewById(R.id.ll_featured);
        ll_recommended = (LinearLayout) view.findViewById(R.id.ll_recommended);*/

        cv_card = (CardView) view.findViewById(R.id.cv_card);

        cv_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Snackbar snackbar = Snackbar
                        .make(cv_card, "Coming Soon", Snackbar.LENGTH_LONG);

                snackbar.show();

            }
        });

        /*ll_new_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new NewCourseFragment();
                loadFragment(fragment);

            }
        });

        ll_featured.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new FeaturedFragment();
                loadFragment(fragment);

            }
        });

        ll_recommended.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new RecommendedFragment();
                loadFragment(fragment);

            }
        });
*/
        videolist[] videolist = new videolist[]{
                new videolist("Goethe & Max Mueller Exam Preparation", "\u20B96500", "10000", "35% Off", "German Language Classes", "https://www.digitalclassworld.com/top_videos/video3.mp4", "https://www.digitalclassworld.com/top_videos/thumb/thumb1.jpg"),
                new videolist("Solid Works", "\u20B9500", "1500", "67% Off", "Sayeem Shaik", "https://www.digitalclassworld.com/top_videos/video2.mp4", "https://www.digitalclassworld.com/top_videos/thumb/thumb2.jpg"),
                new videolist("Std 12th Chemistry", "\u20B9500", "1000", "50% Off", "Suman Chemistry", "https://www.digitalclassworld.com/top_videos/video1.mp4", "https://www.digitalclassworld.com/top_videos/thumb/thumb3.jpeg"),
                new videolist("Spoken English Course", "\u20B93300", "10000", "67% Off", "Toral Education", "https://www.digitalclassworld.com/top_videos/video4.mp4", "https://www.digitalclassworld.com/top_videos/thumb/thumb4.jpeg"),

        };

        VideoViewAdapter adapter = new VideoViewAdapter(getActivity(), videolist);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(adapter);


        movieList = new ArrayList<>();
        movieList1 = new ArrayList<>();


        iv_filter = (ImageView) view.findViewById(R.id.iv_filter);
        iv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Fragment fragment = new FilterItem_Fragemnt();
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("cat_id", movie);
//                fragment.setArguments(bundle);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });

        swifeRefresh.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        // Reload current fragment
                        FragmentTransaction ft = getFragmentManager().beginTransaction();
                        ft.detach(FeatureFragment.this).attach(FeatureFragment.this).commit();
                        swifeRefresh.setRefreshing(false);
                    }
                }
        );

        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        layoutManager.setReverseLayout(false);
        //LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(),new GridLayoutManager(getActivity(), 3));
        //  layoutManager1.setReverseLayout(false);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                DividerItemDecoration.VERTICAL);
        dividerItemDecoration.setDrawable(getContext().getResources().getDrawable(R.drawable.item_decoration));

        rv_categories.addItemDecoration(dividerItemDecoration);

        DividerItemDecoration dividerItemDecoration1 = new DividerItemDecoration(getContext(),
                DividerItemDecoration.HORIZONTAL);
        dividerItemDecoration1.setDrawable(getContext().getResources().getDrawable(R.drawable.item_decoration));
        rv_categories.addItemDecoration(dividerItemDecoration1);


        rv_categories.setLayoutManager(new GridLayoutManager(getActivity(), 3));
        rv_categories.setHasFixedSize(true);
        rv_cources.setLayoutManager(layoutManager);
        imageModelArrayList = new ArrayList<>();

        mPager = (ViewPager) view.findViewById(R.id.pager);
        mPager1 = (AutoScrollViewPager) view.findViewById(R.id.pager1);
        mPager2 = (AutoScrollViewPager) view.findViewById(R.id.pager2);


        indicator = (CirclePageIndicator)
                view.findViewById(R.id.indicator);

        indicator1 = (CirclePageIndicator)
                view.findViewById(R.id.indicator1);

        indicator2 = (CirclePageIndicator)
                view.findViewById(R.id.indicator2);


        et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new SearchFragment();
                loadFragment(fragment);
//                startActivity(new Intent(getContext(), TestActivity.class));
            }
        });

        tv_show_cat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new CategoryFragment();
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("CategoryList", (Serializable) movieList);
//                fragment.setArguments(bundle);
                loadFragment(fragment);
            }
        });

        tv_show_cource.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CategoryModel movie = new CategoryModel();
                movie.setId("183");
                movie.setName("Trending Courses");
                Fragment fragment = new CourceListFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("cat_id", movie);
                fragment.setArguments(bundle);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


        getBanners();

        return view;
    }

    private void getTutorils() {
        Call<CategoryData> call = APIClient.getInstance()
                .getTutorialList();

        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        List<CategoryModel> categoryModelList = response.body().getData();
                        items = new String[categoryModelList.size()];
                        checkedItems = new boolean[categoryModelList.size()];
                        for (int i = 0; i < categoryModelList.size(); i++) {
                            items[i] = categoryModelList.get(i).getName();
                            checkedItems[i] = false;
                        }

                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
//                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getBanners() {
        Call<BannerData> call = APIClient.getInstance()
                .getBannerList();

        call.enqueue(new Callback<BannerData>() {
            @Override
            public void onResponse(Call<BannerData> call, Response<BannerData> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        imageModelArrayList = (ArrayList<BannerDetails>) response.body().getData();
                        init();
                        getCategories();
                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<BannerData> call, Throwable t) {
//                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getCategories() {
        Call<CategoryData> call = APIClient.getInstance()
                .getCategoryList();

        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {


                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        movieList = response.body().getData();
                        mAdapter = new TopCategoriesadapter(getActivity(), movieList);
                        rv_categories.setAdapter(mAdapter);
//                        rv_categories.smoothScrollToPosition(0);


                        getCources();

                        become_tutor.setVisibility(View.VISIBLE);

                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
//                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getCources() {
        Call<CourceDetails> call = APIClient.getInstance()
                .getCourceList("183", "", "", "", "", "", "");


        call.enqueue(new Callback<CourceDetails>() {
            @Override
            public void onResponse(Call<CourceDetails> call, Response<CourceDetails> response) {
                if (response.isSuccessful()) {
                    // Get the User Details from Response
                    movieList1 = response.body().getCoursess();
                    List<CourceDetails> courceDetailsList = response.body().getCoursess();
                    for (int i = 0; i < movieList1.size(); i++) {
                        for (int j = 0; j < courceDetailsList.size(); j++) {
                            if (movieList1.get(i).getId().equals(courceDetailsList.get(j).getId())) {
                                movieList1.get(i).setMain_category_id(courceDetailsList.get(j).getCourse_name());
                                break;
                            }
                        }
                    }
                    mAdapter1 = new CustomITCourceadapter(getActivity(), movieList1, "courselist");
                    rv_cources.setAdapter(mAdapter1);

                    final Integer[] IMAGES = {R.drawable.slide_1};
                    ArrayList<Integer> ImagesArray = new ArrayList<Integer>();

                    indicator1.setCentered(true);
                    mPager1.startAutoScroll();
                    mPager1.setInterval(3000);
                    mPager1.setCycle(true);
                    mPager1.setStopScrollWhenTouch(true);

                    for (int i = 0; i < IMAGES.length; i++) {

                        ImagesArray.add(IMAGES[i]);
                        mPager1.setAdapter(new CustomViewPagerAdapter(getActivity(), ImagesArray));
                        indicator1.setViewPager(mPager1);

                        final float density = getResources().getDisplayMetrics().density;
                        indicator1.setRadius(2 * density);

                        NUM_PAGES = imageModelArrayList.size();

                    }


                    indicator1.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                        @Override
                        public void onPageSelected(int position) {
                            currentPage = position;

                        }

                        @Override
                        public void onPageScrolled(int pos, float arg1, int arg2) {

                        }

                        @Override
                        public void onPageScrollStateChanged(int pos) {

                        }
                    });


                    final Integer[] IMAGES1 = {R.drawable.ic_deal1, R.drawable.ic_deal2, R.drawable.ic_deal3};
                    ArrayList<Integer> DealArray = new ArrayList<Integer>();

                    indicator2.setCentered(true);
                    mPager2.startAutoScroll();
                    mPager2.setInterval(3000);
                    mPager2.setCycle(true);
                    mPager2.setStopScrollWhenTouch(true);

                    for (int i = 0; i < IMAGES1.length; i++) {

                        DealArray.add(IMAGES1[i]);
                        mPager2.setAdapter(new CustomViewPagerDealAdapter(getActivity(), DealArray));
                        indicator2.setViewPager(mPager2);

                        final float density = getResources().getDisplayMetrics().density;
                        indicator2.setRadius(2 * density);


                    }


                    indicator2.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                        @Override
                        public void onPageSelected(int position) {
                            currentPage = position;

                        }

                        @Override
                        public void onPageScrolled(int pos, float arg1, int arg2) {

                        }

                        @Override
                        public void onPageScrollStateChanged(int pos) {

                        }
                    });


                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                }
            }

            @Override
            public void onFailure(Call<CourceDetails> call, Throwable t) {
                Toast.makeText(getActivity(), "failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

//        call.enqueue(new Callback<CourceData>() {
//            @Override
//            public void onResponse(Call<CourceData> call, Response<CourceData> response) {
//                if (response.isSuccessful()) {
//                    if (response.body().getSuccess().equalsIgnoreCase("1"))
//                    {
//                        // Get the User Details from Response
//                        movieList1 = response.body().getData();
//                        List<CourceDetails> courceDetailsList = response.body().getCoursess();
//                        for (int i =0;i<movieList1.size();i++) {
//                            for (int j=0;j<courceDetailsList.size();j++){
//                                if(movieList1.get(i).getId().equals(courceDetailsList.get(j).getId())){
//                                    movieList1.get(i).setMain_category_id(courceDetailsList.get(j).getName());
//                                    break;
//                                }
//                            }
//                        }
//                        mAdapter1 = new CustomITCourceadapter(getActivity(), movieList1,"courselist");
//                        rv_cources.setAdapter(mAdapter1);
//                    }
//
//                } else {
//                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
//                    // Show the Error Message
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CourceData> call, Throwable t) {
////                Toast.makeText(getActivity(), "Network not available : ", Toast.LENGTH_LONG).show();
//            }
//        });
    }

    private void init() {
        if (getActivity() != null) {
// Code goes here.
            mPager.setAdapter(new SlidingImage_Adapter(getActivity(), imageModelArrayList));
            indicator.setViewPager(mPager);
            final float density = getResources().getDisplayMetrics().density;

//Set circle indicator radius
            indicator.setRadius(2 * density);

            NUM_PAGES = imageModelArrayList.size();

            // Auto start of viewpager
            final Handler handler = new Handler();
            final Runnable Update = new Runnable() {
                public void run() {
                    if (currentPage == NUM_PAGES) {
                        currentPage = 0;
                    }
                    mPager.setCurrentItem(currentPage++, true);
                }
            };
            Timer swipeTimer = new Timer();
            swipeTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    handler.post(Update);
                }
            }, 10000, 10000);

            // Pager listener over indicator
            indicator.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

                @Override
                public void onPageSelected(int position) {
                    currentPage = position;

                }

                @Override
                public void onPageScrolled(int pos, float arg1, int arg2) {

                }

                @Override
                public void onPageScrollStateChanged(int pos) {

                }
            });

        }

    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    /*private void getCorces() {
        String corceNme[] = new String[]{"IT & SOFTWARE", "KIDS COURSES", "SPOKEN ENGLISH", "BUSINESS MANAGEMENT"};
        String corceNo[] = new String[]{"250", "120", "150", "180"};
        Integer corceImg[] = new Integer[]{R.drawable.cat_img1, R.drawable.cat_img2, R.drawable.cat_img3, R.drawable.cat_img4};

        for (int i = 0; i < corceNme.length; i++) {
            CategoryModel categoryModel = new CategoryModel();
            categoryModel.setName(corceNme[i]);
            categoryModel.setImg(corceImg[i]);
            categoryModel.setPrice(corceNo[i]);
            movieList.add(categoryModel);
        }
        mAdapter = new TopCategoriesadapter(getActivity(), movieList);
        rv_categories.setAdapter(mAdapter);

    }*/


}
