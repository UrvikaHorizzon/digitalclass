package com.horizzon.vcec.fragments;

import android.content.Context;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.adapter.CustomCCourseadapter;
import com.horizzon.vcec.model.CourceData;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class TutorCourseFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    EditText et_course;
    ImageView iv_back;
    DialogLoader dialogLoader;
    RecyclerView rv_cources;
    List<CourceDetails> movieList;
    private CustomCCourseadapter mAdapter;
    private TextView tv_nofound, tv_cource_title;
    String course_nme;


    public TutorCourseFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TutorCourseFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TutorCourseFragment newInstance(String param1, String param2) {
        TutorCourseFragment fragment = new TutorCourseFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_tutor_course, container, false);
        et_course = view.findViewById(R.id.et_course);
        iv_back = view.findViewById(R.id.iv_back);
        rv_cources = view.findViewById(R.id.rv_cources);
        tv_nofound = view.findViewById(R.id.tv_nofound);
        tv_cource_title = view.findViewById(R.id.tv_cource_title);

        course_nme = getActivity().getSharedPreferences("UserInfo", Context.MODE_PRIVATE).getString("userName", "");
        tv_cource_title.setVisibility(View.GONE);
        et_course.setVisibility(View.VISIBLE);
        movieList = new ArrayList<>();
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true);
        layoutManager1.setReverseLayout(false);
        rv_cources.setLayoutManager(layoutManager1);
        dialogLoader = new DialogLoader(getActivity());
        getCorces();
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).onBackPressed();
            }
        });

        et_course.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AddCourceFragment();
                loadFragment(fragment);
            }
        });
        return view;
    }

    @Override
    public void onDestroy() {
        dialogLoader.hideProgressDialog();
        super.onDestroy();
    }

    private void getCorces() {
        dialogLoader.showProgressDialog();
        Call<CourceData> call = APIClient.getInstance()
                .getCCourceList(course_nme);

        call.enqueue(new Callback<CourceData>() {
            @Override
            public void onResponse(Call<CourceData> call, Response<CourceData> response) {
                dialogLoader.hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        movieList = response.body().getData();
                        if(movieList.size() > 0) {
                            tv_nofound.setVisibility(View.GONE);
                            rv_cources.setVisibility(View.VISIBLE);
                            mAdapter = new CustomCCourseadapter(getActivity(), movieList);
                            rv_cources.setAdapter(mAdapter);

                        } else {
                            tv_nofound.setVisibility(View.VISIBLE);
                            rv_cources.setVisibility(View.GONE);
                        }
                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CourceData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                tv_nofound.setVisibility(View.VISIBLE);
                rv_cources.setVisibility(View.GONE);
            }
        });
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }


}
