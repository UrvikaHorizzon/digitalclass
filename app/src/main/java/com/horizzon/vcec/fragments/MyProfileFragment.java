package com.horizzon.vcec.fragments;


import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.Config;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.RegisterActivity;
import com.horizzon.vcec.SelectRoleActivity;
import com.horizzon.vcec.model.CityData;
import com.horizzon.vcec.model.CityDetails;
import com.horizzon.vcec.model.CountryData;
import com.horizzon.vcec.model.CountryDetails;
import com.horizzon.vcec.model.ProfileData;
import com.horizzon.vcec.model.ProfileDetails;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.model.StateData;
import com.horizzon.vcec.model.StateDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.MyAppPrefsManager;
import com.horizzon.vcec.util.ValidateInputs;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TutorProfile#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String customerID = "";
    private MyAppPrefsManager myAppPrefsManager;
    private DialogLoader dialogLoader;
    EditText et_name, et_email, et_mobile, et_address, et_bdate;
    AutoCompleteTextView et_city, et_state, et_country;
    Button btn_signup;
    ImageView iv_photo, iv_back;
    RadioGroup rg_gender;
    RadioButton radioButton, rb_male, rb_female;
    String city_id, state_id, country_id, gender;
    Calendar myCalendar = Calendar.getInstance();
    List<CountryDetails> countrydetails;
    List<StateDetails> countrydetails1;
    List<CityDetails> countrydetails2;
    ProgressBar progress_phone;
    Button btn_refer;
    TextView tv_logout;

    public MyProfileFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    /*public static TutorProfile newInstance(String param1, String param2) {
        TutorProfile fragment = new TutorProfile();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        // Inflate the layout for this fragment
        final View view = inflater.inflate(R.layout.fragment_profile, container, false);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        customerID = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");
        Log.d("CID", customerID);
        myAppPrefsManager = new MyAppPrefsManager(getActivity());
        dialogLoader = new DialogLoader(getActivity());
        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
        et_address = view.findViewById(R.id.et_address);
        et_name = view.findViewById(R.id.et_name);
        et_email = view.findViewById(R.id.et_email);
        et_mobile = view.findViewById(R.id.et_mobile);
        et_city = view.findViewById(R.id.et_city);
        et_state = view.findViewById(R.id.et_state);
        et_bdate = view.findViewById(R.id.et_bdate);
        et_country = view.findViewById(R.id.et_country);
        btn_signup = view.findViewById(R.id.btn_signup);
        iv_photo = view.findViewById(R.id.iv_photo);
        iv_back = view.findViewById(R.id.iv_back);
        rg_gender = view.findViewById(R.id.rg_gender);
        rb_male = view.findViewById(R.id.rb_male);
        rb_female = view.findViewById(R.id.rb_female);
        progress_phone = view.findViewById(R.id.pb_phone);
        tv_logout = view.findViewById(R.id.tv_logout);
        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent in = new Intent(getActivity(), SelectRoleActivity.class);
                startActivity(in);

            }
        });
        btn_refer = view.findViewById(R.id.btn_refer);
        btn_refer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new ReferFragment();
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
        countrydetails = new ArrayList<>();
        countrydetails1 = new ArrayList<>();
        countrydetails2 = new ArrayList<>();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).onBackPressed();
            }
        });

        et_country.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i = 0; i < countrydetails.size(); i++) {
                    if (countrydetails.get(i).getCountry_name().equals(selection)) {
                        country_id = countrydetails.get(i).getId();
                        return;
                    }
                }
            }
        });

        et_state.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i = 0; i < countrydetails1.size(); i++) {
                    if (countrydetails1.get(i).getState_name().equals(selection)) {
                        state_id = countrydetails1.get(i).getId();
                        return;
                    }
                }
            }
        });

        et_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i = 0; i < countrydetails2.size(); i++) {
                    if (countrydetails2.get(i).getCity_name().equals(selection)) {
                        city_id = countrydetails2.get(i).getId();
                        return;
                    }
                }
            }
        });

        final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateLabel();
            }

        };

        et_bdate.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        if (Config.IS_USER_LOGGED_IN) {
            btn_signup.setEnabled(true);
            getProfile(customerID);
        } else {
            btn_signup.setEnabled(false);
        }

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int selectedId = rg_gender.getCheckedRadioButtonId();
                if (selectedId == -1) {
                    Toast.makeText(getActivity(), "Please select Gender", Toast.LENGTH_LONG).show();
                } else {
                    // find the radiobutton by returned id
                    radioButton = (RadioButton) view.findViewById(selectedId);
                    boolean isValidData = validateInfoForm();
                    if (isValidData) {
                        registerData();
                    }
                }
            }
        });
        return view;
    }

    private void registerData() {
        dialogLoader.showProgressDialog();

        Call<ResponseModel> call = APIClient.getInstance()
                .updateProfile
                        (
                                customerID,
                                et_name.getText().toString().trim(),
                                et_mobile.getText().toString().trim(),
                                radioButton.getText().toString().trim(),
                                et_address.getText().toString().trim(),
                                city_id,
                                state_id,
                                country_id,
                                et_bdate.getText().toString().trim()
                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getData().equalsIgnoreCase("Profile Update successfully")) {
                        // Get the User Details from Response
                        Toast.makeText(getActivity(), "Profile Updated successfully!", Toast.LENGTH_LONG).show();
                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        // Get the Error Message from Response
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!!!", Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getActivity(), "Something went wrong..Please try again later!!!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "Something went wrong..Please try again later!!!!", Toast.LENGTH_LONG).show();

            }
        });
    }

    private boolean validateInfoForm() {
        if (!ValidateInputs.isValidName(et_name.getText().toString().trim())) {
            et_name.setError("Please Enter Valid Name");
            return false;
        } else if (!ValidateInputs.isValidEmail(et_email.getText().toString().trim())) {
            et_email.setError("Please Enter Valid Email");
            return false;
        } else if (!ValidateInputs.isValidPhoneNo(et_mobile.getText().toString().trim())) {
            et_mobile.setError("Please Enter Valid Mobile number");
            return false;
        } else if (et_bdate.getText().toString().trim().isEmpty()) {
            et_bdate.setError("Please Enter Birthdate");
            return false;
        } else if (et_address.getText().toString().trim().isEmpty()) {
            et_address.setError("Please Enter address");
            return false;
        } else if (et_city.getText().toString().trim().isEmpty()) {
            et_city.setError("Please Enter City");
            return false;
        } else if (et_state.getText().toString().trim().isEmpty()) {
            et_state.setError("Please Enter State");
            return false;
        } else if (et_country.getText().toString().trim().isEmpty()) {
            et_country.setError("Please Enter Country");
            return false;
        } else {
            return true;
        }
    }

    private void getProfile(String customerID) {
        dialogLoader.showProgressDialog();
        Call<ProfileData> call = APIClient.getInstance()
                .getProfile(customerID);

        call.enqueue(new Callback<ProfileData>() {
            @Override
            public void onResponse(Call<ProfileData> call, Response<ProfileData> response) {
                dialogLoader.hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        List<ProfileDetails> profileDetailsList = response.body().getData();
                        et_address.setText(profileDetailsList.get(0).getAddress());
                        et_name.setText(profileDetailsList.get(0).getName());
                        et_bdate.setText(profileDetailsList.get(0).getDob());
                        et_email.setText(profileDetailsList.get(0).getEmail());
                        et_mobile.setText(profileDetailsList.get(0).getMobile_no());
                        et_email.setEnabled(false);
                        et_mobile.setEnabled(false);
                        gender = profileDetailsList.get(0).getGender();
                        city_id = profileDetailsList.get(0).getCity_id();
                        state_id = profileDetailsList.get(0).getState_id();
                        country_id = profileDetailsList.get(0).getCountry_id();

                        Picasso.get().load("https://digitalclassworld.com/public/storage/user/" + profileDetailsList.get(0).getProfile_picture()).into(iv_photo);

                        if (gender.equalsIgnoreCase("male")) {
                            rb_male.setChecked(true);
                        } else if (gender.equalsIgnoreCase("female")) {
                            rb_female.setChecked(true);
                        }
                        getCountries();
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                    //     Toast.makeText(getActivity(), response.message(),Toast.LENGTH_LONG).show();

                }


                et_mobile.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                        String enteredValue = cs.toString();
                        String type = "phone";
                        if (enteredValue.length() < 10) {
                            et_mobile.setError("Please Enter Valid Mobile number");
                            et_mobile.requestFocus();
                        } else {
                            if (!ValidateInputs.isValidPhoneNo(enteredValue)) {
                                et_mobile.setError("Please Enter Valid Mobile number");
                                et_mobile.requestFocus();
                            } else
                                checkmobilenumber(enteredValue, type, new RegisterActivity.ApiCallback() {
                                    @Override
                                    public void onResponse(boolean success) {

                                        if (success == true) {
                                            et_mobile.setError("this number already used");
                                            et_mobile.requestFocus();
                                            et_mobile.setNextFocusDownId(R.id.et_mobile);
                                            et_email.setEnabled(false);
                                            et_email.setClickable(false);
                                            et_address.setClickable(false);
                                            et_address.setEnabled(false);
                                            et_country.setClickable(false);
                                            et_country.setEnabled(false);
                                            et_city.setEnabled(false);
                                            et_city.setClickable(false);
                                            et_state.setClickable(false);
                                            et_state.setEnabled(false);
                                            et_bdate.setEnabled(false);
                                            et_bdate.setClickable(false);
                                            et_name.setClickable(false);
                                            et_name.setEnabled(false);
                                            btn_signup.setClickable(false);
                                        }

                                        if (success == false) {
                                            et_email.setEnabled(true);
                                            et_email.setClickable(true);
                                            et_address.setClickable(true);
                                            et_address.setEnabled(true);
                                            et_country.setClickable(true);
                                            et_country.setEnabled(true);
                                            et_city.setEnabled(true);
                                            et_city.setClickable(true);
                                            et_state.setClickable(true);
                                            et_state.setEnabled(true);
                                            et_bdate.setEnabled(true);
                                            et_bdate.setClickable(true);
                                            et_name.setClickable(true);
                                            et_name.setEnabled(true);
                                            btn_signup.setClickable(true);
                                        }

                                    }
                                });
                        }
                    }

                    @Override
                    public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }

                });
            }

            @Override
            public void onFailure(Call<ProfileData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateLabel() {
        String myFormat = "dd-MM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);

        et_bdate.setText(sdf.format(myCalendar.getTime()));
    }

    private void getCountries() {
        Call<CountryData> call = APIClient.getInstance()
                .getCountryList();

        call.enqueue(new Callback<CountryData>() {
            @Override
            public void onResponse(Call<CountryData> call, Response<CountryData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        countrydetails = response.body().getData();
                        String[] mStringArray = new String[countrydetails.size()];
                        for (int i = 0; i < countrydetails.size(); i++) {
                            mStringArray[i] = countrydetails.get(i).getCountry_name();
                            if (countrydetails.get(i).getId().equals(country_id)) {
                                et_country.setText(countrydetails.get(i).getCountry_name());
                            }
                        }
                        if (getActivity() != null) {
                            ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
                            et_country.setThreshold(1);//will start working from first character
                            et_country.setAdapter(countryAdapter);//setting the adapter data into the AutoCompleteTextView
                            getStates();
                        }

                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CountryData> call, Throwable t) {
                Toast.makeText(getActivity(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getStates() {
        Call<StateData> call = APIClient.getInstance()
                .getStateList();

        call.enqueue(new Callback<StateData>() {
            @Override
            public void onResponse(Call<StateData> call, Response<StateData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        countrydetails1 = response.body().getData();
                        String[] mStringArray = new String[countrydetails1.size()];
                        for (int i = 0; i < countrydetails1.size(); i++) {
                            mStringArray[i] = countrydetails1.get(i).getState_name();
                            if (countrydetails1.get(i).getId().equals(state_id)) {
                                et_state.setText(countrydetails1.get(i).getState_name());
                            }
                        }
                        if (getActivity() != null) {
                            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
                            et_state.setThreshold(1);//will start working from first character
                            et_state.setAdapter(stateAdapter);//setting the adapter data into the AutoCompleteTextView
                            getCities();
                        }


                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<StateData> call, Throwable t) {
                Toast.makeText(getActivity(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getCities() {
        Call<CityData> call = APIClient.getInstance()
                .getCityList();

        call.enqueue(new Callback<CityData>() {
            @Override
            public void onResponse(Call<CityData> call, Response<CityData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        countrydetails2 = response.body().getData();
                        String[] mStringArray = new String[countrydetails2.size()];
                        for (int i = 0; i < countrydetails2.size(); i++) {
                            mStringArray[i] = countrydetails2.get(i).getCity_name();
                            if (countrydetails2.get(i).getId().equals(city_id)) {
                                et_city.setText(countrydetails2.get(i).getCity_name());
                            }
                        }
                        if (getActivity() != null) {
                            ArrayAdapter<String> cityyAdapter = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
                            et_city.setThreshold(1);//will start working from first character
                            et_city.setAdapter(cityyAdapter);//setting the adapter data into the AutoCompleteTextView
                        }
                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CityData> call, Throwable t) {
                Toast.makeText(getActivity(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void checkmobilenumber(String number, String type, RegisterActivity.ApiCallback callback) {

        Call<ResponseModel> call = null;

        if (type.equals("phone")) {
            progress_phone.setVisibility(View.VISIBLE);
            call = APIClient.getInstance().check_mobile_number(number);
        }

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                progress_phone.setVisibility(View.GONE);

                if (response.body().getSuccess().equalsIgnoreCase("1")) {
                    callback.onResponse(response.body().getSuccess() != null);
                } else {
                    callback.onResponse(false);
                }

            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                progress_phone.setVisibility(View.GONE);
                call.cancel();
                callback.onResponse(false);

            }
        });
    }

}
