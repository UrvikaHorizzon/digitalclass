package com.horizzon.vcec.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.horizzon.vcec.Config;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.RegisterActivity;
import com.horizzon.vcec.model.CategoryData;
import com.horizzon.vcec.model.CategoryModel;
import com.horizzon.vcec.model.CityData;
import com.horizzon.vcec.model.CityDetails;
import com.horizzon.vcec.model.CountryData;
import com.horizzon.vcec.model.CountryDetails;
import com.horizzon.vcec.model.TutorProfileData;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.model.StateData;
import com.horizzon.vcec.model.StateDetails;
import com.horizzon.vcec.model.TutorProfileDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.ValidateInputs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link TutorProfileFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TutorProfileFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    EditText et_cname, et_pname, et_email, et_mobile, et_addr_institute, et_addr_owner, et_refcode,
            et_fmobile, et_website, et_offc_area, et_system_no, et_admin_offc,
            et_faculty_cabin, et_center, et_bname, et_accnt_name, et_accnt_no, et_ifsc, et_pan_no,
            et_lab, et_air_conditions;
    AutoCompleteTextView et_city, et_state, et_country, et_franchise_type, et_firm_type, et_package;
    Button btn_signup;
    ImageView iv_back;
    DialogLoader dialogLoader;
    String  city_id, state_id, country_id;
    List<CategoryModel> franchisedetails;
    List<CategoryModel> packagedetails;
    List<CountryDetails> countrydetails;
    List<StateDetails> countrydetails1;
    List<CityDetails> countrydetails2;
    private String franchise_id, package_id;
    private String txnid, customerID="", regId="";
    private SharedPreferences sharedPreferences;
    ProgressBar progress_phone, progress_email;
    boolean firsttime;

    public TutorProfileFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TutorProfileFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TutorProfileFragment newInstance(String param1, String param2) {
        TutorProfileFragment fragment = new TutorProfileFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_tutor_profile, container, false);
        sharedPreferences = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE);
        customerID = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");

        et_package = view.findViewById(R.id.et_package);
        et_cname = view.findViewById(R.id.et_cname);
        et_addr_owner = view.findViewById(R.id.et_addr_owner);
        et_addr_institute = view.findViewById(R.id.et_addr_institute);
        et_accnt_name = view.findViewById(R.id.et_accnt_name);
        et_accnt_no = view.findViewById(R.id.et_accnt_no);
        et_admin_offc = view.findViewById(R.id.et_admin_offc);
        et_bname = view.findViewById(R.id.et_bname);
        et_center = view.findViewById(R.id.et_center);
        et_faculty_cabin = view.findViewById(R.id.et_faculty_cabin);
        et_firm_type = view.findViewById(R.id.et_firm_type);
        et_fmobile = view.findViewById(R.id.et_fmobile);
        et_franchise_type = view.findViewById(R.id.et_franchise_type);
        et_ifsc = view.findViewById(R.id.et_ifsc);
        et_offc_area = view.findViewById(R.id.et_offc_area);
        et_air_conditions = view.findViewById(R.id.et_air_conditions);
        et_lab = view.findViewById(R.id.et_lab);
        et_pan_no = view.findViewById(R.id.et_pan_no);
        et_pname = view.findViewById(R.id.et_pname);
        et_refcode = view.findViewById(R.id.et_refcode);
        et_system_no = view.findViewById(R.id.et_system_no);
        et_website = view.findViewById(R.id.et_website);
        et_email = view.findViewById(R.id.et_email);
        et_mobile = view.findViewById(R.id.et_mobile);
        et_city = view.findViewById(R.id.et_city);
        et_state = view.findViewById(R.id.et_state);
        et_country = view.findViewById(R.id.et_country);
        btn_signup = view.findViewById(R.id.btn_signup);
        iv_back = view.findViewById(R.id.iv_back);
        dialogLoader = new DialogLoader(getActivity());
        progress_phone = view.findViewById(R.id.pb_phone);
        progress_email = view.findViewById(R.id.pb_email);
        countrydetails = new ArrayList<>();
        countrydetails1 = new ArrayList<>();
        countrydetails2 = new ArrayList<>();
        franchisedetails = new ArrayList<>();
        packagedetails = new ArrayList<>();





        et_franchise_type.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_franchise_type.showDropDown();
                et_franchise_type.requestFocus();
                return false;
            }
        });

        et_firm_type.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_firm_type.showDropDown();
                et_firm_type.requestFocus();
                return false;
            }
        });


        et_package.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_package.showDropDown();
                et_package.requestFocus();
                return false;
            }
        });

        et_franchise_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<franchisedetails.size();i++) {
                    if(franchisedetails.get(i).getName().equals(selection)) {
                        franchise_id = franchisedetails.get(i).getId();
                        return;
                    }
                }
            }
        });

        et_package.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<packagedetails.size();i++) {
                    if(packagedetails.get(i).getName().equals(selection)) {
                        package_id = packagedetails.get(i).getId();
                        return;
                    }
                }
            }
        });

        et_country.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<countrydetails.size();i++) {
                    if(countrydetails.get(i).getCountry_name().equals(selection)) {
                        country_id = countrydetails.get(i).getId();
                        return;
                    }
                }
            }
        });

        et_state.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<countrydetails1.size();i++) {
                    if(countrydetails1.get(i).getState_name().equals(selection)) {
                        state_id = countrydetails1.get(i).getId();
                        return;
                    }
                }
            }
        });

        et_city.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<countrydetails2.size();i++) {
                    if(countrydetails2.get(i).getCity_name().equals(selection)) {
                        city_id = countrydetails2.get(i).getId();
                        return;
                    }
                }
            }
        });




        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).onBackPressed();

            }
        });


        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                // find the radiobutton by returned id
                boolean isValidData = validateInfoForm();
                if (isValidData) {
                    registerData(regId);
                }
            }
        });







        if(Config.IS_USER_LOGGED_IN) {
            btn_signup.setEnabled(true);
            getProfile(customerID);
        } else {
            btn_signup.setEnabled(false);
        }
        return view;
    }

    private void getProfile(String customerID) {
        dialogLoader.showProgressDialog();
        Call<TutorProfileData> call = APIClient.getInstance()
                .getTutorProfile(customerID);
        call.enqueue(new Callback<TutorProfileData>() {
            @Override
            public void onResponse(Call<TutorProfileData> call, Response<TutorProfileData> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        List<TutorProfileDetails> profileDetailsList = response.body().getData();
                        et_cname.setText(profileDetailsList.get(0).getName());
                        et_pname.setText(profileDetailsList.get(0).getManager_name());
                        et_addr_owner.setText(profileDetailsList.get(0).getOwneraddress());
                        et_addr_institute.setText(profileDetailsList.get(0).getAddress());
                        et_email.setText(profileDetailsList.get(0).getEmail());
                        et_mobile.setText(profileDetailsList.get(0).getContact());

                        if(!profileDetailsList.get(0).getFirm_type().equals("")){
                            et_firm_type.setText(profileDetailsList.get(0).getFirm_type());
                        }else {
                            et_firm_type.setText("Pvt. Ltd");
                        }

                        et_fmobile.setText(profileDetailsList.get(0).getTelephone());
                        et_website.setText(profileDetailsList.get(0).getWebsite());
                        et_offc_area.setText(profileDetailsList.get(0).getCarpet_area());
                        et_system_no.setText(profileDetailsList.get(0).getSystem_number());
                        et_air_conditions.setText(profileDetailsList.get(0).getAir_conditions());
                        et_admin_offc.setText(profileDetailsList.get(0).getAdmin_office());
                        et_lab.setText(profileDetailsList.get(0).getLab());
                        et_faculty_cabin.setText(profileDetailsList.get(0).getFaculty_cabin());
                        et_center.setText(profileDetailsList.get(0).getCenter_other_details());
                        et_bname.setText(profileDetailsList.get(0).getBankname());
                        et_accnt_name.setText(profileDetailsList.get(0).getAcname());
                        et_accnt_no.setText(profileDetailsList.get(0).getAcno());
                        et_ifsc.setText(profileDetailsList.get(0).getIfsc());
                        et_pan_no.setText(profileDetailsList.get(0).getPanno());
                        package_id = profileDetailsList.get(0).getPackage1();
                        city_id = profileDetailsList.get(0).getCity();
                        state_id = profileDetailsList.get(0).getState();
                        country_id = profileDetailsList.get(0).getCountry();
                        franchise_id = profileDetailsList.get(0).getFranchisee_type();
                        regId = profileDetailsList.get(0).getFcm_id();
                        et_email.setEnabled(false);
                        et_mobile.setEnabled(false);


                        final String[] mStringArray = new String[]{"Pvt. Ltd", "Ltd.", "llp.", "Proprietorship", "Partnership", "Government recognized", "Other"};
                        ArrayAdapter<String> countryAdapter1 = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
                        et_firm_type.setThreshold(1);//will start working from first character
                        et_firm_type.setAdapter(countryAdapter1);

                        getFranchies();
                        firsttime=true;

//                        getCountries();
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!",Toast  .LENGTH_SHORT).show();
                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                    Toast.makeText(getActivity(), response.message(),Toast.LENGTH_LONG).show();

                }

//                if (firsttime==true){
//                    et_mobile.addTextChangedListener(new TextWatcher() {
//                        @Override
//                        public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
//
//                        }
//
//                        @Override
//                        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
//                        }
//
//                        @Override
//                        public void afterTextChanged(Editable s) {
//
//                            String enteredValue = s.toString();
//                            String type = "phone";
//                            if (enteredValue.length() < 10) {
//                                et_mobile.setError("Please Enter Valid Mobile number");
//                                et_mobile.requestFocus();
//
//                            } else {
//                                if (!ValidateInputs.isValidPhoneNo(enteredValue)) {
//                                    et_mobile.setError("Please Enter Valid Mobile number");
//                                    et_mobile.requestFocus();
//
//                                } else
//                                    checkmobilenumber(enteredValue, type, new ApiCallback() {
//                                        @Override
//                                        public void onResponse(boolean success) {
//
//                                            if (success == true) {
//
//                                                et_mobile.setError("this number already used");
//                                                et_mobile.requestFocus();
//                                                et_mobile.setNextFocusDownId(R.id.et_mobile);
//                                                et_email.setEnabled(false);
//                                                et_country.setEnabled(false);
//                                                et_city.setEnabled(false);
//                                                et_state.setEnabled(false);
//                                                btn_signup.setClickable(false);
//                                                et_cname.setEnabled(false);
//                                                et_addr_owner.setEnabled(false);
//                                                et_accnt_name.setEnabled(false);
//                                                et_accnt_no.setEnabled(false);
//                                                et_admin_offc.setEnabled(false);
//                                                et_bname.setEnabled(false);
//                                                et_center.setEnabled(false);
//                                                et_faculty_cabin.setEnabled(false);
//                                                et_pan_no.setEnabled(false);
//                                                et_pname.setEnabled(false);
//                                                et_refcode.setEnabled(false);
//                                                et_website.setEnabled(false);
//                                                et_package.setEnabled(false);
//                                                et_franchise_type.setEnabled(false);
//                                                et_firm_type.setEnabled(false);
//                                                et_addr_institute.setEnabled(false);
//                                                et_ifsc.setEnabled(false);
//
//
//                                            }
//
//                                            if (success == false) {
//                                                et_email.setEnabled(true);
//                                                et_country.setEnabled(true);
//                                                et_city.setEnabled(true);
//                                                et_state.setEnabled(true);
//                                                btn_signup.setClickable(true);
//                                                et_addr_owner.setEnabled(true);
//                                                et_accnt_name.setEnabled(true);
//                                                et_accnt_no.setEnabled(true);
//                                                et_admin_offc.setEnabled(true);
//                                                et_bname.setEnabled(true);
//                                                et_center.setEnabled(true);
//                                                et_faculty_cabin.setEnabled(true);
//                                                et_pan_no.setEnabled(true);
//                                                et_pname.setEnabled(true);
//                                                et_refcode.setEnabled(true);
//                                                et_website.setEnabled(true);
//                                                et_package.setEnabled(true);
//                                                et_franchise_type.setEnabled(true);
//                                                et_firm_type.setEnabled(true);
//                                                et_addr_institute.setEnabled(true);
//                                                et_ifsc.setEnabled(true);
//                                            }
//
//                                        }
//                                    });
//                            }
//                        }
//
//                    });
//
//
//                    et_email.addTextChangedListener(new TextWatcher() {
//                        @Override
//                        public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
//                            String enteredValue = cs.toString();
//                            String type = "email";
//                            if (enteredValue.length() > 11) {
//                                if (!ValidateInputs.isValidEmail(et_email.getText().toString().trim())) {
//                                    et_email.setError("Please Enter Valid Email");
//                                    et_email.requestFocus();
//                                } else
//                                    checkmobilenumber(enteredValue, type, new ApiCallback() {
//                                        @Override
//                                        public void onResponse(boolean success) {
//                                            if (success == true) {
//                                                et_email.setError("this email id already used");
//                                                et_email.requestFocus();
//                                                et_email.setNextFocusDownId(R.id.et_email);
//                                                et_mobile.setEnabled(false);
//                                                et_mobile.setClickable(false);
//                                                et_country.setClickable(false);
//                                                et_country.setEnabled(false);
//                                                et_city.setEnabled(false);
//                                                et_city.setClickable(false);
//                                                et_state.setClickable(false);
//                                                et_state.setEnabled(false);
//                                                btn_signup.setClickable(false);
//                                                et_cname.setEnabled(false);
//                                                et_addr_owner.setEnabled(false);
//                                                et_accnt_name.setEnabled(false);
//                                                et_accnt_no.setEnabled(false);
//                                                et_admin_offc.setEnabled(false);
//                                                et_bname.setEnabled(false);
//                                                et_center.setEnabled(false);
//                                                et_faculty_cabin.setEnabled(false);
//                                                et_pan_no.setEnabled(false);
//                                                et_pname.setEnabled(false);
//                                                et_refcode.setEnabled(false);
//                                                et_website.setEnabled(false);
//                                                et_package.setEnabled(false);
//                                                et_franchise_type.setEnabled(false);
//                                                et_firm_type.setEnabled(false);
//                                                et_addr_institute.setEnabled(false);
//                                                et_ifsc.setEnabled(false);
//                                            }
//
//
//                                            if (success == false) {
//                                                et_mobile.setEnabled(true);
//                                                et_mobile.setClickable(true);
//                                                et_country.setClickable(true);
//                                                et_country.setEnabled(true);
//                                                et_city.setEnabled(true);
//                                                et_city.setClickable(true);
//                                                et_state.setClickable(true);
//                                                et_state.setEnabled(true);
//                                                btn_signup.setClickable(true);
//                                                et_addr_owner.setEnabled(true);
//                                                et_accnt_name.setEnabled(true);
//                                                et_accnt_no.setEnabled(true);
//                                                et_admin_offc.setEnabled(true);
//                                                et_bname.setEnabled(true);
//                                                et_center.setEnabled(true);
//                                                et_faculty_cabin.setEnabled(true);
//                                                et_pan_no.setEnabled(true);
//                                                et_pname.setEnabled(true);
//                                                et_refcode.setEnabled(true);
//                                                et_website.setEnabled(true);
//                                                et_package.setEnabled(true);
//                                                et_franchise_type.setEnabled(true);
//                                                et_firm_type.setEnabled(true);
//                                                et_addr_institute.setEnabled(true);
//                                                et_ifsc.setEnabled(true);
//                                            }
//
//                                        }
//                                    });
//
//                            }
//                        }
//
//                        @Override
//                        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
//                        }
//
//                        @Override
//                        public void afterTextChanged(Editable s) {
//
//                        }
//
//                    });
//                }

            }

            @Override
            public void onFailure(Call<TutorProfileData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "Something went wrong..Please try again later!!",Toast  .LENGTH_SHORT).show();
            }
        });
    }

    private void getFranchies() {
        Call<CategoryData> call = APIClient.getInstance()
                .getBranchList();

        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {


                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        franchisedetails = response.body().getData();
                        String[] mStringArray = new String[franchisedetails.size()];
                        for (int i=0;i<franchisedetails.size();i++) {
                            mStringArray[i] = franchisedetails.get(i).getName();
                            if(franchisedetails.get(i).getId().equals(franchise_id)) {
                                et_franchise_type.setText(franchisedetails.get(i).getName());
                            }
                        }
                        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
                        et_franchise_type.setThreshold(0);//will start working from first character
                        et_franchise_type.setAdapter(countryAdapter);//setting the adapter data into the AutoCompleteTextView
                        getPackageList();

                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getPackageList() {
        Call<CategoryData> call = APIClient.getInstance()
                .getPackageList();

        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {


                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        packagedetails = response.body().getData();
                        String[] mStringArray = new String[packagedetails.size()];

                        for (int i=0;i<packagedetails.size();i++) {
                            mStringArray[i] = packagedetails.get(i).getName();
                            if(packagedetails.get(i).getId().equals(package_id)) {
                                et_package.setText(packagedetails.get(i).getName());
                            }
                        }
                        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
                        et_package.setThreshold(1);//will start working from first character
                        et_package.setAdapter(countryAdapter);//setting the adapter data into the AutoCompleteTextView
                        getCountries();

                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private boolean validateInfoForm() {



        if (et_franchise_type.getText().toString().trim().isEmpty()) {
            et_franchise_type.setError("Please select franchise type");
            return false;
        } else if (et_firm_type.getText().toString().trim().isEmpty()) {
            et_firm_type.setError("Please select firm type");
            return false;
        } else if (!ValidateInputs.isValidName(et_cname.getText().toString().trim())) {
            et_cname.setError("Please Enter Center Name");
            return false;
        } else if (!ValidateInputs.isValidName(et_pname.getText().toString().trim())) {
            et_pname.setError("Please Enter Name of Proprietor");
            return false;
        } else if (!ValidateInputs.isValidPhoneNo(et_fmobile.getText().toString().trim())) {
            et_fmobile.setError("Please Enter Valid Mobile Number");
            return false;
        } /*else if (et_website.getText().toString().trim().isEmpty()) {
            et_website.setError("Please Enter Valid Website URL");
            return false;
        }*/ else if (!ValidateInputs.isValidEmail(et_email.getText().toString().trim())) {
            et_email.setError("Please Enter Valid Email");
            return false;
        } else if (!ValidateInputs.isValidPhoneNo(et_mobile.getText().toString().trim())) {
            et_mobile.setError("Please Enter Valid Mobile number");
            return false;
        }else if(et_addr_owner.getText().toString().trim().isEmpty()) {
            et_addr_owner.setError("Please Enter Owner address");
            return false;
        } else if(et_addr_institute.getText().toString().trim().isEmpty()) {
            et_addr_institute.setError("Please Enter address");
            return false;
        } else if(et_city.getText().toString().trim().isEmpty()) {
            et_city.setError("Please Enter City");
            return false;
        } else if(et_state.getText().toString().trim().isEmpty()) {
            et_state.setError("Please Enter State");
            return false;
        } else if(et_country.getText().toString().trim().isEmpty()) {
            et_country.setError("Please Enter Country");
            return false;
        } else if(et_package.getText().toString().trim().isEmpty()) {
            et_package.setError("Please select package");
            return false;
        }/*else if(et_bname.getText().toString().trim().isEmpty()) {
            et_bname.setError("Please Enter Bank Name");
            return false;
        } *//*else if(et_accnt_name.getText().toString().trim().isEmpty()) {
            et_accnt_name.setError("Please enter account holder name");
            return false;
        }*/ /*else if(et_accnt_no.getText().toString().trim().isEmpty()) {
            et_accnt_no.setError("Please enter account no.");
            return false;
        } *//*else if(et_ifsc.getText().toString().trim().isEmpty()) {
            et_ifsc.setError("Please Enter IFSC Code");
            return false;
        } *//*else if(et_pan_no.getText().toString().trim().isEmpty()) {
            et_pan_no.setError("Please Enter Pan Card No.");
            return false;
        }*/ else {
            return true;
        }
    }


    private void getCountries() {
        Call<CountryData> call = APIClient.getInstance()
                .getCountryList();

        call.enqueue(new Callback<CountryData>() {
            @Override
            public void onResponse(Call<CountryData> call, Response<CountryData> response) {


                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        countrydetails = response.body().getData();
                        String[] mStringArray = new String[countrydetails.size()];
                        for (int i=0;i<countrydetails.size();i++) {
                            mStringArray[i] = countrydetails.get(i).getCountry_name();
                            if(countrydetails.get(i).getId().equals(country_id)) {
                                et_country.setText(countrydetails.get(i).getCountry_name());
                            }
                        }
                        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
                        et_country.setThreshold(1);//will start working from first character
                        et_country.setAdapter(countryAdapter);//setting the adapter data into the AutoCompleteTextView
                        getStates();

                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CountryData> call, Throwable t) {
                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getStates() {
        Call<StateData> call = APIClient.getInstance()
                .getStateList();

        call.enqueue(new Callback<StateData>() {
            @Override
            public void onResponse(Call<StateData> call, Response<StateData> response) {


                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        countrydetails1 = response.body().getData();
                        String[] mStringArray = new String[countrydetails1.size()];
                        for (int i=0;i<countrydetails1.size();i++) {
                            mStringArray[i] = countrydetails1.get(i).getState_name();
                            if(countrydetails1.get(i).getId().equals(state_id)) {
                                et_state.setText(countrydetails1.get(i).getState_name());
                            }
                        }
                        ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
                        et_state.setThreshold(1);//will start working from first character
                        et_state.setAdapter(stateAdapter);//setting the adapter data into the AutoCompleteTextView
                        getCities();

                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<StateData> call, Throwable t) {
                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getCities() {
        Call<CityData> call = APIClient.getInstance()
                .getCityList();

        call.enqueue(new Callback<CityData>() {
            @Override
            public void onResponse(Call<CityData> call, Response<CityData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        countrydetails2 = response.body().getData();
                        String[] mStringArray = new String[countrydetails2.size()];
                        for (int i=0;i<countrydetails2.size();i++) {
                            mStringArray[i] = countrydetails2.get(i).getCity_name();
                            if(countrydetails2.get(i).getId().equals(city_id)) {
                                et_city.setText(countrydetails2.get(i).getCity_name());
                            }
                        }
                        ArrayAdapter<String> cityyAdapter = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
                        et_city.setThreshold(1);//will start working from first character
                        et_city.setAdapter(cityyAdapter);//setting the adapter data into the AutoCompleteTextView
                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CityData> call, Throwable t) {
                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }




    private void registerData(String regId) {
        dialogLoader.showProgressDialog();
        Log.e("franchisee_type",franchise_id);
        Log.e("carpet_area",et_offc_area.getText().toString().trim());
        Log.e("frim_type",et_firm_type.getText().toString().trim());
        Log.e("system_number", et_system_no.getText().toString().trim());
        Log.e("center_name", et_cname.getText().toString().trim());
        Log.e("air_conditions",et_air_conditions.getText().toString().trim());
        Log.e("name_of_proprietor", et_pname.getText().toString().trim());
        Log.e("lab",et_lab.getText().toString().trim());
        Log.e("address_of_institue",et_addr_institute.getText().toString().trim());
        Log.e("admin_office",et_admin_offc.getText().toString().trim());
        Log.e("address_of_owner",et_addr_owner.getText().toString().trim());
        Log.e("faculty_cabin",et_faculty_cabin.getText().toString().trim());
        Log.e("center_other_details",et_center.getText().toString().trim());
        Log.e("mobile_number", et_mobile.getText().toString().trim());
        Log.e("faculty_number",et_fmobile.getText().toString().trim());
        Log.e("email",et_email.getText().toString().trim());
        Log.e("website_url",et_website.getText().toString().trim());
        Log.e("country", country_id);
        Log.e("state",state_id);
        Log.e("city", city_id);
        Log.e("package",package_id);
        Log.e("bank_name", et_bname.getText().toString().trim());
        Log.e("ac_holder_name",et_accnt_name.getText().toString().trim());
        Log.e("ac_number",et_accnt_no.getText().toString().trim());
        Log.e("ifsc_code",et_ifsc.getText().toString().trim());
        Log.e("pan_number",et_pan_no.getText().toString().trim());
        Log.e("fcm_id",regId);
        Log.e("id", customerID);


        Call<ResponseModel> call = APIClient.getInstance()
                .updteCenter
                        (
                                franchise_id,
                                et_offc_area.getText().toString().trim(),
                                et_firm_type.getText().toString().trim(),
                                et_system_no.getText().toString().trim(),
                                et_cname.getText().toString().trim(),
                                et_air_conditions.getText().toString().trim(),
                                et_pname.getText().toString().trim(),
                                et_lab.getText().toString().trim(),
                                et_addr_institute.getText().toString().trim(),
                                et_admin_offc.getText().toString().trim(),
                                et_addr_owner.getText().toString().trim(),
                                et_faculty_cabin.getText().toString().trim(),
                                et_center.getText().toString().trim(),
                                et_mobile.getText().toString().trim(),
                                et_fmobile.getText().toString().trim(),
                                et_email.getText().toString().trim(),
                                et_website.getText().toString().trim(),
                                country_id,
                                state_id,
                                city_id,
                                package_id,
                                et_bname.getText().toString().trim(),
                                et_accnt_name.getText().toString().trim(),
                                et_accnt_no.getText().toString().trim(),
                                et_ifsc.getText().toString().trim(),
                                et_pan_no.getText().toString().trim(),
                                regId,
                                customerID



                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        Toast.makeText(getActivity(), response.body().getData(), Toast.LENGTH_LONG).show();
//                        launchPayUMoneyFlow(price);
                    }
                    else if (response.body().getSuccess().equalsIgnoreCase("0"))
                    {
                        // Get the Error Message from Response
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!",Toast  .LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void checkmobilenumber(String number, String type, ApiCallback callback) {

        Call<ResponseModel> call = null;

        if (type.equals("phone")) {
            progress_phone.setVisibility(View.VISIBLE);
            call = APIClient.getInstance().check_mobile_number_tutor(number);
        }


        if (type.equals("email")) {
            progress_email.setVisibility(View.VISIBLE);
            call = APIClient.getInstance().check_email_tutor(number);
        }

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                progress_phone.setVisibility(View.GONE);
                progress_email.setVisibility(View.GONE);

                if (response.body().getSuccess().equalsIgnoreCase("1")) {
                    callback.onResponse(response.body().getSuccess() != null);
                } else {
                    callback.onResponse(false);
                }

            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                progress_phone.setVisibility(View.GONE);
                progress_email.setVisibility(View.GONE);
                call.cancel();
                callback.onResponse(false);

            }
        });
    }

    public interface ApiCallback {
        void onResponse(boolean success);
    }
}
