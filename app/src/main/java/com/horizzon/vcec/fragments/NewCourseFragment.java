package com.horizzon.vcec.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.adapter.CustomITCourceadapter;
import com.horizzon.vcec.adapter.CustomViewPagerAdapter;
import com.horizzon.vcec.adapter.CustomViewPagerDealAdapter;
import com.horizzon.vcec.adapter.NewCoursesAdapter;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;

import java.util.ArrayList;
import java.util.List;


public class NewCourseFragment extends Fragment {

    ImageView iv_back;
    List<CourceDetails> movieList1;
    private NewCoursesAdapter mAdapter1;
    RecyclerView rv_cources;
    DialogLoader dialogLoader;

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    public NewCourseFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment NewCourseFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static NewCourseFragment newInstance(String param1, String param2) {
        NewCourseFragment fragment = new NewCourseFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        View view = inflater.inflate(R.layout.fragment_new_course, container, false);

        dialogLoader = new DialogLoader(getActivity());

        rv_cources = view.findViewById(R.id.rv_cources);
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        layoutManager.setReverseLayout(false);
        rv_cources.setLayoutManager(layoutManager);

        iv_back = view.findViewById(R.id.iv_back);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).onBackPressed();
            }
        });

        movieList1 = new ArrayList<>();
        getCources();

        return view;
    }


    private void getCources() {
        dialogLoader.showProgressDialog();

        Call<CourceDetails> call = APIClient.getInstance()
                .getCourceList("183", "", "", "", "", "", "");


        call.enqueue(new Callback<CourceDetails>() {
            @Override
            public void onResponse(Call<CourceDetails> call, Response<CourceDetails> response) {
                dialogLoader.hideProgressDialog();
                if (response.isSuccessful()) {
                    // Get the User Details from Response
                    movieList1 = response.body().getCoursess();
                    List<CourceDetails> courceDetailsList = response.body().getCoursess();
                    for (int i = 0; i < movieList1.size(); i++) {
                        for (int j = 0; j < courceDetailsList.size(); j++) {
                            if (movieList1.get(i).getId().equals(courceDetailsList.get(j).getId())) {
                                movieList1.get(i).setMain_category_id(courceDetailsList.get(j).getCourse_name());
                                break;
                            }
                        }
                    }
                    mAdapter1 = new NewCoursesAdapter(getActivity(), movieList1, "courselist");
                    rv_cources.setAdapter(mAdapter1);




                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                }
            }

            @Override
            public void onFailure(Call<CourceDetails> call, Throwable t) {
                Toast.makeText(getActivity(), "failure" + t.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });


    }


}