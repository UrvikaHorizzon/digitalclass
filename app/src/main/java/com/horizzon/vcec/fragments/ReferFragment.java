package com.horizzon.vcec.fragments;


import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.model.ReferData;
import com.horizzon.vcec.model.ProfileDetails;
import com.horizzon.vcec.model.UserDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.MyAppPrefsManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link ReferFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReferFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String customerID="", cusType="s";
    private MyAppPrefsManager myAppPrefsManager;
    ImageView iv_back;
    private DialogLoader dialogLoader;
    List<UserDetails> userDetails;
    List<ProfileDetails> profileDetails;
    ListView recyclerView;
    TextView txt_refercode, txt_earning;
    Call<ReferData> call;


    public ReferFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReferFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReferFragment newInstance(String param1, String param2) {
        ReferFragment fragment = new ReferFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_refer, container, false);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        customerID = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");
        cusType = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userType", "");
        myAppPrefsManager = new MyAppPrefsManager(getActivity());
        dialogLoader = new DialogLoader(getActivity());
        txt_earning = view.findViewById(R.id.txt_earning);
        txt_refercode = view.findViewById(R.id.txt_refercode);
        recyclerView = view.findViewById(R.id.recycler_view);
        iv_back = view.findViewById(R.id.iv_back);
       userDetails = new ArrayList<>();
       profileDetails = new ArrayList<>();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).onBackPressed();
            }
        });

        getProfile(customerID);
        return view;
    }

    private void getProfile(String customerID) {
        dialogLoader.showProgressDialog();
        if(cusType.equals("t")) {
            call = APIClient.getInstance()
                    .getReferCList(customerID);
        } else {
            call = APIClient.getInstance()
                    .getReferList(customerID);
        }

        call.enqueue(new Callback<ReferData>() {
            @Override
            public void onResponse(Call<ReferData> call, Response<ReferData> response) {
                dialogLoader.hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("0"))
                    {
                        // Get the User Details from Response
                        int count = 0;

                        profileDetails = response.body().getData();
                        userDetails = response.body().getReferal_user();
                        if(profileDetails.size() > 0){
                            txt_refercode.setText(profileDetails.get(0).getPromation());

                            if(cusType.equals("t")) {
                                txt_refercode.setText(profileDetails.get(0).getReferalcode());
                            }
                            try {
                                if(!profileDetails.get(0).getRefer_persantage().equals("null")) {
                                    txt_earning.setText("\u20B9 "+profileDetails.get(0).getRefer_persantage());
                                } else {
                                    txt_earning.setText("\u20B9 0");
                                }
                            } catch (Exception e){
                                txt_earning.setText("\u20B9 0");
                            }
                        }

                        if(userDetails.size() > 0) {
                            recyclerView.setVisibility(View.VISIBLE);
                            String[] values = new String[userDetails.size()];
                            for (int i=0; i<userDetails.size();i++){
                                count++;
                                values[i] = String.valueOf(count)+". "+userDetails.get(i).getName();
                            }


                            // Assign adapter to ListView


                            if (cusType.equals("t")){
                                ArrayAdapter<String> adapters = new ArrayAdapter<String>(getActivity(),
                                        R.layout.dropdown,0);
                                recyclerView.setAdapter(adapters);
                            }else {

                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(),
                                        R.layout.dropdown, values);
                                recyclerView.setAdapter(adapter);
                            }
                        } else {
                            recyclerView.setVisibility(View.GONE);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!",Toast  .LENGTH_SHORT).show();
                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<ReferData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "Something went wrong..Please try again later!!",Toast  .LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onDestroy() {
        dialogLoader.hideProgressDialog();
        super.onDestroy();
    }
}
