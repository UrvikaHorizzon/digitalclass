package com.horizzon.vcec.fragments;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.adapter.CustomCategoryadapter;
import com.horizzon.vcec.model.CategoryData;
import com.horizzon.vcec.model.CategoryModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.IOnBackPressed;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class CategoryFragment extends Fragment implements IOnBackPressed {

    RecyclerView rv_categories;
    List<CategoryModel> movieList;
    private CustomCategoryadapter mAdapter;
    DialogLoader dialogLoader;
    ImageView iv_back;
    private LinearLayout ll_nofound;
    EditText et_search;

    public CategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        rv_categories = view.findViewById(R.id.rv_categories);
        iv_back = view.findViewById(R.id.iv_back);
        ll_nofound = view.findViewById(R.id.ll_nofound);
        et_search = view.findViewById(R.id.et_search);
        movieList = new ArrayList<>();
        dialogLoader = new DialogLoader(getActivity());
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true);
        layoutManager1.setReverseLayout(false);
        rv_categories.setLayoutManager(layoutManager1);

//        movieList = (List<CategoryModel>) getArguments().getSerializable("CategoryList");
        getCategories();
//        getCorces();

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).onBackPressed();
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                String name = et_search.getText().toString();
                mAdapter.filter(name);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
//                cAdapter.filter(text);
            }
        });
        return view;
    }

    private void getCorces() {
        String corceNme[] = new String[]{"IT & SOFTWARE", "KIDS COURSES", "SPOKEN ENGLISH", "BUSINESS MANAGEMENT",
                "Fashion Designing", "Aviation Courses", "Business Marketing", "Design & Editing", "Competitive Exam",
                "Accounts & Finance", "Science & Technology", "Medical Education", "Hotel Management", "Sports Education",
                "Business Development", "Personality Development", "Teaching & Academic", "Investment Management",
                "Import Export Managment", "Personal & Lifestyle"};

        for (int i = 0; i < corceNme.length; i++) {
            CategoryModel categoryModel = new CategoryModel();
            categoryModel.setName(corceNme[i]);
            movieList.add(categoryModel);
        }
        mAdapter = new CustomCategoryadapter(getActivity(), movieList);
        rv_categories.setAdapter(mAdapter);

    }

    private void getCategories() {
        dialogLoader.showProgressDialog();
        Call<CategoryData> call = APIClient.getInstance()
                .getCategoryList();

        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                dialogLoader.hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        movieList = response.body().getData();

                        if (movieList.size() > 0) {
                            rv_categories.setVisibility(View.VISIBLE);
                            mAdapter = new CustomCategoryadapter(getActivity(), movieList);
                            rv_categories.setAdapter(mAdapter);
                            mAdapter.findItem(movieList);

                        } else {
                            ll_nofound.setVisibility(View.VISIBLE);
                            rv_categories.setVisibility(View.GONE);
                        }
                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public boolean onBackPressed() {
        ((HomeActivity) getActivity()).onBackPressed();
        return true;
    }
}
