package com.horizzon.vcec.fragments;


import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.Paint;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.borjabravo.readmoretextview.ReadMoreTextView;
import com.horizzon.vcec.App;
import com.horizzon.vcec.BuildConfig;
import com.horizzon.vcec.Config;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.SelectRoleActivity;
import com.horizzon.vcec.adapter.CourseCurriculamAdapter;
import com.horizzon.vcec.adapter.CustomVideoadapter;
import com.horizzon.vcec.adapter.DemoListAdapter;
import com.horizzon.vcec.model.CountVideo;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.model.CourseCurriculam;
import com.horizzon.vcec.model.CourseCurriculamDetails;
import com.horizzon.vcec.model.DemoDetails;
import com.horizzon.vcec.model.DemoModel;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.model.ResultDetails;
import com.horizzon.vcec.model.VideoData;
import com.horizzon.vcec.model.VideoDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.AppEnvironment;
import com.horizzon.vcec.util.Database;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.IOnBackPressed;
import com.horizzon.vcec.util.MyAppPrefsManager;
import com.payumoney.core.PayUmoneyConfig;
import com.payumoney.core.PayUmoneyConstants;
import com.payumoney.core.PayUmoneySdkInitializer;
import com.payumoney.core.entity.TransactionResponse;
import com.payumoney.sdkui.ui.utils.PayUmoneyFlowManager;
import com.payumoney.sdkui.ui.utils.ResultModel;
import com.razorpay.Checkout;

import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CouceDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CouceDetailFragment extends Fragment implements IOnBackPressed {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    RecyclerView rv_videoes, demovideoes, rv_course_curriculam;
    int LAUNCH_SECOND_ACTIVITY = 1;
    // TODO: Rename and change types of parameters
    private String txnid;

    CourceDetails courceDetails;

    TextView tv_cource_title, tv_cource_info, tv_price, tv_price1, tv_price2, tv_price3, tv_price4, tv_cource_by, tv_descount, txt_video;
    ImageView iv_cource;
    ReadMoreTextView tv_desc;
    ImageView iv_back, iv_zoom, iv_pdf;
    ImageView video;
    private MediaController mediacontroller;
    private Uri uri;
    private boolean isContinuously = false;
    private ImageView progressBar;
    Button btn_buy, btn_demo;
    ImageView btn_wishlist;
    MyAppPrefsManager myAppPrefsManager;
    private String customerID;
    DialogLoader dialogLoader;
    Button cc_buy;
    private PayUmoneySdkInitializer.PaymentParam mPaymentParams;
    private boolean isDisableExitConfirmation = false;
    List<VideoDetails> movieList;
    private CustomVideoadapter mAdapter;
    private CourseCurriculamAdapter courseCurriculam;

    LinearLayout ll_rte;
    Button btn_subrte;
    RatingBar ratingBar;
    private String rating;
    LinearLayout ll_pdf;
    Database database;
    View recycler_demo_video_layout, btn_demo_layout;

    private ArrayList<DemoDetails> arrayList;
    private ArrayList<CourseCurriculamDetails> arrayList1;

    private String type;
    ImageView btn_share;
    EditText et_search;
    ImageView img_wishlist;
    ImageView iv_home;
    RelativeLayout relativeLayout;
    LinearLayout ll_fees;
    LinearLayout ll_video;
    LinearLayout ll_course_includes;
    LinearLayout ll_course_curriculam;

    public CouceDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CouceDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CouceDetailFragment newInstance(String param1, String param2) {
        CouceDetailFragment fragment = new CouceDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            courceDetails = (CourceDetails) getArguments().getSerializable("CourseDetail");
            type = getArguments().getString("type");

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_couce_detail, container, false);
        setRetainInstance(true);
        //  ll_rte = view.findViewById(R.id.ll_rte);
        //btn_subrte = view.findViewById(R.id.btn_subrte);
        // ratingBar = view.findViewById(R.id.ratingBar);
        tv_cource_title = view.findViewById(R.id.tv_cource_title);
        tv_cource_info = view.findViewById(R.id.tv_cource_info);
        tv_cource_by = view.findViewById(R.id.tv_cource_by);
        txt_video = view.findViewById(R.id.txt_video);
        ll_video = view.findViewById(R.id.ll_video);
        rv_videoes = view.findViewById(R.id.rv_videoes);
        rv_course_curriculam = view.findViewById(R.id.rv_course_curriculam);

        tv_desc = view.findViewById(R.id.tv_desc);
        iv_back = view.findViewById(R.id.iv_back);
        iv_pdf = view.findViewById(R.id.iv_pdf);
        iv_zoom = view.findViewById(R.id.iv_zoom);
        iv_cource = view.findViewById(R.id.iv_cource);
        tv_price = view.findViewById(R.id.tv_price);
        ll_pdf = view.findViewById(R.id.ll_pdf);
        tv_price1 = view.findViewById(R.id.tv_price1);
        /*tv_price2 = view.findViewById(R.id.tv_price2);
        tv_price3 = view.findViewById(R.id.tv_price3);
        tv_price4 = view.findViewById(R.id.tv_price4);*/
        relativeLayout = view.findViewById(R.id.relativeLayout);
        ll_fees = view.findViewById(R.id.ll_fees);
        tv_descount = view.findViewById(R.id.tv_descount);
        btn_buy = view.findViewById(R.id.btn_buynow);
        btn_demo = view.findViewById(R.id.btn_demo);
        btn_wishlist = view.findViewById(R.id.btn_wishlist);
        btn_share = view.findViewById(R.id.btn_share);
        //video = view.findViewById(R.id.video);
        progressBar = view.findViewById(R.id.progrss);
        btn_demo_layout = view.findViewById(R.id.view_demo);
        cc_buy = view.findViewById(R.id.btn_buynow);
        ll_course_includes = view.findViewById(R.id.ll_course_includes);
        ll_course_curriculam = view.findViewById(R.id.ll_course_curriculam);

        //recycler_demo_video_layout = view.findViewById(R.id.recycler_layout);
        demovideoes = view.findViewById(R.id.rv_demo_videoes);
        //et_search = view.findViewById(R.id.et_search);
        iv_home = view.findViewById(R.id.iv_home);
        iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent home = new Intent(getActivity(), HomeActivity.class);
                startActivity(home);
                getActivity().finish();


            }
        });


        img_wishlist = view.findViewById(R.id.img_wishlist);
        img_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Config.IS_USER_LOGGED_IN) {
                    Fragment fragment = new WishlistFragment();
                    loadFragment(fragment);
                } else {
                    Intent intent = new Intent(getActivity(), SelectRoleActivity.class);
                    startActivity(intent);
                }

            }
        });


       /* et_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new CourceListFragment();
                loadFragment(fragment);
            }
        });*/

        myAppPrefsManager = new MyAppPrefsManager(getActivity());
        dialogLoader = new DialogLoader(getActivity());
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
        sharedPreferences = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE);
        customerID = sharedPreferences.getString("userID", "");
        Log.e("user_id", customerID);

        editor = sharedPreferences.edit();
        editor.putString("cID", courceDetails.getId());
        editor.putString("cPrise", courceDetails.getPrice());
        editor.putString("cFlg", "");
        editor.apply();

        movieList = new ArrayList<>();


        /*store data in local database digital course*/
        database = new Database(getActivity());
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 1);
        layoutManager.setReverseLayout(false);
        rv_videoes.setLayoutManager(layoutManager);
        rv_videoes.setNestedScrollingEnabled(false);


        LinearLayoutManager linear = new LinearLayoutManager(getActivity(), RecyclerView.HORIZONTAL, true);
        // GridLayoutManager lv = new LinearLayout(getActivity(), 2);
        linear.setReverseLayout(false);
        demovideoes.setItemAnimator(new DefaultItemAnimator());
        demovideoes.setNestedScrollingEnabled(false);
        demovideoes.setLayoutManager(linear);

        getDemoes(courceDetails.getId());


        LinearLayoutManager course_curriculam = new LinearLayoutManager(getActivity(), RecyclerView.VERTICAL, false);
        // GridLayoutManager lv = new LinearLayout(getActivity(), 2);
        course_curriculam.setReverseLayout(false);
        rv_course_curriculam.setNestedScrollingEnabled(false);
        rv_course_curriculam.setLayoutManager(course_curriculam);


        btn_share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    Intent shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setType("text/plain");
                    shareIntent.putExtra(Intent.EXTRA_SUBJECT, "Digital Class");
                    String shareMessage = "Hey i am learning " + tv_cource_title.getText().toString() + " on Digital Class App by " + tv_cource_by.getText().toString() + " Tutor i recommend to join Digital Class App lots of Tutor available you can learn your course in just simple step Download the Digital Class App now and share with other to join \n\n";
                    shareMessage = shareMessage + "https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                    shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                    startActivity(Intent.createChooser(shareIntent, "choose one"));
                } catch (Exception e) {
                    //e.toString();
                }
            }
        });

        try {
            tv_cource_title.setText(courceDetails.getCourse_name());


        } catch (Exception e) {
            tv_cource_title.setText("Course Description");
        }
        tv_desc.setText(courceDetails.getDescription());
        tv_cource_by.setText(courceDetails.getName());
        tv_cource_info.setText("* Hours : " + courceDetails.getHours() + "\r\n* Duration : " + courceDetails.getMonth() + "\r\n* Chapter : " + courceDetails.getChapter() + "\r\n* Tutorial : " + courceDetails.getLanguage() + " Language\r\n* Certificate : " + courceDetails.getCertificated());
        tv_price.setText("\u20B9" + courceDetails.getPrice());
        tv_price1.setText("\u20B9" + courceDetails.getActual_price());
        tv_price1.setPaintFlags(tv_price1.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);

        /*tv_price2.setText("\u20B9" + courceDetails.getActual_price() + "/-");
        tv_price3.setPaintFlags(tv_price3.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
*/
        double price1 = Double.parseDouble(courceDetails.getPrice());
        double price2 = Double.parseDouble(courceDetails.getActual_price());
        double discount = (price1 / price2) * 100;
        discount = 100 - discount;

        tv_descount.setText(new DecimalFormat("##").format(discount) + "% Off");
        // tv_price4.setText(new DecimalFormat("##").format(discount) + "% Off");





  /*      if (courceDetails.getContent_image() != null && courceDetails.getContent_image().length() > 0) {

//            Picasso.get().load("https://digitalclassworld.com/storage/app/" + courceDetails.getContent_image()).into(iv_cource);
        } else {

            Picasso.get().load("https://digitalclassworld.com/storage/app/" + courceDetails.getImage()).into(iv_cource);
        }
  */      //Picasso.get().load("https://digitalclassworld.com/storage/app/" + courceDetails.getImage()).into(video);
        Checkout.preload(getActivity());

//        mediacontroller = new MediaController(getActivity());
//        mediacontroller.setAnchorView(video);
        final String uriPath = "https://digitalclassworld.com/storage/app/" + courceDetails.getVideo(); //update package name
//        uri = Uri.parse(uriPath);
//        progressBar.setVisibility(View.VISIBLE);
//        video.setVideoURI(uri);
//        video.start();


        arrayList = new ArrayList<>();
        arrayList1 = new ArrayList<>();

//        getDemoes();
        if (courceDetails.getWidh_list().equals("yes")) {
            btn_wishlist.setEnabled(false);
        }

        if (courceDetails.getBuy_now().equals("yes")) {
            btn_buy.setEnabled(false);
            btn_buy.setVisibility(View.GONE);
            btn_wishlist.setVisibility(View.GONE);
            btn_demo.setVisibility(View.GONE);
            cc_buy.setVisibility(View.GONE);
            ll_fees.setVisibility(View.GONE);
            ll_course_includes.setVisibility(View.GONE);

            //  ll_course_curriculam.setVisibility(View.VISIBLE);
            // tv_cource_info.setText("* Hours : " + courceDetails.getHours() + "\r\n* Duration : " + courceDetails.getMonth() + "\r\n* Chapter : " + courceDetails.getChapter() + "\r\n* Tutorial : " + courceDetails.getLanguage() + " Language\r\n* Certificate : " + courceDetails.getCertificated());

            /*if (courceDetails.getPdf_file().equals("")) {

                iv_pdf.setVisibility(View.GONE);
                ll_pdf.setVisibility(View.GONE);


            } else {

                iv_pdf.setVisibility(View.VISIBLE);
                ll_pdf.setVisibility(View.VISIBLE);


            }*/


            getVideoes(courceDetails.getCorses_id());

            demovideoes.setVisibility(View.GONE);
            //recycler_demo_video_layout.setVisibility(View.GONE);
            //btn_demo_layout.setVisibility(View.GONE);

        } else {
            if (type.equals("wishlist")) {
                btn_wishlist.setVisibility(View.GONE);
                getDemoes(courceDetails.getCorses_id());
            }

            demovideoes.setVisibility(View.VISIBLE);
            rv_videoes.setVisibility(View.GONE);
            btn_demo_layout.setVisibility(View.VISIBLE);
        }


        iv_pdf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://digitalclassworld.com/storage/app/" + courceDetails.getPdf_file()));
                startActivity(browserIntent);
            }
        });
        /*progressBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), FullVideoActivity.class);
                intent.putExtra("url", uriPath);
                startActivity(intent);
            }
        });*/
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).onBackPressed();
            }
        });

        /*iv_zoom.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder mBuilder = new AlertDialog.Builder(getActivity());
                View mView = getLayoutInflater().inflate(R.layout.dialog_custom_layout, null);
                PhotoView photoView = mView.findViewById(R.id.imageView);

                if (courceDetails.getContent_image() != null && courceDetails.getContent_image().length() > 0) {
                    Picasso.get().load("https://digitalclassworld.com/storage/app/" + courceDetails.getContent_image()).into(photoView);
                }

                Picasso.get().load("https://digitalclassworld.com/storage/app/" + courceDetails.getImage()).into(photoView);
                mBuilder.setView(mView);
                AlertDialog mDialog = mBuilder.create();
                mDialog.show();
            }
        });*/

        btn_wishlist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Config.IS_USER_LOGGED_IN) {
                    addToWishlist(customerID);
                } else {
                    editor.putString("cFlg", "w");
                    editor.apply();
                    Intent in = new Intent(getActivity(), SelectRoleActivity.class);
                    startActivity(in);
                }
            }
        });
        btn_buy.setText("Buy Now " + tv_price.getText().toString() + "\n" + tv_descount.getText().toString());
        btn_buy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Config.IS_USER_LOGGED_IN) {
//                    Fragment fragment = new BuyNowFragment();
//                    Bundle bundle = new Bundle();
//                    bundle.putSerializable("courceDetails", courceDetails);
//                    fragment.setArguments(bundle);
//                    FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
//                    transaction.replace(R.id.frame_container, fragment);
//                    transaction.addToBackStack(null);
//                    transaction.commit();
//                    buyNow(customerID);

//                    launchPayUMoneyFlow();

                    if (courceDetails.getFee_type().equals("free")) {
                        freeCourse(customerID);
                    }

                    if (courceDetails.getFee_type().equals("paid")) {
                        startPayment(courceDetails.getPrice());
                    }
                } else {
                    editor.putString("cFlg", "w");
                    editor.apply();
                    Intent in = new Intent(getActivity(), SelectRoleActivity.class);
                    startActivity(in);
                }
            }
        });


//        btn_demo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
////                Intent intent = new Intent(getActivity(),DemoVideoActivity.class);
////                intent.putExtra("course_id", courceDetails.getId());
////                startActivity(intent);
////                Intent intent = new Intent(getActivity(), FullVideoActivity.class);
////                intent.putExtra("url", uriPath);
////                startActivity(intent);
//            }
//        });
        return view;
    }

    private void giveRte(String rating) {
        Call<ResponseModel> call = APIClient.getInstance()
                .getRteList(customerID, courceDetails.getCourse_id(), rating);

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {
                if (response.isSuccessful()) {
                    btn_subrte.setVisibility(View.GONE);
                    ratingBar.setIsIndicator(true);
                    // Get the User Details from Response
                    Toast.makeText(getActivity(), response.body().getData(), Toast.LENGTH_LONG).show();

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
            }
        });
    }

    private void getVideoes(String id) {
        Call<VideoData> call = APIClient.getInstance()
                .getVideoList(id, customerID);


        call.enqueue(new Callback<VideoData>() {
            @Override
            public void onResponse(Call<VideoData> call, Response<VideoData> response) {
                if (response.isSuccessful()) {
                    // Get the User Details from Response
                    movieList = response.body().getMsg();
                    if (movieList.size() > 0) {
                        String course_name = response.body().getCourse_name();
                        String view_video = response.body().getView_video();
                        List<ResultDetails> resultDetails = new ArrayList<>();
                        resultDetails = response.body().getResult();
                        ll_video.setVisibility(View.VISIBLE);
                        txt_video.setVisibility(View.VISIBLE);
                        rv_videoes.setVisibility(View.VISIBLE);


                        for (int position = 0; position < 1; position++) {
                            String result = view_video.substring(view_video.lastIndexOf("/") + 1);
//
                            boolean success = !database.check_Course_id(movieList.get(position).getCourse_id());
                            if (success) {
                                CountVideo video = new CountVideo(Integer.parseInt(movieList.get(position).getCourse_id()), Integer.parseInt(result));
                                long id = database.store_course_id(video);
                                if (id != -1) {
                                    Log.e("TAG", "success");
                                } else {
                                    Log.e("TAG", "database error");
                                }
                            }


                        }


                        mAdapter = new CustomVideoadapter(getActivity(), movieList, course_name, view_video, resultDetails, "yes");
                        rv_videoes.setAdapter(mAdapter);
                    }


                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<VideoData> call, Throwable t) {
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result Code is -1 send from Payumoney activity
        if (requestCode == PayUmoneyFlowManager.REQUEST_CODE_PAYMENT && resultCode == RESULT_OK && data !=
                null) {
            TransactionResponse transactionResponse = data.getParcelableExtra(PayUmoneyFlowManager
                    .INTENT_EXTRA_TRANSACTION_RESPONSE);

            ResultModel resultModel = data.getParcelableExtra(PayUmoneyFlowManager.ARG_RESULT);

            // Check which object is non-null
            if (transactionResponse != null && transactionResponse.getPayuResponse() != null) {
                if (transactionResponse.getTransactionStatus().equals(TransactionResponse.TransactionStatus.SUCCESSFUL)) {
                    //Success Transaction
                    String payuResponse = transactionResponse.getPayuResponse();
                    if (payuResponse.contains("success")) {
//                        Toast.makeText(getActivity(), "Payment Succesfully!!", Toast.LENGTH_SHORT).show();
                        buyNow(customerID);
                    } else {
                        Toast.makeText(getActivity(), "Payment Failed", Toast.LENGTH_LONG).show();
                    }
//                    buyNow(customerID);
                } else {
                    //Failure Transaction
                    Toast.makeText(getActivity(), "Payment Failed", Toast.LENGTH_LONG).show();

                }

            } else if (resultModel != null && resultModel.getError() != null) {
                Log.d("PUMONEY", "Error response : " + resultModel.getError().getTransactionResponse());
            } else {
                Log.d("PUMONEY", "Both objects are null!");
            }
        }

        if (requestCode == LAUNCH_SECOND_ACTIVITY) {
            if (resultCode == Activity.RESULT_OK) {
                String result = data.getStringExtra("result");
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }
    }

    private void launchPayUMoneyFlow() {

        PayUmoneyConfig payUmoneyConfig = PayUmoneyConfig.getInstance();

        //Use this to set your custom text on result screen button

        //Use this to set your custom title for the activity
        payUmoneyConfig.setPayUmoneyActivityTitle("PayUMoney Digital Class");

        payUmoneyConfig.disableExitConfirmation(isDisableExitConfirmation);

        PayUmoneySdkInitializer.PaymentParam.Builder builder = new PayUmoneySdkInitializer.PaymentParam.Builder();

        double amount = 0;
        try {
            amount = Double.parseDouble(courceDetails.getPrice());

        } catch (Exception e) {
            e.printStackTrace();
        }
        txnid = "TXNID" + System.currentTimeMillis() + "";
        //String txnId = "TXNID720431525261327973";
        String phone = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userMobile", "");
        ;
        String productName = courceDetails.getName();
        String firstName = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userName", "");
        ;
        String email = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userMail", "");
        ;
        String udf1 = "";
        String udf2 = "";
        String udf3 = "";
        String udf4 = "";
        String udf5 = "";
        String udf6 = "";
        String udf7 = "";
        String udf8 = "";
        String udf9 = "";
        String udf10 = "";

        AppEnvironment appEnvironment = ((App) getActivity().getApplication()).getAppEnvironment();
        builder.setAmount(String.valueOf(amount))
                .setTxnId(txnid)
                .setPhone(phone)
                .setProductName(productName)
                .setFirstName(firstName)
                .setEmail(email)
                .setsUrl(appEnvironment.surl())
                .setfUrl(appEnvironment.furl())
                .setUdf1(udf1)
                .setUdf2(udf2)
                .setUdf3(udf3)
                .setUdf4(udf4)
                .setUdf5(udf5)
                .setUdf6(udf6)
                .setUdf7(udf7)
                .setUdf8(udf8)
                .setUdf9(udf9)
                .setUdf10(udf10)
                .setIsDebug(appEnvironment.debug())
                .setKey(appEnvironment.merchant_Key())
                .setMerchantId(appEnvironment.merchant_ID());

        try {
            mPaymentParams = builder.build();

            /*
             * Hash should always be generated from your server side.
             * */
            //    generateHashFromServer(mPaymentParams);

            /*            *//**
             * Do not use below code when going live
             * Below code is provided to generate hash from sdk.
             * It is recommended to generate hash from server side only.
             * */
            mPaymentParams = calculateServerSideHashAndInitiatePayment1(mPaymentParams);


            PayUmoneyFlowManager.startPayUMoneyFlow(mPaymentParams, getActivity(), R.style.AppTheme_default, true);


        } catch (Exception e) {
            // some exception occurred
            Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }


    private PayUmoneySdkInitializer.PaymentParam calculateServerSideHashAndInitiatePayment1(final PayUmoneySdkInitializer.PaymentParam paymentParam) {

        StringBuilder stringBuilder = new StringBuilder();
        HashMap<String, String> params = paymentParam.getParams();
        stringBuilder.append(params.get(PayUmoneyConstants.KEY) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.TXNID) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.AMOUNT) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.PRODUCT_INFO) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.FIRSTNAME) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.EMAIL) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF1) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF2) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF3) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF4) + "|");
        stringBuilder.append(params.get(PayUmoneyConstants.UDF5) + "||||||");

        AppEnvironment appEnvironment = ((App) getActivity().getApplication()).getAppEnvironment();
        stringBuilder.append(appEnvironment.salt());

        String hash = hashCal(stringBuilder.toString());
        paymentParam.setMerchantHash(hash);

        return paymentParam;
    }

    public static String hashCal(String str) {
        byte[] hashseq = str.getBytes();
        StringBuilder hexString = new StringBuilder();
        try {
            MessageDigest algorithm = MessageDigest.getInstance("SHA-512");
            algorithm.reset();
            algorithm.update(hashseq);
            byte messageDigest[] = algorithm.digest();
            for (byte aMessageDigest : messageDigest) {
                String hex = Integer.toHexString(0xFF & aMessageDigest);
                if (hex.length() == 1) {
                    hexString.append("0");
                }
                hexString.append(hex);
            }
        } catch (NoSuchAlgorithmException ignored) {
        }
        return hexString.toString();
    }

    private void addToWishlist(String customerID) {
        dialogLoader.showProgressDialog();

        Call<ResponseModel> call = APIClient.getInstance()
                .addToWishList
                        (
                                customerID,
                                courceDetails.getId()
                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getData().equalsIgnoreCase("Whislist add successfully")) {
                        // Get the User Details from Response
                        Toast.makeText(getActivity(), "Added to Wishlist!", Toast.LENGTH_LONG).show();
//                        btn_wishlist.setText("Added to Wishlist");
                        btn_wishlist.setEnabled(false);
                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        // Get the Error Message from Response
                        Toast.makeText(getActivity(), response.body().getData(), Toast.LENGTH_LONG).show();
                    } else {
//                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_SHORT).show();
                        Toast.makeText(getActivity(), "Wishlist added successfully", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void buyNow(String customerID) {
        dialogLoader.showProgressDialog();
        Call<ResponseModel> call = APIClient.getInstance()
                .buyNow
                        (
                                customerID,
                                courceDetails.getId(),
                                courceDetails.getPrice(),
                                txnid
                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getData().equalsIgnoreCase("Your Tranzection successfully")) {
                        // Get the User Details from Response
                        Toast.makeText(getActivity(), "Your Transaction successfully!", Toast.LENGTH_LONG).show();
                        getActivity().finish();
                        startActivity(getActivity().getIntent());
                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        // Get the Error Message from Response
                        Toast.makeText(getActivity(), response.body().getData(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();

            }
        });
    }

    public void startPayment(String price) {
        /*
          You need to pass current activity in order to let Razorpay create CheckoutActivity
         */
        String phone = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userMobile", "");
        ;
        String productName = courceDetails.getName();
        String firstName = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userName", "");
        ;
        String email = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userMail", "");
        ;
        price += "00";
        final Activity activity = getActivity();

        final Checkout co = new Checkout();

        try {
            JSONObject options = new JSONObject();
            options.put("name", "Digital Class");
            options.put("description", productName);
            //You can omit the image option to fetch the image from dashboard
            options.put("image", "https://digitalclassworld.com//storage/app/courses/Logo.png");
            options.put("currency", "INR");
            options.put("amount", price);

            JSONObject preFill = new JSONObject();
            preFill.put("email", email);
            preFill.put("contact", phone);

            options.put("prefill", preFill);

            co.open(activity, options);
        } catch (Exception e) {
            Toast.makeText(activity, "Error in payment: " + e.getMessage(), Toast.LENGTH_SHORT)
                    .show();
            e.printStackTrace();
        }
    }


    @Override
    public boolean onBackPressed() {
        return true;
    }


    private void getDemoes(String id) {

        Log.e("CourseDetailFragemnet","calling method");

        dialogLoader.showProgressDialog();
        Call<DemoModel> call = APIClient.getInstance().getDemoList(id);
        call.enqueue(new Callback<DemoModel>() {
            @Override
            public void onResponse(Call<DemoModel> call, Response<DemoModel> response) {
                if (response.isSuccessful()) {
                    dialogLoader.hideProgressDialog();
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        List<DemoDetails> details = response.body().getMsg();
//                        DemoDetails demo=new DemoDetails(video_id,corse_id,type,videolink,image);
                        arrayList.addAll(details);
                        Log.e("CourseDetailFragemnet","calling method"+details);
                    }
                    if (arrayList != null && arrayList.size() > 0) {
//                        recycler_demo_video_layout.setVisibility(View.VISIBLE);
                        Log.e("CourseDetailFragemnet","calling method"+arrayList);
                        DemoListAdapter adapter = new DemoListAdapter(getActivity(), arrayList, id);
                        demovideoes.setAdapter(adapter);
                    }

                }

            }

            @Override
            public void onFailure(Call<DemoModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
            }
        });
    }


    private void freeCourse(String customerID) {
        dialogLoader.showProgressDialog();
        Call<ResponseModel> call = APIClient.getInstance()
                .free_course_subscriptions
                        (
                                customerID,
                                courceDetails.getId(),
                                courceDetails.getPrice(),
                                courceDetails.getFee_type()
                        );

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getData().equalsIgnoreCase("Your course addded successfully")) {
                        // Get the User Details from Response
                        Toast.makeText(getActivity(), "Your course addded successfully!", Toast.LENGTH_LONG).show();
                        getActivity().finish();
                        startActivity(getActivity().getIntent());
                    } else if (response.body().getSuccess().equalsIgnoreCase("0")) {
                        // Get the Error Message from Response
                        Toast.makeText(getActivity(), response.body().getData(), Toast.LENGTH_LONG).show();
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();

            }
        });
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void getcourseCurriculum() {

        Call<CourseCurriculam> call = APIClient.getInstance()
                .getcourseCurriculam();

        call.enqueue(new Callback<CourseCurriculam>() {
            @Override
            public void onResponse(Call<CourseCurriculam> call, Response<CourseCurriculam> response) {
                if (response.isSuccessful()) {

                    Log.d("CourseCurriculam", "SuccessFull" + response.message());


                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        List<CourseCurriculamDetails> details = response.body().getData();

                        arrayList1.addAll(details);


                        if (arrayList1 != null && arrayList1.size() > 0) {
                            Log.d("CourseCurriculam", "Total items" + details);
                            ll_course_curriculam.setVisibility(View.VISIBLE);
                            CourseCurriculamAdapter courseCurriculamAdapter = new CourseCurriculamAdapter(getActivity(), arrayList1);
                            rv_course_curriculam.setAdapter(courseCurriculamAdapter);
                        } else {

                            ll_course_curriculam.setVisibility(View.GONE);
                        }

                    }

                } else {
                    Log.d("CourseCurriculam", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CourseCurriculam> call, Throwable t) {
                Toast.makeText(getActivity(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });

    }

}
