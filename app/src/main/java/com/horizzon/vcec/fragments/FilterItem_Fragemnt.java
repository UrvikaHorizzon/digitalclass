package com.horizzon.vcec.fragments;

import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.adapter.LanguageAdapter;
import com.horizzon.vcec.adapter.checkboxAdapter;
import com.horizzon.vcec.model.CategoryData;
import com.horizzon.vcec.model.CategoryModel;
import com.horizzon.vcec.network.APIClient;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FilterItem_Fragemnt extends Fragment implements View.OnClickListener {


    ImageView backbutton;
    RecyclerView recyclerView,recyclerView_language;

    checkboxAdapter adapter;
    LanguageAdapter languageAdapter;
    TextView count_price, count_fee_type, count_language, count_certificate, count_rating, count_tutors, clear_button;
    LinearLayout layout_price, layout_fee_type, layout_language, layout_certificate, layout_rating, layout_tutors;
    LinearLayout layout_item_price, layout_item_fees, layout_item_langiage, layout_item_certificate,layout_item_tutors;
    FrameLayout layout_item_rating;
    RatingBar ratingBar;
    Button apply_button;
    //    private RadioGroup rg_filter, rg_lnguge, rg_feetype, rg_cerificte;
    private RadioButton radioButton;
    String prise = "", language = "", certificated = "", fee_type = "", rating = "", discount = "", tutoril = "''",cat_id="";
    View view;
    CheckBox price_low, price_heigh, free, paid, certificate_yes, certificate_no,hindi, english;
    boolean price_checked, type_checked, langugage_checked, certificate_checked, rating_checked, tutors_checked;
    ArrayList<CategoryModel> title_list;
    ProgressBar progressBar;
    List<CategoryModel>languageList;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        if(view == null) {
            view = inflater.inflate(R.layout.fileter_fragment_layout, container, false);
            init(view);
        }
        return view;

    }



    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        loadData();
    }

    private void init(View view) {
        // count textview
        recyclerView = view.findViewById(R.id.recycler_view);
        recyclerView_language=view.findViewById(R.id.recycler_view_language);
        progressBar=view.findViewById(R.id.pb);
        count_tutors = view.findViewById(R.id.value_six);
        count_price = view.findViewById(R.id.value_first);
        count_fee_type = view.findViewById(R.id.value_second);
        count_language = view.findViewById(R.id.value_third);
        count_certificate = view.findViewById(R.id.value_four);
        //count_rating = view.findViewById(R.id.value_fifth);
        clear_button = view.findViewById(R.id.clear_button);
        layout_price = view.findViewById(R.id.layout_first);
        layout_fee_type = view.findViewById(R.id.layout_second);
        layout_language = view.findViewById(R.id.layout_third);
        layout_certificate = view.findViewById(R.id.layout_fourth);
       // layout_rating = view.findViewById(R.id.layout_fifth);
        layout_tutors = view.findViewById(R.id.layout_sixth);
        hindi = view.findViewById(R.id.ch_hindi);
        english = view.findViewById(R.id.ch_eng);
        // layout item
        layout_item_price = view.findViewById(R.id.item_layout_first);
        layout_item_fees = view.findViewById(R.id.item_layout_second);
        layout_item_langiage = view.findViewById(R.id.item_layout_third);
        layout_item_certificate = view.findViewById(R.id.item_layout_fourth);
        layout_item_rating = view.findViewById(R.id.item_layout_fifth);
        layout_item_tutors = view.findViewById(R.id.item_layout_sixth);
        price_low = view.findViewById(R.id.price_low);
        price_heigh = view.findViewById(R.id.price_high);
        free = view.findViewById(R.id.ch_free);
        paid = view.findViewById(R.id.ch_paid);
        certificate_yes = view.findViewById(R.id.ch_yes);
        certificate_no = view.findViewById(R.id.ch_no);
        ratingBar = view.findViewById(R.id.ratingBar);
        backbutton = view.findViewById(R.id.back_btn);
        apply_button = view.findViewById(R.id.btn_apply);
        ratingBar = view.findViewById(R.id.ratingBar);




    }

    public void loadData(){

        layout_price.setOnClickListener(this);
        layout_fee_type.setOnClickListener(this);
        layout_language.setOnClickListener(this);
        layout_certificate.setOnClickListener(this);
      //  layout_rating.setOnClickListener(this);
        layout_tutors.setOnClickListener(this);
        clear_button.setOnClickListener(this);
        apply_button.setOnClickListener(this);

        languageList=new ArrayList<>();
        title_list=new ArrayList<>();



        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true);
        layoutManager.setReverseLayout(false);
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, true);
        layoutManager1.setReverseLayout(false);
        recyclerView.setLayoutManager(layoutManager1);
        recyclerView_language.setLayoutManager(layoutManager);
        adapter = new checkboxAdapter(getActivity(),title_list);
        languageAdapter = new LanguageAdapter(getActivity(),languageList);

        getTutorils();
        getLanguages();
        price_checked = true;
        count_price.setText("");
        count_tutors.setText("");
        count_fee_type.setText("");
        count_language.setText("");
        count_certificate.setText("");
//        count_rating.setText("");

        price_heigh.setChecked(false);
        price_low.setChecked(false);
        paid.setChecked(false);
        free.setChecked(false);
        certificate_yes.setChecked(false);
        certificate_no.setChecked(false);


        layout_price.setVisibility(View.VISIBLE);
        layout_item_fees.setVisibility(View.GONE);
        layout_item_langiage.setVisibility(View.GONE);
        layout_item_certificate.setVisibility(View.GONE);
        layout_item_rating.setVisibility(View.GONE);
        layout_item_tutors.setVisibility(View.GONE);

        layout_price.setBackgroundColor(getResources().getColor(R.color.white));
        layout_fee_type.setBackgroundColor(getResources().getColor(R.color.white));
        layout_certificate.setBackgroundColor(getResources().getColor(R.color.white));
        layout_language.setBackgroundColor(getResources().getColor(R.color.white));
        layout_tutors.setBackgroundColor(getResources().getColor(R.color.white));
//        layout_rating.setBackgroundColor(getResources().getColor(R.color.white));
        layout_item_price.setVisibility(View.VISIBLE);
        ratingBar.setRating(0.0f);

        price_low.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    price_heigh.setChecked(false);
                }
            }
        });

        price_heigh.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    price_low.setChecked(false);
                }
            }
        });


        free.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    paid.setChecked(false);
                }
            }
        });


        paid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    free.setChecked(false);
                }
            }
        });


//        hindi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    english.setChecked(false);
//                }
//            }
//        });
//
//
//        english.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            @Override
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                if (isChecked) {
//                    hindi.setChecked(false);
//                }
//            }
//        });

        certificate_no.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    certificate_yes.setChecked(false);
                }
            }
        });


        certificate_yes.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    certificate_no.setChecked(false);
                }
            }
        });

        backbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new FeatureFragment();
                FragmentTransaction transaction = ((HomeActivity) getActivity()).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });


    }


    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.layout_first:
                layout_price.setVisibility(View.VISIBLE);
                layout_price.setBackgroundColor(getResources().getColor(R.color.white));
                layout_item_price.setVisibility(View.VISIBLE);


                if (type_checked) {
                    secondItem();
                }

                if (langugage_checked) {
                    thridItem();
                }
               if (certificate_checked) {
                    fourthItem();
                }

                /*if (rating_checked) {
                    fifthItem();
                }*/

                if (tutors_checked){
                    sixthItem();
                }

                price_checked = true;

                break;

            case R.id.layout_second:
                layout_fee_type.setVisibility(View.VISIBLE);
                layout_fee_type.setBackgroundColor(getResources().getColor(R.color.white));
                layout_item_fees.setVisibility(View.VISIBLE);

                if (price_checked == true) {
                    firstItem();
                }
                if (langugage_checked) {
                    thridItem();

                }
                if (certificate_checked) {
                    fourthItem();
                }
                /*if (rating_checked) {
                    fifthItem();
                }*/

                if (tutors_checked){
                    sixthItem();
                }
                type_checked = true;
                break;


            case R.id.layout_third:
                layout_language.setVisibility(View.VISIBLE);
                layout_language.setBackgroundColor(getResources().getColor(R.color.white));
                layout_item_langiage.setVisibility(View.VISIBLE);
                if (price_checked) {
                    firstItem();
                    price_checked = false;

                }
                if (type_checked) {
                    secondItem();
                    type_checked = false;
                }
                if (certificate_checked) {
                    fourthItem();
                }
                /*if (rating_checked) {
                    fifthItem();
                }*/
                if (tutors_checked){
                    sixthItem();
                }

                langugage_checked = true;
                break;

            case R.id.layout_fourth:
                layout_certificate.setVisibility(View.VISIBLE);
                layout_certificate.setBackgroundColor(getResources().getColor(R.color.white));
                layout_item_certificate.setVisibility(View.VISIBLE);

                if (price_checked) {
                    firstItem();
                    price_checked = false;

                }
                if (type_checked) {
                    secondItem();
                    type_checked = false;
                }
                if (langugage_checked) {
                    thridItem();
                }

                if (tutors_checked){
                    sixthItem();
                }

                certificate_checked = true;

                break;

            /*case R.id.layout_fifth:
                layout_rating.setVisibility(View.VISIBLE);
                layout_rating.setBackgroundColor(getResources().getColor(R.color.white));
                layout_item_rating.setVisibility(View.VISIBLE);

                if (price_checked) {
                    firstItem();
                    price_checked = false;

                }
                if (type_checked) {
                    secondItem();
                    type_checked = false;
                }
                if (langugage_checked) {
                    thridItem();
                }

                if (certificate_checked) {
                    fourthItem();
                }

                if (tutors_checked){
                    sixthItem();
                }

                rating_checked = true;
                break;
            */
            case R.id.layout_sixth:
                layout_tutors.setVisibility(View.VISIBLE);
                layout_tutors.setBackgroundColor(getResources().getColor(R.color.white));
                layout_item_tutors.setVisibility(View.VISIBLE);

//                title_list=new ArrayList<>();
//                title_list.clear();
//                getTutorils();
//                adapter = new checkboxAdapter(getActivity(),title_list);
//                recyclerView.setAdapter(adapter);
//                adapter.notifyDataSetChanged();

                if (price_checked) {
                    firstItem();
                    price_checked = false;

                }
                if (type_checked) {
                    secondItem();
                    type_checked = false;
                }
                if (langugage_checked) {
                    thridItem();
                }

                if (certificate_checked) {
                    fourthItem();
                }

               /*if (rating_checked){
                    fifthItem();
                }*/

                tutors_checked = true;

                break;


            case R.id.clear_button:
                filterClear();
                break;

            case R.id.btn_apply:
                apply_data();
                break;


        }

    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    private void apply_data() {
        firstItem();
        secondItem();
        thridItem();
       // fourthItem();
      //  fifthItem();
        sixthItem();

        Fragment fragment = new CourceListFragment();
        Bundle bundle = new Bundle();
        bundle.putInt("key",1);
        bundle.putString("filter_type", prise);
        bundle.putString("fee_type", fee_type);
        bundle.putString("language", language);
        bundle.putString("certificated", certificated);
        bundle.putString("rating", "");
        bundle.putString("tutoril",tutoril);
        fragment.setArguments(bundle);
        FragmentTransaction transaction = ((HomeActivity) getActivity()).getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    private void filterClear() {
//        init(view);
        loadData();
    }


    public void firstItem() {
        layout_price.setBackgroundColor(getResources().getColor(R.color.white));
        layout_item_price.setVisibility(View.GONE);

        if (price_low.isChecked()) {
            prise = price_low.getText().toString().trim();
            count_price.setText(String.valueOf(1));
        } else if (price_heigh.isChecked()) {
            prise = price_heigh.getText().toString().trim();
            count_price.setText("1");
        } else {
            count_price.setText("");
        }

        price_checked = false;
    }

    private void secondItem() {
        layout_fee_type.setBackgroundColor(getResources().getColor(R.color.white));
        layout_item_fees.setVisibility(View.GONE);

        type_checked = false;

        if (paid.isChecked()) {
            fee_type = paid.getText().toString().trim();
            count_fee_type.setText("1");

        } else if (free.isChecked()) {
            fee_type = free.getText().toString().trim();
            count_fee_type.setText("1");

        } else {
            count_fee_type.setText("");
        }
    }

    private void thridItem() {
        layout_language.setBackgroundColor(getResources().getColor(R.color.white));
        layout_item_langiage.setVisibility(View.GONE);
        langugage_checked = false;

//        if (english.isChecked()) {
//            language ="English";
//            Toast.makeText(getActivity(), ""+language, Toast.LENGTH_SHORT).show();
//            count_language.setText("1");
//
//        } else if (hindi.isChecked()) {
//            language ="Gujarati";
//            count_language.setText("1");
//        } else {
//            count_language.setText("");
//        }

        int count=((LanguageAdapter)languageAdapter).totalCount();
        if (count>0){
            count_language.setText(String.valueOf(count));
        }else {
            count_language.setText("");
        }

        List<CategoryModel> stList = ((LanguageAdapter)languageAdapter).getStudentist();


        ArrayList<CategoryModel>list=new ArrayList<>();
        for (int i = 0; i < stList.size(); i++) {
            if (languageAdapter.getStudentist().get(i).isSelected()){
                CategoryModel singleStudent = stList.get(i);
                list.add(singleStudent);
            }
//            cat_id=stList.get(i).getId();
        }
        String[] array = new String[list.size()];
        for (int i=0;i<list.size();i++){
            array[i]=list.get(i).getLanguageName();
        }
        language = android.text.TextUtils.join(",",array);
    }


    public void fourthItem() {
        layout_certificate.setBackgroundColor(getResources().getColor(R.color.white));
        layout_item_certificate.setVisibility(View.GONE);

        if (certificate_yes.isChecked()) {
            certificated = certificate_yes.getText().toString().trim();
            count_certificate.setText(String.valueOf(1));
        } else if (certificate_no.isChecked()) {
            certificated = certificate_no.getText().toString().trim();
            count_certificate.setText("1");
        } else {
            count_certificate.setText("");
        }

        certificate_checked = false;
    }

    /*public void fifthItem() {
        layout_rating.setBackgroundColor(getResources().getColor(R.color.white));
        layout_item_rating.setVisibility(View.GONE);
        if (!String.valueOf(ratingBar.getRating()).equals("0.0")) {
            String rting = String.valueOf(ratingBar.getRating());
            if (rting.endsWith(".0")) {
                rting = rting.replace(".0", "");
                rating = rting;
                count_rating.setText(rating);
            } else {
                rating = rting;
                count_rating.setText(rating);
            }

        } else {
            count_rating.setText("");
        }
        rating_checked = false;

    }
*/
    public void sixthItem() {
        layout_tutors.setBackgroundColor(getResources().getColor(R.color.white));
        layout_item_tutors.setVisibility(View.GONE);
        rating_checked = false;

        int count=((checkboxAdapter)adapter).totalCount();
        if (count>0){
            count_tutors.setText(String.valueOf(count));
        }else {
            count_tutors.setText("");
        }

        List<CategoryModel> stList = ((checkboxAdapter)adapter).getStudentist();


        ArrayList<CategoryModel>list=new ArrayList<>();
        for (int i = 0; i < stList.size(); i++) {

            if (adapter.getStudentist().get(i).isSelected()){

                CategoryModel singleStudent = stList.get(i);
                list.add(singleStudent);
            }
//            cat_id=stList.get(i).getId();
        }


        String[] array = new String[list.size()];
       for (int i=0;i<list.size();i++){
           array[i]=list.get(i).getName();
       }

        tutoril = android.text.TextUtils.join(",",array);
        Log.e("TAG",tutoril);

//        tutoril = android.text.TextUtils.join(",", Collections.singleton(tutoril));
//        Toast.makeText(getActivity(),
//                "Selected Tutors: \n" + data, Toast.LENGTH_LONG)
//                .show();
    }





    private void getTutorils() {
//        progressBar.setVisibility(View.VISIBLE);
        Call<CategoryData> call = APIClient.getInstance()
                .getTutorialList();
        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {

                        title_list.clear();
                        adapter.clear();
                        // Get the User Details from Response
                        List<CategoryModel> categoryModelList = response.body().getData();

//                        items = new String[categoryModelList.size()];
//                        checkedItems = new boolean[categoryModelList.size()];
                        for (int i = 0; i < categoryModelList.size(); i++) {
//                            items[i] = categoryModelList.get(i).getName();
//                            checkedItems[i] = false;
                            CategoryModel model=new CategoryModel();
                            if (categoryModelList.get(i).getStatus().equalsIgnoreCase("1")){

                                model.setName(categoryModelList.get(i).getName());

                            }
                            title_list.add(model);

                        }
//                        progressBar.setVisibility(View.GONE);
                        Collections.sort(title_list, new Comparator<CategoryModel>() {
                            @Override
                            public int compare(CategoryModel lhs, CategoryModel rhs) {
                                // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                                return lhs.getName().compareTo(rhs.getName());
                            }
                        });


                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        adapter.setTotalCount(0);
                        count_tutors.setText("");
                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
                progressBar.setVisibility(View.GONE);
//                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });

    }


    private void getLanguages() {
        Call<CategoryData> call = APIClient.getInstance()
                .getLanguageList();

//        dialogLoader.hideProgressDialog();
        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                if (response.body().getSuccess().equalsIgnoreCase("1"))
                {

                    languageList.clear();
                    languageAdapter.clear();
                    List<CategoryModel> categoryModelList = response.body().getData();

//                        items = new String[categoryModelList.size()];
//                        checkedItems = new boolean[categoryModelList.size()]

                    for (int i = 0; i < categoryModelList.size(); i++) {
//                            items[i] = categoryModelList.get(i).getName();
//                            checkedItems[i] = false;
                        CategoryModel model=new CategoryModel();
                        if (categoryModelList.get(i).getLanguage_status().equalsIgnoreCase("1")){

                            model.setLanguageName(categoryModelList.get(i).getLanguageName());

                        }
                        languageList.add(model);

                    }

                    recyclerView_language.setAdapter(languageAdapter);
                    languageAdapter.notifyDataSetChanged();
                    languageAdapter.setTotalCount(0);
//                    count_language.setText("");
                }else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {

            }
        });

    }

}
