package com.horizzon.vcec.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.YouTubeVideoActivity;
import com.horizzon.vcec.adapter.CustomSubscriptadapter;
import com.horizzon.vcec.adapter.DemoVideoAdapter;
import com.horizzon.vcec.model.CommissionModel;
import com.horizzon.vcec.model.CourceData;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.model.DemoDetails;
import com.horizzon.vcec.model.DemoModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class TutoeWalletFragment extends Fragment implements SharedPreferences.OnSharedPreferenceChangeListener {


    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    EditText et_course;
    ImageView iv_back;
    DialogLoader dialogLoader;
    RecyclerView rv_cources;
    List<CourceDetails> movieList;
    private CustomSubscriptadapter mAdapter;
    private TextView tv_nofound, tv_cource_title;
    String course_nme;
    TextView txt_refercode, txt_earning, txt_save;
    SharedPreferences sharedPreferences;
    String tutor_id;


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TutoeSubscriptFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TutoeWalletFragment newInstance(String param1, String param2) {
        TutoeWalletFragment fragment = new TutoeWalletFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        // Inflate the layout for this fragment
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tutor_wallet, container, false);
        txt_earning = view.findViewById(R.id.txt_earning);
        txt_refercode = view.findViewById(R.id.txt_refercode);
        iv_back = view.findViewById(R.id.iv_back);
        txt_save = view.findViewById(R.id.txt_save);
        dialogLoader = new DialogLoader(getActivity());
        sharedPreferences = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE);
        course_nme = getActivity().getSharedPreferences("UserInfo", Context.MODE_PRIVATE).getString("userName", "");
        tutor_id = sharedPreferences.getString("userID", "");
//        sharedpref=getActivity().getSharedPreferences("wallet", MODE_PRIVATE);

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                int rupees = 0;
                ((HomeActivity) getActivity()).onBackPressed();

            }
        });


        movieList = new ArrayList<>();
        getCorces();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getSharedPreferences("wallet", MODE_PRIVATE)
                .registerOnSharedPreferenceChangeListener(this);
    }

    //    private void getCorces() {
//        dialogLoader.showProgressDialog();
//        Call<CourceData> call = APIClient.getInstance()
//                .getCCourceList(course_nme);
//
//        call.enqueue(new Callback<CourceData>() {
//            @Override
//            public void onResponse(Call<CourceData> call, Response<CourceData> response) {
//                dialogLoader.hideProgressDialog();
//                if (response.isSuccessful()) {
//                    if (response.body().getSuccess().equalsIgnoreCase("1"))
//                    {
//                        // Get the User Details from Response
//                        movieList = response.body().getData();
//                        if(movieList.size() > 0) {
//                            tv_nofound.setVisibility(View.GONE);
//                            rv_cources.setVisibility(View.VISIBLE);
//                            mAdapter = new CustomSubscriptadapter(getActivity(), movieList);
//                            rv_cources.setAdapter(mAdapter);
//
//                        } else {
//                            tv_nofound.setVisibility(View.VISIBLE);
//                            rv_cources.setVisibility(View.GONE);
//                        }
//                    }
//
//                } else {
//                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
//                    // Show the Error Message
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CourceData> call, Throwable t) {
//                dialogLoader.hideProgressDialog();
//                tv_nofound.setVisibility(View.VISIBLE);
//                rv_cources.setVisibility(View.GONE);
//            }
//        });
//    }

    private void getCorces() {

        dialogLoader.showProgressDialog();
        Call<CourceData> call = APIClient.getInstance()
                .getCCourceList(course_nme);
        call.enqueue(new Callback<CourceData>() {

            @Override
            public void onResponse(Call<CourceData> call, Response<CourceData> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        movieList = response.body().getData();
                    }

                    if (movieList.size() > 0) {
                        int rupees = 0;
                        for (int i = 0; i < movieList.size(); i++) {
                            rupees=rupees+Integer.parseInt(movieList.get(i).getPrice());
//                            editor.putInt("rupees",rupees);
//                            editor.apply();
//                            editor.commit();
                        }
                        getDemoes(tutor_id, rupees);


                    }


                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CourceData> call, Throwable t) {
                dialogLoader.hideProgressDialog();

            }
        });

    }

    @Override
    public void onDestroy() {
        dialogLoader.hideProgressDialog();
        txt_refercode.setText("0");
        txt_earning.setText("0");
        txt_save.setText("0");
        sharedPreferences.edit().clear();
        super.onDestroy();
    }


    private void getDemoes(String id, int rupees) {
        Call<DemoModel> call = APIClient.getInstance().getCommision(id);
        call.enqueue(new Callback<DemoModel>() {
            @Override
            public void onResponse(Call<DemoModel> call, Response<DemoModel> response) {
                if (response.isSuccessful()) {
//                    dialogLoader.hideProgressDialog();
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {


                        List<CommissionModel> list = new ArrayList<>();
                        list = response.body().getData();

                        for (int i = 0; i < 1; i++) {
//                            Log.e("discount",list.get(i).getCommission())

                            if (list.get(i).getCommission()!=null) {
                                txt_refercode.setText(rupees + " " + "Rupees");
                                calculateDiscount(rupees, Integer.parseInt(list.get(i).getCommission()));
                                dialogLoader.hideProgressDialog();
                            }else {
                                txt_refercode.setText("0");
                                txt_earning.setText("0");
                                txt_save.setText("0");
                                dialogLoader.hideProgressDialog();
                            }
                        }


                    }
                }
            }

            @Override
            public void onFailure(Call<DemoModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
            }
        });
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
//        if (key.equals("rupees")) {
//            int rupees= sharedPreferences.getInt("rupees", 0);
//            txt_refercode.setText(rupees + " " + "Rupees");
//            getDemoes(tutor_id,rupees);
//        }
    }

    private void calculateDiscount(int rupees, int percent) {

        double number = Double.parseDouble(String.valueOf(rupees));
        double percentage = Double.parseDouble(String.valueOf(percent));

        if (percentage >= 0 && percentage <= 100) {
            double savedprice = (percentage / 100.0) * number;
            double finalprice = number - savedprice;
            DecimalFormat precision = new DecimalFormat("0.00");
            txt_save.setText(precision.format(savedprice)+ " " + "Rupees");
            txt_earning.setText(precision.format(finalprice) + " " + "Rupees");
            dialogLoader.hideProgressDialog();
        } else {

        }
    }
}
