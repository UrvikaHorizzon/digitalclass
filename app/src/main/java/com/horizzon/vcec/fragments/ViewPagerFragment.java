package com.horizzon.vcec.fragments;

import android.app.Activity;
import android.app.FragmentManager;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.google.android.youtube.player.YouTubeThumbnailLoader;
import com.google.android.youtube.player.YouTubeThumbnailView;
import com.horizzon.vcec.R;
import com.horizzon.vcec.VideoListActivity;
import com.horizzon.vcec.model.DemoVideoModel;
import com.horizzon.vcec.model.ModelVideoList;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.model.VideoDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.FullScreenMediaController;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.horizzon.vcec.VideoListActivity.API_KEY;
import static com.horizzon.vcec.VideoListActivity.VIDEO_ID;

public class ViewPagerFragment extends Fragment implements View.OnClickListener {


//
//    private Unbinder unbinder;
//    View view;
//    String text;
//    private static final String VIDEO_URL = "video_key";
//    private static final String VIDEO_POSITION = "Position";
//    private TextView ivTextView;
//    private View rootView;
//    private Boolean isStarted = false;
//    private Boolean isVisible = false;
//    String url = "https://digitalclassworld.com/storage/app/";
//    String video_url = "";
//
//    VideoDetails details = new VideoDetails();
//    private MediaController mediaController;
//    FragmentManager fm;
//    String tag, pos;
//    YouTubePlayer youTubePlayer1;
//    private FragmentActivity myContext;
//    private YouTubePlayer YPlayer;
//    public static ViewPagerFragment newInstance(VideoDetails data, int position) {
//        ViewPagerFragment fragment = new ViewPagerFragment();
//        Bundle bundle = new Bundle(1);
//        bundle.putSerializable(VIDEO_URL, data);
//        bundle.putInt(VIDEO_POSITION, position);
//        fragment.setArguments(bundle);
//        return fragment;
//    }
//
//
//    @Override
//    public void onAttach(Activity activity) {
//
//        if (activity instanceof FragmentActivity) {
//            myContext = (FragmentActivity) activity;
//        }
//
//        super.onAttach(activity);
//    }
//
//
//    @Nullable
//    @Override
//    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//
//
//        if (view == null) {
//            view = inflater.inflate(R.layout.viewpager_view_layout, container, false);
//            unbinder = ButterKnife.bind(this, view);
//            init();
//            // Initialise your layout her
//
//            Toast.makeText(getActivity(), "hii", Toast.LENGTH_SHORT).show();
//        }
//        return view;
//    }
//
//    private void init() {
//
//        if (getUserVisibleHint()) {
//            fm = getActivity().getFragmentManager();
//            tag = YouTubePlayerSupportFragment.class.getSimpleName();
//            details = (VideoDetails) getArguments().getSerializable(VIDEO_URL);
//
//            String[] paths = details.getLink().split("/");
////            paths[3] = paths[3].replace("?showinfo=0","");
//            VIDEO_ID = paths[3].trim();
//
//            fm = getActivity().getFragmentManager();
//            tag = YouTubePlayerSupportFragment.class.getSimpleName();
//          FragmentTransaction ft = getChildFragmentManager().beginTransaction();
//            YouTubePlayerSupportFragment  playerFragment = YouTubePlayerSupportFragment.newInstance();
//            ft.replace(R.id.rv_videos, playerFragment, tag);
//            ft.commit();
//
//
//            playerFragment.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {
//                @Override
//                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
//
//                    if (!b) {
//                        YPlayer = youTubePlayer;
//                        YPlayer.setFullscreen(true);
//                        YPlayer.loadVideo(VIDEO_ID);
//                        YPlayer.play();
//                    }
//                }
//
//                @Override
//                public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
//
//                }
//            });
//
//
//        }
//
//
//    }



    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play_video:
//                if (null != youTubePlayer1 && !youTubePlayer1.isPlaying())
//                    youTubePlayer1.play();
                break;
            case R.id.pause_video:
//                if (null != youTubePlayer1 && youTubePlayer1.isPlaying())
//                    youTubePlayer1.pause();
                break;
        }
    }
}
