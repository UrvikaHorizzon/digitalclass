package com.horizzon.vcec.fragments;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.FileProvider;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.model.CategoryData;
import com.horizzon.vcec.model.CategoryModel;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.model.UserData;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.EditTextWithSuffix;
import com.horizzon.vcec.util.FileUtils;
import com.horizzon.vcec.util.ValidateInputs;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddCourceFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddCourceFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    CourceDetails courceDetails;
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    EditText et_cname, et_desc, et_demo, et_youtube_url, et_aprice, et_dprice, et_courseimg,
            et_coursefile, et_courseimg1;
    EditTextWithSuffix et_chapter, et_chours, et_month;
    AutoCompleteTextView et_franchise_type, et_firm_type, et_language, et_certificate, et_centernme, et_feetype;
    Button btn_signup;
    ImageView iv_back, iv_photo, iv_cphoto, iv_cphoto1, iv_pdf;
    DialogLoader dialogLoader;
    List<CategoryModel> categorydetails;
    List<CategoryModel> subcategorydetails;
    List<CategoryModel> tutorildetails;
    private String cat_id="", sub_cat_id="", video="", course_id="", centre_id="";
    String[] mediaColumns = {MediaStore.Video.Media._ID};
    private Activity mActivity;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private File file;
    private File cfile;
    private File cfile1;
    private File cfile2;
    int update_flag = 0;
    Call<ResponseModel> call;
    MultipartBody.Part fileToUpload=null;
    MultipartBody.Part fileToUpload1=null;
    MultipartBody.Part fileToUpload2=null;
    MultipartBody.Part fileToUpload3=null;
    MultipartBody.Part fileToUpload4=null;
    private String photo="";
    private String photo1="";
    private String photo2="";
    List<CategoryModel>languageList;
    public AddCourceFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddCourceFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddCourceFragment newInstance(String param1, String param2) {
        AddCourceFragment fragment = new AddCourceFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = getActivity();
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);

        }
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_cource, container, false);
        //checking the permission
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        et_aprice = view.findViewById(R.id.et_aprice);
        et_centernme = view.findViewById(R.id.et_centernme);
        et_dprice = view.findViewById(R.id.et_dprice);
        et_feetype = view.findViewById(R.id.et_feetype);
        et_courseimg = view.findViewById(R.id.et_courseimg);
        iv_cphoto = view.findViewById(R.id.iv_cphoto);
        iv_cphoto1 = view.findViewById(R.id.iv_cphoto1);
        et_certificate = view.findViewById(R.id.et_certificate);
        et_chapter = view.findViewById(R.id.et_chapter);
        et_chours = view.findViewById(R.id.et_chours);
        et_cname = view.findViewById(R.id.et_cname);
        et_demo = view.findViewById(R.id.et_demo);
        et_desc = view.findViewById(R.id.et_desc);
        et_firm_type = view.findViewById(R.id.et_firm_type);
        et_franchise_type = view.findViewById(R.id.et_franchise_type);
        et_language = view.findViewById(R.id.et_language);
        et_month = view.findViewById(R.id.et_month);
        et_youtube_url = view.findViewById(R.id.et_youtube_url);
        et_coursefile = view.findViewById(R.id.et_coursefile);
        et_courseimg1 = view.findViewById(R.id.et_courseimg1);
        btn_signup = view.findViewById(R.id.btn_signup);
        iv_back = view.findViewById(R.id.iv_back);
        iv_photo = view.findViewById(R.id.iv_photo);
        iv_pdf = view.findViewById(R.id.iv_pdf);
        centre_id = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");



        dialogLoader = new DialogLoader(getActivity());

        categorydetails = new ArrayList<>();
        languageList=new ArrayList<>();
        subcategorydetails = new ArrayList<>();
        tutorildetails = new ArrayList<>();

        getLanguages();

        final String[] mStringArray1 = new String[]{"Yes", "No"};
        ArrayAdapter<String> countryAdapter2 = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray1);
        et_certificate.setThreshold(50);//will start working from first character
        et_certificate.setAdapter(countryAdapter2);

        final String[] mStringArray2 = new String[]{"Free", "Paid"};
        ArrayAdapter<String> countryAdapter3 = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray2);
        et_feetype.setThreshold(50);//will start working from first character
        et_feetype.setAdapter(countryAdapter3);

        if (getArguments() != null) {
            courceDetails = (CourceDetails) getArguments().getSerializable("CourseDetail");
            update_flag = 1;
            if(courceDetails.getCertificated().equalsIgnoreCase("yes")) {
                et_certificate.setText("Yes");
            } else {
                et_certificate.setText("No");
            }
//            if(courceDetails.getLanguage().equalsIgnoreCase("English")) {
//                et_language.setSelection(0);
//            } else if(courceDetails.getLanguage().equalsIgnoreCase("Hindi")) {
//                et_language.setSelection(1);
//            } else {
//                et_certificate.setSelection(2);
//            }
//            et_certificate.setText(courceDetails.getCertificated());
            et_language.setText(courceDetails.getLanguage());
            et_chours.setText(courceDetails.getHours());
            et_chapter.setText(courceDetails.getChapter());
            et_desc.setText(courceDetails.getDescription());
            et_cname.setText(courceDetails.getName());
            et_month.setText(courceDetails.getMonth());
            if(courceDetails.getFee_type().equalsIgnoreCase("free")) {
                et_feetype.setText("Free");
            } else {
                et_feetype.setText("Paid");

            }
            et_dprice.setText(courceDetails.getPrice());
            et_aprice.setText(courceDetails.getActual_price());
            sub_cat_id = courceDetails.getMain_category_id();
            cat_id = courceDetails.getParent_id();
            course_id = courceDetails.getId();
            btn_signup.setText("Update Course");
            iv_cphoto.setVisibility(View.VISIBLE);
            iv_cphoto1.setVisibility(View.VISIBLE);
            et_courseimg.setText("Course Image Selected");
            et_courseimg1.setText("Course Content Image Selected");
            try {
                photo="selected1";
             Picasso.get().load("https://digitalclassworld.com/storage/app/"+courceDetails.getImage()).into(iv_cphoto);
             Picasso.get().load("https://digitalclassworld.com/storage/app/"+courceDetails.getContent_image()).into(iv_cphoto1);
                if(courceDetails.getVideo()!=null) {
                    et_demo.setText("Video Uploaded");
                    et_demo.setEnabled(false);
                }
            } catch (Exception e){

            }
            if(courceDetails.getPdf_file()!=null) {
                et_coursefile.setText("Course Content File Selected");
                iv_pdf.setVisibility(View.VISIBLE);

                iv_pdf.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://digitalclassworld.com/storage/app/"+courceDetails.getPdf_file()));
                        startActivity(browserIntent);
                    }
                });
            }

            et_courseimg.setEnabled(true);
            et_courseimg1.setEnabled(true);
            et_coursefile.setEnabled(true);
            et_demo.setEnabled(true);



        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).onBackPressed();
            }
        });

        et_certificate.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_certificate.showDropDown();
                et_certificate.requestFocus();
                return false;
            }
        });

        et_language.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_language.showDropDown();
                et_language.requestFocus();
                return false;
            }
        });

        et_franchise_type.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_franchise_type.showDropDown();
                et_franchise_type.requestFocus();
                return false;
            }
        });

        et_firm_type.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_firm_type.showDropDown();
                et_firm_type.requestFocus();
                return false;
            }
        });

        et_feetype.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_feetype.showDropDown();
                et_feetype.requestFocus();
                return false;
            }
        });

        et_centernme.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_centernme.showDropDown();
                et_centernme.requestFocus();
                return false;
            }
        });

        et_demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);
            }
        });

        et_courseimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 200);
            }
        });

        et_courseimg1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 201);
            }
        });

        et_coursefile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                try {
                    startActivityForResult(Intent.createChooser(intent, "Select PDF"), 202);
                } catch (ActivityNotFoundException e) {
                    // Instruct the user to install a PDF reader here, or something
                }

            }
        });

        et_franchise_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<categorydetails.size();i++) {
                    if(categorydetails.get(i).getName().equals(selection)) {
                        cat_id = categorydetails.get(i).getId();
                        getSubCategoryList(cat_id);
                        return;
                    }
                }
            }
        });

        et_firm_type.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<subcategorydetails.size();i++) {
                    if(subcategorydetails.get(i).getName().equals(selection)) {
                        sub_cat_id = subcategorydetails.get(i).getId();
                        return;
                    }
                }
            }
        });

        et_centernme.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                for (int i=0;i<tutorildetails.size();i++) {
                    if(tutorildetails.get(i).getName().equals(selection)) {
                        centre_id = tutorildetails.get(i).getId();
                        return;
                    }
                }
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isValidData = validateInfoForm();
                if (isValidData) {
//                    Log.d("RESPONSE", cat_id+",\n"+sub_cat_id+",\n"+centre_id+",\n"+et_centernme.getText().toString()+",\n"+
//                            et_aprice.getText().toString().trim()+",\n"+
//                            et_certificate.getText().toString().trim()+",\n"+
//                            et_chapter.getText().toString().trim()+",\n"+
//                            et_chours.getText().toString().trim()+",\n"+
//                            et_cname.getText().toString().trim()+",\n"+
//                            et_desc.getText().toString().trim()+",\n"+
//                            et_language.getText().toString().trim()+",\n"+
//                            et_month.getText().toString().trim()+",\n"+
//                            file+",\n"+
//                            cfile+",\n"+
//                            cfile1+",\n"+
//                            cfile2+",\n"
//                            );
                    registerData();
                }
            }
        });

        getCategories();
        return view;
    }

    private void getLanguages() {
        Call<CategoryData> call = APIClient.getInstance()
                .getLanguageList();

        dialogLoader.hideProgressDialog();
      call.enqueue(new Callback<CategoryData>() {
          @Override
          public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
              if (response.body().getSuccess().equalsIgnoreCase("1"))
              {
                  languageList=response.body().getData();

                  if(getActivity()!=null) {
                      String[] mStringArray = new String[languageList.size()];
                      for (int i=0;i<languageList.size();i++) {
                          mStringArray[i] = languageList.get(i).getLanguageName();
//                            if(!cat_id.equals("")){
//                                if(cat_id.equals(languageList.get(i).getId())){
//                                    et_franchise_type.setText(categorydetails.get(i).getName());
//                                }
//                            }
                      }
                      ArrayAdapter<String> countryAdapter1 = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
                      et_language.setThreshold(100);//will start working from first character
                      et_language.setAdapter(countryAdapter1);

                  }
              }else {
                  Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
              }
          }

          @Override
          public void onFailure(Call<CategoryData> call, Throwable t) {

          }
      });

    }

    private void registerData() {

        dialogLoader.showProgressDialog();
//        try {
//            RequestBody mFile = RequestBody.create(MediaType.parse("*/*"), file);
//            fileToUpload = MultipartBody.Part.createFormData("video", file.getName(), mFile);
//        } catch (Exception e){
//
//        }

        try {
            RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), cfile);
            fileToUpload1 = MultipartBody.Part.createFormData("image", cfile.getName(), mFile1);
        } catch (Exception e){

        }

        try {
            RequestBody mFile3 = RequestBody.create(MediaType.parse("application/pdf"), cfile2);
            fileToUpload3 = MultipartBody.Part.createFormData("pdf_file", cfile2.getName(), mFile3);
        } catch (Exception e){

        }

        try {
            RequestBody mFile2 = RequestBody.create(MediaType.parse("image/*"), cfile1);
            fileToUpload2 = MultipartBody.Part.createFormData("course_image", cfile1.getName(), mFile2);
        } catch (Exception e){
            try {
                RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), cfile);
                fileToUpload2 = MultipartBody.Part.createFormData("course_image", cfile.getName(), mFile1);
            } catch (Exception e1){

            }

        }

        RequestBody main_category_id = RequestBody.create(MediaType.parse("text/plain"), cat_id);
        RequestBody name = RequestBody.create(MediaType.parse("text/plain"), sub_cat_id);
        RequestBody mobile = RequestBody.create(MediaType.parse("text/plain"), et_cname.getText().toString().trim());
        RequestBody gender = RequestBody.create(MediaType.parse("text/plain"), et_desc.getText().toString().trim());
        RequestBody email = RequestBody.create(MediaType.parse("text/plain"), et_chapter.getText().toString().trim());
        RequestBody address = RequestBody.create(MediaType.parse("text/plain"), et_chours.getText().toString().trim());
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), et_month.getText().toString().trim());
        RequestBody bdate = RequestBody.create(MediaType.parse("text/plain"), et_language.getText().toString().trim());
        RequestBody pwd = RequestBody.create(MediaType.parse("text/plain"), et_certificate.getText().toString().trim());
        RequestBody aprice = RequestBody.create(MediaType.parse("text/plain"), et_aprice.getText().toString().trim());
        RequestBody dprice = RequestBody.create(MediaType.parse("text/plain"), et_dprice.getText().toString().trim());
        RequestBody feetype = RequestBody.create(MediaType.parse("text/plain"), et_feetype.getText().toString().trim());
        RequestBody cname = RequestBody.create(MediaType.parse("text/plain"), centre_id);
        RequestBody sequence = RequestBody.create(MediaType.parse("text/plain"), "0");



        if(update_flag == 1) {
            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), course_id);
            RequestBody cnme = RequestBody.create(MediaType.parse("text/plain"), courceDetails.getName());
//            RequestBody cold = null, pold = null, vold = null;
//            try {
//                cold = RequestBody.create(MediaType.parse("text/plain"), courceDetails.getImage());
//                vold = RequestBody.create(MediaType.parse("text/plain"), courceDetails.getVideo());
//                pold = RequestBody.create(MediaType.parse("text/plain"), courceDetails.getPdf_file());
//            } catch (Exception e){
//
//            }

            Log.e("id",  cat_id);
            Log.e("main_category_id", sub_cat_id);
            Log.e("name", String.valueOf(name));
            Log.e("aprice", String.valueOf(aprice));
            Log.e("feetype", String.valueOf(feetype));
            Log.e(" gender", String.valueOf(gender));
            Log.e(" gender", String.valueOf(gender));
            Log.e("mobile", String.valueOf(mobile));
            Log.e("email", String.valueOf(email));
            Log.e("address", String.valueOf(address));
            Log.e(" city", String.valueOf(city));
            Log.e("bdate", String.valueOf(bdate));
            Log.e("pwd", String.valueOf(pwd));
            Log.e("sequence", String.valueOf(sequence));
            Log.e("cname", String.valueOf(cname));
            call = APIClient.getInstance()
                    .updateCourse
                            (
                                    id,
                                    main_category_id, name, aprice, dprice, feetype,
                                    gender,gender,  mobile, email, address, city, bdate, pwd, sequence,cnme
                            );
        } else {
            call = APIClient.getInstance()
                    .registerCourse
                            (
                                    main_category_id, name, cname, aprice, dprice, feetype,
                                    gender, mobile, email, address, city, bdate, pwd, sequence,
                                    fileToUpload1, fileToUpload2, fileToUpload3
                            );
        }

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        Toast.makeText(getActivity(), response.body().getData().toString(), Toast.LENGTH_LONG).show();
                        ((HomeActivity)getActivity()).onBackPressed();
                    }
                    else if (response.body().getSuccess().equalsIgnoreCase("0"))
                    {
                        // Get the Error Message from Response
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!", Toast.LENGTH_LONG).show();
                    }
                    else {
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!",Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.Q)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            //the image URI
            Uri selectedImage = data.getData();
            video = "selected";

//            String pth = getPathFromUri(getActivity(), selectedImage);
//            Bitmap bitmap = BitmapFactory.decodeFile(pth);
            iv_photo.setVisibility(View.VISIBLE);
            iv_photo.setImageBitmap(getThumbnailPathForLocalFile(getActivity(), selectedImage));
            et_demo.setText("Video selected");
            Log.d("VIDEO", getPathFromUri(getActivity(), selectedImage));

            file = new File(getPathFromUri(getActivity(), selectedImage));

            //calling the upload file method after choosing the file
//            uploadFile(selectedImage, "My Image");
        } else  if (requestCode == 200 && resultCode == RESULT_OK && data != null) {
            //the image URI
            Uri selectedImage = data.getData();
            photo = "selected";

            String pth = getPathFromUri(getActivity(), selectedImage);
            Bitmap bitmap = BitmapFactory.decodeFile(pth);
            iv_cphoto.setVisibility(View.VISIBLE);
            iv_cphoto.setImageBitmap(bitmap);
            et_courseimg.setText("Course Image selected");

            cfile = new File(getPathFromUri(getActivity(), selectedImage));

            //calling the upload file method after choosing the file
//            uploadFile(selectedImage, "My Image");
        }else  if (requestCode == 201 && resultCode == RESULT_OK && data != null) {
            //the image URI
            Uri selectedImage = data.getData();
            photo1 = "selected";

            String pth = getPathFromUri(getActivity(), selectedImage);
            Bitmap bitmap = BitmapFactory.decodeFile(pth);
            iv_cphoto1.setVisibility(View.VISIBLE);
            iv_cphoto1.setImageBitmap(bitmap);
            et_courseimg1.setText("Course Content Image selected");

            cfile1 = new File(getPathFromUri(getActivity(), selectedImage));

            //calling the upload file method after choosing the file
//            uploadFile(selectedImage, "My Image");
        }else if(resultCode != RESULT_CANCELED){
                if (requestCode == 202 && resultCode == RESULT_OK && data != null) {
                    Uri selectedImage = data.getData();
                    Toast.makeText(getActivity(), ""+String.valueOf(selectedImage), Toast.LENGTH_SHORT).show();
                    photo2 = "selected";

                    iv_pdf.setVisibility(View.VISIBLE);
                    iv_pdf.setEnabled(false);
                    et_coursefile.setText("Course Content File selected");
                    cfile2 = new File( FileUtils.getPath(getActivity(),selectedImage));

                }
            }


            //calling the upload file method after choosing the file
//            uploadFile(selectedImage, "My Image");

    }

    // Providing Thumbnail For Selected Image
    public Bitmap getThumbnailPathForLocalFile(Activity context, Uri fileUri) {
        long fileId = getFileId(context, fileUri);
        return MediaStore.Video.Thumbnails.getThumbnail(context.getContentResolver(),
                fileId, MediaStore.Video.Thumbnails.MICRO_KIND, null);
    }

    // Getting Selected File ID
    public long getFileId(Activity context, Uri fileUri) {
        Cursor cursor = context.managedQuery(fileUri, mediaColumns, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
            return cursor.getInt(columnIndex);
        }
        return 0;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public String getPathFromUri(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private boolean validateInfoForm() {
        if (et_franchise_type.getText().toString().trim().isEmpty()) {
            et_franchise_type.setError("Please select category");
            return false;
        } else if (et_firm_type.getText().toString().trim().isEmpty()) {
            et_firm_type.setError("Please select sub category");
            return false;
        } else if (!ValidateInputs.isValidName(et_cname.getText().toString().trim())) {
            et_cname.setError("Please Enter Cource Name");
            return false;
        }  else if (et_chapter.getText().toString().trim().isEmpty()) {
            et_chapter.setError("Please Enter No. of Chapter");
            return false;
        } else if (et_chours.getText().toString().trim().isEmpty()) {
            et_chours.setError("Please Enter No.of Hours");
            return false;
        } else if (et_month.getText().toString().trim().isEmpty()) {
            et_month.setError("Please Enter No. of Month");
            return false;
        } else if (et_language.getText().toString().trim().isEmpty()) {
            et_language.setError("Please select language");
            return false;
        }else if (et_certificate.getText().toString().trim().isEmpty()) {
            et_certificate.setError("Please select certificate");
            return false;
        } else if (et_feetype.getText().toString().trim().isEmpty()) {
            et_feetype.setError("Please select fee type");
            return false;
        } else if (et_aprice.getText().toString().trim().isEmpty()) {
            et_aprice.setError("Please enter actual price");
            return false;
        }else if (et_dprice.getText().toString().trim().isEmpty()) {
            et_dprice.setError("Please enter discount price");
            return false;
        }else if (et_desc.getText().toString().trim().isEmpty()) {
            et_desc.setError("Please enter description");
            return false;
        }else if (!photo.contains("selected")) {
            et_courseimg.setError("Please upload course image");
            return false;
        }
//        }else if (!photo1.equals("selected")) {
//            et_courseimg1.setError("Please upload course content image");
//            return false;
//        }
        else {
            return true;
        }
    }

    private void getSubCategoryList(String cat_id) {
        Call<CategoryData> call = APIClient.getInstance()
                .getSubCategoryList(cat_id);

        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        subcategorydetails = response.body().getData();

                        if (subcategorydetails!=null && subcategorydetails.size()>0){
                            String[] mStringArray = new String[subcategorydetails.size()];
                            for (int i=0;i<subcategorydetails.size();i++) {
                                mStringArray[i] = subcategorydetails.get(i).getName();
                                if(!sub_cat_id.equals("")){
                                    if(sub_cat_id.equals(subcategorydetails.get(i).getId())){
                                        et_firm_type.setText(subcategorydetails.get(i).getName());
                                    }
                                }
                            }
                            if (mStringArray!=null && mStringArray.length>0){
                                ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(mActivity, R.layout.dropdown, mStringArray);
                                et_firm_type.setThreshold(100);//will start working from first character
                                et_firm_type.setAdapter(countryAdapter);//setting the adapter data into the AutoCompleteTextView
                            }
                        }
                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getCategories() {
        Call<CategoryData> call = APIClient.getInstance()
                .getCategoryList();

        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {

                dialogLoader.hideProgressDialog();
//                getTutorils();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        categorydetails = response.body().getData();
                        if(getActivity()!=null) {
                            String[] mStringArray = new String[categorydetails.size()];
                            for (int i=0;i<categorydetails.size();i++) {
                                mStringArray[i] = categorydetails.get(i).getName();
                                if(!cat_id.equals("")){
                                    if(cat_id.equals(categorydetails.get(i).getId())){
                                        et_franchise_type.setText(categorydetails.get(i).getName());
                                    }
                                }
                            }

                            ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
                            et_franchise_type.setThreshold(100);//will start working from first character
                            et_franchise_type.setAdapter(countryAdapter);//setting the adapter data into the AutoCompleteTextView
                            if(!cat_id.equals("")){
                                getSubCategoryList(cat_id);
                            }
                        }
                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    public String getRealPathFromURI (Uri contentUri) {
        String path = null;
        String[] proj = { MediaStore.MediaColumns.DATA };
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            path = cursor.getString(column_index);
        }
        cursor.close();
        return path;
    }

    private void getTutorils() {
        Call<CategoryData> call = APIClient.getInstance()
                .getTutorialList();

        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        tutorildetails = response.body().getData();
                        String[] mStringArray = new String[tutorildetails.size()];
                        for (int i=0;i<tutorildetails.size();i++) {
                            mStringArray[i] = tutorildetails.get(i).getName();
                        }
                        ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(mActivity, R.layout.dropdown, mStringArray);
                        et_centernme.setThreshold(100);//will start working from first character
                        et_centernme.setAdapter(countryAdapter);
                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
//                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

}
