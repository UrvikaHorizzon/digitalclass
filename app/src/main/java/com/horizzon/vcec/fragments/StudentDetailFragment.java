package com.horizzon.vcec.fragments;


import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.horizzon.vcec.Config;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.adapter.StudentDetailadapter;
import com.horizzon.vcec.model.StudentData;
import com.horizzon.vcec.model.StudentDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.MyAppPrefsManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link StudentDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class StudentDetailFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "CourseId";
    private static final String ARG_PARAM2 = "type";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String customerID="";
    RecyclerView rv_cources;
    List<StudentDetails> movieList;
    DialogLoader dialogLoader;
    private StudentDetailadapter mAdapter;
    ImageView iv_back;
    private MyAppPrefsManager myAppPrefsManager;
    int count = 0;


    public StudentDetailFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment StudentDetailFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static StudentDetailFragment newInstance(String param1, String param2) {
        StudentDetailFragment fragment = new StudentDetailFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_student_detail, container, false);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        customerID = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");
        rv_cources = view.findViewById(R.id.recycler_view);
        iv_back = view.findViewById(R.id.iv_back);

        movieList = new ArrayList<>();
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true);
        layoutManager1.setReverseLayout(false);
        rv_cources.setLayoutManager(layoutManager1);
        myAppPrefsManager = new MyAppPrefsManager(getActivity());
        dialogLoader = new DialogLoader(getActivity());
        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
        if(Config.IS_USER_LOGGED_IN) {
            getCorces();
        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).onBackPressed();
            }
        });
        return view;
    }

    private void getCorces() {
        dialogLoader.showProgressDialog();
        Call<StudentData> call = APIClient.getInstance()
                .getStudentList(mParam1);

        call.enqueue(new Callback<StudentData>() {
            @Override
            public void onResponse(Call<StudentData> call, Response<StudentData> response) {
                dialogLoader.hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("0"))
                    {
                        // Get the User Details from Response
                        movieList = response.body().getData();
                        if(movieList.size() > 0) {
                            rv_cources.setVisibility(View.VISIBLE);
                            for(int i=0;i<=movieList.size()-1;i++) {
                                count++;
                                movieList.get(i).setSrno(String.valueOf(count));
                            }
                            mAdapter = new StudentDetailadapter(getActivity(), movieList);
                            rv_cources.setAdapter(mAdapter);


                        } 
                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<StudentData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "No data Found!!", Toast.LENGTH_LONG).show();
            }
        });
    }

}
