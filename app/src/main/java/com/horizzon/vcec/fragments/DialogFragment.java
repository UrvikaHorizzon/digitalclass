package com.horizzon.vcec.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.model.CourceDetails;

public class DialogFragment extends androidx.fragment.app.DialogFragment {
    CourceDetails courceDetails;
    String course_id;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view=inflater.inflate(R.layout.dialog,container,false);

        if (getArguments() != null) {
            courceDetails = (CourceDetails) getArguments().getSerializable("CourseDetail");
            course_id=getArguments().getString("key");

        }
        view.findViewById(R.id.dialog_btn_course_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                getDialog().dismiss();

                Fragment fragment = new TutorVideoFragment();
                Bundle bundle = new Bundle();
                bundle.putString("CourseId",course_id);
                fragment.setArguments(bundle);
                FragmentTransaction transaction = ((HomeActivity)getActivity()).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();



            }
        });

        view.findViewById(R.id.dialog_btn_demo_video).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDialog().dismiss();
                Fragment fragment = new TutorDemoVideoFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("CourseDetail",courceDetails);
                bundle.putString("CourseId",course_id);
                fragment.setArguments(bundle);
                FragmentTransaction transaction = ((HomeActivity)getActivity()).getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();

            }
        });

        return view;
    }
}
