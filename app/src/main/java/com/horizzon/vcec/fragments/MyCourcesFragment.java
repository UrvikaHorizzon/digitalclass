package com.horizzon.vcec.fragments;

import android.content.pm.ActivityInfo;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.Config;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.adapter.CustomCourceadapter;
import com.horizzon.vcec.adapter.CustomWishlistAdapter;
import com.horizzon.vcec.model.CourceData;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.MyAppPrefsManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class MyCourcesFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String customerID="";
    RecyclerView rv_cources;
    List<CourceDetails> movieList;
    DialogLoader dialogLoader;
    private CustomWishlistAdapter mAdapter;
    ImageView iv_back;
    TextView tv_cource_title, tv_nofound;
    private MyAppPrefsManager myAppPrefsManager;

    public MyCourcesFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MyCourcesFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyCourcesFragment newInstance(String param1, String param2) {
        MyCourcesFragment fragment = new MyCourcesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_wishlist, container, false);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        customerID = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");
        Log.e("custome_id",customerID);
        rv_cources = view.findViewById(R.id.rv_cources);
        iv_back = view.findViewById(R.id.iv_back);
        tv_nofound = view.findViewById(R.id.tv_nofound);
        tv_cource_title = view.findViewById(R.id.tv_title);
        tv_cource_title.setText("My Courses");
//        Toast.makeText(getActivity(), customerID, Toast.LENGTH_LONG).show();

        movieList = new ArrayList<>();
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true);
        layoutManager1.setReverseLayout(false);
        rv_cources.setLayoutManager(layoutManager1);
        myAppPrefsManager = new MyAppPrefsManager(getActivity());
        dialogLoader = new DialogLoader(getActivity());
        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
        if(Config.IS_USER_LOGGED_IN) {
            getCorces();
        }

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).onBackPressed();
            }
        });
        return view;
    }

    private void getCorces() {
        dialogLoader.showProgressDialog();
        Call<CourceData> call = APIClient.getInstance()
                .getMyCourceList(customerID);

        call.enqueue(new Callback<CourceData>() {
            @Override
            public void onResponse(Call<CourceData> call, Response<CourceData> response) {
                dialogLoader.hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1"))
                    {
                        // Get the User Details from Response
                        movieList = response.body().getData();
                        if(movieList.size() > 0) {
                            tv_nofound.setVisibility(View.GONE);
                            rv_cources.setVisibility(View.VISIBLE);
                            for(int i=0;i<movieList.size();i++) {
                                movieList.get(i).setBuy_now("yes");
                            }
                            mAdapter = new CustomWishlistAdapter(getActivity(), movieList, "no", "courselist");
                            rv_cources.setAdapter(mAdapter);


                        } else {
                            tv_nofound.setVisibility(View.VISIBLE);
                            rv_cources.setVisibility(View.GONE);
                        }
                    }

                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CourceData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
//                Toast.makeText(getActivity(), "No data Found!!", Toast.LENGTH_LONG).show();
                tv_nofound.setVisibility(View.VISIBLE);
                rv_cources.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onDestroy() {
        dialogLoader.hideProgressDialog();
        super.onDestroy();
    }
}
