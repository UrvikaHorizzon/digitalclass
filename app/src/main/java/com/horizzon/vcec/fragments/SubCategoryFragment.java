package com.horizzon.vcec.fragments;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.SelectRoleActivity;
import com.horizzon.vcec.adapter.CustomCategoryadapter;
import com.horizzon.vcec.model.CategoryData;
import com.horizzon.vcec.model.CategoryModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.IOnBackPressed;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubCategoryFragment extends Fragment implements IOnBackPressed {

    RecyclerView rv_categories;
    List<CategoryModel> movieList;
    private CustomCategoryadapter mAdapter;
    DialogLoader dialogLoader;
    ImageView iv_back;
    CategoryModel cat_id;
    TextView tv_title;
    EditText et_search;
    Button fab_register;
    ImageView iv_home;
    public SubCategoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((HomeActivity) getActivity()).setDrawerEnabled(false);

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_category, container, false);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        rv_categories = view.findViewById(R.id.rv_categories);
        iv_back = view.findViewById(R.id.iv_back);
        tv_title = view.findViewById(R.id.tv_title);
        movieList = new ArrayList<>();
        et_search = view.findViewById(R.id.et_search);
        iv_home = view.findViewById(R.id.iv_home);
        iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               Intent home = new Intent(getActivity(),HomeActivity.class);
               startActivity(home);
               getActivity().finish();

            }
        });
        fab_register = view.findViewById(R.id.fab_register);
        fab_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent register = new Intent(getActivity(), SelectRoleActivity.class);
                register.putExtra("become_tutor", "tutor");
                startActivity(register);
            }
        });

        cat_id = (CategoryModel) getArguments().getSerializable("cat_id");
        tv_title.setText(cat_id.getName());

        dialogLoader = new DialogLoader(getActivity());
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true);
        layoutManager1.setReverseLayout(false);
        rv_categories.setLayoutManager(layoutManager1);

//        movieList = (List<CategoryModel>) getArguments().getSerializable("CategoryList");
        getCategories();
//        getCorces();
        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                String name = et_search.getText().toString();
                mAdapter.filter(name);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
//                cAdapter.filter(text);
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).onBackPressed();
            }
        });
        return view;
    }

    private void getCorces() {
        String corceNme[] = new String[]{"IT & SOFTWARE", "KIDS COURSES", "SPOKEN ENGLISH", "BUSINESS MANAGEMENT",
                "Fashion Designing", "Aviation Courses", "Business Marketing", "Design & Editing", "Competitive Exam",
                "Accounts & Finance", "Science & Technology", "Medical Education", "Hotel Management", "Sports Education",
                "Business Development", "Personality Development", "Teaching & Academic", "Investment Management",
                "Import Export Managment", "Personal & Lifestyle"};

        for (int i = 0; i < corceNme.length; i++) {
            CategoryModel categoryModel = new CategoryModel();
            categoryModel.setName(corceNme[i]);
            movieList.add(categoryModel);
        }
        mAdapter = new CustomCategoryadapter(getActivity(), movieList);
        rv_categories.setAdapter(mAdapter);

    }

    private void getCategories() {
        dialogLoader.showProgressDialog();
        Call<CategoryData> call = APIClient.getInstance()
                .getSubCategoryList(cat_id.getId());

        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                dialogLoader.hideProgressDialog();
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        movieList = response.body().getData();
                        mAdapter = new CustomCategoryadapter(getActivity(), movieList);
                        rv_categories.setAdapter(mAdapter);
                        mAdapter.findItem(movieList);
                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }


    @Override
    public boolean onBackPressed() {
        return true;
    }
}
