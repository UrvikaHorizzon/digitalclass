package com.horizzon.vcec.fragments;


import android.content.pm.ActivityInfo;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.model.ExamDetails;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link AddExamFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddExamFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    EditText et_que, et_opta, et_optb, et_optc, et_optd;
    AutoCompleteTextView et_vtype;
    Button btn_signup;
    ImageView iv_back;
    DialogLoader dialogLoader;
    private String ans = "", video_id="";
    int update_flag = 0;
    Call<ResponseModel> call;
    ExamDetails examDetails;

    public AddExamFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddExamFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddExamFragment newInstance(String param1, String param2) {
        AddExamFragment fragment = new AddExamFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
           if(getArguments().getString("video_id")!=null) {
               video_id = getArguments().getString("video_id","");
           }

           if(getArguments().getSerializable("QueId")!=null) {
               examDetails = (ExamDetails) getArguments().getSerializable("QueId");
               update_flag = 1;
           }
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_exam, container, false);
        et_opta = view.findViewById(R.id.et_opta);
        et_optb = view.findViewById(R.id.et_optb);
        et_optc = view.findViewById(R.id.et_optc);
        et_optd = view.findViewById(R.id.et_optd);
        et_que = view.findViewById(R.id.et_que);
        et_vtype = view.findViewById(R.id.et_vtype);
        btn_signup = view.findViewById(R.id.btn_signup);
        iv_back = view.findViewById(R.id.iv_back);

        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        dialogLoader = new DialogLoader(getActivity());

        final String[] mStringArray = new String[]{"Option A", "Option B", "Option C", "Option D"};
        ArrayAdapter<String> countryAdapter1 = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
        et_vtype.setThreshold(100);//will start working from first character
        et_vtype.setAdapter(countryAdapter1);
        
        if(update_flag == 1) {
            et_que.setText(examDetails.getQuestion());
            et_opta.setText(examDetails.getOption_a());
            et_optb.setText(examDetails.getOption_b());
            et_optc.setText(examDetails.getOption_c());
            et_optd.setText(examDetails.getOption_d());
            ans = examDetails.getAnswer();
            if(examDetails.getAnswer().equals("a")) {
                et_vtype.setText("Option A");
            }else if(examDetails.getAnswer().equals("b")) {
                et_vtype.setText("Option B");
            }else if(examDetails.getAnswer().equals("c")) {
                et_vtype.setText("Option C");
            }else if(examDetails.getAnswer().equals("d")) {
                et_vtype.setText("Option D");
            }
            btn_signup.setText("Update Question");
        }
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).onBackPressed();
            }
        });

        et_vtype.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_vtype.showDropDown();
                et_vtype.requestFocus();
                return false;
            }
        });

        et_vtype.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if(selection.equalsIgnoreCase("Option A")) {
                   ans = "a";
                }else if(selection.equalsIgnoreCase("Option B")) {
                   ans = "b";
                }else if(selection.equalsIgnoreCase("Option C")) {
                   ans = "c";
                }else if(selection.equalsIgnoreCase("Option D")) {
                   ans = "d";
                }
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isValidData = validateInfoForm();
                if (isValidData) {
                    registerData();
                }
            }
        });
        

        return view;
    }

    private boolean validateInfoForm() {
        if (et_vtype.getText().toString().trim().isEmpty()) {
            et_vtype.setError("Please select answer");
            return false;
        } else if (et_que.getText().toString().trim().isEmpty()) {
            et_que.setError("Please Enter Question");
            return false;
        } else if (et_opta.getText().toString().trim().isEmpty()) {
            et_opta.setError("Please enter option a");
            return false;
        } else if (et_optb.getText().toString().trim().isEmpty()) {
            et_optb.setError("Please enter option b");
            return false;
        } else if (et_optc.getText().toString().trim().isEmpty()) {
            et_optc.setError("Please enter option c");
            return false;
        } else if (et_optd.getText().toString().trim().isEmpty()) {
            et_optd.setError("Please enter option d");
            return false;
        } else {
            return true;
        }
    }

    private void registerData() {
        dialogLoader.showProgressDialog();

        if(update_flag == 1) {
            call = APIClient.getInstance()
                    .centerQueEdit
                            (
                                    examDetails.getId(),
                                    et_que.getText().toString().trim(),
                                    et_opta.getText().toString().trim(),
                                    et_optb.getText().toString().trim(),
                                    et_optc.getText().toString().trim(),
                                    et_optd.getText().toString().trim(),
                                    ans
                            );
        } else {

            call = APIClient.getInstance()
                    .centerQue
                            (
                                    video_id,
                                    et_que.getText().toString().trim(),
                                    et_opta.getText().toString().trim(),
                                    et_optb.getText().toString().trim(),
                                    et_optc.getText().toString().trim(),
                                    et_optd.getText().toString().trim(),
                                    ans
                            );
        }

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("0"))
                    {
                        // Get the Error Message from Response
                        Toast.makeText(getActivity(), response.body().getData(), Toast.LENGTH_LONG).show();
                        ((HomeActivity)getActivity()).onBackPressed();
                    }
                    else {
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!",Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getActivity(), "Something went wrong..Please try again later!!!!", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "Something went wrong..Please try again later!!!!", Toast.LENGTH_LONG).show();

            }
        });
    }

}
