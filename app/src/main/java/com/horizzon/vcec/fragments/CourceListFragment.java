package com.horizzon.vcec.fragments;


import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.SelectRoleActivity;
import com.horizzon.vcec.adapter.CustomCourceadapter;
import com.horizzon.vcec.model.CategoryData;
import com.horizzon.vcec.model.CategoryModel;
import com.horizzon.vcec.model.CourceData;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.IOnBackPressed;

import java.util.ArrayList;
import java.util.List;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CourceListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CourceListFragment extends Fragment implements IOnBackPressed {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private CategoryModel cat_id;
    private String filter_type = "", id = null;
    TextView tv_tutor;

    RecyclerView rv_cources;
    List<CourceDetails> movieList;
    DialogLoader dialogLoader;
    private CustomCourceadapter mAdapter;
    ImageView iv_back;
    TextView tv_cource_title;
    LinearLayout ll_nofound;
    private EditText et_search;
    private ImageView iv_filter;
    private Dialog dialog;
    private RadioGroup rg_filter, rg_lnguge, rg_feetype, rg_cerificte;
    private RadioButton radioButton;
    RatingBar ratingBar;
    String[] items;
    Dialog dialog1;
    ArrayList itemsSelected = new ArrayList();
    String prise = "", language = "", certificated = "", fee_type = "", rating = "", discount = "", tutoril = "''";
    int dilogvisible = 0;
    private boolean[] checkedItems;
    List<CourceDetails> filterResponseList;
    Button fab_register;
    ImageView iv_home;
    EditText et_name, et_phone, et_subject;
    Button btn_send;

    public CourceListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CourceListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CourceListFragment newInstance(String param1, String param2) {
        CourceListFragment fragment = new CourceListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cource_list, container, false);


        tv_cource_title = view.findViewById(R.id.tv_cource_title);
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        if (getArguments() != null) {
            try {
                cat_id = (CategoryModel) getArguments().getSerializable("cat_id");
                tv_cource_title.setText(cat_id.getName());
                id = cat_id.getId();

            } catch (Exception e) {
                filter_type = getArguments().getString("filter_type");
                fee_type = getArguments().getString("fee_type");
                language = getArguments().getString("language");
                certificated = getArguments().getString("certificated");
                rating = getArguments().getString("rating");
                tutoril = getArguments().getString("tutoril");
                dilogvisible = getArguments().getInt("key");


                Log.e("filter_type", filter_type);
                Log.e("fee_type", fee_type);
                Log.e("language", language);
                Log.e("certified", certificated);
                Log.e("rating", rating);
                Log.e("tutoril", tutoril);

                if (filter_type.equals("Price - High to Low")) {
                    prise = "1";
                } else if (filter_type.equals("Price - Low to High")) {
                    prise = "2";
                }
                if (fee_type.equals("Free")) {
                    fee_type = "free";
                } else if (fee_type.equals("Paid")) {
                    fee_type = "paid";
                }
                if (certificated.equals("Yes")) {
                    certificated = "Yes";
                } else if (filter_type.equals("No")) {
                    certificated = "NO";
                }
                tv_cource_title.setText("Search Results");
            }

        }
        rv_cources = view.findViewById(R.id.rv_cources);
        iv_back = view.findViewById(R.id.iv_back);
        ll_nofound = view.findViewById(R.id.ll_nofound);
        et_search = view.findViewById(R.id.et_search);
        iv_filter = view.findViewById(R.id.iv_filter);
        iv_home = view.findViewById(R.id.iv_home);


        et_name = view.findViewById(R.id.et_name);
        et_phone = view.findViewById(R.id.et_phone);
        et_subject = view.findViewById(R.id.et_subject);
        btn_send = view.findViewById(R.id.btn_send);
        btn_send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                String name = et_name.getText().toString();
                String phone = et_phone.getText().toString();
                String subject = et_subject.getText().toString();

                if (TextUtils.isEmpty(name)) {

                    et_name.setError("Name is required");
                    et_name.requestFocus();

                    return;

                } else if (TextUtils.isEmpty(phone)) {

                    et_phone.setError("Mobile Number is required");
                    et_phone.requestFocus();
                    return;
                } else if (TextUtils.isEmpty(subject)) {

                    et_subject.setError("Required");
                    et_subject.requestFocus();
                    return;
                } else {

                    try {
                        Intent gmail = new Intent(Intent.ACTION_SEND);
                        gmail.putExtra(Intent.EXTRA_EMAIL, new String[]{"info@digitalclassworld.com"});
                        gmail.setData(Uri.parse("info@digitalclassworld.com"));
                        gmail.putExtra(Intent.EXTRA_SUBJECT, et_subject.getText().toString());
                        gmail.setType("plain/text");
                        gmail.putExtra(Intent.EXTRA_TEXT, et_name.getText().toString() + "\n\n" + et_phone.getText().toString() + "\n\n" + et_subject.getText().toString());
                        startActivity(Intent.createChooser(gmail, "Choose Email"));
                    } catch (Exception e) {
                        //e.toString();
                    }


                }

            }
        });

        iv_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent home = new Intent(getActivity(), HomeActivity.class);
                startActivity(home);
                getActivity().finish();
            }
        });
        fab_register = view.findViewById(R.id.fab_register);
        fab_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent register = new Intent(getActivity(), SelectRoleActivity.class);
                register.putExtra("become_tutor", "tutor");
                startActivity(register);
            }
        });

        movieList = new ArrayList<>();
        LinearLayoutManager layoutManager1 = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, true);
        layoutManager1.setReverseLayout(false);
        rv_cources.setLayoutManager(layoutManager1);
        dialogLoader = new DialogLoader(getActivity());

        filterResponseList = new ArrayList<>();
        getCorces();

        et_search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    performSearch();
                    return true;
                }
                return false;
            }
        });

        et_search.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if (movieList.size() > 0) {
                    String name = et_search.getText().toString();
                    mAdapter.filter(name);
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {
                // TODO Auto-generated method stub

            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
//                cAdapter.filter(text);
            }
        });

        iv_filter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                dialog = new Dialog(getActivity());
//                // Include dialog.xml file
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                dialog.setContentView(R.layout.dlog_filter_layout);
//                dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
//                dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//                dialog.setCanceledOnTouchOutside(true);
//                dialog.setCancelable(true);
//
//                getTutorils();
//
//                rg_filter = dialog.findViewById(R.id.rg_filter);
//                rg_cerificte = dialog.findViewById(R.id.rg_certificte);
//                rg_lnguge = dialog.findViewById(R.id.rg_lnguge);
//                rg_feetype = dialog.findViewById(R.id.rg_feetype);
//                ratingBar = dialog.findViewById(R.id.ratingBar);
//                tv_tutor = dialog.findViewById(R.id.tv_tutor);
//                tv_tutor.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//                        builder.setTitle("Select Tutors");
//                        builder.setMultiChoiceItems(items, checkedItems,
//                                new DialogInterface.OnMultiChoiceClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int selectedItemId,
//                                                        boolean isSelected) {
//                                        if (isSelected) {
//                                            itemsSelected.add(selectedItemId);
//                                            checkedItems[selectedItemId] = true;
//                                        } else if (itemsSelected.contains(selectedItemId)) {
//                                            itemsSelected.remove(Integer.valueOf(selectedItemId));
//                                            checkedItems[selectedItemId] = false;
//                                        }
//                                    }
//                                })
//                                .setPositiveButton("Done", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        //Your logic when OK button is clicked
//                                        dialog1.dismiss();
//                                        if (itemsSelected.size() > 0) {
//                                            String[] supportedPhones = new String[itemsSelected.size()];
//                                            for (int i = 0; i < itemsSelected.size(); i++) {
//                                                supportedPhones[i] = "'" + items[(int) itemsSelected.get(i)] + "'";
//
////                                                supportedPhones[i] =  items[(int) itemsSelected.get(i)];
//                                            }
//                                            tutoril = android.text.TextUtils.join(",", supportedPhones);
//                                        }
//                                    }
//                                })
//                                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialog, int id) {
//                                        dialog1.dismiss();
//                                    }
//                                });
//                        dialog1 = builder.create();
//                        dialog1.show();
//                    }
//                });
//                Button btn_login = dialog.findViewById(R.id.btn_apply);
//                btn_login.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        dialog.dismiss();
//                        int selectedId = rg_filter.getCheckedRadioButtonId();
//                        if (selectedId != -1) {
//                            radioButton = (RadioButton) dialog.findViewById(selectedId);
//                            prise = radioButton.getText().toString().trim();
//                        } else {
//                            prise = "";
//                        }
//                        int selectedId1 = rg_feetype.getCheckedRadioButtonId();
//                        if (selectedId1 != -1) {
//                            radioButton = (RadioButton) dialog.findViewById(selectedId1);
//                            fee_type = radioButton.getText().toString().trim();
//                        } else {
//                            fee_type = "";
//                        }
//                        int selectedId2 = rg_lnguge.getCheckedRadioButtonId();
//                        if (selectedId2 != -1) {
//                            radioButton = (RadioButton) dialog.findViewById(selectedId2);
//                            language = radioButton.getText().toString().trim();
//                        } else {
//                            language = "";
//                        }
//                        int selectedId3 = rg_cerificte.getCheckedRadioButtonId();
//                        if (selectedId3 != -1) {
//                            radioButton = (RadioButton) dialog.findViewById(selectedId3);
//                            certificated = radioButton.getText().toString().trim();
//                        } else {
//                            certificated = "";
//                        }
//                        if (!String.valueOf(ratingBar.getRating()).equals("0.0")) {
//                            String rting = String.valueOf(ratingBar.getRating());
//                            if (rting.endsWith(".0")) {
//                                rting = rting.replace(".0", "");
//                                rating = rting;
//                            } else {
//                                rating = rting;
//                            }
//                        }
//
//                        if (prise.equals("Price - High to Low")) {
//                            prise = "1";
//                        } else if (prise.equals("Price - Low to High")) {
//                            prise = "2";
//                        }
//                        if (fee_type.equals("Free")) {
//                            fee_type = "free";
//                        } else if (fee_type.equals("Paid")) {
//                            fee_type = "paid";
//                        }
//                        if (certificated.equals("Yes")) {
//                            certificated = "Yes";
//                        } else if (filter_type.equals("No")) {
//                            certificated = "NO";
//                        }
//                        getCorces();
//                    }
//                });


//                dialog.show();

                Fragment fragment = new FilterItem_Fragemnt();
//                Bundle bundle = new Bundle();
//                bundle.putSerializable("cat_id", movie);
//                fragment.setArguments(bundle);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).onBackPressed();
            }
        });

        return view;
    }

    private void performSearch() {
        et_search.clearFocus();
        InputMethodManager in = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        in.hideSoftInputFromWindow(et_search.getWindowToken(), 0);

        getSerch(et_search.getText().toString().trim());


    }

    private void getSerch(String trim) {
        dialogLoader.showProgressDialog();
        Call<CourceData> call = APIClient.getInstance()
                .getSerchList(trim);

        call.enqueue(new Callback<CourceData>() {
            @Override
            public void onResponse(Call<CourceData> call, Response<CourceData> response) {
                dialogLoader.hideProgressDialog();
//                filter_type = "";
                et_search.setText("");
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        movieList = response.body().getData();
                        if (movieList.size() > 0) {
                            // ll_nofound.setVisibility(View.GONE);
                            rv_cources.setVisibility(View.VISIBLE);
                            mAdapter = new CustomCourceadapter(getActivity(), movieList, "no", "courselist");
                            rv_cources.setAdapter(mAdapter);
                            mAdapter.findItem(movieList);

                        } else {
                            ll_nofound.setVisibility(View.VISIBLE);
                            rv_cources.setVisibility(View.GONE);
                        }
                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CourceData> call, Throwable t) {
//                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getTutorils() {


        Call<CategoryData> call = APIClient.getInstance()
                .getTutorialList();

        call.enqueue(new Callback<CategoryData>() {
            @Override
            public void onResponse(Call<CategoryData> call, Response<CategoryData> response) {
                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        // Get the User Details from Response
                        List<CategoryModel> categoryModelList = response.body().getData();
                        items = new String[categoryModelList.size()];
                        checkedItems = new boolean[categoryModelList.size()];
                        for (int i = 0; i < categoryModelList.size(); i++) {
                            items[i] = categoryModelList.get(i).getName();
                            checkedItems[i] = false;
                        }
                    }

                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<CategoryData> call, Throwable t) {
//                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void getCorces() {
        dialogLoader.showProgressDialog();

        Call<CourceDetails> call = APIClient.getInstance()
                .getCourceList(id, prise, fee_type, language, certificated, rating, tutoril);
        call.enqueue(new Callback<CourceDetails>() {
            @Override
            public void onResponse(Call<CourceDetails> call, Response<CourceDetails> response) {
                if (response.isSuccessful()) {
                    dialogLoader.hideProgressDialog();
                    movieList = new ArrayList(response.body().getCoursess());
                    if (movieList.size() > 0) {
                        List<CourceDetails> courceDetailsList = response.body().getCoursess();
                        for (int i = 0; i < movieList.size(); i++) {
                            for (int j = 0; j < courceDetailsList.size(); j++) {
                                if (movieList.get(i).getId().equals(courceDetailsList.get(j).getId())) {
                                    movieList.get(i).setMain_category_id(courceDetailsList.get(j).getName());
                                    break;
                                }
                            }
                        }
                        ll_nofound.setVisibility(View.GONE);
                        rv_cources.setVisibility(View.VISIBLE);
                        mAdapter = new CustomCourceadapter(getActivity(), movieList, "no", "courselist");
                        rv_cources.setAdapter(mAdapter);
                        mAdapter.findItem(movieList);

                    } else {
                        ll_nofound.setVisibility(View.VISIBLE);
                        rv_cources.setVisibility(View.GONE);
                    }
                }

            }

            @Override
            public void onFailure(Call<CourceDetails> call, Throwable t) {
                dialogLoader.hideProgressDialog();
            }
        });

//        call.enqueue(new Callback<JsonObject>() {
//            @Override
//            public void onResponse(Call<JsonObject> call, Response<JsonObject> response) {
//                JsonArray user_array= response.body().getAsJsonArray("coursess");
//
//            }
//
//            @Override
//            public void onFailure(Call<JsonObject> call, Throwable t) {
//                Log.e("TAG","failure");
//            }
//        });


//        call.enqueue(new Callback<CourceData>() {
//            @Override
//            public void onResponse(Call<CourceData> call, Response<CourceData> response) {
//                dialogLoader.hideProgressDialog();
//                CourceData data=response.body();
//                Log.d("TAG", "onSuccess:");
//                Toast.makeText(getContext(), "data: "+data.getData().size(), Toast.LENGTH_SHORT).show();
//            }
//
//            @Override
//            public void onFailure(Call<CourceData> call, Throwable t) {
//                dialogLoader.hideProgressDialog();
//                Log.d("TAG", "onFailure:"+t.getMessage());
//            }
//        });

//        call.enqueue(new Callback<CourceData>() {
//            @Override
//            public void onResponse(Call<CourceData> call, Response<CourceData> response) {
//
//                CourceData data=response.body();
//
//                if (data!=null){
//
//                }
//                dialogLoader.hideProgressDialog();
//                getTutorils();
//                if (response.isSuccessful()) {
//                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
//                        // Get the User Details from Response
//                        movieList = response.body().getData();
//                        if (movieList.size() > 0) {
//                            List<CourceDetails> courceDetailsList = response.body().getCoursess();
//                            for (int i = 0; i < movieList.size(); i++) {
//                                for (int j = 0; j < courceDetailsList.size(); j++) {
//                                    if (movieList.get(i).getId().equals(courceDetailsList.get(j).getId())) {
//                                        movieList.get(i).setMain_category_id(courceDetailsList.get(j).getName());
//                                        break;
//                                    }
//                                }
//                            }
//                            tv_nofound.setVisibility(View.GONE);
//                            rv_cources.setVisibility(View.VISIBLE);
//                            mAdapter = new CustomCourceadapter(getActivity(), movieList, "no", "courselist");
//                            rv_cources.setAdapter(mAdapter);
//                            mAdapter.findItem(movieList);
//
//                        } else {
//                            tv_nofound.setVisibility(View.VISIBLE);
//                            rv_cources.setVisibility(View.GONE);
//                        }
//                    }
//
//                } else {
//                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
//                    // Show the Error Message
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<CourceData> call, Throwable t) {
//                Toast.makeText(getActivity(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
//                dialogLoader.hideProgressDialog();
//            }
//        });


//        call.enqueue(new Callback<CourceData>() {
//            @Override
//            public void onResponse(Call<CourceData> call, Response<CourceData> response) {
//                dialogLoader.hideProgressDialog();
//                filter_type = "";
//                tutoril = "";
//                rating = "";
//                itemsSelected = new ArrayList();
//                getTutorils();
//                if (response.isSuccessful()) {
//                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
//                        // Get the User Details from Response
////                        movieList = response.body().getData();
//
//                        CourceData data=response.body();
//                        Log.e("Student name", data.getData().get(0).getName());
////                        if (movieList.size() > 0) {
////                            List<CourceDetails> courceDetailsList = response.body().getCoursess();
////                            for (int i = 0; i < movieList.size(); i++) {
////                                for (int j = 0; j < courceDetailsList.size(); j++) {
////                                    if (movieList.get(i).getId().equals(courceDetailsList.get(j).getId())) {
////                                        movieList.get(i).setMain_category_id(courceDetailsList.get(j).getName());
////                                        break;
////                                    }
////                                }
////                            }
////                            tv_nofound.setVisibility(View.GONE);
////                            rv_cources.setVisibility(View.VISIBLE);
////                            mAdapter = new CustomCourceadapter(getActivity(), movieList, "no", "courselist");
////                            rv_cources.setAdapter(mAdapter);
////                            mAdapter.findItem(movieList);
////
////                        } else {
////                            tv_nofound.setVisibility(View.VISIBLE);
////                            rv_cources.setVisibility(View.GONE);
////                        }
//                    }
//
//                } else {
//                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
//                    // Show the Error Message
//                }
//            }
//
//            @Override
//            public void onFailure(Call<CourceData> call, Throwable t) {
//                dialogLoader.hideProgressDialog();
//                Toast.makeText(getActivity(), "NetworkCallFailure : " + t, Toast.LENGTH_LONG).show();
//                Log.e("filter_log",t.getMessage());
//            }
//        });


    }

    @Override
    public boolean onBackPressed() {
        return true;
    }
}
