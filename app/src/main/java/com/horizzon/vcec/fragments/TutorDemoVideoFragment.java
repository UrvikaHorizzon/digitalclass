package com.horizzon.vcec.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.horizzon.vcec.Config;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.adapter.CustomDemoAdapter;
import com.horizzon.vcec.model.CourceDetails;
import com.horizzon.vcec.model.DemoDetails;
import com.horizzon.vcec.model.ResultDetails;
import com.horizzon.vcec.model.demodata;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.MyAppPrefsManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

public class TutorDemoVideoFragment extends Fragment {

    private static final String ARG_PARAM1 = "CourseId";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    RecyclerView rv_videoes;

    // TODO: Rename and change types of parameters
    ImageView iv_back;
    FloatingActionButton btn_add;
    MyAppPrefsManager myAppPrefsManager;
    private String customerID, course_id;
    DialogLoader dialogLoader;
    List<DemoDetails> movieList;
    private CustomDemoAdapter mAdapter;
    TextView tv_nofound,tv_title;
    CourceDetails courceDetails;



    public static TutorDemoVideoFragment newInstance(String param1, String param2) {
        TutorDemoVideoFragment fragment = new TutorDemoVideoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public TutorDemoVideoFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            course_id = getArguments().getString(ARG_PARAM1);
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        View view =  inflater.inflate(R.layout.fragment_tutor_video, container, false);
        rv_videoes = view.findViewById(R.id.rv_categories);
        tv_nofound = view.findViewById(R.id.tv_nofound);
        iv_back = view.findViewById(R.id.iv_back);
        btn_add = view.findViewById(R.id.btn_add);
        myAppPrefsManager = new MyAppPrefsManager(getActivity());
        dialogLoader = new DialogLoader(getActivity());
        tv_title=view.findViewById(R.id.tv_title);
        tv_title.setText("Demo Lesson");
        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
        sharedPreferences = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE);
        customerID = sharedPreferences.getString("userID", "");

        Log.e("user_id",customerID);

        if (getArguments() != null) {
            courceDetails = (CourceDetails) getArguments().getSerializable("CourseDetail");
            getVideoes(courceDetails);
        }

        movieList = new ArrayList<>();
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        layoutManager.setReverseLayout(false);
        rv_videoes.setLayoutManager(layoutManager);
        rv_videoes.setNestedScrollingEnabled(false);


        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AddDemoVideoFragment();
                Bundle bundle = new Bundle();
                bundle.putSerializable("CourseDetail",courceDetails);
                fragment.setArguments(bundle);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).onBackPressed();
            }
        });

        return view;
    }


    private void getVideoes(CourceDetails courceDetails) {
        dialogLoader.showProgressDialog();
        Call<demodata> call = APIClient.getInstance()
                .getDemoVideoList(courceDetails.getCourse_id(),courceDetails.getId(),"demovideo",courceDetails.getName());




        call.enqueue(new Callback<demodata>() {
            @Override
            public void onResponse(Call<demodata> call, Response<demodata> response) {
                if (response.isSuccessful()) {
                    dialogLoader.hideProgressDialog();
                    // Get the User Details from Response
                    movieList = response.body().getMsg();
                    if (movieList.size() > 0) {
                        String course_name = response.body().getCourse_name();
                        String view_video = response.body().getView_video();
                        List<ResultDetails> resultDetails = new ArrayList<>();
                        resultDetails = response.body().getResult();
                        rv_videoes.setVisibility(View.VISIBLE);
                        tv_nofound.setVisibility(View.GONE);

                        mAdapter = new CustomDemoAdapter(getActivity(), movieList, course_name, view_video, resultDetails, "yes");
                        rv_videoes.setAdapter(mAdapter);
                        dialogLoader.hideProgressDialog();
                    } else {
                        tv_nofound.setVisibility(View.VISIBLE);
                        rv_videoes.setVisibility(View.GONE);
                        dialogLoader.hideProgressDialog();
                    }


                } else {
                    Log.d("RES", "Not SuccessFull Error Msg For Logcat" + response.message());
                    tv_nofound.setVisibility(View.VISIBLE);
                    rv_videoes.setVisibility(View.GONE);
                    dialogLoader.hideProgressDialog();
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<demodata> call, Throwable t) {
                tv_nofound.setVisibility(View.VISIBLE);
                rv_videoes.setVisibility(View.GONE);
                dialogLoader.hideProgressDialog();
            }
        });


    }
}
