package com.horizzon.vcec.fragments;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.core.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.model.ResponseModel;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * to handle interaction events.
 * Use the {@link AddVideoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AddVideoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "CourseId";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    EditText et_vname, et_desc, et_demo, et_youtube_url, et_courseimg;
    AutoCompleteTextView et_vype;
    Button btn_signup;
    ImageView iv_back, iv_photo, iv_cphoto;
    DialogLoader dialogLoader;
    private String video = "";
    String[] mediaColumns = {MediaStore.Video.Media._ID};
    LinearLayout ll_video;

    // TODO: Rename and change types of parameters
    private File file;
    private File cfile;
    int update_flag = 0;
    Call<ResponseModel> call;
    MultipartBody.Part fileToUpload = null;
    MultipartBody.Part fileToUpload1 = null;
    private String photo = "", video_type = "";


    public AddVideoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AddVideoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AddVideoFragment newInstance(String param1, String param2) {
        AddVideoFragment fragment = new AddVideoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        ((HomeActivity) getActivity()).setDrawerEnabled(false);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_video, container, false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    1);
        }
        getActivity().setRequestedOrientation(
                ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);

        et_courseimg = view.findViewById(R.id.et_courseimg);
        iv_cphoto = view.findViewById(R.id.iv_cphoto);
        et_vname = view.findViewById(R.id.et_vname);
        et_vype = view.findViewById(R.id.et_vtype);
        et_demo = view.findViewById(R.id.et_demo);
        et_desc = view.findViewById(R.id.et_desc);
        et_youtube_url = view.findViewById(R.id.et_youtube_url);
        btn_signup = view.findViewById(R.id.btn_signup);
        iv_back = view.findViewById(R.id.iv_back);
        iv_photo = view.findViewById(R.id.iv_photo);
        ll_video = view.findViewById(R.id.ll_video);


        dialogLoader = new DialogLoader(getActivity());

        final String[] mStringArray = new String[]{"Channel Unlisted Link"};
        ArrayAdapter<String> countryAdapter1 = new ArrayAdapter<String>(getActivity(), R.layout.dropdown, mStringArray);
        et_vype.setThreshold(100);//will start working from first character
        et_vype.setAdapter(countryAdapter1);
        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity) getActivity()).onBackPressed();
            }
        });

        et_vype.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                et_vype.showDropDown();
                et_vype.requestFocus();
                return false;
            }
        });

        et_vype.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String selection = (String) parent.getItemAtPosition(position);
                if(selection.equalsIgnoreCase("Channel Unlisted Link")) {
                    video_type = "link";
                    et_youtube_url.setVisibility(View.VISIBLE);
                    ll_video.setVisibility(View.GONE);
                } else {
                    video_type = "file";
                    et_youtube_url.setVisibility(View.GONE);
                    ll_video.setVisibility(View.VISIBLE);

                }
            }
        });

        et_demo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 100);
            }
        });

        et_courseimg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, 200);
            }
        });

        btn_signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean isValidData = validateInfoForm();
                if (isValidData) {
//                    Log.d("RESPONSE", cat_id+",\n"+sub_cat_id+",\n"+centre_id+",\n"+et_centernme.getText().toString()+",\n"+
//                            et_aprice.getText().toString().trim()+",\n"+
//                            et_certificate.getText().toString().trim()+",\n"+
//                            et_chapter.getText().toString().trim()+",\n"+
//                            et_chours.getText().toString().trim()+",\n"+
//                            et_cname.getText().toString().trim()+",\n"+
//                            et_desc.getText().toString().trim()+",\n"+
//                            et_language.getText().toString().trim()+",\n"+
//                            et_month.getText().toString().trim()+",\n"+
//                            file+",\n"+
//                            cfile+",\n"+
//                            cfile1+",\n"+
//                            cfile2+",\n"
//                            );
                    registerData();
                }
            }
        });

        return view;
    }

    private void registerData() {

        String urls=et_youtube_url.getText().toString();
//        Toast.makeText(getActivity(), ""+urls, Toast.LENGTH_SHORT).show();
        dialogLoader.showProgressDialog();
        try {
            RequestBody mFile = RequestBody.create(MediaType.parse("*/*"), file);
            fileToUpload = MultipartBody.Part.createFormData("file", file.getName(), mFile);
        } catch (Exception e){

        }

        try {
            RequestBody mFile1 = RequestBody.create(MediaType.parse("image/*"), cfile);
            fileToUpload1 = MultipartBody.Part.createFormData("image", cfile.getName(), mFile1);
        } catch (Exception e){

        }


        RequestBody sub_category_id = RequestBody.create(MediaType.parse("text/plain"), mParam1);
        RequestBody videotype = RequestBody.create(MediaType.parse("text/plain"), video_type);
        RequestBody videoflag = RequestBody.create(MediaType.parse("text/plain"), "coursevideo");
        RequestBody video_desc = RequestBody.create(MediaType.parse("text/plain"), et_desc.getText().toString().trim());
        RequestBody lessonname = RequestBody.create(MediaType.parse("text/plain"), et_vname.getText().toString().trim());
        RequestBody  url = RequestBody.create(MediaType.parse("text/plain"), et_youtube_url.getText().toString().trim());
        RequestBody city = RequestBody.create(MediaType.parse("text/plain"), "yes");

//        if(update_flag == 1) {
//            RequestBody id = RequestBody.create(MediaType.parse("text/plain"), course_id);
//            RequestBody cnme = RequestBody.create(MediaType.parse("text/plain"), courceDetails.getName());
////            RequestBody cold = null, pold = null, vold = null;
////            try {
////                cold = RequestBody.create(MediaType.parse("text/plain"), courceDetails.getImage());
////                vold = RequestBody.create(MediaType.parse("text/plain"), courceDetails.getVideo());
////                pold = RequestBody.create(MediaType.parse("text/plain"), courceDetails.getPdf_file());
////            } catch (Exception e){
////
////            }
//            call = APIClient.getInstance()
//                    .updateCourse
//                            (
//                                    id,
//                                    main_category_id, name, aprice, dprice, feetype,
//                                    gender,gender,  mobile, email, address, city, bdate, pwd, sequence,cnme
//                            );
//        } else {
            call = APIClient.getInstance()
                    .registerVideo
                            (sub_category_id,videotype,videoflag,lessonname, video_desc,url, city,
                                    fileToUpload1
                            );
//        }

        call.enqueue(new Callback<ResponseModel>() {
            @Override
            public void onResponse(Call<ResponseModel> call, Response<ResponseModel> response) {

                dialogLoader.hideProgressDialog();

                if (response.isSuccessful()) {
                    if (response.body().getSuccess().equalsIgnoreCase("0") && response.body().getData().equalsIgnoreCase("video upload successfully!"))
                    {
                        Toast.makeText(getActivity(), response.body().getData().toString(), Toast.LENGTH_LONG).show();
                        ((HomeActivity)getActivity()).onBackPressed();
                    }

                    else {
                        Toast.makeText(getActivity(), "Something went wrong..Please try again later!!",Toast.LENGTH_SHORT).show();
                    }
                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat");
                    // Show the Error Message
                    Toast.makeText(getActivity(), response.message(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<ResponseModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
                Toast.makeText(getActivity(), "NetworkCallFailure : "+t, Toast.LENGTH_LONG).show();
            }
        });
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == RESULT_OK && data != null) {
            //the image URI
            Uri selectedImage = data.getData();
            video = "selected";

//            String pth = getPathFromUri(getActivity(), selectedImage);
//            Bitmap bitmap = BitmapFactory.decodeFile(pth);
            iv_photo.setVisibility(View.VISIBLE);
            iv_photo.setImageBitmap(getThumbnailPathForLocalFile(getActivity(), selectedImage));
            et_demo.setText("Video File selected");
            Log.d("VIDEO", getPathFromUri(getActivity(), selectedImage));

            file = new File(getPathFromUri(getActivity(), selectedImage));

            //calling the upload file method after choosing the file
//            uploadFile(selectedImage, "My Image");
        } else if (requestCode == 200 && resultCode == RESULT_OK && data != null) {
            //the image URI
            Uri selectedImage = data.getData();
            photo = "selected";

            String pth = getPathFromUri(getActivity(), selectedImage);
            Bitmap bitmap = BitmapFactory.decodeFile(pth);
            iv_cphoto.setVisibility(View.VISIBLE);
            iv_cphoto.setImageBitmap(bitmap);
            et_courseimg.setText("Video Image selected");

            cfile = new File(getPathFromUri(getActivity(), selectedImage));

            //calling the upload file method after choosing the file
//            uploadFile(selectedImage, "My Image");
        }
    }

    // Providing Thumbnail For Selected Image
    public Bitmap getThumbnailPathForLocalFile(Activity context, Uri fileUri) {
        long fileId = getFileId(context, fileUri);
        return MediaStore.Video.Thumbnails.getThumbnail(context.getContentResolver(),
                fileId, MediaStore.Video.Thumbnails.MICRO_KIND, null);
    }

    // Getting Selected File ID
    public long getFileId(Activity context, Uri fileUri) {
        Cursor cursor = context.managedQuery(fileUri, mediaColumns, null, null, null);
        if (cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Video.Media._ID);
            return cursor.getInt(columnIndex);
        }
        return 0;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    Toast.makeText(getActivity(), "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public String getPathFromUri(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }

    private boolean validateInfoForm() {
        if (et_vype.getText().toString().trim().isEmpty()) {
            et_vype.setError("Please select Video Type");
            return false;
        } else if (et_vname.getText().toString().trim().isEmpty()) {
            et_vname.setError("Please Enter Video Name");
            return false;
        } else if (et_desc.getText().toString().trim().isEmpty()) {
            et_desc.setError("Please enter description");
            return false;
        } else if (!photo.contains("selected")) {
            et_courseimg.setError("Please upload video image");
            return false;
        } else if (video_type.equals("link")) {
            if (et_youtube_url.getText().toString().trim().isEmpty()) {
                et_youtube_url.setError("Please Enter YouTube URL");
                return false;
            } else {
                return true;
            }
        }else if (video_type.equals("file")) {
            if (!video.contains("selected")) {
                et_demo.setError("Please upload video file");
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }
}
