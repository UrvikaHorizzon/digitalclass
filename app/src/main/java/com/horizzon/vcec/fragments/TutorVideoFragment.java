package com.horizzon.vcec.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.horizzon.vcec.Config;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.adapter.CustomVideoadapter;
import com.horizzon.vcec.model.ResultDetails;
import com.horizzon.vcec.model.VideoData;
import com.horizzon.vcec.model.VideoDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.MyAppPrefsManager;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TutorVideoFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TutorVideoFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "CourseId";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences;
    RecyclerView rv_videoes;

    // TODO: Rename and change types of parameters
    ImageView iv_back;
    FloatingActionButton btn_add;
    MyAppPrefsManager myAppPrefsManager;
    private String customerID, course_id;
    DialogLoader dialogLoader;
    List<VideoDetails> movieList;
    private CustomVideoadapter mAdapter;
    TextView tv_nofound,tv_title;


    public TutorVideoFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TutorVideoFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TutorVideoFragment newInstance(String param1, String param2) {
        TutorVideoFragment fragment = new TutorVideoFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            course_id = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        ((HomeActivity) getActivity()).setDrawerEnabled(false);

        View view =  inflater.inflate(R.layout.fragment_tutor_video, container, false);
        rv_videoes = view.findViewById(R.id.rv_categories);
        tv_nofound = view.findViewById(R.id.tv_nofound);
        iv_back = view.findViewById(R.id.iv_back);
        btn_add = view.findViewById(R.id.btn_add);
        tv_title=view.findViewById(R.id.tv_title);
        tv_title.setText("Course Videos");
        myAppPrefsManager = new MyAppPrefsManager(getActivity());
        dialogLoader = new DialogLoader(getActivity());
        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();
        sharedPreferences = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE);
        customerID = sharedPreferences.getString("userID", "");

        movieList = new ArrayList<>();
        GridLayoutManager layoutManager = new GridLayoutManager(getActivity(), 2);
        layoutManager.setReverseLayout(false);
        rv_videoes.setLayoutManager(layoutManager);
        rv_videoes.setNestedScrollingEnabled(false);
        getVideoes(course_id);

        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new AddVideoFragment();
                Bundle bundle = new Bundle();
                bundle.putString("CourseId", course_id);
                fragment.setArguments(bundle);
                FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
                transaction.replace(R.id.frame_container, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });

        iv_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((HomeActivity)getActivity()).onBackPressed();
            }
        });

        return view;
    }

    private void getVideoes(String course_id) {
        dialogLoader.showProgressDialog();
        Call<VideoData> call = APIClient.getInstance()
                .getVideoList(course_id, customerID);



        call.enqueue(new Callback<VideoData>() {
            @Override
            public void onResponse(Call<VideoData> call, Response<VideoData> response) {
                if (response.isSuccessful()) {
                    dialogLoader.hideProgressDialog();
                    // Get the User Details from Response
                    movieList = response.body().getMsg();
                    if(movieList.size()>0) {
                        String course_name = response.body().getCourse_name();
                        String view_video = response.body().getView_video();
                        List<ResultDetails> resultDetails = new ArrayList<>();
                        resultDetails = response.body().getResult();
                        rv_videoes.setVisibility(View.VISIBLE);
                        tv_nofound.setVisibility(View.GONE);

                        mAdapter = new CustomVideoadapter(getActivity(), movieList, course_name, view_video, resultDetails, "no");
                        rv_videoes.setAdapter(mAdapter);
                    } else {
                        tv_nofound.setVisibility(View.VISIBLE);
                        rv_videoes.setVisibility(View.GONE);
                    }


                } else {
                    Log.d("RES","Not SuccessFull Error Msg For Logcat"+response.message());
                    tv_nofound.setVisibility(View.VISIBLE);
                    rv_videoes.setVisibility(View.GONE);
                    // Show the Error Message
                }
            }

            @Override
            public void onFailure(Call<VideoData> call, Throwable t) {
                tv_nofound.setVisibility(View.VISIBLE);
                rv_videoes.setVisibility(View.GONE);
            }
        });
    }

}
