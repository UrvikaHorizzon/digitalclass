package com.horizzon.vcec.fragments;


import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horizzon.vcec.Config;
import com.horizzon.vcec.HomeActivity;
import com.horizzon.vcec.R;
import com.horizzon.vcec.SelectRoleActivity;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.MyAppPrefsManager;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import static android.content.Context.MODE_PRIVATE;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TutorDadhboardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TutorDadhboardFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    LinearLayout ll_cmanage, ll_smanage, ll_wallet, ll_profile;
    TextView tv_logout;
    private MyAppPrefsManager myAppPrefsManager;
    private SharedPreferences sharedPreferences;
    private String customerID = "";
    private DialogLoader dialogLoader;
    LinearLayout ll_earn;

    public TutorDadhboardFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TutorDadhboardFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TutorDadhboardFragment newInstance(String param1, String param2) {
        TutorDadhboardFragment fragment = new TutorDadhboardFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        ((HomeActivity) getActivity()).versionChecker();

        ((HomeActivity) getActivity()).setDrawerEnabled(false);

        customerID = getActivity().getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");
        Log.d("CID", customerID);
        myAppPrefsManager = new MyAppPrefsManager(getActivity());
        dialogLoader = new DialogLoader(getActivity());
        Config.IS_USER_LOGGED_IN = myAppPrefsManager.isUserLoggedIn();

        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_tutor_dadhboard, container, false);
        ll_profile = view.findViewById(R.id.ll_profile);
        ll_cmanage = view.findViewById(R.id.ll_cmanage);
        ll_smanage = view.findViewById(R.id.ll_smanage);
        ll_wallet = view.findViewById(R.id.ll_wallet);
        tv_logout = view.findViewById(R.id.tv_logout);
        ll_earn = view.findViewById(R.id.ll_earn);

        ll_earn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new ReferFragment();
                loadFragment(fragment);

            }
        });


        tv_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getActivity(), SelectRoleActivity.class);
                startActivity(i);


            }
        });
        ll_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new TutorProfileFragment();
                loadFragment(fragment);
            }
        });

        ll_cmanage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new TutorCourseFragment();
                loadFragment(fragment);
            }
        });

        ll_smanage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment fragment = new TutoeSubscriptFragment();
                loadFragment(fragment);
            }
        });

        ll_wallet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Fragment fragment = new TutoeWalletFragment();
                loadFragment(fragment);
            }
        });
        return view;
    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }


}
