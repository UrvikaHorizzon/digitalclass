package com.horizzon.vcec;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.res.Configuration;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.horizzon.vcec.adapter.CustomVListadapter;
import com.horizzon.vcec.adapter.DemoListAdapter;
import com.horizzon.vcec.adapter.DemoVideoAdapter;
import com.horizzon.vcec.model.CountVideo;
import com.horizzon.vcec.model.DemoDetails;
import com.horizzon.vcec.model.DemoModel;
import com.horizzon.vcec.model.ResultDetails;
import com.horizzon.vcec.model.VideoDetails;
import com.horizzon.vcec.network.APIClient;
import com.horizzon.vcec.util.Database;
import com.horizzon.vcec.util.DialogLoader;
import com.horizzon.vcec.util.FullScreenMediaController;
import com.horizzon.vcec.util.MyAppPrefsManager;
import com.horizzon.vcec.util.OnVideoClickListner;
import com.universalvideoview.UniversalMediaController;
import com.universalvideoview.UniversalVideoView;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.horizzon.vcec.util.MyAppPrefsManager.VIDEO_COUNT;

public class YouTubeVideoActivity extends AppCompatActivity implements View.OnClickListener, OnVideoClickListner, UniversalVideoView.VideoViewCallback {

    private UniversalVideoView videoView;
    private UniversalMediaController mediaController;
    public static String videoName;
    RecyclerView rv_videoes;
    //    ProgressBar progressBar;
    TextView tv_vname;
    public static List<DemoDetails> movieVList = new ArrayList<>();
    public static String course_id, v_type, course_name, video_id;
    private DemoVideoAdapter mAdapter;
    public String view_video, customerID;
    int youtubez_flg = 0;
    Menu menu;
    private static final int RECOVERY_DIALOG_REQUEST = 1;
    public static final String API_KEY = "AIzaSyDapMnujfMpvv-PNwGKPblZcHLeyW7HSMw";
    Database database;
    //https://www.youtube.com/watch?v=<VIDEO_ID>
    public static String VIDEO_ID = "-m3V8w_7vhk";
    YouTubePlayer youTubePlayer1;
    FragmentManager fm;
    String tag, pos;
    private int total_count = 0;
    private int count = 0;
    private static final String TAG = "Full Video Activity";
    MyAppPrefsManager manager;
    DialogLoader dialogLoader;
    // YouTube player view
    YouTubePlayerFragment playerFragment;
    private FrameLayout f_layout, mVideoLayout;
    private RelativeLayout mYoutubeLayout;
    private View mPlayButtonLayout;
    private TextView mPlayTimeTextView;
    private Handler mHandler = null;
    private SeekBar mSeekBar;
    Uri videoUri;
    private int mSeekPosition;
    private int cachedHeight;
    private boolean isFullscreen;
    LinearLayout linear_recycler_view;
    private String video_link;
    private static final String SEEK_POSITION_KEY = "SEEK_POSITION_KEY";
    int positon = 0;
    boolean doubleBackToExitPressedOnce = false;
    int orientation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE, WindowManager.LayoutParams.FLAG_SECURE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_you_tube_video);
        mVideoLayout = findViewById(R.id.video_layout);
        videoView = findViewById(R.id.video);
        videoView.setSecure(true);
        mediaController = findViewById(R.id.media_controller);
        mYoutubeLayout = findViewById(R.id.youtube_layout);
//        progressBar = findViewById(R.id.progrss);
        tv_vname = findViewById(R.id.tv_vname);
        rv_videoes = findViewById(R.id.rv_videoes);
        f_layout = findViewById(R.id.content);
        linear_recycler_view = findViewById(R.id.linear_recycler_view);
        //Add play button to explicitly play video in YouTubePlayerView
        mPlayButtonLayout = findViewById(R.id.video_control);
        findViewById(R.id.play_video).setOnClickListener(this);
        findViewById(R.id.pause_video).setOnClickListener(this);
        findViewById(R.id.orienatation).setOnClickListener(this);
        database = new Database(this);
        mPlayTimeTextView = (TextView) findViewById(R.id.play_time);
        mSeekBar = (SeekBar) findViewById(R.id.video_seekbar);
        mSeekBar.setOnSeekBarChangeListener(mVideoSeekBarChangeListener);
        mHandler = new Handler();
        dialogLoader = new DialogLoader(YouTubeVideoActivity.this);

        getSupportActionBar().setTitle("Videos");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        manager = new MyAppPrefsManager(this);
        customerID = getSharedPreferences("UserInfo", MODE_PRIVATE).getString("userID", "");

        GridLayoutManager lv = new GridLayoutManager(YouTubeVideoActivity.this, 1);
        lv.setReverseLayout(false);
        rv_videoes.setNestedScrollingEnabled(false);
        rv_videoes.setLayoutManager(lv);

//        youTubeView = (YouTubePlayerSupportFragment) getSupportFragmentManager().findFragmentById(R.id.youtube_fragment);

        fm = getFragmentManager();
        tag = YouTubePlayerFragment.class.getSimpleName();
        playerFragment = (YouTubePlayerFragment) fm.findFragmentByTag(tag);
        if (playerFragment == null) {
            FragmentTransaction ft = fm.beginTransaction();
            playerFragment = YouTubePlayerFragment.newInstance();
            ft.replace(R.id.content, playerFragment, tag);
            ft.commit();
        }

        if (getIntent().getStringExtra("url") != null) {
            v_type = getIntent().getStringExtra("url");
            videoName = getIntent().getStringExtra("video_name");
            course_name = getIntent().getStringExtra("course_name");
            course_id = getIntent().getStringExtra("course_id");
            video_id = getIntent().getStringExtra("video_id");
            video_link = getIntent().getStringExtra("videolink");
            positon = getIntent().getIntExtra("position", 0);
            movieVList = (List<DemoDetails>) getIntent().getSerializableExtra("video");
            getSupportActionBar().setTitle("Demo Lecture");
            for (int i = 0; i < movieVList.size(); i++) {
                if (movieVList.get(i).getId().equals(video_id)) {
                    pos = String.valueOf(i);
                    onVideoClick(Integer.parseInt(pos), v_type);
                }
            }

        }

        Log.e("course_id", course_id);
        Log.e("customer_id", customerID);
        getDemoes(course_id);
        checkOrientation();

         orientation = YouTubeVideoActivity.this.getResources().getConfiguration().orientation;
    }

    private void checkOrientation() {
        if (v_type.equals("file")) {
        } else {
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200.0f, getResources().getDisplayMetrics());
            ActionBar actionBar = getSupportActionBar();
            ViewGroup.LayoutParams params = mYoutubeLayout.getLayoutParams();
            if (getResources().getConfiguration().orientation ==
                    Configuration.ORIENTATION_PORTRAIT) {
                if (!actionBar.isShowing())
                    actionBar.show();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = height;
                mYoutubeLayout.requestLayout();
                rv_videoes.setVisibility(View.VISIBLE);
            } else {
                if (actionBar.isShowing())
                    actionBar.hide();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                mYoutubeLayout.requestLayout();
                rv_videoes.setVisibility(View.GONE);
            }
        }
    }

    private void playVideo() {
        videoView.setMediaController(mediaController);
        videoView.setVideoURI(videoUri);
        videoView.setVideoViewCallback(this);
        videoView.start();
        if (mSeekPosition > 0) {
            videoView.seekTo(mSeekPosition);
            videoView.start();

        }
    }

    SeekBar.OnSeekBarChangeListener mVideoSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            long lengthPlayed = (youTubePlayer1.getDurationMillis() * progress) / 100;
            youTubePlayer1.seekToMillis((int) lengthPlayed);
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };


    YouTubePlayer.PlaybackEventListener mPlaybackEventListener = new YouTubePlayer.PlaybackEventListener() {
        @Override
        public void onBuffering(boolean arg0) {
        }

        @Override
        public void onPaused() {
            mHandler.removeCallbacks(runnable);
        }

        @Override
        public void onPlaying() {
            mHandler.postDelayed(runnable, 100);
            displayCurrentTime();
        }

        @Override
        public void onSeekTo(int arg0) {
            mHandler.postDelayed(runnable, 100);
        }

        @Override
        public void onStopped() {
            mHandler.removeCallbacks(runnable);
        }
    };

    YouTubePlayer.PlayerStateChangeListener mPlayerStateChangeListener = new YouTubePlayer.PlayerStateChangeListener() {
        @Override
        public void onAdStarted() {
        }

        @Override
        public void onError(YouTubePlayer.ErrorReason arg0) {
        }

        @Override
        public void onLoaded(String arg0) {
        }

        @Override
        public void onLoading() {
        }

        @Override
        public void onVideoEnded() {
        }

        @Override
        public void onVideoStarted() {
            mSeekBar.setProgress(0);
            displayCurrentTime();
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.play_video:
                if (null != youTubePlayer1 && !youTubePlayer1.isPlaying())
                    youTubePlayer1.play();
                break;
            case R.id.pause_video:
                if (null != youTubePlayer1 && youTubePlayer1.isPlaying())
                    youTubePlayer1.pause();
                break;

            case R.id.orienatation:
                changeScreenOrientation();
                Toast.makeText(this, "", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void displayCurrentTime() {
        if (null == youTubePlayer1) return;

        if (youTubePlayer1 != null) {
            String formattedTime = formatTime(youTubePlayer1.getDurationMillis() - youTubePlayer1.getCurrentTimeMillis());
            mPlayTimeTextView.setText(formattedTime);
        }

    }

    @Override
    public void onDestroy() {
        if (v_type.equalsIgnoreCase("File Server")) {
            if (youTubePlayer1 != null) {
                youTubePlayer1.release();
            }
        }

        super.onDestroy();
    }

    private String formatTime(int millis) {
        int seconds = millis / 1000;
        int minutes = seconds / 60;
        int hours = minutes / 60;

        return (hours == 0 ? "--:" : hours + ":") + String.format("%02d:%02d", minutes % 60, seconds % 60);
    }


    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            displayCurrentTime();
//            mHandler.postDelayed(this, 100);
        }
    };

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            onBackPressed(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return true;
    }

    private void setVideoAreaSize() {
        mVideoLayout.post(new Runnable() {
            @Override
            public void run() {
                int width = mVideoLayout.getWidth();
                cachedHeight = (int) (width * 405f / 720f);
//                cachedHeight = (int) (width * 3f / 4f);
//                cachedHeight = (int) (width * 9f / 16f);
                ViewGroup.LayoutParams videoLayoutParams = mVideoLayout.getLayoutParams();
                videoLayoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
                videoLayoutParams.height = cachedHeight;
                mVideoLayout.setLayoutParams(videoLayoutParams);
                videoView.setVideoPath(String.valueOf(YouTubeVideoActivity.this.videoUri));
                videoView.requestFocus();
            }
        });

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (v_type.equalsIgnoreCase("File Server")) {
            Log.d(TAG, "onSaveInstanceState Position=" + videoView.getCurrentPosition());
            outState.putInt(SEEK_POSITION_KEY, mSeekPosition);
        }

    }

    @Override
    protected void onRestoreInstanceState(Bundle outState) {
        super.onRestoreInstanceState(outState);

        if (v_type.equalsIgnoreCase("File Server")) {
            mSeekPosition = outState.getInt(SEEK_POSITION_KEY);
            Log.d(TAG, "onRestoreInstanceState Position=" + mSeekPosition);
        }


    }

    @Override
    protected void onPause() {
        super.onPause();

        if (v_type.equalsIgnoreCase("File Server")) {
            Log.d(TAG, "onPause ");
            if (videoView != null && videoView.isPlaying()) {
                mSeekPosition = videoView.getCurrentPosition();
                Log.d(TAG, "onPause mSeekPosition=" + mSeekPosition);
                videoView.pause();
            }

        }

    }


    @Override
    public void onScaleChange(boolean isFullscreen) {
        this.isFullscreen = isFullscreen;
        if (isFullscreen) {
            ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoLayout.setLayoutParams(layoutParams);
            switchTitleBar(false);
            linear_recycler_view.setVisibility(View.GONE);

        } else {
            ViewGroup.LayoutParams layoutParams = mVideoLayout.getLayoutParams();
            layoutParams.width = ViewGroup.LayoutParams.MATCH_PARENT;
            layoutParams.height = this.cachedHeight;
            mVideoLayout.setLayoutParams(layoutParams);
            linear_recycler_view.setVisibility(View.VISIBLE);
            switchTitleBar(true);

        }


    }

    private void switchTitleBar(boolean show) {
        ActionBar supportActionBar = getSupportActionBar();
        if (supportActionBar != null) {
            if (show) {
                supportActionBar.show();
            } else {
                supportActionBar.hide();
            }
        }
    }

    @Override
    public void onPause(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onPause UniversalVideoView callback");
    }

    @Override
    public void onStart(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onStart UniversalVideoView callback");
    }

    @Override
    public void onBufferingStart(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onBufferingStart UniversalVideoView callback");
    }

    @Override
    public void onBufferingEnd(MediaPlayer mediaPlayer) {
        Log.d(TAG, "onBufferingEnd UniversalVideoView callback");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == RECOVERY_DIALOG_REQUEST) {
            // Retry initialization if user performed a recovery action
//            getYouTubePlayerProvider().initialize(Config.DEVELOPER_KEY, this);
        }

    }

    @Override
    public void onVideoClick(int pos, String type) {
        DemoDetails videoDetails = movieVList.get(pos);
        mSeekBar.setProgress(0);
        if (type.equalsIgnoreCase("File Server")) {
            try {
                if (youTubePlayer1 != null) {
                    youTubePlayer1.pause();
                    FragmentManager fm = getFragmentManager();
                    FragmentTransaction ft = fm.beginTransaction();
                    Fragment fragment = new Fragment();
                    ft.replace(R.id.content, fragment);
                    ft.commit();
                }
                youtubez_flg = 0;
                mVideoLayout.setVisibility(View.VISIBLE);
//            youTubeView.setVisibility(View.GONE);
                f_layout.setVisibility(View.GONE);
                mPlayButtonLayout.setVisibility(View.GONE);
                mYoutubeLayout.setVisibility(View.GONE);

                videoUri = Uri.parse("https://digitalclassworld.com/storage/app/" + videoDetails.getVideoLink());
                playVideo();
                setVideoAreaSize();

            } catch (Exception e) {
                FragmentManager fm = getFragmentManager();
                FragmentTransaction ft = fm.beginTransaction();
                Fragment fragment = new Fragment();
                ft.replace(R.id.content, fragment);
                ft.commit();
                youtubez_flg = 0;
                mVideoLayout.setVisibility(View.VISIBLE);
//            youTubeView.setVisibility(View.GONE);
                f_layout.setVisibility(View.GONE);
                mPlayButtonLayout.setVisibility(View.GONE);
                mYoutubeLayout.setVisibility(View.GONE);

                videoUri = Uri.parse("https://digitalclassworld.com/storage/app/" + videoDetails.getVideoLink());
                playVideo();
                setVideoAreaSize();
            }

        } else {
            mVideoLayout.setVisibility(View.GONE);
            f_layout.setVisibility(View.VISIBLE);
            mYoutubeLayout.setVisibility(View.VISIBLE);
            youtubez_flg = 1;
            videoView.stopPlayback();
            mSeekBar.setProgress(0);

//            String[] paths = videoDetails.getVideoLink().split("=");
////            paths[3] = paths[3].replace("?showinfo=0","");
            VIDEO_ID = getVideoIdFromYoutubeUrl(videoDetails.getVideoLink());


            fm = getFragmentManager();
            tag = YouTubePlayerFragment.class.getSimpleName();
            FragmentTransaction ft = fm.beginTransaction();
            playerFragment = YouTubePlayerFragment.newInstance();
            ft.replace(R.id.content, playerFragment, tag);
            ft.commit();

            playerFragment.initialize(API_KEY, new YouTubePlayer.OnInitializedListener() {
                @Override
                public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                    if(VIDEO_ID !=null){

                        youTubePlayer1 = youTubePlayer;
                        displayCurrentTime();
                        mSeekBar.setProgress(0);
                        youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
                        youTubePlayer.loadVideo(VIDEO_ID);


                    }else{

                        Toast.makeText(getApplicationContext(), "You did't add video link yet", Toast.LENGTH_SHORT).show();

                    }


//                    mPlayButtonLayout.setVisibility(View.VISIBLE);

                    // Add listeners to YouTubePlayer instance
                    youTubePlayer.setPlayerStateChangeListener(mPlayerStateChangeListener);
                    youTubePlayer.setPlaybackEventListener(mPlaybackEventListener);
                }

                @Override
                public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
                    Toast.makeText(YouTubeVideoActivity.this, "Error while initializing YouTubePlayer.", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public String getVideoIdFromYoutubeUrl(String url) {
        String videoId = null;
        String regex = "http(?:s)?:\\/\\/(?:m.)?(?:www\\.)?youtu(?:\\.be\\/|be\\.com\\/(?:watch\\?(?:feature=youtu.be\\&)?v=|v\\/|embed\\/|user\\/(?:[\\w#]+\\/)+))([^&#?\\n]+)";
        Pattern pattern = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(url);
        if (matcher.find()) {
            videoId = matcher.group(1);
        }
        return videoId;
    }

    private void getDemoes(String id) {

//        dialogLoader.showProgressDialog();
        Call<DemoModel> call = APIClient.getInstance().getDemoList(id);
        call.enqueue(new Callback<DemoModel>() {
            @Override
            public void onResponse(Call<DemoModel> call, Response<DemoModel> response) {
                if (response.isSuccessful()) {
//                    dialogLoader.hideProgressDialog();

                    if (response.body().getSuccess().equalsIgnoreCase("1")) {
                        List<DemoDetails> details = response.body().getMsg();
                        movieVList.clear();
                        movieVList.addAll(details);
                        if (movieVList != null && movieVList.size() > 0) {
                            mAdapter = new DemoVideoAdapter(YouTubeVideoActivity.this, movieVList, YouTubeVideoActivity.this);
                            rv_videoes.setAdapter(mAdapter);
                        }

                    }
                }
            }

            @Override
            public void onFailure(Call<DemoModel> call, Throwable t) {
                dialogLoader.hideProgressDialog();
            }
        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {

        if (v_type.equals("file")) {

        } else {
            int height = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 200.0f, getResources().getDisplayMetrics());
            ActionBar actionBar = getSupportActionBar();
            ViewGroup.LayoutParams params = mYoutubeLayout.getLayoutParams();

            if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
                if (actionBar.isShowing())
                    actionBar.hide();
                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = ViewGroup.LayoutParams.MATCH_PARENT;
                mYoutubeLayout.requestLayout();
                rv_videoes.setVisibility(View.GONE);
            } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT) {
                if (!actionBar.isShowing())
                    actionBar.show();

                params.width = ViewGroup.LayoutParams.MATCH_PARENT;
                params.height = height;
                mYoutubeLayout.requestLayout();
                rv_videoes.setVisibility(View.VISIBLE);
            }
        }

        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {

//        startActivity(new Intent(this,HomeActivity.class));
        Intent returnIntent = new Intent();
        returnIntent.putExtra("result","activity");
        setResult(Activity.RESULT_OK,returnIntent);
        finish();
        super.onBackPressed();
    }


    private void changeScreenOrientation() {

        if (getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_PORTRAIT) {
            setRequestedOrientation(Build.VERSION.SDK_INT < 9 ?
                    ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE :
                    ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);


        } else {
            setRequestedOrientation(Build.VERSION.SDK_INT < 9 ?
                    ActivityInfo.SCREEN_ORIENTATION_PORTRAIT :
                    ActivityInfo.SCREEN_ORIENTATION_SENSOR_PORTRAIT);
        }
        if (Settings.System.getInt(getContentResolver(),
                Settings.System.ACCELEROMETER_ROTATION, 0) == 1) {
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR);
                }
            }, 4000);
        }
    }
}
